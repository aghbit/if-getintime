package ideafactory.getintime.service.authentication

import ideafactory.getintime.domain.exception.NoLoggedUserException
import ideafactory.getintime.domain.exception.NoRequiredRoleException
import ideafactory.getintime.domain.model.workingmodel.task.Task
import ideafactory.getintime.domain.model.workingmodel.user.Employee
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by Grzegorz Miejski (SG0221133) on 1/15/2015.
 */
class TaskPermissionServiceTest extends Specification {

    def TaskPermissionService instance
    def IUserAuthenticationService authenticationService

    @Shared
    def Employee currentEmployee = Mock(Employee)


    void setup() {
        authenticationService = Mock(IUserAuthenticationService)
        instance = new TaskPermissionService(authenticationService: authenticationService)
        authenticationService.currentUser >> Optional.of(currentEmployee)
    }

    def "should throw exception when cannot update task based on roles"() {
        given:
        def Task task = Mock(Task)
        task.getAssigned() >> Mock(Employee)
        task.getCreatedBy() >> Mock(Employee)
        currentEmployee.hasAnyRole(EmployeeType.DIRECTOR, EmployeeType.MANAGER) >> false

        when:
        instance.checkIfCanUpdateTask(task)

        then:
        thrown(NoRequiredRoleException)
    }

    def "should throw exception when not having required role to update task"() {
        given:
        def Task task = Mock(Task)
        currentEmployee.hasAnyRole(EmployeeType.DIRECTOR, EmployeeType.MANAGER) >> false

        when:
        instance.checkIfCanUpdateTask(task)

        then:
        thrown(NoRequiredRoleException)
    }

    def "should throw exception when no logged user during task retrieving"() {
        when:
        instance.checkIfCanRetrieveTask(Mock(Task))

        then:
        authenticationService.getCurrentUser() >> Optional.empty()
        thrown(NoLoggedUserException)
    }

    def "should throw exception when no logged user during task update"() {
        when:
        instance.checkIfCanUpdateTask(Mock(Task))

        then:
        authenticationService.getCurrentUser() >> Optional.empty()
        thrown(NoLoggedUserException)
    }
}
