package ideafactory.getintime.service.CRUD.task.subtask;

import ideafactory.geintime.commons.preconditions.NullPointerStatusException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

/**
 * @author Grzegorz Miejski on 23.04.14.
 */
@RunWith(value = MockitoJUnitRunner.class)
public class SubtaskServiceTest {
    
    @InjectMocks
    ISubtaskService instance = new SubtaskService();
    
    @Mock
    ITaskDAO taskDAO;
    
    Task parentTask;
    Task subtask;
    
    Long parentTaskId = 10L;
    
    @Before
    public void setUp() throws Exception {
        parentTask = mock(Task.class);
        subtask = mock(Task.class);
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenSubtaskIsANull() throws Exception {
        // given
        subtask = null;
        // when
        instance.addSubtask(parentTaskId, subtask);
        // then
    }
    
    @Test
    public void shouldGetParentTaskWhenAddingSubtask() throws Exception {
        // given
        when(taskDAO.find(parentTaskId)).thenReturn(parentTask);
        // when
        instance.addSubtask(parentTaskId, subtask);
        // then
        verify(taskDAO).find(parentTaskId);
    }
    
    @Test(expected = NoTaskFoundException.class)
    public void shouldThrowExceptionWhenNoParentTaskFound() throws Exception {
        // given
        when(taskDAO.find(parentTaskId)).thenReturn(null);
        // when
        instance.addSubtask(parentTaskId, subtask);
        // then
    }
    
    @Test
    public void shouldTryToAddSubtaskToParentTask() throws Exception {
        // given
        when(taskDAO.find(parentTaskId)).thenReturn(parentTask);
        // when
        instance.addSubtask(parentTaskId, subtask);
        // then
        verify(parentTask).addSubTask(subtask);
    }
}
