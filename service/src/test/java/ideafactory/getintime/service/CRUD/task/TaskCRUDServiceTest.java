package ideafactory.getintime.service.CRUD.task;

import ideafactory.geintime.commons.preconditions.NullPointerStatusException;
import ideafactory.getintime.domain.exception.NoLoggedUserException;
import ideafactory.getintime.domain.exception.NoRequiredRoleException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Percentage;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;
import ideafactory.getintime.persistence.dao.IGenericDAO;
import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;
import ideafactory.getintime.service.authentication.ITaskPermissionService;
import ideafactory.getintime.service.authentication.IUserAuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Grzegorz Miejski on 30.03.14.
 */
@RunWith(value = MockitoJUnitRunner.class)
public class TaskCRUDServiceTest {
    
    @Mock
    IGenericDAO<Task, Long> dao;
    
    @Mock
    IUserAuthenticationService userAuthenticationService;
    
    @Mock
    ITaskPermissionService taskPermissionService;
    
    @InjectMocks
    TaskCRUDService instance = new TaskCRUDService();
    
    Task task;
    Long taskId = 10L;
    LocalDateTime CURRENT_TIME;
    LocalDate CURRENT_DATE;
    long minutesToLog;
    int progressPercentage;
    
    Optional<Employee> employeeOptional;
    Employee employee;
    
    @Before
    public void setUp() throws Exception {
        CURRENT_TIME = LocalDateTime.of(2000, 1, 2, 3, 20);
        CURRENT_DATE = CURRENT_TIME.toLocalDate();
        task = mock(Task.class);
        minutesToLog = 10;
        progressPercentage = 50;
        employee = mock(Employee.class);
        employeeOptional = Optional.of(employee);
        when(userAuthenticationService.getCurrentUser()).thenReturn(employeeOptional);
    }
    
    @Test
    public void shouldStartWorkingOnTask() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.startWorkingOnTask(taskId);
        // then
        verify(dao).find(taskId);
        verify(employee).startWorkingOnTask(task);
        
    }
    
    @Test(expected = NoTaskFoundException.class)
    public void shouldThrowExceptionIfNoTaskFoundDuringWorkStart() throws Exception {
        
        // given
        when(dao.find(taskId)).thenThrow(NoTaskFoundException.class);
        // when
        instance.startWorkingOnTask(taskId);
        // then
        
    }
    
    @Test
    public void shouldSaveTaskAfterStartWOrkingOnIt() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.startWorkingOnTask(taskId);
        // then
        verify(dao).save(task);
    }
    
    @Test
    public void shouldStopWorkingOnTask() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.stopWorkingOnTask(taskId);
        // then
        verify(dao).find(taskId);
        verify(employee).stopWorkingOnTask(any(Task.class), any(LocalDateTime.class));
    }
    
    @Test(expected = NoTaskFoundException.class)
    public void shouldThrowExceptionIfNoTaskFoundDuringWorkStop() throws Exception {
        
        // given
        when(dao.find(taskId)).thenThrow(NoTaskFoundException.class);
        // when
        instance.stopWorkingOnTask(taskId);
        // then
        
    }
    
    @Test
    public void shouldSaveTaskAfterStopWorking() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.startWorkingOnTask(taskId);
        // then
        verify(dao).save(task);
    }
    
    @Test
    public void shouldLogTimeToATask() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.logTime(taskId, minutesToLog);
        // then
        verify(dao).find(taskId);
        verify(employee).logTime(task, minutesToLog);
    }
    
    @Test(expected = NoTaskFoundException.class)
    public void shouldThrowExceptionIfNoTaskFoundDuringTimeLogging() throws Exception {
        
        // given
        when(dao.find(taskId)).thenThrow(NoTaskFoundException.class);
        // when
        instance.logTime(taskId, minutesToLog);
        // then
        
    }
    
    @Test
    public void shouldSaveTaskAfterTimeLogging() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.logTime(taskId, minutesToLog);
        // then
        verify(dao).save(task);
    }
    
    @Test
    public void shouldLogTimeAndProgressPercentageToATask() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.logTimeAndPercentage(taskId, minutesToLog, progressPercentage);
        // then
        verify(dao).find(taskId);
        verify(employee).updateWorkProgressPercentage(task, progressPercentage);
        verify(employee).logTime(task, minutesToLog);
    }
    
    @Test(expected = NoTaskFoundException.class)
    public void shouldThrowExceptionIfNoTaskFoundDuringTimeAndPercentageLogging() throws Exception {
        
        // given
        when(dao.find(taskId)).thenThrow(NoTaskFoundException.class);
        // when
        instance.logTimeAndPercentage(taskId, minutesToLog, progressPercentage);
        // then
        
    }
    
    @Test
    public void shouldSaveTaskAfterTimeAndPercentageLogging() throws Exception {
        
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.logTimeAndPercentage(taskId, minutesToLog, progressPercentage);
        // then
        verify(dao).save(task);
    }
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionWhenNoCurrentUserDuringWorkStart() throws Exception {
        // given
        employeeOptional = Optional.empty();
        when(userAuthenticationService.getCurrentUser()).thenReturn(employeeOptional);
        // when
        instance.startWorkingOnTask(taskId);
        // then
    }
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionWhenNoCurrentUserDuringWorkStop() throws Exception {
        // given
        employeeOptional = Optional.empty();
        when(userAuthenticationService.getCurrentUser()).thenReturn(employeeOptional);
        // when
        instance.stopWorkingOnTask(taskId);
        // then
    }
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionWhenNoCurrentUserDuringWorkFinish() throws Exception {
        // given
        employeeOptional = Optional.empty();
        when(userAuthenticationService.getCurrentUser()).thenReturn(employeeOptional);
        // when
        instance.finishWorkingOnTask(taskId);
        // then
    }
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionWhenCancellingTaskAsNotSpecifiedUser() throws Exception {
        // given
        employeeOptional = Optional.empty();
        when(userAuthenticationService.getCurrentUser()).thenReturn(employeeOptional);
        // when
        instance.cancelWorkingOnTask(taskId);
        // then
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenUpdatingNullTask() throws Exception {
        // given
        
        // when
        instance.update(null);
        // then
        
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenUpdatingTaskWithNullId() throws Exception {
        // given
        when(task.getId()).thenReturn(null);
        // when
        instance.update(task);
        // then
        
    }
    
    @Test(expected = NoTaskFoundException.class)
    public void shouldThrowExceptionWhenUpdatingNonexistentTask() throws Exception {
        // given
        when(employee.hasAnyRole(EmployeeType.MANAGER, EmployeeType.DIRECTOR)).thenReturn(true);
        when(task.getId()).thenReturn(taskId);
        when(dao.find(taskId)).thenReturn(null);
        // when
        instance.update(task);
        // then
        
    }
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionWhenUpdatingTaskAsNobody() throws Exception {
        // given
        doThrow(NoLoggedUserException.class).when(taskPermissionService).checkIfCanUpdateTask(
                any(Task.class));
        when(dao.find(taskId)).thenReturn(task);
        when(task.getId()).thenReturn(taskId);
        
        // when
        instance.update(task);
    }
    
    @Test
    public void shouldUpdateTaskInstanceAsManagerOrDirector() throws Exception {
        // given
        Task persistedTask = mock(Task.class);
        when(employee.hasAnyRole(EmployeeType.MANAGER, EmployeeType.DIRECTOR)).thenReturn(true);
        when(task.getId()).thenReturn(1L);
        when(instance.getDAO().find(1L)).thenReturn(persistedTask);
        // when
        instance.update(task);
        // then
        verify(persistedTask).update(task);
        verify(dao).save(persistedTask);
    }
    
    @Test(expected = NoRequiredRoleException.class)
    public void shouldThrowExceptionWhenNotManagerOrDirectorOrTaskCreatorDuringUpdate()
            throws Exception {
        // given
        when(dao.find(taskId)).thenReturn(task);
        when(task.getId()).thenReturn(taskId);
        doThrow(NoRequiredRoleException.class).when(taskPermissionService).checkIfCanUpdateTask(
                any(Task.class));
        
        // when
        instance.update(task);
    }
    
    @Test
    public void shouldUpdateTaskInstanceAsCreatorOfTask() throws Exception {
        // given
        Task persistedTask = mock(Task.class);
        when(task.getId()).thenReturn(1L);
        when(instance.getDAO().find(1L)).thenReturn(persistedTask);
        when(employee.hasAnyRole(EmployeeType.MANAGER, EmployeeType.DIRECTOR)).thenReturn(false);
        when(userAuthenticationService.getCurrentUser()).thenReturn(Optional.of(employee));
        when(task.getCreatedBy()).thenReturn(employee);
        // when
        instance.update(task);
        // then
        
    }
    
    @Test
    public void shouldUpdateWorkProgressOfTask() throws Exception {
        // given
        when(dao.find(taskId)).thenReturn(task);
        // when
        instance.logProgressPercentage(taskId, progressPercentage);
        // then
        verify(employee).updateWorkProgressPercentage(task, progressPercentage);
        
    }
    
    @Test
    public void shouldReturnEmptyListIfNoEmployeeIsLoggedIn() throws Exception {
        // given
        when(userAuthenticationService.getCurrentUser()).thenReturn(Optional.<Employee> empty());
        // when
        List<Task> tasks = instance.getLoggedEmployeeTasks(LocalDate.now(), LocalDate.now());
        // then
        assertThat(tasks).isEmpty();
    }
    
    @Test(expected = NoRequiredRoleException.class)
    public void shouldThrowExceptionWhenNormalEmployeeReopensTask() throws Exception {
        // given
        when(employee.hasAnyRole(EmployeeType.MANAGER, EmployeeType.DIRECTOR)).thenReturn(false);
        when(task.getCreatedBy()).thenReturn(mock(Employee.class));
        when(userAuthenticationService.getCurrentUser()).thenReturn(Optional.of(employee));
        when(dao.find(taskId)).thenReturn(task);
        doThrow(new NoRequiredRoleException()).when(taskPermissionService).checkIfCanUpdateTask(
                task);
        
        // when
        instance.reopenTask(taskId, CURRENT_DATE, CURRENT_DATE, 10);
    }
    
    @Test
    public void shouldReopenTaskSuccessfullyAsManagerOrDirector() throws Exception {
        // given
        when(dao.find(taskId)).thenReturn(task);
        
        // when
        instance.reopenTask(taskId, CURRENT_DATE, CURRENT_DATE, 10);
        
        // then
        verify(task).reopen(CURRENT_DATE, CURRENT_DATE, new Percentage(10));
        verify(dao).save(task);
    }
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionWhenNoLoggedUser() throws Exception {
        // given
        doThrow(NoLoggedUserException.class).when(taskPermissionService).checkIfCanUpdateTask(
                any(Task.class));
        when(dao.find(10L)).thenReturn(task);
        
        // when
        instance.reopenTask(taskId, CURRENT_DATE, CURRENT_DATE, 10);
    }
    
    @Test
    public void shouldReopenTaskSuccessfullyAsCreatorOfTask() throws Exception {
        // given
        when(dao.find(taskId)).thenReturn(task);
        
        // when
        instance.reopenTask(taskId, CURRENT_DATE, CURRENT_DATE, 10);
        
        // then
        verify(task).reopen(CURRENT_DATE, CURRENT_DATE, new Percentage(10));
        verify(dao).save(task);
    }
}
