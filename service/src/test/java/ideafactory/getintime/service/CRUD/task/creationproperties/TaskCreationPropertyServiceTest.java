package ideafactory.getintime.service.CRUD.task.creationproperties;

import ideafactory.getintime.service.CRUD.task.creationproperty.ITaskCreationPropertyService;
import ideafactory.getintime.service.CRUD.task.creationproperty.TaskCreationPropertyService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@RunWith(value = MockitoJUnitRunner.class)
public class TaskCreationPropertyServiceTest {
    
    private ITaskCreationPropertyService instance;
    
    @Before
    public void setUp() throws Exception {
        instance = new TaskCreationPropertyService();
    }
    
    @Test
    public void shouldHaveAtLeastOnePriorityDefined() {
        
        assertThat(instance.getPossiblePriorities()).isNotEmpty();
    }
    
    @Test
    public void shouldHaveAtLeastTwoTypesDefined() {
        
        assertThat(instance.getPossibleTypes().size()).isGreaterThanOrEqualTo(2);
    }
    
    @Test
    public void shouldHaveAtLeastFourStatuesDefined() {
        
        assertThat(instance.getPossibleStatues().size()).isGreaterThanOrEqualTo(4);
    }
}
