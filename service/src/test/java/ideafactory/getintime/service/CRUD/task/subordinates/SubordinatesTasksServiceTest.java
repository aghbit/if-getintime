package ideafactory.getintime.service.CRUD.task.subordinates;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.exception.NoLoggedUserException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.IGenericDAO;
import ideafactory.getintime.service.authentication.IUserAuthenticationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author Grzegorz Miejski on 11.06.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class SubordinatesTasksServiceTest {
    
    @InjectMocks
    private ISubordinatesTasksService instance = new SubordinatesTasksService();
    
    @Mock
    private IUserAuthenticationService userAuthenticationService;
    
    @Mock
    IGenericDAO<Task, Long> dao;
    
    private LocalDate from = LocalDate.now();
    private LocalDate to = LocalDate.now().plusDays(1);
    
    @Test(expected = NoLoggedUserException.class)
    public void shouldThrowExceptionIfNoCurrentUser() throws Exception {
        // given
        when(userAuthenticationService.getCurrentUser()).thenReturn(Optional.empty());
        // when
        instance.getCurrentUserSubordinatesTasks(null, null);
        // then
        verify(userAuthenticationService).getCurrentUser();
    }
    
    @Test
    public void shouldGetSubordinatesTasksForCurrentUser() throws Exception {
        // given
        Employee employee = mock(Employee.class);
        when(userAuthenticationService.getCurrentUser()).thenReturn(Optional.of(employee));
        // when
        instance.getCurrentUserSubordinatesTasks(from, to);
        // then
        verify(dao).search(any(Search.class));
    }
}
