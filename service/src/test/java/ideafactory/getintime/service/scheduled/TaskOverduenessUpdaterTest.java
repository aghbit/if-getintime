package ideafactory.getintime.service.scheduled;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.task.TaskBuilder;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author Grzegorz Miejski on 11.04.14.
 */
@RunWith(value = MockitoJUnitRunner.class)
public class TaskOverduenessUpdaterTest {
    
    @InjectMocks
    private TaskOverduenessUpdater instance = new TaskOverduenessUpdater();
    
    @Mock
    private ITaskDAO taskDAO;
    
    private List<Task> taskList;
    
    private Search search;
    
    @Before
    public void setUp() throws Exception {
        search = mock(Search.class);
        Task task1 = createTask(LocalDate.now().minusDays(1));
        taskList = new ArrayList<>(Arrays.asList(task1));
        given(taskDAO.search(search)).willReturn(taskList.stream().map(t -> (Object)t).collect(toList()));
    }
    
    @Test
    public void shouldCallTaskDaoFindAll() throws Exception {
        
        // given
        // when
        instance.updateTasksOverdueValue();
        // then
        verify(taskDAO).search(any(Search.class));
    }
    
    @Test
    public void shouldSetOverduePropertyToAllTasks() throws Exception {

        //given
        //when
        instance.updateTasksOverdueValue();
        //then
        taskList.stream().map(t -> assertThat(((Task)t).isOverdue()).isEqualTo(true));
    }
    
    @Test
    public void shouldSaveChangedTasks() throws Exception {
        
        // given
        
        // when
        instance.updateTasksOverdueValue();
        // then
        verify(taskDAO).save();
    }
    
    @Test
    public void shouldUpdateOverdue5MinutesAfterMidnight() throws Exception {
        
        Scheduled scheduled = instance.getClass().getMethod("updateTasksOverdueValue")
                .getAnnotation(Scheduled.class);
        
        assertThat(scheduled).isNotNull();
        assertThat(scheduled.cron()).isEqualTo("0 5 0 * * *");
    }
    
    private LocalDate today() {
        return LocalDate.now();
    }
    
    private Task createTask(LocalDate deadline) {
        return new TaskBuilder().setAssigneeDeadline(new Term(deadline))
                .setOwnerDeadline(new Term(deadline.plusDays(1))).createTask();
    }
}
