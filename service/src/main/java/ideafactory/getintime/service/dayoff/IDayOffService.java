package ideafactory.getintime.service.dayoff;

import ideafactory.geintime.commons.dayoff.DayOffManager;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Piotr Góralczyk
 */
public interface IDayOffService {
    DayOffManager getDayOffManager();
    
    Set<LocalDate> getDaysOff(LocalDate from, LocalDate to);
}
