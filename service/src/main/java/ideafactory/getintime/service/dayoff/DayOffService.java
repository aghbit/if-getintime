package ideafactory.getintime.service.dayoff;

import ideafactory.geintime.commons.dayoff.DayOffManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Piotr Góralczyk
 */
@Service
public class DayOffService implements IDayOffService {
    private DayOffManager dayOffManager;
    
    @Autowired
    public DayOffService(DayOffManager dayOffManager) {
        this.dayOffManager = dayOffManager;
    }
    
    @Override
    public DayOffManager getDayOffManager() {
        return dayOffManager;
    }
    
    @Override
    public Set<LocalDate> getDaysOff(LocalDate from, LocalDate to) {
        return dayOffManager.getDaysOff(from, to);
    }
}
