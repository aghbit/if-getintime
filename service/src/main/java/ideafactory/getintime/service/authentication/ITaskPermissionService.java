package ideafactory.getintime.service.authentication;

import ideafactory.getintime.domain.model.workingmodel.task.Task;

/**
 * Created by slonka on 29.05.14.
 */
public interface ITaskPermissionService {
    
    public void checkIfCanRetrieveTask(Task task);
    
    void checkIfCanUpdateTask(Task task);
}
