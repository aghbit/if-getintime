package ideafactory.getintime.service.authentication;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Maciej Ciołek
 */
public class Credentials {
    private String password;
    private String username;
    
    @JsonCreator
    public Credentials(@JsonProperty("username") String username,
            @JsonProperty("password") String password) {
        this.password = password;
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
}
