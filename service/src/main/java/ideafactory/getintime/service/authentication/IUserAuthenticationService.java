package ideafactory.getintime.service.authentication;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

import java.util.Optional;

/**
 * @author partyks
 */
public interface IUserAuthenticationService {
    Optional<Employee> getCurrentUser();
    
    void authenticateUser(Credentials credentials);
    
    void removeCurrentAuthenticationToken();
    
}
