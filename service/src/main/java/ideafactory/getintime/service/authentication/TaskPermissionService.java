package ideafactory.getintime.service.authentication;

import ideafactory.getintime.domain.exception.NoLoggedUserException;
import ideafactory.getintime.domain.exception.NoRequiredRoleException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by slonka on 29.05.14.
 */
@Service
public class TaskPermissionService implements ITaskPermissionService {
    
    @Autowired
    IUserAuthenticationService authenticationService;
    
    @Override
    public void checkIfCanRetrieveTask(Task task) {
        Employee employee = currentlyLoggedUser();
        if (isNotManagerNorDirector(employee) && isNotAssigneeNorOwner(employee, task)) {
            throw new NoRequiredRoleException(
                    "Musisz być dyrektorem, managerem, twórcą lub wykonawcą zadania!",
                    "Exception.Task.NotEnoughPrivelegesToSeeTask");
        }
    }
    
    private boolean isNotManagerNorDirector(Employee employee) {
        return !employee.hasAnyRole(EmployeeType.DIRECTOR, EmployeeType.MANAGER);
    }
    
    private boolean isNotAssigneeNorOwner(Employee employee, Task task) {
        return !task.getAssigned().equals(employee) && !task.getCreatedBy().equals(employee);
    }
    
    @Override
    public void checkIfCanUpdateTask(Task task) {
        Employee employee = currentlyLoggedUser();
        if (isNotManagerNorDirector(employee)) {
            throw new NoRequiredRoleException("Musisz być dyrektorem lub managerem!",
                    "Exception.Task.NotEnoughPrivilegesToSeeTask");
        }
    }
    
    private Employee currentlyLoggedUser() {
        Optional<Employee> employeeOptional = authenticationService.getCurrentUser();
        return employeeOptional.orElseThrow(NoLoggedUserException::new);
    }
}
