package ideafactory.getintime.service.authentication;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.CRUD.employees.crud.IEmployeeCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 
 * @author Maciej Ciołek
 */
@Service("userAuthenticationService")
public class UserAuthenticationService implements IUserAuthenticationService {
    @Autowired
    IEmployeeCRUDService employeeCRUDService;
    
    @Autowired
    AuthenticationManager authenticationManager;
    
    @Override
    @Transactional
    public Optional<Employee> getCurrentUser() {
        final String username = getUsernameFromContext();
        return employeeCRUDService.findByUsername(username);
    }
    
    @Override
    public void authenticateUser(Credentials credentials) throws AuthenticationException {
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(
                credentials.getUsername(), credentials.getPassword());
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    @Override
    public void removeCurrentAuthenticationToken() {
        SecurityContextHolder.clearContext();
    }
    
    private String getUsernameFromContext() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        return authentication.getName();
    }
}
