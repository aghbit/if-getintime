package ideafactory.getintime.service.authentication;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

/**
 * @author Maciej Ciołek
 */
public class LoginResponse {
    private Boolean loggedIn = false;
    private Employee employee = null;
    
    private LoginResponse(Boolean loggedIn, Employee employee) {
        this.loggedIn = loggedIn;
        this.employee = employee;
    }
    
    public static LoginResponse success(Employee employee) {
        return new LoginResponse(true, employee);
    }
    
    public static LoginResponse failed() {
        return new LoginResponse(false, null);
    }
    
    public Boolean getLoggedIn() {
        return loggedIn;
    }
    
    private void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    public Employee getEmployee() {
        return employee;
    }
    
    private void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
