package ideafactory.getintime.service.CRUD.employees.crud;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.CRUD.abstractCRUD.ICrudService;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author partyks
 */
public interface IEmployeeCRUDService extends ICrudService<Employee, Long> {
    
    List<Employee> getSubordinatesOfEmployeeWithId(Long id);
    
    Set<Task> getTasksOfEmployeeWithId(Long id);
    
    Optional<Employee> findByUsername(String username);
}
