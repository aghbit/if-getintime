package ideafactory.getintime.service.CRUD.abstractCRUD;

import java.util.List;

/**
 * @author Michal Partyka
 */
public interface ICrudService<T, K> {
    T getByPK(K PK);
    
    List<T> getList();
    
    void update(T entity);
    
    void save(T entity);
    
    void remove(T entity);
    
    void removeByPK(K id);
}
