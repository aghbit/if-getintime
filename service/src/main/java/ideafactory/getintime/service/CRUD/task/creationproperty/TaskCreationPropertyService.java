package ideafactory.getintime.service.CRUD.task.creationproperty;

import com.google.common.collect.Lists;
import ideafactory.getintime.domain.task.creationproperty.TaskCreationProperty;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Service
public class TaskCreationPropertyService implements ITaskCreationPropertyService {

    private static final List<TaskCreationProperty<Integer>> possiblePriorities =
            Lists.newArrayList(1, 2, 3, 4)
                    .stream().map(TaskCreationProperty<Integer>::new).collect(Collectors.toList());

    private static final List<TaskCreationProperty<String>> possibleTypes =
            Lists.newArrayList("CYCLIC", "STANDARD")
                    .stream().map(TaskCreationProperty<String>::new).collect(Collectors.toList());

    private static final List<TaskCreationProperty<String>> possibleStatues =
            Lists.newArrayList("TODO", "DONE", "IN_PROGRESS", "STOPPED", "CANCELLED")
                    .stream().map(TaskCreationProperty<String>::new).collect(Collectors.toList());


    @Override
    public List<TaskCreationProperty<Integer>> getPossiblePriorities() {
        return possiblePriorities;
    }

    @Override
    public List<TaskCreationProperty<String>> getPossibleTypes() {
        return possibleTypes;
    }

    @Override
    public List<TaskCreationProperty<String>> getPossibleStatues() {
        return possibleStatues;
    }
}
