package ideafactory.getintime.service.CRUD.abstractCRUD;

import ideafactory.getintime.persistence.dao.IGenericDAO;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Michal Partyka
 */
public abstract class CRUDService<T, K> implements ICrudService<T, K> {
    private IGenericDAO<T, K> instanceDAO;
    
    public IGenericDAO<T, K> getDAO() {
        return instanceDAO;
    }
    
    protected void setDAO(IGenericDAO<T, K> dao) {
        this.instanceDAO = dao;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public T getByPK(K PK) {
        return getDAO().find(PK);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public List<T> getList() {
        return getDAO().findAll();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void update(T entity) {
        getDAO().save(entity);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void save(T entity) {
        getDAO().save(entity);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void remove(T entity) {
        getDAO().remove(entity);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void removeByPK(K id) {
        getDAO().removeById(id);
    }
}