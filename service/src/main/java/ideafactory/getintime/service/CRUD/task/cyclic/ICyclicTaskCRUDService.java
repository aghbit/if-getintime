package ideafactory.getintime.service.CRUD.task.cyclic;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.service.CRUD.abstractCRUD.ICrudService;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public interface ICyclicTaskCRUDService extends ICrudService<CyclicTask, Long> {
}
