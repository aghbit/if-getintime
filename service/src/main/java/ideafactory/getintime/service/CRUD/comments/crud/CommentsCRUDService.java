package ideafactory.getintime.service.CRUD.comments.crud;

import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.TaskComment;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import ideafactory.getintime.persistence.dao.comments.Comments;
import ideafactory.getintime.service.CRUD.abstractCRUD.CRUDService;
import ideafactory.getintime.service.CRUD.task.ITaskCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @author slonka
 */
@Service
public class CommentsCRUDService extends CRUDService<TaskComment, Long> implements
        ICommentsCRUDService {
    @Autowired
    protected void setDAO(Comments dao) {
        super.setDAO(dao);
    }
    
    @Autowired
    private ITaskDAO taskDao;
    
    @Override
    @Transactional
    public Set<TaskComment> getCommentsByTask(Long id) {
        Task task = taskDao.find(id);
        GetintimePreconditions.checkNotNull(task, "Exception.NoTaskFoundException");
        return fetchComments(task);
    }
    
    @Transactional
    public void addCommentToTask(Long id, TaskComment taskComment) {
        Task task = taskDao.find(id);
        GetintimePreconditions.checkNotNull(task, "Exception.NoTaskFoundException");
        task.addComment(taskComment);
        taskDao.save(task);
    }
    
    private Set<TaskComment> fetchComments(Task task) {
        final Set<TaskComment> comments = task.getComments();
        comments.size();
        return comments;
    }
}
