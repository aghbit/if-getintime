package ideafactory.getintime.service.CRUD.task.creationproperty;

import ideafactory.getintime.domain.task.creationproperty.TaskCreationProperty;

import java.util.List;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public interface ITaskCreationPropertyService {
    List<TaskCreationProperty<Integer>> getPossiblePriorities();
    
    List<TaskCreationProperty<String>> getPossibleTypes();
    
    List<TaskCreationProperty<String>> getPossibleStatues();
}
