package ideafactory.getintime.service.CRUD.task;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.service.CRUD.abstractCRUD.ICrudService;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Grzegorz Miejski on 13.12.13.
 */
public interface ITaskCRUDService extends ICrudService<Task, Long> {
    
    Task getByPKWithSubtasks(Long id);
    
    void update(Task task);
    
    List<Task> getLoggedEmployeeTasks(LocalDate from, LocalDate to);
}