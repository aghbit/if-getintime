package ideafactory.getintime.service.CRUD.task.cyclic.template;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by mariusz89016 on 5/10/14.
 */
public interface ICyclicTaskService {
    
    List<Task> getTasksByIdWithDateRange(Long id, LocalDate from, LocalDate to);
    
    List<CyclicTask> getAllWithDateRange(LocalDate from, LocalDate to);
}
