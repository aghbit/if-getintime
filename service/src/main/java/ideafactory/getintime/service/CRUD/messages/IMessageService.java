package ideafactory.getintime.service.CRUD.messages;

import ideafactory.getintime.domain.messages.Message;

/**
 * @author Lukasz Raduj raduj.lukasz@gmail.com
 */
public interface IMessageService {

    public default Message getDefaultMessage() {
        return new Message("--Hello from rest service--");
    }
}
