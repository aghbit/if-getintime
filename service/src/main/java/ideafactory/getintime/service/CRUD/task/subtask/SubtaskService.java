package ideafactory.getintime.service.CRUD.task.subtask;

import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.exception.NoLoggedUserException;
import ideafactory.getintime.domain.exception.NoRequiredRoleException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;
import ideafactory.getintime.service.authentication.ITaskPermissionService;
import ideafactory.getintime.service.CRUD.task.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * @author Grzegorz Miejski on 23.04.14.
 */
@Service
public class SubtaskService implements ISubtaskService {
    
    @Autowired
    private ITaskDAO taskDAO;
    
    @Autowired
    private ITaskService taskService;
    
    @Autowired
    private ITaskPermissionService taskPermissionService;
    
    @Override
    @Transactional
    public void addSubtask(Long parentTaskId, Task subtask) {
        
        GetintimePreconditions.checkNotNull(subtask, "Exception.Subtask.NullReference");
        
        Task parentTask = taskDAO.find(parentTaskId);
        taskDAO.save(subtask);

        if (parentTask == null) {
            throw new NoTaskFoundException(parentTaskId);
        }
        
        parentTask.addSubTask(subtask);
    }
    
    @Transactional
    public Set<Task> getSubtasks(Long id) {
        Set<Task> subTasksTotal = new HashSet<>();
        Stack<Task> taskToVisit = new Stack<>();
        
        Task task = taskService.getByPKWithSubtasks(id);
        taskToVisit.push(task);
        
        Set<Task> subTasks = null;
        while (!taskToVisit.empty()) {
            task = taskToVisit.pop();

            try {
                taskPermissionService.checkIfCanRetrieveTask(task);
            } catch(NoLoggedUserException|NoRequiredRoleException e) {
                continue;
            }

            subTasks = task.getSubTasks();
            subTasksTotal.addAll(subTasks);
            subTasks.stream().map(subtask -> taskToVisit.push(taskService.getByPKWithSubtasks(subtask.getId())));
        }
        
        return subTasks;
    }
}
