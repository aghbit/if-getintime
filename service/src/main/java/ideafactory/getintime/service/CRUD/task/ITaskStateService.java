package ideafactory.getintime.service.CRUD.task;

import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;

import java.time.LocalDate;

/**
 * Created by mariusz89016 on 3/6/14.
 */
public interface ITaskStateService {
    void startWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException;
    
    void stopWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException;
    
    void finishWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException;
    
    void cancelWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException;
    
    void reopenTask(long taskId, LocalDate newOwnerDeadline, LocalDate newAssigneeDeadline,
            Integer percentageValue) throws IllegalStateException, NoTaskFoundException;
}
