package ideafactory.getintime.service.CRUD.task;

import ideafactory.getintime.service.ITaskLogTimeService;
import ideafactory.getintime.service.ITaskProgressPercentageService;

/**
 * Created by mariusz89016 on 3/6/14.
 */
public interface ITaskService extends ITaskCRUDService, ITaskStateService, ITaskLogTimeService,
        ITaskProgressPercentageService {
}
