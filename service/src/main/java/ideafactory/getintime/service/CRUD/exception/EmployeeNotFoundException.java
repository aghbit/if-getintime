package ideafactory.getintime.service.CRUD.exception;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Maciej Ciołek
 */
public class EmployeeNotFoundException extends StatusException {
    public EmployeeNotFoundException(String username) {
        super("Employee not found with username [" + username + "]", "Employee.NotFound");
    }
}
