package ideafactory.getintime.service.CRUD.task.cyclic.template;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;
import ideafactory.getintime.service.CRUD.abstractCRUD.ICrudService;

public interface ITemplateTaskCRUDService extends ICrudService<TemplateTask, Long> {
}
