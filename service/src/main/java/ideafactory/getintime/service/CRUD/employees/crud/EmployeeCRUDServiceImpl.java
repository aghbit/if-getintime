package ideafactory.getintime.service.CRUD.employees.crud;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.user.SubordinatesVisitor;
import ideafactory.getintime.persistence.dao.employees.IEmployeeDAO;
import ideafactory.getintime.service.CRUD.abstractCRUD.CRUDService;
import ideafactory.getintime.service.CRUD.exception.DuplicatedUsernamesException;
import org.hibernate.NonUniqueObjectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author partyks
 */
@Service
public class EmployeeCRUDServiceImpl extends CRUDService<Employee, Long> implements
        IEmployeeCRUDService {
    @Autowired
    protected void setDAO(IEmployeeDAO dao) {
        super.setDAO(dao);
    }
    
    @Override
    @Transactional
    public List<Employee> getSubordinatesOfEmployeeWithId(Long id) {
        Employee employee = this.getByPK(id);
        List<Employee> subordinates = applyTreeVisitor(employee);
        subordinates.remove(employee);
        return subordinates;
    }
    
    @Override
    @Transactional
    public Set<Task> getTasksOfEmployeeWithId(Long id) {
        Employee employee = this.getByPK(id);
        employee.getTasks().size();
        return employee.getTasks();
    }
    
    @Override
    @Transactional
    public Optional<Employee> findByUsername(String username) {
        final Search search = new Search();
        search.addFilterEqual("username", username);
        
        try {
            final Employee employee = getDAO().searchUnique(search);
            return Optional.ofNullable(employee);
        } catch (NonUniqueObjectException exception) {
            throw new DuplicatedUsernamesException(username);
        }
    }
    
    private List<Employee> applyTreeVisitor(Employee employee) {
        SubordinatesVisitor subordinatesVisitor = new SubordinatesVisitor();
        employee.accept(subordinatesVisitor);
        return subordinatesVisitor.getSubordinates();
    }
}
