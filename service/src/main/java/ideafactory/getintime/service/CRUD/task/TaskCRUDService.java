package ideafactory.getintime.service.CRUD.task;

import com.googlecode.genericdao.search.Search;
import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.exception.NoLoggedUserException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Percentage;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import ideafactory.getintime.service.CRUD.abstractCRUD.CRUDService;
import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;
import ideafactory.getintime.service.authentication.ITaskPermissionService;
import ideafactory.getintime.service.authentication.IUserAuthenticationService;
import ideafactory.getintime.service.strategy.TaskRetrieverStrategy;
import ideafactory.getintime.service.strategy.WithOverdueRetrievingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Grzegorz Miejski on 13.12.13.
 */
@Service
public class TaskCRUDService extends CRUDService<Task, Long> implements ITaskService {
    
    @Autowired
    private IUserAuthenticationService authenticationService;
    
    @Autowired
    private ITaskPermissionService taskPermissionService;
    
    @Autowired
    protected void setDAO(ITaskDAO dao) {
        super.setDAO(dao);
    }
    
    private Employee getCurrentUser() {
        Optional<Employee> currentUserOptional = authenticationService.getCurrentUser();
        if (!currentUserOptional.isPresent()) {
            throw new NoLoggedUserException();
        }
        return currentUserOptional.get();
    }
    
    private Task findTask(long taskId) {
        Task task = getDAO().find(taskId);
        if (task == null) {
            throw new NoTaskFoundException(taskId);
        }
        return task;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void startWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(taskId);
        
        currentUser.startWorkingOnTask(task);
        getDAO().save(task);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void stopWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(taskId);
        
        currentUser.stopWorkingOnTask(task, LocalDateTime.now());
        getDAO().save(task);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void finishWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(taskId);
        
        currentUser.doneWorkingOnTask(task, LocalDateTime.now());
        getDAO().save(task);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void cancelWorkingOnTask(long taskId) throws IllegalStateException, NoTaskFoundException {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(taskId);
        
        currentUser.cancelTask(task, LocalDateTime.now());
        getDAO().save(task);
        
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void logTime(long taskId, long timeInMinutes) {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(taskId);
        
        currentUser.logTime(task, timeInMinutes);
        getDAO().save(task);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void logTimeAndPercentage(long taskId, long timeInMinutes, int percentage) {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(taskId);
        
        currentUser.logTime(task, timeInMinutes);
        currentUser.updateWorkProgressPercentage(task, percentage);
        
        getDAO().save(task);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
    public void logProgressPercentage(Long id, int percentageValue) {
        Employee currentUser = getCurrentUser();
        
        Task task = findTask(id);
        
        currentUser.updateWorkProgressPercentage(task, percentageValue);
        
        getDAO().save(task);
    }
    
    @Override
    @Transactional
    public Task getByPKWithSubtasks(Long id) {
        Task task = findTask(id);
        
        // load subtasks
        task.getSubTasks().size();
        
        return task;
    }
    
    @Override
    @Transactional
    public void update(Task task) {
        GetintimePreconditions.checkNotNull(task, "Exception.Task.NullUpdateReference");
        GetintimePreconditions.checkNotNull(task.getId(), "Exception.Task.NullUpdateId");
        
        Task persistedTask = findTask(task.getId());
        
        taskPermissionService.checkIfCanUpdateTask(persistedTask);
        
        persistedTask.update(task);
        getDAO().save(persistedTask);
    }
    
    @Override
    @Transactional
    public List<Task> getLoggedEmployeeTasks(LocalDate from, LocalDate to) {
        
        Employee loggedEmployee;
        Optional<Employee> employeeOptional = authenticationService.getCurrentUser();
        
        if (employeeOptional.isPresent()) {
            loggedEmployee = employeeOptional.get();
        } else {
            return new ArrayList<>();
        }
        
        TaskRetrieverStrategy taskRetrieverStrategy = new WithOverdueRetrievingStrategy();
        
        Search search = taskRetrieverStrategy.getRetrievingStrategy(from, to, loggedEmployee);
        return getDAO().search(search);
    }
    
    @Override
    public void reopenTask(long taskId, LocalDate newOwnerDeadline, LocalDate newAssigneeDeadline,
            Integer percentageValue) throws IllegalStateException, NoTaskFoundException {
        Task taskToReopen = findTask(taskId);
        
        taskPermissionService.checkIfCanUpdateTask(taskToReopen);
        
        taskToReopen.reopen(newOwnerDeadline, newAssigneeDeadline, new Percentage(percentageValue));
        
        getDAO().save(taskToReopen);
    }
}
