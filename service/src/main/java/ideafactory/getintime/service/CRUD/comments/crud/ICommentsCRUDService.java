package ideafactory.getintime.service.CRUD.comments.crud;

import ideafactory.getintime.domain.model.workingmodel.task.TaskComment;
import ideafactory.getintime.service.CRUD.abstractCRUD.ICrudService;

import java.util.Set;

/**
 * @author slonka
 */
public interface ICommentsCRUDService extends ICrudService<TaskComment, Long> {
    public void addCommentToTask(Long id, TaskComment taskComment);
    
    Set<TaskComment> getCommentsByTask(Long id);
}
