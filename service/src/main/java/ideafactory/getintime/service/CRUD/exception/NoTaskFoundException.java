package ideafactory.getintime.service.CRUD.exception;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Grzegorz Miejski on 07.02.14.
 */
public class NoTaskFoundException extends StatusException {
    
    private final static String MESSAGE_TEMPLATE = "No task found with id: ";
    private final static String ERROR_CODE = "Exception.NoTaskFoundException";
    
    public NoTaskFoundException(Long taskId) {
        super(MESSAGE_TEMPLATE + taskId, ERROR_CODE);
    }
}
