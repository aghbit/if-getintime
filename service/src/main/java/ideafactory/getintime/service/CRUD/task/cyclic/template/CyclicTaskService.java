package ideafactory.getintime.service.CRUD.task.cyclic.template;

import com.google.common.collect.Sets;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.persistence.dao.task.cyclic.ICyclicTaskDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by mariusz89016 on 5/10/14.
 */
@Service
public class CyclicTaskService implements ICyclicTaskService {
    
    @Autowired
    private ICyclicTaskDAO cyclicTaskDAO;
    
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public List<Task> getTasksByIdWithDateRange(Long id, LocalDate from, LocalDate to) {
        CyclicTask cyclicTask = cyclicTaskDAO.find(id);
        List<Task> filteredList = cyclicTask.getTasks().stream()
                .filter(x -> x.getAssigneeDeadline().getDate().isAfter(from.minusDays(1)))
                .filter(x -> x.getAssigneeDeadline().getDate().isBefore(to.plusDays(1))).collect(Collectors.toList());
        return filteredList;
    }
    
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public List<CyclicTask> getAllWithDateRange(LocalDate from, LocalDate to) {
        Search search = new Search();
        search.addFilter(Filter.and(Filter.greaterOrEqual("tasks.assigneeDeadline.date", from),
                Filter.lessOrEqual("tasks.assigneeDeadline.date", to)));
        search.setDistinct(true);
        
        return cyclicTaskDAO.search(search);
    }
}
