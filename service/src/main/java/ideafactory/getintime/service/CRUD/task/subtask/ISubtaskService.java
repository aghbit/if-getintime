package ideafactory.getintime.service.CRUD.task.subtask;

import ideafactory.getintime.domain.model.workingmodel.task.Task;

import java.util.Set;

/**
 * @author Grzegorz Miejski on 23.04.14.
 */
public interface ISubtaskService {
    
    void addSubtask(Long parentTaskId, Task subtask);
    
    public Set<Task> getSubtasks(Long id);
}
