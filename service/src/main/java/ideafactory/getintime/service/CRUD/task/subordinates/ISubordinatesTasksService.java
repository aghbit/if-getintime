package ideafactory.getintime.service.CRUD.task.subordinates;

import ideafactory.getintime.domain.model.workingmodel.task.Task;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public interface ISubordinatesTasksService {
    
    List<Task> getCurrentUserSubordinatesTasks(LocalDate from, LocalDate to);
}
