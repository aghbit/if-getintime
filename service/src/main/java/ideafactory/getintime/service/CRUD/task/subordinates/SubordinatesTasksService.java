package ideafactory.getintime.service.CRUD.task.subordinates;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.exception.NoLoggedUserException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import ideafactory.getintime.service.CRUD.abstractCRUD.CRUDService;
import ideafactory.getintime.service.authentication.IUserAuthenticationService;
import ideafactory.getintime.service.strategy.SubordinatesTasksWithOverdueRetrievingStrategy;
import ideafactory.getintime.service.strategy.TaskRetrieverStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Component
public class SubordinatesTasksService extends CRUDService<Task, Long> implements
        ISubordinatesTasksService {
    
    @Autowired
    protected void setDAO(ITaskDAO dao) {
        super.setDAO(dao);
    }
    
    @Autowired
    private IUserAuthenticationService authenticationService;
    
    @Override
    @Transactional
    public List<Task> getCurrentUserSubordinatesTasks(LocalDate from, LocalDate to) {
        final Optional<Employee> currentUserOptional = authenticationService.getCurrentUser();
        
        final Employee currentUser = currentUserOptional.orElseThrow(NoLoggedUserException::new);

        TaskRetrieverStrategy taskRetrieverStrategy = new SubordinatesTasksWithOverdueRetrievingStrategy();

        Search search = taskRetrieverStrategy.getRetrievingStrategy(from, to, currentUser);

        List<Task> result = getDAO().search(search);

        return result;
    }
}
