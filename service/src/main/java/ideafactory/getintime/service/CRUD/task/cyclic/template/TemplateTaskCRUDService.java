package ideafactory.getintime.service.CRUD.task.cyclic.template;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;
import ideafactory.getintime.persistence.dao.task.cyclic.template.ITemplateTaskDAO;
import ideafactory.getintime.service.CRUD.abstractCRUD.CRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateTaskCRUDService extends CRUDService<TemplateTask, Long> implements
        ITemplateTaskCRUDService {
    
    @Autowired
    protected void setDAO(ITemplateTaskDAO dao) {
        super.setDAO(dao);
    }
}
