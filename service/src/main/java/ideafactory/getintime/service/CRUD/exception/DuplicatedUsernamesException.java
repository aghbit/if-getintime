package ideafactory.getintime.service.CRUD.exception;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Maciej Ciołek
 */
public class DuplicatedUsernamesException extends StatusException {
    public DuplicatedUsernamesException(String username) {
        super("Duplicated username for username: [" + username + "]");
    }
}
