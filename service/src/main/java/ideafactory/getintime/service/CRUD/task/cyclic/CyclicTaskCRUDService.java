package ideafactory.getintime.service.CRUD.task.cyclic;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.persistence.dao.task.cyclic.ICyclicTaskDAO;
import ideafactory.getintime.service.CRUD.abstractCRUD.CRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Service
public class CyclicTaskCRUDService extends CRUDService<CyclicTask, Long> implements
        ICyclicTaskCRUDService {
    
    @Autowired
    protected void setDAO(ICyclicTaskDAO dao) {
        super.setDAO(dao);
    }
}
