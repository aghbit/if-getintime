package ideafactory.getintime.service;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

import java.util.Optional;

/**
 * @author Grzegorz Miejski on 29.03.14.
 */
public interface ITaskProgressPercentageService {
    
    void logProgressPercentage(Long id, int percentageValue);
    
}
