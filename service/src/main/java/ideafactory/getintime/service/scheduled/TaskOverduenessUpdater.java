package ideafactory.getintime.service.scheduled;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.task.properties.TaskStatus;
import ideafactory.getintime.persistence.dao.ITaskDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.transaction.annotation.Isolation.SERIALIZABLE;

/**
 * @author Grzegorz Miejski on 11.04.14.
 */
@Component
public class TaskOverduenessUpdater {
    
    @Autowired
    private ITaskDAO taskDAO;
    
    @Scheduled(cron = "0 5 0 * * *")
    @Transactional(isolation = SERIALIZABLE)
    public void updateTasksOverdueValue() {
        Search search = new Search();

        search.addFilterEqual("overdue", false);
        search.addFilterLessThan("assigneeDeadline.date", LocalDate.now());
        search.addFilterNotEqual("status", TaskStatus.DONE);
        search.addFilterNotEqual("status", TaskStatus.CANCELLED);

        List<Task> tasks = taskDAO.search(search);
        
        tasks.forEach(task -> task.setOverdue(true));
        
        taskDAO.save(tasks.toArray(new Task[tasks.size()]));
    }
}
