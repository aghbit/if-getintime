package ideafactory.getintime.service.filters;

import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.model.workingmodel.user.filters.Filter;
import ideafactory.getintime.service.CRUD.employees.crud.IEmployeeCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @author partyks
 */
@Service
public class FilterService implements IFilterService {
    @Autowired
    IEmployeeCRUDService employeeService;
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public void saveFilterForEmployeeWith(Long id, final Filter filter) {
        GetintimePreconditions.checkNotNull(filter, "InternalError");
        final Employee employee = employeeService.getByPK(id);
        
        GetintimePreconditions.checkNotNull(employee, "Employee.NotFound");
        employee.addFilter(filter);
        employeeService.save(employee);
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public Set<Filter> getFiltersForEmployeeWith(Long id) {
        final Employee employee = employeeService.getByPK(id);
        GetintimePreconditions.checkNotNull(employee, "Employee.NotFound");
        
        employee.getFilters().size();
        return employee.getFilters();
    }
    
    @Override
    @Transactional
    public void removeFilter(Long filterId, Long employeeId) {
        final Employee employee = employeeService.getByPK(employeeId);
        
        GetintimePreconditions.checkNotNull(employee, "Employee.NotFound");
        employee.removeFilterWithId(filterId);
        employeeService.save(employee);
    }
}
