package ideafactory.getintime.service.filters;

import ideafactory.getintime.domain.model.workingmodel.user.filters.Filter;

import java.util.Set;

/**
 * @author partyks
 */
public interface IFilterService {
    void saveFilterForEmployeeWith(Long id, Filter filter);
    
    Set<Filter> getFiltersForEmployeeWith(Long id);
    
    void removeFilter(Long filterId, Long employeeId);
}
