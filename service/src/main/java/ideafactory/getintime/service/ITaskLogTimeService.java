package ideafactory.getintime.service;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

import java.util.Optional;

public interface ITaskLogTimeService {
    void logTime(long taskId, long timeInMinutes);
    
    void logTimeAndPercentage(long taskId, long timeInMinutes, int percentage);
    
}
