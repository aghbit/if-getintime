package ideafactory.getintime.service.security;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.CRUD.employees.crud.EmployeeCRUDServiceImpl;
import ideafactory.getintime.service.CRUD.employees.crud.IEmployeeCRUDService;
import ideafactory.getintime.service.CRUD.exception.EmployeeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Maciej Ciołek
 */
public class UserDetailsServiceImpl implements UserDetailsService {
    
    @Autowired
    private IEmployeeCRUDService employeeCRUDService;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Employee> employeeOptional = employeeCRUDService.findByUsername(username);
        if (!employeeOptional.isPresent()) {
            throw new EmployeeNotFoundException(username);
        } else {
            return employeeOptional.get();
        }
        
    }
}
