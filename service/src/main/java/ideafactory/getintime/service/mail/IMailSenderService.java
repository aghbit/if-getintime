package ideafactory.getintime.service.mail;

import ideafactory.getintime.domain.support.SupportTicket;

public interface IMailSenderService {
    public void sendSupportTicket(SupportTicket supportTicket);
}
