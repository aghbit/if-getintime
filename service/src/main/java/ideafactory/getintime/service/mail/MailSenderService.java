package ideafactory.getintime.service.mail;

import ideafactory.getintime.domain.support.SupportTicket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class MailSenderService implements IMailSenderService {
    
    @Autowired
    private MailSender mailSender;
    
    @Value("${mail.sender}")
    private String sender;
    
    @Value("${mail.recipient}")
    private String recipient;
    
    @Override
    @Transactional
    public void sendSupportTicket(SupportTicket ticket) {
        
        String fullSender = String.format("\"%s\" <%s>", ticket.getReporter(), sender);
        
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(ticket.getSubject());
        message.setText(ticket.getDescription());
        message.setFrom(fullSender);
        message.setTo(recipient);
        
        mailSender.send(message);
    }
}
