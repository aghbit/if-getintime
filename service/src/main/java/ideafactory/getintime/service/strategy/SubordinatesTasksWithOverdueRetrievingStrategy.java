package ideafactory.getintime.service.strategy;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.user.SubordinatesVisitor;

import java.time.LocalDate;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * @author Grzegorz Miejski on 11.06.14.
 */
public class SubordinatesTasksWithOverdueRetrievingStrategy implements TaskRetrieverStrategy {
    @Override
    public Search getRetrievingStrategy(LocalDate from, LocalDate to, Employee loggedEmployee) {
        
        Set<Long> subordinatesIds = getSubordinatesIds(loggedEmployee);
        
        Search search = new Search();
        search.addFilterIn("taskProperties.assigned.id", subordinatesIds);
        
        search.addFilter(Filter.or(
                Filter.equal("overdue", true),
                Filter.and(Filter.greaterOrEqual("assigneeDeadline.date", from),
                        Filter.lessOrEqual("assigneeDeadline.date", to))));
        
        return search;
    }
    
    private Set<Long> getSubordinatesIds(Employee loggedEmployee) {
        SubordinatesVisitor subordinatesVisitor = new SubordinatesVisitor();
        loggedEmployee.accept(subordinatesVisitor);
        Set<Long> subordinatesIds = subordinatesVisitor.getSubordinates().stream().map(x -> x.getId()).collect(toSet());
        subordinatesIds.remove(loggedEmployee.getId());
        return subordinatesIds;
    }
}
