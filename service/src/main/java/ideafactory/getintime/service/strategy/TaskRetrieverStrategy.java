package ideafactory.getintime.service.strategy;

import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;

import java.time.LocalDate;

/**
 * @author Grzegorz Miejski on 29.05.14.
 */
public interface TaskRetrieverStrategy {
    Search getRetrievingStrategy(LocalDate from, LocalDate to, Employee loggedEmployee);
}
