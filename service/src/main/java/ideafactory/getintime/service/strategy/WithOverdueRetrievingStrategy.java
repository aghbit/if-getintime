package ideafactory.getintime.service.strategy;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.task.properties.TaskStatus;

import java.time.LocalDate;

/**
 * @author Grzegorz Miejski on 29.05.14.
 */
public class WithOverdueRetrievingStrategy implements TaskRetrieverStrategy {
    
    @Override
    public Search getRetrievingStrategy(LocalDate from, LocalDate to, Employee loggedEmployee) {
        Search search = new Search();
        search.addFilterEqual("taskProperties.assigned.id", loggedEmployee.getId());
        search.addFilter(Filter.or(
                Filter.equal("overdue", true),
                Filter.and(Filter.greaterOrEqual("assigneeDeadline.date", from),
                        Filter.lessOrEqual("assigneeDeadline.date", to)),
                Filter.equal("status", TaskStatus.IN_PROGRESS)));
        return search;
    }
}
