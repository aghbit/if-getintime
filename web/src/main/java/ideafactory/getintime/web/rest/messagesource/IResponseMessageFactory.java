package ideafactory.getintime.web.rest.messagesource;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public interface IResponseMessageFactory {
    
    ResponseMessage getMessage(String description);
}
