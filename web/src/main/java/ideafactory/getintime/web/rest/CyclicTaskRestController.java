package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.task.cyclic.CyclicTaskBuilder;
import ideafactory.getintime.service.CRUD.task.cyclic.ICyclicTaskCRUDService;
import ideafactory.getintime.service.CRUD.task.cyclic.template.ICyclicTaskService;
import ideafactory.getintime.service.dayoff.IDayOffService;
import ideafactory.getintime.web.rest.cyclictask.CyclicTaskCreationResponse;
import ideafactory.getintime.web.rest.cyclictask.CyclicTaskRequestObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Maciej Ciołek
 */
@Controller
public class CyclicTaskRestController {
    
    @Autowired
    private ICyclicTaskCRUDService cyclicTaskCRUDService;
    
    @Autowired
    private ICyclicTaskService cyclicTaskService;
    
    @Autowired
    private IDayOffService dayOffService;
    
    @PreAuthorize("hasAnyRole('MANAGER','DIRECTOR')")
    @RequestMapping(value = "/tasks/cyclic", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<CyclicTaskCreationResponse> saveCyclicTask(
            @RequestBody CyclicTaskRequestObject requestObject) {
        final CyclicTaskBuilder cyclicTaskBuilder = new CyclicTaskBuilder();
        final CyclicTask cyclicTask = cyclicTaskBuilder
                .withTemplateTask(requestObject.getTemplateTask())
                .withDayOffManager(dayOffService.getDayOffManager()).from(requestObject.getFrom())
                .to(requestObject.getTo()).build();
        cyclicTaskCRUDService.save(cyclicTask);
        final Map<Term, Set<Term>> dayOffMovedTerms = cyclicTaskBuilder.getDayOffMovedTerms();
        final Map<Term, Set<Integer>> endOfMonthMovedTerms = cyclicTaskBuilder
                .getEndOfMonthMovedTerms();
        final CyclicTaskCreationResponse response = new CyclicTaskCreationResponse(
                dayOffMovedTerms, endOfMonthMovedTerms);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
    
    @PreAuthorize("hasAnyRole('MANAGER','DIRECTOR')")
    @RequestMapping(value = "/tasks/cyclic/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<CyclicTask> getCyclicTask(@PathVariable("id") Long id) {
        CyclicTask cyclicTask = cyclicTaskCRUDService.getByPK(id);
        return new ResponseEntity<>(cyclicTask, HttpStatus.OK);
    }
    
    @PreAuthorize("hasAnyRole('MANAGER','DIRECTOR')")
    @RequestMapping(value = "/tasks/cyclic/{id}/tasks", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Task>> getTasks(@PathVariable("id") Long id,
            @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
            @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to) {
        List<Task> tasks = cyclicTaskService.getTasksByIdWithDateRange(id, from, to);
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/cyclic", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<CyclicTask>> getCyclicTasks(
            @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
            @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to) {
        List<CyclicTask> cyclicTasks = cyclicTaskService.getAllWithDateRange(from, to);
        
        return new ResponseEntity<>(cyclicTasks, HttpStatus.OK);
    }
    
}
