package ideafactory.getintime.web.rest.cyclictask;

import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;

import java.util.Map;
import java.util.Set;

/**
 * @author Piotr Góralczyk
 */
public class CyclicTaskCreationResponse {
    private final Map<Term, Set<Term>> dayOffMovedTerms;
    private final Map<Term, Set<Integer>> endOfMonthMovedTerms;
    
    public CyclicTaskCreationResponse(Map<Term, Set<Term>> dayOffMovedTerms,
            Map<Term, Set<Integer>> endOfMonthMovedTerms) {
        this.dayOffMovedTerms = dayOffMovedTerms;
        this.endOfMonthMovedTerms = endOfMonthMovedTerms;
    }
    
    public Map<Term, Set<Term>> getDayOffMovedTerms() {
        return dayOffMovedTerms;
    }
    
    public Map<Term, Set<Integer>> getEndOfMonthMovedTerms() {
        return endOfMonthMovedTerms;
    }
}
