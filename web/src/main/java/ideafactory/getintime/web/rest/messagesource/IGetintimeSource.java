package ideafactory.getintime.web.rest.messagesource;

/**
 * @author Maciej Ciołek
 */
public interface IGetintimeSource {
    String getMessage(String errorCode);
}
