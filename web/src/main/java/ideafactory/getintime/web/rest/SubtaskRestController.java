package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.service.CRUD.task.subtask.ISubtaskService;
import ideafactory.getintime.web.rest.messagesource.IResponseMessageFactory;
import ideafactory.getintime.web.rest.messagesource.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Manages all actions connected with subtask.
 * 
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Controller
public class SubtaskRestController {
    
    @Autowired
    private ISubtaskService subtaskService;
    
    @Autowired
    private IResponseMessageFactory messageFactory;
    
    @RequestMapping(value = "/tasks/{id}/subtask", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public ResponseEntity<ResponseMessage> addSubtask(@PathVariable("id") Long id,
            @RequestBody Task subtask) {
        
        subtaskService.addSubtask(id, subtask);
        
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyAddedSubtask"),
                HttpStatus.OK);
    }
}
