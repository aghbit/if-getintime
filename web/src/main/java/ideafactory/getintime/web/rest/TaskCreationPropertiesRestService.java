package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.task.creationproperty.TaskCreationProperty;
import ideafactory.getintime.service.CRUD.task.creationproperty.ITaskCreationPropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Properties required on front end side to create a new task.
 * 
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Controller
public class TaskCreationPropertiesRestService {
    
    @Autowired
    private ITaskCreationPropertyService creationPropertyService;
    
    @RequestMapping(value = "/utils/task-priorities", method = RequestMethod.GET)
    public ResponseEntity<List<TaskCreationProperty<Integer>>> getPossibleTaskPriorities() {
        
        return new ResponseEntity<>(creationPropertyService.getPossiblePriorities(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/utils/task-types", method = RequestMethod.GET)
    public ResponseEntity<List<TaskCreationProperty<String>>> getPossibleTypes() {
        
        return new ResponseEntity<>(creationPropertyService.getPossibleTypes(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/utils/task-statuses", method = RequestMethod.GET)
    public ResponseEntity<List<TaskCreationProperty<String>>> getPossibleStatues() {
        
        return new ResponseEntity<>(creationPropertyService.getPossibleStatues(), HttpStatus.OK);
    }
}
