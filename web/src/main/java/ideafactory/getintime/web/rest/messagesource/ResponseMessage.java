package ideafactory.getintime.web.rest.messagesource;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public class ResponseMessage {
    private final String message;
    
    public ResponseMessage(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
}
