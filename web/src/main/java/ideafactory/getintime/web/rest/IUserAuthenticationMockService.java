package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

import java.util.Optional;

/**
 * @author partyks
 */
public interface IUserAuthenticationMockService {
    Optional<Employee> getCurrentUser();
    
    void setCurrentUser(long id);
}
