package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.TaskComment;
import ideafactory.getintime.service.CRUD.comments.crud.ICommentsCRUDService;
import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;
import ideafactory.getintime.service.CRUD.task.ITaskCRUDService;
import ideafactory.getintime.service.CRUD.task.ITaskService;
import ideafactory.getintime.service.CRUD.task.cyclic.ICyclicTaskCRUDService;
import ideafactory.getintime.web.rest.messagesource.GetintimeMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * @author slonka
 */
@Controller
public class TaskCommentRestController {
    
    @Autowired
    private GetintimeMessageSource messageSource;
    
    @Autowired
    private ICommentsCRUDService commentsCRUDService;
    
    @RequestMapping(value = "/tasks/{id}/comments", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<TaskComment>> getComments(@PathVariable("id") Long id)
            throws NoTaskFoundException {
        Set<TaskComment> comments = commentsCRUDService.getCommentsByTask(id);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/comments", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> postComment(@PathVariable("id") Long id,
            @RequestBody TaskComment taskComment) throws NoTaskFoundException {
        commentsCRUDService.addCommentToTask(id, taskComment);
        return new ResponseEntity<>(messageSource.getMessage("TaskComment.SuccessfullyAdded"),
                HttpStatus.CREATED);
    }
}
