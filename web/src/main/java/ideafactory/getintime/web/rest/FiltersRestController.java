package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.user.filters.Filter;
import ideafactory.getintime.service.filters.IFilterService;
import ideafactory.getintime.web.rest.messagesource.GetintimeMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

/**
 * @author Michal Partyka
 */
@Controller
public class FiltersRestController {
    
    @Autowired
    private IFilterService filterService;
    
    @Autowired
    private GetintimeMessageSource messageSource;
    
    @RequestMapping(value = "/employees/{id}/filters", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<Filter> getFilters(@PathVariable("id") Long id, @RequestBody Filter filter) {
        filterService.saveFilterForEmployeeWith(id, filter);
        return new ResponseEntity<>(filter, HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/employees/{id}/filters", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<Filter>> getAllFilters(@PathVariable("id") Long id) {
        Set<Filter> filters = filterService.getFiltersForEmployeeWith(id);
        return new ResponseEntity<>(filters, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/employees/{id}/filters/{filterId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeFilter(@PathVariable("id") Long id,
            @PathVariable("filterId") Long filterId) {
        filterService.removeFilter(filterId, id);
        return new ResponseEntity<>(messageSource.getMessage("Filter.SuccessfullyRemoved"),
                HttpStatus.OK);
    }
}
