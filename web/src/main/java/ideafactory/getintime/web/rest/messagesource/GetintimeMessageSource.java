package ideafactory.getintime.web.rest.messagesource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @author Maciej Ciołek
 */
@Component
public class GetintimeMessageSource implements MessageSource, IGetintimeSource {
    @Autowired
    private MessageSource messageSource;
    
    @Override
    public String getMessage(String s, Object[] objects, String s2, Locale locale) {
        return messageSource.getMessage(s, objects, s2, locale);
    }
    
    @Override
    public String getMessage(String s, Object[] objects, Locale locale)
            throws NoSuchMessageException {
        return messageSource.getMessage(s, objects, locale);
    }
    
    @Override
    public String getMessage(MessageSourceResolvable messageSourceResolvable, Locale locale)
            throws NoSuchMessageException {
        return messageSource.getMessage(messageSourceResolvable, locale);
    }
    
    @Override
    public String getMessage(String errorCode) {
        return messageSource.getMessage(errorCode, null, LocaleContextHolder.getLocale());
    }
}
