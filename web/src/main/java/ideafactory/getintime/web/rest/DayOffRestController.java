package ideafactory.getintime.web.rest;

import ideafactory.getintime.service.dayoff.DayOffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

/**
 * @author Piotr Góralczyk
 */
@Controller
public class DayOffRestController {
    @Autowired
    private DayOffService dayOffService;
    
    @RequestMapping(value = "/dayoff/{from}/{to}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<String>> getDaysOff(
            @PathVariable("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
            @PathVariable("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to) {
        Set<String> daysOff = dayOffService.getDaysOff(from, to)
                .stream()
                .map(LocalDate::toString)
                .collect(toSet());
        return new ResponseEntity<>(daysOff, HttpStatus.OK);
    }
}
