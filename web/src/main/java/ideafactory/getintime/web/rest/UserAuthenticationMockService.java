package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.CRUD.employees.crud.IEmployeeCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Mock implementation of UserAuthenticationService. After we will introduce
 * domain logging, we will be able to switch to proper implementation. It
 * returns employee if there is an employee in database. Otherwise it returns
 * null
 * 
 * @author partyks
 */
@Service("userAuthenticationMockService")
public class UserAuthenticationMockService implements IUserAuthenticationMockService {
    @Autowired
    IEmployeeCRUDService employeeCRUDService;
    
    private static Employee employee = null;
    
    @Override
    @Transactional
    public Optional<Employee> getCurrentUser() {
        if (employee == null) {
            List<Employee> list = employeeCRUDService.getList();
            UserAuthenticationMockService.employee = list.size() > 0 ? list.get(0) : null;
        }
        return Optional.ofNullable(employee);
    }
    
    @Override
    @Transactional
    public void setCurrentUser(long id) {
        UserAuthenticationMockService.employee = employeeCRUDService.getByPK(id);
    }
}
