package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.service.CRUD.exception.NoTaskFoundException;
import ideafactory.getintime.service.CRUD.task.ITaskCRUDService;
import ideafactory.getintime.service.CRUD.task.ITaskService;
import ideafactory.getintime.service.CRUD.task.cyclic.ICyclicTaskCRUDService;
import ideafactory.getintime.service.CRUD.task.subordinates.ISubordinatesTasksService;
import ideafactory.getintime.service.CRUD.task.subtask.ISubtaskService;
import ideafactory.getintime.service.authentication.ITaskPermissionService;
import ideafactory.getintime.service.authentication.IUserAuthenticationService;
import ideafactory.getintime.web.rest.messagesource.IResponseMessageFactory;
import ideafactory.getintime.web.rest.messagesource.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Controller
public class TaskRestController {
    
    @Autowired
    private IResponseMessageFactory messageFactory;
    
    @Autowired
    private ITaskCRUDService taskCrudService;
    @Autowired
    private ICyclicTaskCRUDService cyclicTaskCRUDService;
    @Autowired
    private ITaskService taskService;
    @Autowired
    IUserAuthenticationService authenticationService;
    @Autowired
    ITaskPermissionService taskPermissionService;
    @Autowired
    ISubtaskService subtaskService;
    
    @Autowired
    ISubordinatesTasksService subordinatesTasksService;
    
    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Task> getTask(@PathVariable("id") Long id) {
                Task task = taskService.getByPK(id);
        taskPermissionService.checkIfCanRetrieveTask(task);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/subtasks", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<Task>> getSubtasksForTask(@PathVariable("id") Long id) {
        return new ResponseEntity<>(subtaskService.getSubtasks(id), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/siblingtasks", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Set<Task>> getSiblingtasksForTask(@PathVariable("id") Long id) {
        Task task = taskService.getByPK(id);
        Set<Task> siblingTasks = null;
        
        if (task.getParentTask() != null) {
            Task parentTask = taskService.getByPKWithSubtasks(task.getParentTask().getId());
            siblingTasks = parentTask.getSubTasks();
            siblingTasks.remove(task);
        }
        
        return new ResponseEntity<>(siblingTasks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Task> updateTask(@PathVariable("id") Long id, @RequestBody Task task) {
        task.setId(id);
        // TODO(partyks): talk about validation but IMO CRUD should perform some
        // validation on update. Have no idea how
        // to achieve it correctly - the only one is just to have responsibility
        // chain of some updateValidationStrategies

        taskCrudService.update(task);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<ResponseMessage> saveTask(@RequestBody Task task) {
        taskService.save(task);
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyCreated"),
                HttpStatus.CREATED);
    }
    
    @RequestMapping(value = "/tasks/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Task>> getAllTasks() {
        List<Task> tasks = taskService.getList();
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Task>> getLoggedUsersTasks(
            @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
            @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to) {
        List<Task> tasks = taskService.getLoggedEmployeeTasks(from, to);
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/start", method = RequestMethod.POST, produces = " application/json", consumes = "application/json")
    public ResponseEntity<ResponseMessage> startWorkingOn(@PathVariable("id") Long id) {
        taskService.startWorkingOnTask(id);
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyStartedWorkingOn"),
                HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/stop", method = RequestMethod.PUT)
    public ResponseEntity<ResponseMessage> stopWorkingOn(@PathVariable("id") Long id)
            throws NoTaskFoundException {
        taskService.stopWorkingOnTask(id);
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyStoppedWorkingOn"),
                HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/finish", method = RequestMethod.POST)
    public ResponseEntity<ResponseMessage> finishWorkingOn(@PathVariable("id") Long id)
            throws NoTaskFoundException {
        taskService.finishWorkingOnTask(id);
        return new ResponseEntity<>(
                messageFactory.getMessage("Task.SuccessfullyFinishedWorkingOn"), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/cancel", method = RequestMethod.PUT)
    public ResponseEntity<ResponseMessage> cancelTask(@PathVariable("id") Long id)
            throws NoTaskFoundException {
        taskService.cancelWorkingOnTask(id);
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyCancelled"),
                HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/time/{timeInMinutes}", method = RequestMethod.PUT)
    public ResponseEntity<ResponseMessage> logTime(@PathVariable("id") Long id,
            @PathVariable("timeInMinutes") Long timeInMinutes) throws NoTaskFoundException {
        taskService.logTime(id, timeInMinutes);
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyLoggedTime"),
                HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/work-percent/{percent}", method = RequestMethod.PUT)
    public ResponseEntity<ResponseMessage> logWorkProgressPercentage(@PathVariable("id") Long id,
            @PathVariable("percent") Integer percent) throws NoTaskFoundException {
        taskService.logProgressPercentage(id, percent);
        
        return new ResponseEntity<>(
                messageFactory.getMessage("Task.SuccessfullyLoggedWorkPercentDone"), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/time-and-progress/{timeInMinutes}/{percent}", method = RequestMethod.PUT)
    public ResponseEntity<ResponseMessage> logWorkProgressPercentageAndTime(
            @PathVariable("id") Long id, @PathVariable("timeInMinutes") Long timeInMinutes,
            @PathVariable("percent") Integer percent) throws NoTaskFoundException {
        taskService.logTimeAndPercentage(id, timeInMinutes, percent);
        
        return new ResponseEntity<>(
                messageFactory.getMessage("Task.SuccessfullyLoggedWorkPercentDone"), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/subordinates", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> getCurrentUserSubordinatesTasks(
            @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate from,
            @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate to) {
        List<Task> subordinatesTasks = subordinatesTasksService.getCurrentUserSubordinatesTasks(
                from, to);
        
        return new ResponseEntity<>(subordinatesTasks, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/tasks/{id}/reopen/{percent}")
    public ResponseEntity<ResponseMessage> reopenTask(
            @PathVariable("id") Long taskId,
            @PathVariable("percent") Integer percent,
            @RequestParam("ownerDeadline") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate ownerDeadline,
            @RequestParam("assigneeDeadline") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate assigneeDeadline) {
        taskService.reopenTask(taskId, ownerDeadline, assigneeDeadline, percent);
        
        return new ResponseEntity<>(messageFactory.getMessage("Task.SuccessfullyReopened"),
                HttpStatus.OK);
    }
    
}
