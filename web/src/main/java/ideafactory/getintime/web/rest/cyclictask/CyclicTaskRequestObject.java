package ideafactory.getintime.web.rest.cyclictask;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ideafactory.getintime.domain.mappers.LocalDateDeserializer;
import ideafactory.getintime.domain.mappers.LocalDateSerializer;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;

import java.time.LocalDate;

/**
 * @author Maciej Ciołek
 */
public class CyclicTaskRequestObject {
    private TemplateTask templateTask;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate from;
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate to;
    
    @JsonCreator
    public CyclicTaskRequestObject(@JsonProperty("templateTask") TemplateTask templateTask,
            @JsonProperty("from") LocalDate from, @JsonProperty("to") LocalDate to) {
        this.templateTask = templateTask;
        this.from = from;
        this.to = to;
    }
    
    public TemplateTask getTemplateTask() {
        return templateTask;
    }
    
    public void setTemplateTask(TemplateTask templateTask) {
        this.templateTask = templateTask;
    }
    
    public LocalDate getFrom() {
        return from;
    }
    
    public void setFrom(LocalDate from) {
        this.from = from;
    }
    
    public LocalDate getTo() {
        return to;
    }
    
    public void setTo(LocalDate to) {
        this.to = to;
    }
}
