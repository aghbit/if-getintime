package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.CRUD.employees.crud.IEmployeeCRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Set;

/**
 * @author partyks
 */
@Controller
public class EmployeeRestController {
    
    @Autowired
    private IEmployeeCRUDService employeeService;
    
    @RequestMapping(value = "/employees", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        List<Employee> employees = employeeService.getList();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/employees/{id}/subordinates")
    public ResponseEntity<List<Employee>> getSubordinates(@PathVariable("id") Long id) {
        List<Employee> employees = employeeService.getSubordinatesOfEmployeeWithId(id);
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/employees/{id}/tasks")
    public ResponseEntity<Set<Task>> getTasks(@PathVariable("id") Long id) {
        Set<Task> tasksOfEmployee = employeeService.getTasksOfEmployeeWithId(id);
        return new ResponseEntity<>(tasksOfEmployee, HttpStatus.OK);
    }
}
