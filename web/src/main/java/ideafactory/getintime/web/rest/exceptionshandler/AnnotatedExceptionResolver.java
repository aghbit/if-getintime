package ideafactory.getintime.web.rest.exceptionshandler;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerExceptionResolver;

/**
 * @author Maciej Ciołek
 */
@Component
public class AnnotatedExceptionResolver extends AnnotationMethodHandlerExceptionResolver {
    public AnnotatedExceptionResolver() {
        setOrder(HIGHEST_PRECEDENCE);
    }
}
