package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.messages.Message;
import ideafactory.getintime.service.CRUD.messages.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Lukasz Raduj raduj.lukasz@gmail.com
 */
@Controller
public class MessagesController {
    
    @Autowired
    private IMessageService messageService;
    
    @RequestMapping(value = "/message", method = RequestMethod.GET)
    public ResponseEntity<Message> getMessage() {
        return new ResponseEntity<>(messageService.getDefaultMessage(), HttpStatus.OK);
    }
}
