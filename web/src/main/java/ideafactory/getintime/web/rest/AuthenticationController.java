package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.authentication.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

/**
 * @author Maciej Ciołek
 */
@Controller
public class AuthenticationController {
    
    @Autowired
    @Qualifier("userAuthenticationService")
    private IUserAuthenticationService userAuthenticationService;
    
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<LoginResponse> login(@RequestBody Credentials credentials) {
        try {
            userAuthenticationService.authenticateUser(credentials);
            final Optional<Employee> employee = userAuthenticationService.getCurrentUser();
            return new ResponseEntity<LoginResponse>(LoginResponse.success(employee.get()),
                    HttpStatus.OK);
        } catch (AuthenticationException ex) {
            return new ResponseEntity<LoginResponse>(LoginResponse.failed(), HttpStatus.OK);
        }
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Boolean> logout() {
        userAuthenticationService.removeCurrentAuthenticationToken();
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
}
