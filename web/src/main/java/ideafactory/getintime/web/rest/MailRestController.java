package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.support.SupportTicket;
import ideafactory.getintime.service.mail.IMailSenderService;
import ideafactory.getintime.web.rest.messagesource.IResponseMessageFactory;
import ideafactory.getintime.web.rest.messagesource.ResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MailRestController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(MailRestController.class);
    
    @Autowired
    private IMailSenderService mailSenderService;
    
    @Autowired
    private IResponseMessageFactory messageFactory;
    
    private static final String PROBLEM_REPORTED = "Mail.ProblemReported";
    private static final String PROBLEM_REPORTING_ERROR = "Mail.ProblemReportingError";
    
    @RequestMapping(value = "/support-ticket", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<ResponseMessage> sendSupportTicket(
            @RequestBody SupportTicket supportTicket) {
        
        try {
            LOGGER.debug("Sending support ticket: {}", supportTicket);
            
            mailSenderService.sendSupportTicket(supportTicket);
            
            return produceResponse(PROBLEM_REPORTED, HttpStatus.OK);
        } catch (MailException ex) {
            LOGGER.error("Error while sending support ticket", ex);
            
            return produceResponse(PROBLEM_REPORTING_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    private ResponseEntity<ResponseMessage> produceResponse(String key, HttpStatus status) {
        ResponseMessage message = messageFactory.getMessage(key);
        
        return new ResponseEntity<>(message, status);
    }
}
