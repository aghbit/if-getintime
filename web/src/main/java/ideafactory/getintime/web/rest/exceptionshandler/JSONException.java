package ideafactory.getintime.web.rest.exceptionshandler;

import com.google.common.collect.ImmutableMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

/**
 * @author Maciej Ciołek
 */
public class JSONException {
    private String message;
    
    public JSONException(String message) {
        this.message = message;
    }
    
    public ModelAndView asModelAndView() {
        MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
        return new ModelAndView(jsonView, ImmutableMap.of("error", message));
    }
}
