package ideafactory.getintime.web.rest;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.service.authentication.IUserAuthenticationService;
import ideafactory.getintime.web.rest.messagesource.GetintimeMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

/**
 * @author Maciej Ciołek
 */
@Controller
public class CurrentUserRestController {
    @Autowired
    IUserAuthenticationService userService;
    
    @Autowired
    IUserAuthenticationMockService userMockService;
    
    @Autowired
    private GetintimeMessageSource messageSource;
    
    @RequestMapping(value = "/authentication/current-user", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Employee> getCurrentUser() {
        final Optional<Employee> employee = userService.getCurrentUser();
        
        return new ResponseEntity<>(employee.orElse(null), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/authentication/current-user-mock/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> setCurrentUserMock(@PathVariable("id") Long id) {
        userMockService.setCurrentUser(id);
        return new ResponseEntity<>(messageSource.getMessage("MockedLogin.Success"), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/authentication/current-user-mock", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Employee> getCurrentUserMock() {
        final Optional<Employee> employee = userMockService.getCurrentUser();
        
        return new ResponseEntity<>(employee.orElse(null), HttpStatus.OK);
    }
}
