package ideafactory.getintime.web.rest.exceptionshandler;

import ideafactory.geintime.commons.exceptions.StatusException;
import ideafactory.getintime.web.rest.messagesource.GetintimeMessageSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Maciej Ciołek
 */
@ControllerAdvice
public class ExceptionController {
    private static final Logger LOGGER = Logger.getLogger(ExceptionController.class);
    
    @Autowired
    GetintimeMessageSource messageSource;
    
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) throws IOException {
        LOGGER.error("Request: " + req.getRequestURL() + " raised " + exception);
        
        String message = exception.getMessage();
        if (exception instanceof StatusException) {
            StatusException statusException = (StatusException) exception;
            message = messageSource.getMessage(statusException.getErrorCode());
        }
        
        if (exception.getCause() != null
                && exception.getCause().getCause() instanceof StatusException) {
            StatusException statusException = (StatusException) exception.getCause().getCause();
            message = messageSource.getMessage(statusException.getErrorCode());
        }
        return new JSONException(message).asModelAndView();
    }
}
