package ideafactory.getintime.web.rest.messagesource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Component
public class ResponseMessageFactory implements IResponseMessageFactory {
    
    @Autowired
    private GetintimeMessageSource messageSource;
    
    public ResponseMessage getMessage(String description) {
        return new ResponseMessage(messageSource.getMessage(description));
    }
}
