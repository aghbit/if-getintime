'use strict';

controllers.controller('ActiveTaskController', [
    '$scope', '$interval', '$modal', '$dialogs', 'TaskActionsService', 'TaskMethodsService',
    function ($scope, $interval, $modal, $dialogs, TaskActionsService, TaskMethodsService) {

        var timer;
        var currentTime = 0;

        $scope.logProgressMessage = "Zapisz postęp pracy";

        function WorkingTime(seconds) {
            this.allSeconds = seconds;

            this.updateTime = function (seconds) {
                this.allSeconds += seconds;
                this.seconds = ("0" + parseInt(this.allSeconds % 60)).slice(-2);
                this.minutes = ("0" + parseInt((this.allSeconds / 60) % 60)).slice(-2);
                this.hours = parseInt((this.allSeconds / 3600));
            }
        }

        var updateTime = function () {
            $scope.activeTask.workingTime.updateTime(1);
        };

        function initWorkingTime() {
            $scope.activeTask.workingTime.updateTime(0);
        }

        var setActiveTask = function (task) {
            var timeDifference = 0;
            if (task.workingStartDate) {
                var startDate = task.workingStartDate.startDate;
                var date = startDateToDate(startDate);
                timeDifference = parseInt((new Date() - date) / 1000);
            }

            $scope.activeTask = task;
            $scope.activeTask.workingTime = new WorkingTime(timeDifference + task.loggedTime * 60);

            $scope.calculateCurrentPercentage = function() {
                return Math.floor((100*task.workingTime.allSeconds)/(60*task.taskProperties.estimatedTime));
            };

            $scope.activeTask.percentage = $scope.calculateCurrentPercentage();
            initWorkingTime();

            if (timer !== undefined) {
                $interval.cancel(timer);
            }

            timer = $interval(updateTime, 1000);
        };

        var selectAlreadyWorkingTask = function () {
            $scope.clearActiveTask();
            angular.forEach($scope.data.tasks, function (task) {
                if (task.status == 'IN_PROGRESS') {
                    setActiveTask(task);
                    return;
                }
            });
        };

        $scope.$on('$destroy', function () {
            $interval.cancel(timer);
        });

        $scope.$on('LOADED_TASKS', function () {
            selectAlreadyWorkingTask();
        });

        $scope.clearActiveTask = function() {
            $interval.cancel(timer);
            $scope.activeTask = null;
        };

        $scope.$on('STOPPED_WORKING_ON_TASK', function (event, message) {
            $interval.cancel(timer);
            $scope.activeTask = null;
        });

        $scope.isWorkingOnTask = function () {
            return ($scope.activeTask != null);
        };

        $scope.openLogProgressModal = function (taskId, progressPercentage) {
            var logProgressModal = $modal.open({
                templateUrl: '/partials/views/log_progress.html',
                controller: 'LogProgressController',
                resolve: {
                    taskId: function () {
                        return taskId
                    },
                    progressPercentage: function () {
                        return progressPercentage
                    }
                }
            });

            logProgressModal.result.then(function () {
                $scope.$root.$broadcast('UPDATE_TASKS_LIST_EVENT', {});
            });
        };

        $scope.finishWorkingOnTask = function (taskId) {
            TaskActionsService.finish({id: taskId},
                function (successResult) {
                    $scope.activeTask = null;
                    $scope.$root.$broadcast('UPDATE_TASKS_LIST_EVENT', {});
                    $scope.$root.$broadcast('STOPPED_WORKING_ON_TASK');
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                })
        };

        $scope.openStopWorkingModal = function (progressPercentage) {
            var modalInstance = $modal.open({
                templateUrl: '/partials/views/work_stop_modal.html',
                controller: 'TaskStopController',
                resolve: {
                    task: function () {
                        return $scope.activeTask;
                    },
                    progressPercentage: function () {
                        return progressPercentage
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.$root.$broadcast('UPDATE_TASKS_LIST_EVENT', {});
            })
        };

        $scope.isEstimatedTimeExceeded = function (task) {
            return TaskMethodsService.isEstimatedTimeExceeded(task);
        };
    }]
);