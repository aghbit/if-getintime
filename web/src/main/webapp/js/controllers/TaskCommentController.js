'use strict';

controllers.controller('TaskCommentController', ['$scope', '$stateParams', '$filter', '$dialogs', 'CommentsService', 'AuthenticationService',
    function ($scope, $stateParams, $filter, $dialogs, CommentsService, AuthenticationService) {

        function convertTimeToSting(date) {
            return $filter('date')(date, "d.MM.yyyy H:mm");
        }

        function loadComments() {
            $scope.comments = CommentsService.getComments({id: $stateParams.id}, function (comments) {
                    $scope.comments = comments;
                }, function (errorResult) {
                    $dialogs.error("Could not get comments!", errorResult.data);
                }
            );
        }

        loadComments();

        var currentUser = AuthenticationService.currentUser(function (currentUser) {

        }, function () {
            $dialogs.error("Could not get current user / create comment!");
        });

        $scope.addTaskComment = function () {
            $scope.newComment = {
                text: $scope.input.text,
                employee: {id: currentUser.id, firstName: currentUser.firstName, secondName: currentUser.secondName}
            };

            CommentsService.saveComment({id: $stateParams.id}, $scope.newComment, function () {
                loadComments();
            }, function (errorResult) {
                $dialogs.error("Could not refresh comments!", errorResult.data);
            });

            $scope.clearInput();
        };

        $scope.clearInput = function() {
            $scope.input = {
                text: ''
            };
        };
    }]);
