/**
 * Created by Grzesiek on 20.03.14.
 */
"use strict";

controllers.controller('ReportBugController', ['$scope', '$modalInstance', '$dialogs', '$location', 'BugsService', 'user',
    function ($scope, $modalInstance, $dialogs, $location, BugsService, user) {

        $scope.message = {
            reporter: user.firstName + " " + user.secondName
        };

        $scope.reportBug = function (message) {
            BugsService.report(message,
                function (successResult) {
                    $scope.ok();
                },
                function (errorResult) {
                    $dialogs.error("Error occurred on reporting a bug!", errorResult.data.error);
                    $scope.cancel();
                });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            $modalInstance.close();
        }

    }]);
