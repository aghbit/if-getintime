"use strict";
controllers.controller('TaskDetailsController',
    ['$scope', '$stateParams','SubTaskService', '$dialogs', '$modal', '$state', '$stateParams',
        'TaskService', 'TaskActionsService', 'AuthenticationService', 'TaskMethodsService',
        function ($scope, $stateParams, SubTaskService, $dialogs, $modal, $state, $stateParam,
                  TaskService, TaskActionsService, AuthenticationService, TaskMethodsService) {

        $scope.task = null;

        $scope.addSubtaskButtonDisabled = null;
        $scope.editTaskButtonDisabled = null;
        $scope.startTaskButtonDisabled = null;
        $scope.logProgressButtonDisabled = null;
        $scope.finishTaskButtonDisabled = null;
        $scope.cancelTaskButtonDisabled = null;
        $scope.stopWorkingModalDisabled = null;
        $scope.currentUser = AuthenticationService.currentUser();

        var refreshButtonsStates = function refreshButtonsStates() {
            $scope.addSubtaskButtonDisabled = $scope.task.status == "DONE" || $scope.task.status == "CANCELLED";
            $scope.editTaskButtonDisabled = $scope.task.status == "DONE" || $scope.task.status == "CANCELLED";
            $scope.startTaskButtonDisabled = $scope.task.status == "IN_PROGRESS" || $scope.task.status == "DONE"
                || $scope.task.status  == "CANCELLED";
            $scope.logProgressButtonDisabled = $scope.task.status == "IN_PROGRESS" || $scope.task.status == "DONE" || $scope.task.status == "CANCELLED" || !AuthenticationService.isAuthorized(['MANAGER', 'DIRECTOR']);
            $scope.logProgressButtonHid = !AuthenticationService.isAuthorized(['MANAGER', 'DIRECTOR']);
            $scope.finishTaskButtonDisabled = $scope.task.status != "IN_PROGRESS" && $scope.task.status != "STOPPED";
            $scope.cancelTaskButtonDisabled = $scope.task.status == "CANCELLED" || $scope.task.status == "DONE";
            $scope.stopWorkingModalDisabled = $scope.task.status != "IN_PROGRESS";
        };

        function setLogProgressMessage(task) {
            if (task.status == 'DONE') {
                $scope.logProgressMessage = "Zadanie jest już zakończone";
            } else {
                $scope.logProgressMessage = "Zapisz postęp pracy";
            }
        }

        var refreshTask = function refreshTask () {
            $scope.task = TaskService.getTask({id: $stateParams.id},
                function(successResult) {
                    refreshButtonsStates();
                    setLogProgressMessage(successResult);
                },
                function(errorResult) {
                    $dialogs.error( "Error", errorResult.data.error);
                }
            );
        };
        refreshTask();

            SubTaskService.updateNumberOfSubtasks();

            $scope.hasSubtasks = function () {
                $scope.subtasks = SubTaskService.getSubTasks();
                return $scope.subtasks.length > 0;
            };

        $scope.hasSiblingtasks = function () {
            return $scope.siblingtasks.length > 0;
        };

        $scope.isSubtaskMine = function (task) {
            return task.taskProperties.assigned.id == $scope.currentUser.id ||
                task.taskProperties.createdBy.id == $scope.currentUser.id
        };

        var getSiblingtasks = function () {
            $scope.siblingtasks = TaskService.getSiblingtasks({id: $stateParams.id},
                function(succesResult) {
                },
                function(errorResult) {
                    $dialogs.error( "Error", errorResult.data.error);
                }
            );
        };
        getSiblingtasks();

        $scope.cancelTask = function cancelTask () {
            TaskActionsService.cancelTask( { id : $scope.task.id },
                function( successResult ) {
                    $dialogs.notify( "Anulowano zadanie", "Zadanie zostało pomyślnie anulowane" );
                    refreshTask();
                },
                function( errorResult ) {
                    $dialogs.error( "Error", errorResult.data.error );
                });
        };

        $scope.addSubtask = function addSubtask() {
            var parentTask = $scope.task;

            var modalInstance = $modal.open({
                templateUrl: 'partials/views/add_subtask.html',
                controller: 'AddSubTaskController',
                resolve: {
                    parentTask: function () {
                        return parentTask;
                    }
                }
            });

            modalInstance.result.then(function () {
                refreshTask();
            });
        };

        $scope.startWorkingOnTask = function startWorkingOnTask () {
            TaskActionsService.start({id: $scope.task.id},
                function (successResult) {
                    $dialogs.notify("Success", successResult.message);
                    refreshTask();
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                });
        };

        $scope.editTask = function editTask() {
            var taskId = $scope.task.id;

            var modalInstance = $modal.open({
                templateUrl: 'partials/views/edit_task.html',
                controller: 'EditTaskController',
                resolve: {
                       taskId: function () {
                            return taskId;
                       }
                }
            });

            modalInstance.result.then(function () {
                refreshTask();
            });

        };

        $scope.logWorkingProgress = function logWorkingProgress(taskId, progressPercentage) {

            if (progressPercentage < 100 && AuthenticationService.isAuthorized(['MANAGER', 'DIRECTOR'])) {
                var logProgressModal = $modal.open({
                    templateUrl: '/partials/views/log_progress.html',
                    controller: 'LogProgressController',
                    resolve: {
                        taskId: function () {
                            return taskId;
                        },
                        progressPercentage: function () {
                            return progressPercentage;
                        }
                    }
                });

                logProgressModal.result.then(function () {
                    refreshTask();
                });
            }
        };

        $scope.endWorkingOnTask = function endWorkingOnTask() {
            TaskActionsService.finish({id: $scope.task.id},
                function (successResult) {
                    $dialogs.notify("Success", successResult.message);
                    refreshTask();
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                })
        };

        $scope.openStopWorkingModal = function (taskId, progressPercentage) {
            var modalInstance = $modal.open({
                templateUrl: '/partials/views/work_stop_modal.html',
                controller: 'TaskStopController',
                resolve: {
                    taskId: function () {
                        return taskId;
                    },
                    progressPercentage: function () {
                        return progressPercentage
                    }
                }
            });

            modalInstance.result.then(function () {
                refreshTask();
            })
        };

        // TODO refactor progressbar to directive?
        $scope.logProgressFromBar = function(taskId, progressPercentage) {
            if ( $scope.task.status == 'IN_PROGRESS') {
                $scope.openStopWorkingModal(taskId, progressPercentage);
            } else {
                $scope.logWorkingProgress(taskId, progressPercentage);
            }
        };

        $scope.isEstimatedTimeExceeded = function (task) {
            return TaskMethodsService.isEstimatedTimeExceeded(task);
        };
    }]);