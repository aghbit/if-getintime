'use strict';

controllers.controller('SubordinatesTasksTableController', ['$scope', '$dialogs', 'TaskListingService', 'EmployeesService',
    function ($scope, $dialogs, TaskListingService, EmployeesService) {

        $scope.$on('FILTERS_LOADED_EVENT', function (event, dateRange) {
            updateTasks(dateRange.from, dateRange.to);
        });

        $scope.$on('VIEW_TOGGLE_EVENT', function (event, dateRange) {
            updateTasks(dateRange.from, dateRange.to);
        });

        $scope.$on('UPDATE_TASKS_LIST_EVENT', function (event, dateRange) {
            updateTasks(dateRange.from, dateRange.to);
        });

        var updateTasks = function (dateFrom, dateTo) {
            TaskListingService.getSubordinatesTasks({from: dateToYMD(dateFrom), to: dateToYMD(dateTo)},
                function (successResult) {
                    $scope.data.tasks = successResult;
                    $scope.alreadyWorkingOnTask = $scope.isAlreadyWorkingOnTask();
                    $scope.$emit('LOADED_TASKS');
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                }
            );
        };

        EmployeesService.query({},
            function (successResult) {
                $scope.visibleAssignees = successResult;
                $scope.setVisibleToAll();
            },
            function (errorResult) {
                $dialogs.notify("Error", errorResult.data.error);
            });

        $scope.togglePerson = function (index, event) {
            $scope.visibleAssignees[index].checked = !$scope.visibleAssignees[index].checked;
            event.stopPropagation();
        };

        $scope.setVisibleToAll = function (event) {
            angular.forEach($scope.visibleAssignees, function (visibleAssignee) {
                visibleAssignee.checked = true;
            });
            if (event != null) {
                event.stopPropagation();
            }
        };

        $scope.clearVisibleAssignees = function (event) {
            angular.forEach($scope.visibleAssignees, function (visibleAssignee) {
                visibleAssignee.checked = false;
            });
            event.stopPropagation();
        }

    }]);
