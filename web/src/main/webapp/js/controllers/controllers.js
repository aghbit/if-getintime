'use strict';

/* Controllers */

var controllers = angular.module('getintime.controllers',
    [
        'ngResource',
        'getintime.services',
        'ui.bootstrap',
        'dialogs'
    ]);

