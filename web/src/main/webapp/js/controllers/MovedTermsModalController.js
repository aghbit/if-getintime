'use strict';

controllers.controller('MovedTermsModalController', function($scope, $modalInstance, dayOffMovedTerms, endOfMonthMovedTerms) {
    $scope.dayOffMovedTerms = dayOffMovedTerms;

    $scope.endOfMonthMovedTerms = endOfMonthMovedTerms;

    $scope.dateToYMD = dateToYMD;

    $scope.close = function() {
        $modalInstance.close();
    };
});
