'use strict';

controllers.controller('TaskActionController', [
    '$scope', '$modal', '$dialogs',
    'TaskActionsService', 'TaskService', 'AuthenticationService', function ($scope, $modal, $dialogs, TaskActionsService, TaskService, AuthenticationService) {

        var refreshTasks = function () {
            $scope.$emit('UPDATE_TASKS_LIST_EVENT', {});
        };

        $scope.activeTaskIds = [];

        /* add subtask */
        $scope.addSubTask = function (parentTask) {
            var modalInstance = openAddSubTaskModal(parentTask);
            modalInstance.result.then(function () {
                refreshTasks();
            })
        };

        var openAddSubTaskModal = function (parentTask) {
            return $modal.open({
                templateUrl: 'partials/views/add_subtask.html',
                controller: 'AddSubTaskController',
                resolve: {
                    parentTask: function () {
                        return parentTask;
                    }
                }
            })
        };
        /* end add subtask */

        /* edit task */
        $scope.editTask = function (taskId) {
            var modalInstance = $scope.openEditTaskModal(taskId);
            modalInstance.result.then(function () {
                refreshTasks();
            });
        };

        $scope.openEditTaskModal = function (taskId) {
            return $modal.open({
                templateUrl: 'partials/views/edit_task.html',
                controller: 'EditTaskController',
                resolve: {
                    taskId: function () {
                        return taskId;
                    }
                }
            })
        };
        /* end edit task */

        /* start working on task */
        $scope.startWorkingOnTask = function (taskId) {
            TaskActionsService.start({id: taskId},
                function (successResult) {
                    refreshTasks();
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                });
        };
        /* end start working on task */

        /* stop working */
        $scope.openStopWorkingModal = function (taskId, progressPercentage) {
            var modalInstance = $modal.open({
                templateUrl: '/partials/views/work_stop_modal.html',
                controller: 'TaskStopController',
                resolve: {
                    taskId: function () {
                        return taskId;
                    },
                    progressPercentage: function () {
                        return progressPercentage
                    }
                }
            });

            modalInstance.result.then(function () {
                refreshTasks();
            })
        };
        /* end stop working */


        /* finish working */
        $scope.finishWorkingOnTask = function (taskId) {
            TaskService.getTask({id: taskId}, function (successResult) {
                if (successResult.status === "IN_PROGRESS") {
                    finishTaskOnBackend(taskId);
                    return;
                }
                if (successResult.status === "STOPPED") {
                    openFinishStoppedTaskModal(taskId);
                }
            });
        };

        var openFinishStoppedTaskModal = function (taskId) {
            var modalInstance = $modal.open({
                templateUrl: '/partials/views/work_finish_stopped_task_modal.html',
                controller: 'LogProgressController',
                resolve: {
                    taskId: function () {
                        return taskId;
                    },
                    progressPercentage: function () {
                        return 100;
                    }
                }
            });

            modalInstance.result.then(function () {
                refreshTasks();
            })
        };

        var finishTaskOnBackend = function (taskId) {
            TaskActionsService.finish({id: taskId},
                function (successResult) {
                    $dialogs.notify("Success", successResult.message);
                    refreshTasks();
                    $scope.$root.$broadcast('STOPPED_WORKING_ON_TASK');
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                });

        };
        /* end finish working */


        /* log progress */
        $scope.openLogProgressModal = function (taskId, progressPercentage) {

            if (!AuthenticationService.isAuthorized(['MANAGER', 'DIRECTOR'])) {
                return;
            }

            var logProgressModal = $modal.open({
                templateUrl: '/partials/views/log_progress.html',
                controller: 'LogProgressController',
                resolve: {
                    taskId: function () {
                        return taskId
                    },
                    progressPercentage: function () {
                        return progressPercentage
                    }
                }
            });

            logProgressModal.result.then(function () {
                refreshTasks();
            });
        };
        /* end log progress */


        /* cancel task */
        $scope.cancelWorkingOnTask = function cancelWorkingOnTask(taskId) {
            TaskActionsService.cancelTask({ id: taskId },
                function (successResult) {
                    $dialogs.notify("Success", successResult.message);
                    refreshTasks();
                    $scope.$root.$broadcast('STOPPED_WORKING_ON_TASK');
                },
                function (errorResult) {
                    $dialogs.error("Error", errorResult.data.error);
                });
        };
        /* end cancel task */

    }]);
