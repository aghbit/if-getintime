/**
 * Created by Grzesiek on 20.03.14.
 */
"use strict";

controllers.controller('TaskStopController', ['$scope', '$modalInstance', '$dialogs', '$location', 'TaskActionsService', 'TaskListingService',  'task', 'progressPercentage',
    function ($scope, $modalInstance, $dialogs, $location, TaskActionsService, TaskListingService, task, progressPercentage) {

        $scope.task = {
            id: task.id
        };

        $scope.calculateDefaultPercentage = function() {
            var percentage = Math.floor((100*task.workingTime.allSeconds)/(60*task.taskProperties.estimatedTime));
            return percentage < 100 ? percentage : 100;
        };

        $scope.percentageWasChangedByTheUser = function() {
            return  Math.abs(task.percentage - progressPercentage) > 100/task.taskProperties.estimatedTime;
        };


        $scope.generateDefaultPercentage = function() {
            if(task.taskProperties.estimatedTime == undefined ||
                $scope.percentageWasChangedByTheUser()) {
                return progressPercentage;
            }
            return $scope.calculateDefaultPercentage();
        };

        $scope.percentToLog = $scope.generateDefaultPercentage();


        $scope.stopWorking = function (taskId, percentToLog) {
            TaskActionsService.stop({
                    id: task.id
                },
                function (successResult) {
                    $scope.$root.$broadcast('STOPPED_WORKING_ON_TASK');
                    $scope.logProgressPercentage(taskId, percentToLog);
                },
                function (errorResult) {
                    $dialogs.error("Error occurred on stop working!", errorResult.data.error);
                    $scope.cancel();
                });
        };

        $scope.logProgressPercentage = function (taskId, percentToLog) {
            TaskActionsService.logPercent({id: task.id, percent: percentToLog}, function (successResult) {
                $scope.ok();
            }, function (errorResult) {
                $dialogs.error("Error occurred!", errorResult.data.error);
                $scope.cancel();
            });
        };

        $scope.canStopWorking = function (taskId, percentToLog) {
            return !!(percentToLog > 0 && percentToLog <= 100);

        };


        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            $modalInstance.close();
        }

    }]);