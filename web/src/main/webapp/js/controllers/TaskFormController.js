'use strict';

controllers.controller('TaskFormCtrl', ['$scope', '$timeout', function ($scope, $timeout) {

    $scope.task = {};

    $scope.task.ownerDeadline = new Date();
    $scope.task.assigneeDeadline = new Date();

    // stuff connected with calendar and deadlines starts here
    $scope.todayOwnerDeadline = function () {
        $scope.task.ownerDeadline = new Date();
    };

    $scope.todayAssigneeDeadline = function () {
        $scope.task.assigneeDeadline = new Date();
    };

    $scope.showWeeks = true;
    $scope.toggleWeeks = function () {
        $scope.showWeeks = !$scope.showWeeks;
    };

    $scope.clearOwnerDeadline = function () {
        $scope.task.ownerDeadline = null;
    };

    $scope.clearAssigneeDeadline = function () {
        $scope.task.assigneeDeadline = null;
    }

    $scope.disabled = function (date, mode) {
        if ($scope.daysOff === undefined) {
            return false;
        } else {
            return (mode === 'day' && containsDate($scope.daysOff, date));
        }
    };

    $scope.open = function () {
        $timeout(function () {
            $scope.opened = true;
        });
    };

    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];

    $scope.format = $scope.formats[0];
    //calendar end

    $scope.isCollapsed = true;

    $scope.assigneeDeadlineIsAfterOwnerDeadline = function () {
        return $scope.task.assigneeDeadline > $scope.task.ownerDeadline;
    }
}]);