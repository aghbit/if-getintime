/**
 * Created by Anita on 07.04.14.
 */
'use strict';

controllers.controller('MockedLoginController',
    ['$scope', '$dialogs', 'EmployeesService', 'AuthenticationMockService',
        function ($scope, $dialogs, EmployeesService, AuthenticationMockService) {

            $scope.currentUser = AuthenticationMockService.currentUser();
            EmployeesService.query({}, function (success) {
                $scope.employees = success;
            }, function (fail) {
                $dialogs.error("Error occurred!", fail.data);
            });

            $scope.setCurrentUser = function (employeeId) {
                AuthenticationMockService.setCurrentUser({id: employeeId}, function (success) {
                    $scope.currentUser = AuthenticationMockService.currentUser();
                }, function (error) {
                    $dialogs.error("Error occurred!", fail.data);
                });
            };
        }]);