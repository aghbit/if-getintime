'use strict';

controllers.controller('AdminPanelController', ['$scope', function($scope) {

    $scope.isOpen = {
        tasks: true,
        employees: true
    };
}]);
