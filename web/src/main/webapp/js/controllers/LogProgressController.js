/**
 * Created by Jacek Żyła on 17.03.14.
 */
"use strict";

controllers.controller('LogProgressController', ['$scope', '$modalInstance', '$dialogs', '$location',
    'TaskActionsService', 'TaskListingService', 'taskId', 'progressPercentage',
    function ($scope, $modalInstance, $dialogs, $location,
              TaskActionsService, TaskListingService, taskId, progressPercentage) {

        $scope.task = {
            id: taskId
        };

        $scope.progress = {
            timeInMinutesToLog: 0,
            percentToLog: progressPercentage
        };

        $scope.settings = {
            hourStep: 1,
            minuteStep: 5,
            allowZero: false
        };

        $scope.logProgress = function (taskId, timeInMinutesToLog, percentToLog) {
            TaskActionsService.logPercentAndTime({id: taskId, percent: percentToLog, timeInMinutes: timeInMinutesToLog}, function (successResult) {
                $scope.$root.$broadcast('STOPPED_WORKING_ON_TASK');
                $scope.ok();
            }, function (errorResult) {
                $dialogs.error("Error occurred!", errorResult.data.error);
                $scope.cancel();
            });
        };

        $scope.logPercent = function (taskId, percentToLog) {
            TaskActionsService.logPercent({
                    id: taskId,
                    percent: percentToLog
                },
                function (successResult) {
                    $scope.ok();
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                    $scope.cancel();
                });
        };

        $scope.canLogProgress = function (taskId, timeInMinutesToLog, percentToLog) {
            return timeInMinutesToLog > 0 && percentToLog > 0 && percentToLog <= 100;
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            $modalInstance.close();
        }
    }]);