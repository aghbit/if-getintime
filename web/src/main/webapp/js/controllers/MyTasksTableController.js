'use strict';

controllers.controller('MyTasksTableController', ['$scope', '$dialogs', 'TaskListingService', 'EmployeesService', 'TasksDateFilterRangeService',
    function ($scope, $dialogs, TaskListingService, EmployeesService, TasksDateFilterRangeService) {

        $scope.$on('FILTERS_LOADED_EVENT', function (event, dateRange) {
            TasksDateFilterRangeService.setDateRange(dateRange);
            updateTasks();
        });

        $scope.$on('VIEW_TOGGLE_EVENT', function(event, dateRange) {
            TasksDateFilterRangeService.setDateRange(dateRange);
            updateTasks();
        });

        $scope.$on('UPDATE_TASKS_LIST_EVENT', function (event, dateRange) {
            TasksDateFilterRangeService.setDateRange(dateRange);
            updateTasks();
        });

        var updateTasks = function () {
            var dateFrom = TasksDateFilterRangeService.getDateRange().from;
            var dateTo = TasksDateFilterRangeService.getDateRange().to;

            TaskListingService.getTasks({from: dateToYMD(dateFrom), to: dateToYMD(dateTo)},
                function (successResult) {
                    $scope.data.tasks = successResult;
                    $scope.alreadyWorkingOnTask = $scope.isAlreadyWorkingOnTask();
                    $scope.$root.$broadcast('LOADED_TASKS');
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                }
            );
        };
    }]);
