'use strict';

controllers.controller('NewStandardTaskController', [
    '$scope', '$location', '$modalInstance', '$dialogs',
    'TaskService', 'EmployeesService', 'AuthenticationService', 'TaskPropertiesService', 'DayOffService', 'USER_ROLES',
    function ($scope, $location, $modalInstance, $dialogs, TaskService, EmployeesService, AuthenticationService, TaskPropertiesService, DayOffService, USER_ROLES) {


        function Task() {
            this.loggedTime = 0;
            this.progressPercentage = {percentageValue: 0};
            this.status = "TODO";
            this.taskDescription = {
                name: "",
                description: ""
            };
            this.taskProperties = {
                priority: undefined,
                estimatedTime: undefined,
                assigned: {},
                createdBy: {},
                parentTaskDescription: { id : null, name : null }
            };
            this.assigneeDeadline = soonestWorkingDay(dateNow,dateNow);
            this.ownerDeadline = soonestWorkingDay(dateNow,dateNow);
            this.type = "STANDARD";
            this.percentage = 0;
        }

        $scope.serverData = {
            possibleAssignees: [],
            possiblePriorities: []
        };

        $scope.helpers = {
            hourStep: 1,
            minuteStep: 10,
            withEstimatedTime: false
        };

        var currentUser = AuthenticationService.currentUser();

        var dateNow = new Date();
        var dateInAYear = new Date().addYears(1);

        var changedOwnerDeadline = false;
        var changedAssigneeDeadline = false;

        var prepareNewStandardTask = function () {

            var result = new Task();

            DayOffService.getDaysOff(dateNow, dateInAYear).then(function (daysOff) {
                result.assigneeDeadline = result.ownerDeadline = $scope.assigneeMinimumDeadline = soonestWorkingDay(dateNow, daysOff);
                $scope.ownerMinimumDeadline = result.assigneeDeadline;
                $scope.ownerMaximumDeadline = earlierWorkingDay(dateInAYear, daysOff);
                $scope.assigneeMaximumDeadline = result.ownerDeadline;
                $scope.serverData.daysOff = daysOff;
            });

            EmployeesService.subordinates({id: currentUser.id}, function (successResult) {
                $scope.serverData.possibleAssignees = successResult;
                $scope.serverData.possibleAssignees.unshift(currentUser);
                result.taskProperties.assigned = $scope.serverData.possibleAssignees[0];
            }, function (failResult) {
                $dialogs.error('Error occurred', failResult.data.error)
            });

            TaskPropertiesService.getPossiblePriorities({}, function(successResult) {
                $scope.serverData.possiblePriorities = successResult;
                result.taskProperties.priority = $scope.serverData.possiblePriorities[0];
            }, function(failResult) {
                $dialogs.error('Error occurred', failResult.data.error)
            });

            return result;
        };

        $scope.task = prepareNewStandardTask();

        $scope.$watch(function () {
                return $scope.task.assigneeDeadline
            }, function (newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.ownerMinimumDeadline = $scope.task.assigneeDeadline;
                changedAssigneeDeadline = true;
                if ($scope.task.assigneeDeadline && $scope.task.assigneeDeadline > $scope.task.ownerDeadline) {

                    $scope.task.ownerDeadline = $scope.task.assigneeDeadline;
                }
            }
        });

        $scope.$watch(function(){
            return $scope.task.ownerDeadline
        }, function(newValue, oldValue) {
            if (newValue != oldValue) {
                changedOwnerDeadline = true;
            }
        });


        $scope.createNewStandardTask = function () {

            /* create deep copy of task - in order to prevents errors */
            var task = jQuery.extend(true, {}, $scope.task);

            if (currentUser.type === USER_ROLES.normal) {
                task.ownerDeadline = {date: dateToYMD(task.assigneeDeadline)};
            } else {
                task.ownerDeadline = {date: dateToYMD(task.ownerDeadline)};
            }

            task.assigneeDeadline = {date: dateToYMD(task.assigneeDeadline)};

            var currDate = dateToYMD(new Date());
            if(((task.ownerDeadline.date != currDate ) && !changedOwnerDeadline) || ((task.assigneeDeadline.date != currDate) && !changedAssigneeDeadline)) {
                $dialogs.notify("Warning!", "Proszę zmienić domyślną datę (domyślna data wskazuje na dzień wolny od pracy) !");
                return;
            }

            task.taskProperties.createdBy = currentUser;

            if (!$scope.helpers.withEstimatedTime) {
                task.taskProperties.estimatedTime = undefined;
            }

            TaskService.create(task, function (successResult) {
                $scope.ok();
                $dialogs.notify('Success', successResult.message);
            }, function (errorResult) {
                $scope.ok();
                $dialogs.error("Problems occurred creating a task!", errorResult.data.error);
            });
        };

        $scope.setAssignee = function (val) {
            $scope.task.taskProperties.assigned = val;
        };

        $scope.setPriority = function (val) {
            $scope.task.taskProperties.priority = val;
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            $modalInstance.close();
        };
    }
]);
