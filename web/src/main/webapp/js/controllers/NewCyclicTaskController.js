'use strict';

controllers.controller('NewCyclicTaskController',
    ['$scope', '$modal', '$dialogs', '$state', 'EmployeesService', 'TaskPropertiesService', 'AuthenticationService', 'CyclicTaskService', 'DayOffService',
        function ($scope, $modal, $dialogs, $state, EmployeesService, TaskPropertiesService, AuthenticationService, CyclicTaskService, DayOffService) {

            var workingDays = [
                {name: 'Monday', is: false},
                {name: 'Tuesday', is: false},
                {name: 'Wednesday', is: false},
                {name: 'Thursday', is: false},
                {name: 'Friday', is: false}
            ];

            var allMonthDays = function () {
                var result = [];
                for (var i = 1; i <= 31; i++) {
                    result.push({which: i, isPicked: false});
                }

                return result;
            };

            $scope.helpers = {
                isWeeklyOpen: false,
                hourStep: 1,
                minuteStep: 10,
                allowZero: false
            };

            function CyclicTask() {
                this.name = "";
                this.description = "";
                this.createdBy = {};
                this.assigned = {};
                this.priority = [];
                this.estimatedTime = 0;
                this.from = null;
                this.to = null;
                this.weekly = workingDays;
                this.monthly = allMonthDays();
            }

            var currentUser = AuthenticationService.currentUser();

            $scope.serverData = {
                priorities: null,
                minDate: null,
                maxDate: null
            };


            var setUpNewCyclicTask = function() {
                var result = new CyclicTask();

                var dateNow = new Date();
                var dateInAYear = new Date().addYears(1);

                DayOffService.getDaysOff(dateNow, dateInAYear).then(function(daysOff) {
                    result.from = $scope.serverData.minDate = soonestWorkingDay(dateNow, daysOff);
                    result.to = $scope.serverData.maxDate = earlierWorkingDay(dateInAYear, daysOff);

                    $scope.serverData.daysOff = daysOff;
                });

                EmployeesService.subordinates({id: currentUser.id}, function (successResult) {
                    $scope.serverData.possibleAssignees = successResult;
                    $scope.serverData.possibleAssignees.push(currentUser);

                    result.assigned = $scope.serverData.possibleAssignees[0];

                    TaskPropertiesService.getPossiblePriorities({}, function(successResult) {
                        $scope.serverData.priorities = successResult;
                        result.priority = $scope.serverData.priorities[0];
                    });
                }, function (failResult) {
                    $dialogs.error('Error occurred', failResult.data.error)
                });

                /* close weekly days picker */
                $scope.helpers.weeklyOpen = false;

                return result;
            };

            $scope.input = setUpNewCyclicTask();

            $scope.setAssignee = function (newAssignee) {
                $scope.input.assigned = newAssignee;
            };

            $scope.setPriority = function (newPriority) {
                $scope.input.priority = newPriority;
            };

            $scope.toggleIsPresent = function (index) {
                $scope.input.monthly[index].isPicked = !$scope.input.monthly[index].isPicked;
            };

            var extractPickedDaysOfWeek = function (weekDays) {
                var daysOfWeek = [];
                if(!$scope.helpers.isWeeklyOpen) {
                    return [];
                }
                for (var i = 0; i < weekDays.length; i++) {
                    if (weekDays[i].is === true) {
                        daysOfWeek.push(weekDays[i].name.toUpperCase());
                    }
                }

                return daysOfWeek;
            };

            var extractPickedDaysOfMonth = function (monthDays) {
                var daysOfMonth = [];
                if($scope.helpers.isWeeklyOpen) {
                    return [];
                }
                for (var i = 0; i < monthDays.length; i++) {
                    if (monthDays[i].isPicked === true) {
                        daysOfMonth.push(monthDays[i].which)
                    }
                }
                return daysOfMonth;
            };

            // Converts array of moved terms from server response format: '"to" date string: ["from" terms]'
            // to usable format: '{to: date, from: [converted "from" terms]}'.
            // "from" terms are converted using fromParser.
            // The resulting array is sorted by "to" dates and the "from" arrays are sorted using the given comparator.
            function extractMovedTerms(serverMovedTerms, fromParser, fromComparator) {
                var movedTerms = [];
                angular.forEach(serverMovedTerms, function (serverFroms, serverToDate) {
                    var froms = [];
                    angular.forEach(serverFroms, function (serverFrom) {
                        froms.push(fromParser(serverFrom));
                    });
                    froms.sort(fromComparator);
                    movedTerms.push({
                        to: parseDate(serverToDate),
                        from: froms
                    });
                });
                movedTerms.sort(function compare(term1, term2) {
                    return term1.to.getTime() - term2.to.getTime();
                });
                return movedTerms;
            };

            $scope.createNewCyclicTask = function () {

                var cyclicTaskCreationRequest = {
                    templateTask: {
                        taskDescription: { name: $scope.input.name, description: $scope.input.description },
                        taskProperties: {
                            createdBy: currentUser,
                            estimatedTime: $scope.input.estimatedTime,
                            assigned: $scope.input.assigned,
                            priority: $scope.input.priority
                        },
                        daysOfMonth: extractPickedDaysOfMonth($scope.input.monthly),
                        daysOfWeek: extractPickedDaysOfWeek($scope.input.weekly)
                    },
                    from: dateToYMD($scope.input.from),
                    to: dateToYMD($scope.input.to)
                };
                CyclicTaskService.save(cyclicTaskCreationRequest, function (cyclicTaskCreationResponse) {
                    var dayOffMovedTerms = extractMovedTerms(cyclicTaskCreationResponse.dayOffMovedTerms,
                        function (term) {
                            return parseDate(term.date);
                        },
                        function (date1, date2) {
                            return date1.getTime() - date2.getTime();
                        }
                    );
                    var endOfMonthMovedTerms = extractMovedTerms(cyclicTaskCreationResponse.endOfMonthMovedTerms,
                        function (dayOfMonth) {
                            return dayOfMonth;
                        },
                        function (day1, day2) {
                            return day1 - day2;
                        }
                    );
                    if (dayOffMovedTerms.length != 0 || endOfMonthMovedTerms != 0) {
                        $modal.open({
                            templateUrl: '/partials/views/moved_terms_modal.html',
                            controller: 'MovedTermsModalController',
                            backdrop: 'static',
                            resolve: {
                                dayOffMovedTerms: function() {
                                    return dayOffMovedTerms;
                                },
                                endOfMonthMovedTerms: function() {
                                    return endOfMonthMovedTerms;
                                }
                            }
                        });
                    }
                    $state.transitionTo('admin-panel.tasks_cyclic_list');
                }, function (failResult) {
                    $dialogs.error('Wystąpił błąd', failResult.data.error)
                });

                /* clean input form */
                $scope.input = setUpNewCyclicTask();
                $scope.newCyclicTaskForm.$setPristine(true);
            };
        }]);