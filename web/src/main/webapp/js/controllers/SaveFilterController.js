"use strict";

controllers.controller('SaveFilterController', ['$scope', '$dialogs', '$modalInstance', 'FiltersService', 'filter', 'userId',
    function($scope, $dialogs, $modalInstance, FiltersService, filter, userId) {

        $scope.filterName = '';
        $scope.filter = filter;
        $scope.userId = userId;

        $scope.saveCurrentFilterWithName = function(filterName) {
            $scope.filter.id = null;
            $scope.filter.name = filterName;
            $scope.filter.date.from = dateToYMD($scope.filter.date.from);
            $scope.filter.date.to = dateToYMD($scope.filter.date.to);
            FiltersService.save({id: $scope.userId}, $scope.filter,
                function(successResult) {
                    $scope.ok();
                },
                function(errorResult) {
                    $dialogs.error("Couldn't save filter", errorResult.data)
                }
            );
        };

        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        }
    }]);