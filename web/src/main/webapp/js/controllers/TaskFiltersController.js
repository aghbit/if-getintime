controllers.controller('TaskFiltersController', ['$scope', '$modal', '$dialogs',
    'FiltersPredicateService', 'TaskPropertiesService', 'FiltersService', 'AuthenticationService', '$q', 'ListingTasksStateService', '$state',
    function ($scope, $modal, $dialogs, FiltersPredicateService, TaskPropertiesService, FiltersService, AuthenticationService, $q, ListingTasksStateService, $state, $rootScope) {

        ListingTasksStateService.updateCurrentViewName($state.current.name);
        $scope.myTasksView = ListingTasksStateService.myTasksView;

        $scope.$on('event:GetInTimeLogoClicked', function(){
            $scope.myTasksView = true;
        });

        $scope.toggleView = function () {
            $scope.myTasksView = !ListingTasksStateService.myTasksView;

            $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                if (ListingTasksStateService.hasViewChanged(toState.name)) {
                    ListingTasksStateService.updateCurrentViewName(toState.name);
                    $scope.$broadcast('VIEW_TOGGLE_EVENT', {
                        from: $scope.predicatesParent.taskPredicate.date.from,
                        to: $scope.predicatesParent.taskPredicate.date.to
                    });
                }
            })
        };
        $scope.dateToYMD = dateToYMD;

        var showFilterLoadingError = function (errorResult) {
            return $dialogs.error("Error occurred during filters loading", errorResult.data.error);
        };

        var priorities = TaskPropertiesService.getPossiblePriorities({}, function (successResult) {
            /* inMultipleDropdown directive consumes strings only */
            $scope.priorities = successResult.map(String);
        }, function (errorResult) {
            showFilterLoadingError(errorResult);
        });

        var types = TaskPropertiesService.getPossibleTypes({}, function (successResult) {
            $scope.types = successResult;
        }, function (errorResult) {
            showFilterLoadingError(errorResult);
        });

        var statues = TaskPropertiesService.getPossibleStatuses({}, function (successResult) {
            $scope.statuses = successResult;
        }, function (errorResult) {
            showFilterLoadingError(errorResult);
        });

        var afterCreationPropertiesLoaded = function () {
            $scope.loadFilters();
        };

        $q.all([types.$promise, priorities.$promise, statues.$promise])
            .then(afterCreationPropertiesLoaded);

        var predefinedFilters = function (priorities, types, statuses) {
            var statusesWithoutCancelledOn = angular.copy(statuses);
            statusesWithoutCancelledOn.splice(statusesWithoutCancelledOn.indexOf('CANCELLED'), 1)
            var todayFilter = {
                name: 'Dzisiaj',
                taskDescription: {
                    name: '',
                    description: ''
                },
                priorities: angular.copy(priorities),
                types: angular.copy(types),
                statuses: angular.copy(statusesWithoutCancelledOn),
                date: {
                    from: dateToYMD(Date.today()),
                    to: dateToYMD(Date.today())
                }
            };

            var tomorrowFilter = angular.copy(todayFilter);
            tomorrowFilter.name = 'Jutro';
            tomorrowFilter.date.from = dateToYMD(Date.today().addDays(1));
            tomorrowFilter.date.to = tomorrowFilter.date.from;

            var thisWeekFilter = angular.copy(todayFilter);
            thisWeekFilter.name = 'Najbliższe 7 dni';
            thisWeekFilter.date.to = dateToYMD(Date.today().addDays(7));

            return [todayFilter, tomorrowFilter, thisWeekFilter];
        };

        /* sidebar filters */
        $scope.resetFilters = function () {
            $scope.setFilter($scope.filters[0]);
        };

        $scope.loadFilters = function () {

            FiltersService.get({id: $scope.currentEmployee.id},
                function (successResult) {
                    angular.forEach(successResult, function (filter) {
                        filter.priorities = filter.priorities.map(String);
                    });
                    $scope.filters = predefinedFilters($scope.priorities, $scope.types, $scope.statuses).concat(successResult);
                    if($scope.predicatesParent.taskPredicate == null) {
                        $scope.setFilter($scope.filters[0]);
                    }
                    startWatchingDatePickers();

                    $scope.$broadcast('FILTERS_LOADED_EVENT', {
                        from: $scope.predicatesParent.taskPredicate.date.from,
                        to: $scope.predicatesParent.taskPredicate.date.to
                    });
                },
                function (errorResult) {
                    showFilterLoadingError(errorResult);
                }
            );
        }

        $scope.currentUser = AuthenticationService.currentUser();

        /* main-bar filters */
        $scope.predicatesParent = FiltersPredicateService.predicatesParent;

        $scope.deleteFilter = function (e, filter) {
            e.stopImmediatePropagation();
            if (!$scope.isPredefined(filter)) {
                FiltersService.delete({id: $scope.currentEmployee.id, filterId: filter.id}, filter,
                    function (successResult) {
                        $scope.loadFilters();
                    },
                    function (errorResult) {
                        showFilterLoadingError(errorResult);
                    }
                );
            }
        };


        $scope.openSaveFilterModal = function (filter) {
            var saveFilterModal = $modal.open({
                templateUrl: '/partials/views/save_filter.html',
                controller: 'SaveFilterController',
                resolve: {
                    filter: function () {
                        return filter;
                    },
                    userId: function () {
                        return $scope.currentEmployee.id;
                    }
                }
            });

            saveFilterModal.result.then(function () {
                $scope.loadFilters();
            });
        };

        $scope.isPredefined = function (filter) {
            return typeof filter.id === "undefined";
        };

        $scope.setFilter = function (filter) {
            $scope.predicatesParent.taskPredicate = angular.copy(filter);
        };

        function startWatchingDatePickers() {
            $scope.$watch(function () {
                return $scope.predicatesParent.taskPredicate.date;
            }, function (newDate, oldDate) {
                // dates come from backend as strings
                var dateRange = {
                    from: parseDate(newDate.from),
                    to: parseDate(newDate.to)
                };
                if (dateRange.from > dateRange.to && !datesEqualIgnoringTime(dateRange.from, parseDate(oldDate.from))) {
                    newDate.to = newDate.from;
                } else if (dateRange.to < dateRange.from && !datesEqualIgnoringTime(dateRange.to, parseDate(oldDate.to))) {
                    newDate.from = newDate.to;
                } else if (dateRange.from && dateRange.to) {
                    $scope.$broadcast('UPDATE_TASKS_LIST_EVENT', {from: newDate.from, to: newDate.to });
                }
            }, true);
        }
    }]);