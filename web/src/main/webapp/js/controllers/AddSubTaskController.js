'use strict';

controllers.controller('AddSubTaskController', [
    '$scope', '$location', '$modalInstance', '$dialogs', 'TaskActionsService', 'EmployeesService', 'AuthenticationService',
    'TaskPropertiesService', 'DayOffService', 'parentTask', 'SubTaskService',
    function ($scope, $location, $modalInstance, $dialogs, TaskActionsService, EmployeesService, AuthenticationService,
              TaskPropertiesService, DayOffService, parentTask, SubTaskService) {

        var currentUser = AuthenticationService.currentUser();

        function Task() {
            this.loggedTime = 0;
            this.progressPercentage = {percentageValue: 0};
            this.status = "TODO";
            this.taskDescription = {
                name: "",
                description: ""
            };
            this.taskProperties = {
                priority: undefined,
                estimatedTime: undefined,
                assigned: {},
                createdBy: {},
                parentTaskDescription: { id : null, name : null }
            };
            this.assigneeDeadline = {date: null};
            this.ownerDeadline = {date: null};
            this.type = "STANDARD";
        }

        $scope.serverData = {
            possibleAssignees: [],
            possiblePriorities: []
        };

        $scope.helpers = {
            hourStep: 1,
            minuteStep: 10,
            allowZero: false
        };

        $scope.task = new Task();

        TaskPropertiesService.getPossiblePriorities({},
            function (successResult) {
                $scope.serverData.possiblePriorities = successResult;
                $scope.task.taskProperties.priority = $scope.serverData.possiblePriorities[0];
            },
            function (errorResult) {
                $dialogs.error("Couldn't get possible priorities from server", errorResult.data.error);
            });

        $scope.task.taskProperties.createdBy = parentTask.taskProperties.createdBy;
        $scope.task.taskProperties.assigned = parentTask.taskProperties.assigned;

        EmployeesService.subordinates({id: AuthenticationService.currentUser().id }, function (successResult) {
                $scope.serverData.possibleAssignees = successResult;
                successResult.unshift(AuthenticationService.currentUser());
                $scope.task.assigned = $scope.serverData.possibleAssignees[0];
            },
            function (errorResult) {
                $dialogs.error("Problem occurred during fetching possible employees", errorResult.data.error);
            }
        );

        $scope.parentTaskAssigneeDeadlineDate = parseDate(parentTask.assigneeDeadline.date);
        $scope.parentTaskOwnerDeadlineDate = parseDate(parentTask.ownerDeadline.date);

        $scope.ownerMaximumDeadline = $scope.parentTaskOwnerDeadlineDate;

        var dateNow = new Date();

        DayOffService.getDaysOff(dateNow, $scope.ownerMaximumDeadline).then(function(daysOff) {
            $scope.serverData.daysOff = daysOff;
            $scope.task.assigneeDeadline = { date: $scope.parentTaskAssigneeDeadlineDate };
            $scope.task.ownerDeadline = { date: $scope.parentTaskOwnerDeadlineDate };

            $scope.assigneeMinimumDeadline = soonestWorkingDay(dateNow, daysOff);
            $scope.assigneeMaximumDeadline = $scope.parentTaskAssigneeDeadlineDate;
            $scope.ownerMinimumDeadline = $scope.task.assigneeDeadline.date;
        });

        $scope.data = {
            parentTask: parentTask
        };

        $scope.setAssignee = function (assignee) {
            $scope.task.taskProperties.assigned = assignee;
        };

        $scope.setPriority = function (priority) {
            $scope.task.taskProperties.priority = priority;
        };

        $scope.addSubtask = function () {
            var subtask = $scope.task;
            subtask.assigneeDeadline = {date: dateToYMD(subtask.assigneeDeadline.date)};
            subtask.ownerDeadline = {date: dateToYMD(subtask.ownerDeadline.date)};
            subtask.taskProperties.createdBy = currentUser;
            TaskActionsService.addSubtask({id: parentTask.id}, subtask, function (successResult) {
                $dialogs.notify('Success', successResult.message);
                $scope.ok();
                SubTaskService.updateNumberOfSubtasks();
            }, function (errorResult) {
                $dialogs.error('Error occurred', errorResult.data.error);
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.ok = function () {
            $modalInstance.close();
        };

    }
]);
