'use strict';

controllers.controller('LoginCtrl', ['$scope', '$rootScope', '$modalInstance', '$state', 'AuthenticationService', '$dialogs', 'authService', 'nextStateName',
    function ($scope, $rootScope, $modalInstance,  $state, AuthenticationService, $dialogs, authService, nextStateName) {

    $scope.credentials = {
        username: '',
        password: ''
    };

    $scope.login = function(){
        AuthenticationService.login($scope.credentials, function(loggedIn){
            if(loggedIn){
                authService.loginConfirmed(AuthenticationService.currentUser());
                $modalInstance.close();
                $state.transitionTo(nextStateName,{},{
                    reload: true,
                    notify: true
                });
            }else{
                $dialogs.error("Podany login lub hasło są niepoprawne.", "Spróbuj ponownie.");
            }

        });

    }
}]);