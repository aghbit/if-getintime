'use strict';

controllers.controller('EditTaskController',
    ['$scope', '$dialogs', '$modalInstance', 'EmployeesService', 'TaskPropertiesService',
        'AuthenticationService', 'TaskService', 'DayOffService', 'taskId',
        function ($scope, $dialogs, $modalInstance, EmployeesService, TaskPropertiesService,
                  AuthenticationService, TaskService, DayOffService, taskId) {

            $scope.serverData = {
                possibleAssignees: [],
                possiblePriorities: []
            };

            var currentUser = AuthenticationService.currentUser(function (successResult) {
            }, function (errorResult) {
                $dialogs.error("Wystąpił błąd podczas pobierania danych o użytkowniku", errorResult.data.error);
            });

            $scope.editAssignee = function (newAssignee) {
                $scope.task.taskProperties.assigned = newAssignee;
            };

            $scope.helpers = {
                hourStep: 1,
                minuteStep: 10,
                withEstimatedTime: false
            };

            $scope.task = TaskService.getTask({id: taskId}, function (successResult) {
                    $scope.helpers.withEstimatedTime = successResult.taskProperties.estimatedTime != null;

                    var assigneeMinimumDeadline;
                    var ownerMinimumDeadline;
                    var assigneeDisabledDaysArrays = [];
                    var ownerDisabledDaysArrays = [];

                    function isDeadlineOverdue(deadline){
                        return parseDate(deadline) < todayWithoutHours();
                    }

                    if ($scope.task.overdue) {
                        assigneeMinimumDeadline = parseDate($scope.task.assigneeDeadline.date);
                        assigneeDisabledDaysArrays.push(generateDaysArrayExclusive(assigneeMinimumDeadline, todayWithoutHours()));
                        if(isDeadlineOverdue($scope.task.ownerDeadline.date)){
                            ownerMinimumDeadline = parseDate($scope.task.ownerDeadline.date);
                            ownerDisabledDaysArrays.push(generateDaysArrayExclusive(ownerMinimumDeadline, todayWithoutHours()));
                        } else {
                            ownerMinimumDeadline = todayWithoutHours();
                        }
                    } else {
                        ownerMinimumDeadline = parseDate($scope.task.assigneeDeadline.date);
                        assigneeMinimumDeadline = todayWithoutHours();
                    }

                    var ownerMaximumDeadline = parseDate($scope.task.ownerDeadline.date).addMonths(6);

                    EmployeesService.subordinates({id: currentUser.id}, function (successResult) {
                        $scope.serverData.possibleAssignees = successResult;
                        $scope.serverData.possibleAssignees.unshift(currentUser);
                    }, function (failResult) {
                        $dialogs.error('Error occurred', failResult.data.error)
                    });

                    TaskPropertiesService.getPossiblePriorities({}, function (successResult) {
                        $scope.serverData.possiblePriorities = successResult;
                    }, function (failResult) {
                        $dialogs.error('Error occurred', failResult.data.error);
                    });

                    DayOffService.getDaysOff(assigneeMinimumDeadline, ownerMaximumDeadline).then(function (daysOff) {
                        $scope.serverData.assigneeMinimumDeadline = soonestWorkingDay(assigneeMinimumDeadline, daysOff);
                        $scope.serverData.ownerMaximumDeadline = earlierWorkingDay(ownerMaximumDeadline, daysOff);
                        $scope.serverData.ownerMinimumDeadline = soonestWorkingDay(ownerMinimumDeadline, daysOff);
                        $scope.serverData.ownerCurrentMinDate = $scope.serverData.ownerMinimumDeadline;
                        assigneeDisabledDaysArrays.push(daysOff);
                        ownerDisabledDaysArrays.push(daysOff);
                        $scope.serverData.assigneeDisabledDays = mergeArrayOfArrays(assigneeDisabledDaysArrays);
                        $scope.serverData.ownerDisabledDays = mergeArrayOfArrays(ownerDisabledDaysArrays);


                        function resolveCurrentOwnerMinDate() {
                            if(parseDate($scope.task.assigneeDeadline.date) > $scope.serverData.ownerMinimumDeadline){
                                $scope.serverData.ownerCurrentMinDate = parseDate($scope.task.assigneeDeadline.date);
                            } else {
                                $scope.serverData.ownerCurrentMinDate = $scope.serverData.ownerMinimumDeadline;
                            }
                        }

                        $scope.$watch('task.assigneeDeadline.date', function(oldValue, newValue){
                            resolveCurrentOwnerMinDate();
                        });
                    });
                },
                function (errorResult) {
                    $dialogs.error("Wystąpił błąd podczas pobierania zadania", errorResult.data.error);
                }
            );

            $scope.setAssignee = function (assignee) {
                $scope.task.taskProperties.assigned = assignee;
            };

            $scope.setPriority = function (priority) {
                $scope.task.taskProperties.priority = priority;
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            function deleteEstimatedTimeIfUnwanted() {
                if (!$scope.helpers.withEstimatedTime) {
                    $scope.task.taskProperties.estimatedTime = null;
                }
            }

            function deleteCyclicTaskIdFromTask() {
                delete $scope.task["cyclicTaskId"];
            }

            $scope.editTask = function () {
                deleteCyclicTaskIdFromTask();
                deleteEstimatedTimeIfUnwanted();

                TaskService.updateTask({id: taskId}, $scope.task, function (successResult) {
                    $modalInstance.close();
                }, function (errorResult) {
                    $modalInstance.close();
                    $dialogs.error("Problems during task update!", errorResult.data.error);
                });
            };
        }]);