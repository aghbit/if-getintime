'use strict';

/*
 Main controller of tasks listing. Is responsible for sorting and managing nested controllers.
 */

controllers.controller('TasksListingController', ['$scope', '$timeout', 'TaskListingService', 'TaskActionsService', '$dialogs',
    '$modal', 'FiltersService', 'AuthenticationService', 'FiltersPredicateService', 'TaskMethodsService',
    function ($scope, $timeout, TaskListingService, TaskActionsService, $dialogs, $modal,
              FiltersService, AuthenticationService, FiltersPredicateService, TaskMethodsService) {

        $scope.$on('FILTERS_LOADED_EVENT', function (event, dateRange) {
            updateTasks(dateRange.from, dateRange.to);
        });

        $scope.data = {};

        $scope.filtersLoaded = false;

        $scope.alreadyWorkingOnTask = false;

        $scope.activeTaskIds = [];
        $scope.currentEmployee = AuthenticationService.currentUser();

        $scope.predicatesParent = FiltersPredicateService.predicatesParent;

        $scope.resetFilters = function () {
            $scope.predicate = '';
            $scope.taskSearch = {};
            $scope.$broadcast('CLEAR_DATE_FILTERS_EVENT');
        };

        $scope.dates = {from: '', to: ''};

        $scope.sortingColumnButtonClass = function (columnName) {
            if ($scope.currentColumnOrderBy.indexOf(columnName) > -1) {
                return 'ascending';
            } else if ($scope.currentColumnOrderBy.indexOf('-' + columnName) > -1) {
                return 'descending';
            } else {
                return 'notSorted';
            }
        };

        var updateTasks = function (dateFrom, dateTo) {
            TaskListingService.getTasks({from: dateToYMD(dateFrom), to: dateToYMD(dateTo)},
                function (successResult) {
                    $scope.tasks = successResult;
                    $scope.alreadyWorkingOnTask = $scope.isAlreadyWorkingOnTask();
                    $scope.$broadcast('LOADED_TASKS');
                },
                function (errorResult) {
                    $dialogs.error("Error occurred!", errorResult.data.error);
                }
            );
        };

        $scope.$on('UPDATE_TASKS_LIST_EVENT', function (event, dateRange) {
            if (!dateRange.from || !dateRange.to) {
                dateRange = $scope.predicatesParent.taskPredicate.date;
            }
            updateTasks(dateRange.from, dateRange.to);
        });

        $scope.shouldAlreadyBeCompleted = function (task) {
            return task.overdue;
        };

        $scope.currentColumnOrderBy = ['assigneeDeadline.date', '-taskProperties.priority'];

        $scope.applyColumnOrderBy = function (evt, columnName) {
            var elementIndex = getIndexOfOrderElement($scope.currentColumnOrderBy, columnName);
            if (evt.shiftKey) {
                applyMultiColumnSorting(elementIndex, columnName);
            } else {
                if (isAscendingColumnSortingApplied(elementIndex, columnName)) {
                    $scope.currentColumnOrderBy.length = 0;
                    $scope.currentColumnOrderBy.push('-' + columnName);
                }
                else {
                    $scope.currentColumnOrderBy.length = 0;
                    $scope.currentColumnOrderBy.push(columnName);
                }
            }
        };

        function applyMultiColumnSorting(elementIndex, columnName) {
            if (isAscendingColumnSortingApplied(elementIndex, columnName)) {
                $scope.currentColumnOrderBy[elementIndex] = '-' + columnName;
            }
            else if (isDescendingColumnSortingApplied(elementIndex, columnName)) {
                $scope.currentColumnOrderBy[elementIndex] = columnName;
            }
            else {
                $scope.currentColumnOrderBy.push(columnName);
            }
        }

        function isAscendingColumnSortingApplied(elementIndex, columnName) {
            return $scope.currentColumnOrderBy[elementIndex] == columnName;
        }

        function isDescendingColumnSortingApplied(elementIndex, columnName) {
            return $scope.currentColumnOrderBy[elementIndex] == '-' + columnName;
        }

        function getIndexOfOrderElement(columnNamesArray, columnName) {
            return columnNamesArray.indexOf(columnName) > -1 ? columnNamesArray.indexOf(columnName) : columnNamesArray.indexOf('-' + columnName)
        }

        $scope.isAlreadyWorkingOnTask = function () {
            var alreadyWorkingOnTask = false;
            angular.forEach($scope.tasks, function (value, key) {
                if (value.status === 'IN_PROGRESS') {
                    alreadyWorkingOnTask = true;
                    return true;
                }
            });
            return alreadyWorkingOnTask;
        };

        $scope.canStartWorkingOnTask = function (task) {
            return task.status != 'CANCELLED' && task.status != "DONE" && !$scope.alreadyWorkingOnTask;
        };

        $scope.canLogTimeToTask = function (task) {
            return task.status != 'DONE' && task.status != 'CANCELLED' && !$scope.alreadyWorkingOnTask && AuthenticationService.isAuthorized(['MANAGER', 'DIRECTOR']);
        };

        $scope.canFinishWorkingOnTask = function (task) {
            return task.status != 'DONE' && task.status != 'CANCELLED' && task.status != 'TODO';
        };

        $scope.canStopWorkingOnTask = function (task) {
            return task.status != 'DONE' && task.status != 'CANCELLED' && task.status != 'TODO' && task.status != 'STOPPED';
        };

        $scope.canCancelTask = function (task) {
            return task.status != 'CANCELLED' && task.status != 'DONE';
        };

        $scope.canAddSubtaskToTask = function (task) {
            return task.status != 'DONE' && task.status != 'CANCELLED';
        };

        $scope.isDone = function (task) {
            return task.status == 'DONE';
        };

        $scope.areCompletedTasksVisible = function () {
            if ($scope.predicatesParent.taskPredicate != null) {
                return $scope.predicatesParent.taskPredicate.statuses.indexOf('DONE') != -1;
            }
            return false;
        };

        $scope.toggleVisibilityOfCompletedTasks = function () {
            var statuses = angular.copy($scope.predicatesParent.taskPredicate.statuses);
            var index = statuses.indexOf('DONE');
            if (index > -1) {
                statuses.splice(index, 1);
            }
            else {
                statuses.push('DONE');
            }
            $scope.predicatesParent.taskPredicate.statuses = statuses;
        };

        $scope.canEditTask = function (task) {
            return AuthenticationService.isAuthorized(['MANAGER', 'DIRECTOR']) || AuthenticationService.isCurrentlyLogged(task.taskProperties.createdBy);
        };

        $scope.isEstimatedTimeExceeded = function (task) {
            return TaskMethodsService.isEstimatedTimeExceeded(task);
        };
    }]);