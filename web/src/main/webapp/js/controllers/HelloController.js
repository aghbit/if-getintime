'use strict';

controllers.controller('HelloCtrl', ['$scope', 'MessageFactory',
    function ($scope, MessageFactory) {

    $scope.messages = {
        controllerMessage:'--hello from controller--'
    }

    MessageFactory.get({}, function (messageFactory) {
        $scope.messages.welcomeMessage = messageFactory.message;
    })
}]);