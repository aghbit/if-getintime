'use strict';

controllers.controller('CyclicTaskController',
    ['$scope', '$dialogs', '$resource', 'CyclicTaskService',
        'EmployeesService', 'CyclicTaskDateFilterRangeService',
    function ($scope, $dialogs, $resource, CyclicTaskService,
              EmployeesService, CyclicTaskDateFilterRangeService) {
        var tasksOnceLoaded = false;

        $scope.dateFilter = CyclicTaskDateFilterRangeService.getDateRange();

        $scope.$watch('dateFilter.from', function(newValue){
            CyclicTaskDateFilterRangeService.setFromDate(newValue);
            if(tasksOnceLoaded){
                loadTasks();
            }
        });

        $scope.$watch('dateFilter.to', function(newValue){
            CyclicTaskDateFilterRangeService.setToDate(newValue);
            if(tasksOnceLoaded){
                loadTasks();
            }
        });


        loadTasks();

        function wrapCheckboxAssignee() {
            for (var i = 0; i < $scope.visibleAssignees.length; i++) {
                $scope.visibleAssignees[i].checked = true;
            }
        }

        EmployeesService.query({},
            function (successResult) {
                $scope.visibleAssignees = successResult;
                wrapCheckboxAssignee();
            },
            function (errorResult) {
                $dialogs.notify("Error", errorResult.data.error);
            }
        );

        function loadTasks() {
            CyclicTaskService.query({from: dateToYMD($scope.dateFilter.from), to: dateToYMD($scope.dateFilter.to)},
                function (successResult) {
                    $scope.cyclicTasks = successResult;
                    tasksOnceLoaded = true;
                },
                function (errorResult) {
                    $dialogs.notify("Error", errorResult.data.error);
                }
            );
        }

        $scope.sortingColumnButtonClass = function (columnName) {
            if ($scope.currentColumnOrderBy.indexOf(columnName) > -1) {
                return 'ascending';
            } else if ($scope.currentColumnOrderBy.indexOf('-' + columnName) > -1) {
                return 'descending';
            } else {
                return 'notSorted';
            }
        };

        $scope.currentColumnOrderBy = ['-templateTask.taskProperties.priority'];

        $scope.applyColumnOrderBy = function (evt, columnName) {
            var elementIndex = getIndexOfOrderElement($scope.currentColumnOrderBy, columnName);
            if (evt.shiftKey) {
                applyMultiColumnSorting(elementIndex, columnName);
            } else {
                if (isAscendingColumnSortingApplied(elementIndex, columnName)) {
                    $scope.currentColumnOrderBy.length = 0;
                    $scope.currentColumnOrderBy.push('-' + columnName);
                }
                else {
                    $scope.currentColumnOrderBy.length = 0;
                    $scope.currentColumnOrderBy.push(columnName);
                }
            }
        };

        function applyMultiColumnSorting(elementIndex, columnName) {
            if (isAscendingColumnSortingApplied(elementIndex, columnName)) {
                $scope.currentColumnOrderBy[elementIndex] = '-' + columnName;
            }
            else if (isDescendingColumnSortingApplied(elementIndex, columnName)) {
                $scope.currentColumnOrderBy[elementIndex] = columnName;
            }
            else {
                $scope.currentColumnOrderBy.push(columnName);
            }
        }

        function isAscendingColumnSortingApplied(elementIndex, columnName) {
            return $scope.currentColumnOrderBy[elementIndex] == columnName;
        }

        function isDescendingColumnSortingApplied(elementIndex, columnName) {
            return $scope.currentColumnOrderBy[elementIndex] == '-' + columnName;
        }

        function getIndexOfOrderElement(columnNamesArray, columnName) {
            return columnNamesArray.indexOf(columnName) > -1 ? columnNamesArray.indexOf(columnName) : columnNamesArray.indexOf('-' + columnName)
        }

        $scope.togglePerson = function (index, event) {
            $scope.visibleAssignees[index].checked = !$scope.visibleAssignees[index].checked;
            event.stopPropagation();
        };

        $scope.setVisibleToAll = function (event) {
            for (var i = 0; i < $scope.visibleAssignees.length; i++) {
                $scope.visibleAssignees[i].checked = true;
            }
            event.stopPropagation();
        };

        $scope.clearVisibleAssignees = function (event) {
            for (var i = 0; i < $scope.visibleAssignees.length; i++) {
                $scope.visibleAssignees[i].checked = false;
            }
            event.stopPropagation();
        };
    }]);