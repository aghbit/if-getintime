'use strict';

controllers.controller('CyclicTaskDetailsController',
    ['$scope', '$dialogs', '$location', '$stateParams', 'CyclicTaskDetailsService',
        'AuthenticationService', 'CyclicTaskDateFilterRangeService', '$modal',
    function ($scope, $dialogs, $location, $stateParams, CyclicTaskDetailsService,
              AuthenticationService, CyclicTaskDateFilterRangeService, $modal) {
        var tasksOnceLoaded = false;

        $scope.dateFilter = CyclicTaskDateFilterRangeService.getDateRange();

        $scope.id = $stateParams.id;

        CyclicTaskDetailsService.getTaskById({ id: $scope.id },
            function (successResult) {
                $scope.cyclicTask = successResult;
                loadTasks();
            },
            function (errorResult) {
                $dialogs.notify("Error", errorResult.data.error);
            }
        );
        function loadTasks() {
            CyclicTaskDetailsService.getTasksByIdWithDateRange({ id: $scope.id, from: dateToYMD($scope.dateFilter.from), to: dateToYMD($scope.dateFilter.to) },
                function (successResult) {
                    $scope.cyclicTask.tasks = successResult;
                    tasksOnceLoaded = true;
                },
                function (errorResult) {
                    $dialogs.notify("Error", errorResult.data.error);
                }
            );
        }

        $scope.$watch('dateFilter.from', function (newValue, oldValue) {
            CyclicTaskDateFilterRangeService.setFromDate(newValue);
            if (tasksOnceLoaded) {
                loadTasks();
            }
        });

        $scope.$watch('dateFilter.to', function (newValue) {
            CyclicTaskDateFilterRangeService.setToDate(newValue);
            if (tasksOnceLoaded) {
                loadTasks();
            }
        });

        $scope.sortingColumnButtonClass = function (columnName) {
            if ($scope.currentColumnOrderBy.indexOf(columnName) > -1) {
                return 'ascending';
            } else if ($scope.currentColumnOrderBy.indexOf('-' + columnName) > -1) {
                return 'descending';
            } else {
                return 'notSorted';
            }
        };

        $scope.currentColumnOrderBy = ['assigneeDeadline.date', '-taskProperties.priority'];

        $scope.applyColumnOrderBy = function (evt, columnName) {
            var elementIndex = getIndexOfOrderElement($scope.currentColumnOrderBy, columnName);
            if (evt.shiftKey) {
                applyMultiColumnSorting(elementIndex, columnName);
            } else {
                if (isAscendingColumnSortingApplied(elementIndex, columnName)) {
                    $scope.currentColumnOrderBy.length = 0;
                    $scope.currentColumnOrderBy.push('-' + columnName);
                }
                else {
                    $scope.currentColumnOrderBy.length = 0;
                    $scope.currentColumnOrderBy.push(columnName);
                }
            }
        };

        function applyMultiColumnSorting(elementIndex, columnName) {
            if (isAscendingColumnSortingApplied(elementIndex, columnName)) {
                $scope.currentColumnOrderBy[elementIndex] = '-' + columnName;
            }
            else if (isDescendingColumnSortingApplied(elementIndex, columnName)) {
                $scope.currentColumnOrderBy[elementIndex] = columnName;
            }
            else {
                $scope.currentColumnOrderBy.push(columnName);
            }
        }

        function isAscendingColumnSortingApplied(elementIndex, columnName) {
            return $scope.currentColumnOrderBy[elementIndex] == columnName;
        }

        function isDescendingColumnSortingApplied(elementIndex, columnName) {
            return $scope.currentColumnOrderBy[elementIndex] == '-' + columnName;
        }

        function getIndexOfOrderElement(columnNamesArray, columnName) {
            return columnNamesArray.indexOf(columnName) > -1 ? columnNamesArray.indexOf(columnName) : columnNamesArray.indexOf('-' + columnName)
        }
    }]);