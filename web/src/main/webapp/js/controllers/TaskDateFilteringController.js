'use strict';

controllers.controller('TaskDateFilteringCtrl', ['$scope', '$timeout', function ($scope, $timeout) {

    $scope.dateFilterRange = {id: 0};

    $scope.openDateFrom = function () {
        $timeout(function () {
            $scope.fromOpened = true;
        });
    };

    $scope.openDateTo = function () {
        $timeout(function () {
            $scope.toOpened = true;
        });
    };

    $scope.clearDateFrom = function () {
        $scope.dateFrom = undefined;
    };

    $scope.clearDateTo = function () {
        $scope.dateTo = undefined;
    };

    $scope.$watch(function () {
        return $scope.dateFrom;
    }, function () {
        $scope.minDate = $scope.dateFrom;
        $scope.dates.from = $scope.dateFrom;
        if ($scope.radioButtonsDateRangeFilterApplied.from === false) {
            $scope.dateFilterRange.id = 0;
        }
        $scope.radioButtonsDateRangeFilterApplied.from = false;
    });

    $scope.$watch(function () {
        return $scope.dateTo;
    }, function () {
        $scope.maxDate = $scope.dateTo;
        $scope.dates.to = $scope.dateTo;
        if ($scope.radioButtonsDateRangeFilterApplied.to === false) {
            $scope.dateFilterRange.id = 0;
        }
        $scope.radioButtonsDateRangeFilterApplied.to = false;
    });

    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };

    $scope.format = 'yyyy-MM-dd';

    $scope.filters = [
        {text: 'Today', id: 1},
        {text: 'This week', id: 2},
        {text: 'This month', id: 3}
    ];

    $scope.radioButtonsDateRangeFilterApplied = {from: false, to: false};


    $scope.applyDateFilter = function (filterId) {
        $scope.radioButtonsDateRangeFilterApplied = {from: true, to: true};

        if (filterId == $scope.dateFilterRange.id) {
            $scope.dateFrom = undefined;
            $scope.dateTo = undefined;
            $scope.dateFilterRange.id = 0;
            return;
        }
        var today = Date.today();
        switch (filterId) {
            case 1:
                $scope.dateTo = today;
                $scope.dateFrom = today;
                break;
            case 2:
                showTasksFromCurrentWeek(today);
                break;
            case 3:
                $scope.dateFrom = Date.today().moveToFirstDayOfMonth();
                $scope.dateTo = Date.today().moveToLastDayOfMonth();
                break;
            default :
                $scope.dateFrom = undefined;
                $scope.dateTo = undefined;
                $scope.dateFilterRange.id = 0;
        }
    };

    function showTasksFromCurrentWeek(today) {
        var fromDate;
        var toDate;
        if (today.is().sunday()) {
            fromDate = Date.today().previous().monday();
            toDate = Date.today();
        } else if (today.is().monday()) {
            fromDate = Date.today();
            toDate = Date.today().next().sunday();
        } else {
            fromDate = Date.today().previous().monday();
            toDate = Date.today().next().sunday();
        }
        $scope.dateFrom = fromDate;
        $scope.dateTo = toDate;
    }

    $scope.$on('CLEAR_DATE_FILTERS_EVENT', function () {
        $scope.dateFrom = undefined;
        $scope.dateTo = undefined;
    });
}]);
