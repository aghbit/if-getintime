"use strict";

function Map() {
    this.tasks={}

    this.setEarliestTaskInstance = function (cyclicTaskId, taskInstance) {
        //we want to display the earliest instance of task
        if(this.tasks[cyclicTaskId] == null || taskInstance.ownerDeadline < this.tasks[cyclicTaskId].ownerDeadline){
            this.tasks[cyclicTaskId] = taskInstance;
        }
    };

    this.getTaskInstances = function () {
        return this.tasks;
    };
}

filters.filter('cyclicTasksGroupingFilter', [function () {
    return function (tasks,isCyclicGrouping) {
        if (isCyclicGrouping) {
            var result = [];
            var map = new Map();
            angular.forEach(tasks, function (task) {
                if (task != undefined) {
                    //standard task has exactly one instance
                    if (task.type == 'STANDARD' || task.overdue) {
                        result.push(task);
                    }else {
                        map.setEarliestTaskInstance(task.cyclicTaskId, task)}
                }
            });
            var cyclic_tasks=map.getTaskInstances();
            for(var key in cyclic_tasks) {
               result.push(cyclic_tasks[key]);
            };
            return result;
        }else return tasks
    }
}]);