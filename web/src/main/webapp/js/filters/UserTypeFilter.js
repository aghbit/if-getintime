filters.filter('userType', ['USER_ROLES', function(USER_ROLES) {
    var patterns = {
        NORMAL: 'Pracownik',
        MANAGER:'Menadżer',
        DIRECTOR:'Dyrektor'
    }


    return function(input) {
        input = patterns[input];
        return input;
    };
}]);