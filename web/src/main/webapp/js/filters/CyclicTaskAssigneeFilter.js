"use strict";

filters.filter('cyclicTaskAssigneeFilter', [function () {
    return function (cyclicTasks, visibleAssignees) {
        var arrayOfAssignees = [];
        angular.forEach(cyclicTasks, function(cyclicTask) {
            angular.forEach(visibleAssignees, function(assignee) {
                if (assignee.checked === true &&
                    cyclicTask.templateTask.taskProperties.assigned.firstName === assignee.firstName &&
                    cyclicTask.templateTask.taskProperties.assigned.secondName === assignee.secondName) {
                        arrayOfAssignees.push(cyclicTask);
                }
            })
        });
        return arrayOfAssignees;
    }
}]);