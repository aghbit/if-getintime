'use strict'

filters.filter('joinBy', function() {
    return function(input, delimeter) {
        delimeter = delimeter || ", ";
        input = input || [];

        return input.join(delimeter);
    };
});