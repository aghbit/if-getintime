"use strict";

filters.filter('standardTaskAssigneeFilter', [function () {
    return function (standardTasks, visibleAssignees) {
        var arrayOfAssignees = [];
        angular.forEach(standardTasks, function(standardTask) {
            angular.forEach(visibleAssignees, function(assignee) {
                if (assignee.checked === true &&
                    standardTask.taskProperties.assigned.firstName === assignee.firstName &&
                    standardTask.taskProperties.assigned.secondName === assignee.secondName) {
                    arrayOfAssignees.push(standardTask);
                }
            })
        });
        return arrayOfAssignees;
    }
}]);