"use strict";

filters.filter('dateFilter', [function () {
    return function (tasks, dates) {
        var result = [];
        var overdueTasks = [];

        angular.forEach(tasks, function (task) {
            var taskDate = new Date(task.ownerDeadline.date);
            taskDate.clearTime();

            if ( task.status != 'DONE' && taskDate < Date.today()) {
                overdueTasks.push(task)
                return;
            }

            if (dates.dateFrom != undefined && dates.dateFrom > taskDate) {
                return;
            } else if (dates.dateTo != undefined && dates.dateTo < taskDate) {
                return;
            }else {
                result.push(task);
            }
        });
        result = overdueTasks.concat(result);
        return result;
    }

}]);