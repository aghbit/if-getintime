"use strict";

filters.filter('taskFilter', function () {
    return function (tasks, predicate) {

        if (predicate === null) {
            return tasks;
        }

        var results = [];
        var priorities = predicate.priorities.map(Number);

        angular.forEach(tasks, function (task) {
            if (task.overdue || task.status == 'IN_PROGRESS') {
                results.push(task);
                return;
            }
            if (priorities.indexOf(task.taskProperties.priority) == -1) {
                return;
            }
            if (predicate.types.indexOf(task.type) == -1) {
                return;
            }
            if (predicate.statuses.indexOf(task.status) == -1) {
                return;
            }
            if (!dateInDatesInterval(task.assigneeDeadline.date, predicate.date.from, predicate.date.to)) {
                return;
            }
            if (!task.taskDescription.name.toLowerCase().contains(predicate.taskDescription.name.toLowerCase())) {
                return;
            }
            results.push(task);
        });

        return results;
    };
});


