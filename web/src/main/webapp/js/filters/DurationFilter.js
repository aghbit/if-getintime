"use strict";

filters.filter('duration', function () {
    function pad(number, fieldSize) {
        var numberString = number + "";
        while (numberString.length < fieldSize) {
            numberString = "0" + numberString;
        }
        return numberString;
    }

    function completeHours(duration) {
        return ~~(duration / 60); // integer division hack
    }

    function remainingMinutes(duration) {
        return duration % 60;
    }

    return function (durationMinutes) {
        if (durationMinutes != null) {
            return completeHours(durationMinutes) + ":" + pad(remainingMinutes(durationMinutes), 2);
        }
        return '\u2013';
    };
});