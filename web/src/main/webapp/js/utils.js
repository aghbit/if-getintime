function unwrap(collection) {
    var result = [];
    angular.forEach(collection, function(element) {
        result.push(element.value);
    });

    return result;
}

function dateToYMD(date) {
    if ( angular.isString(date)) {
        return date;
    }
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}

function earlierWorkingDay(from, daysOff) {
    return findWorkingDay(from, daysOff, -1);
}

function soonestWorkingDay(from, daysOff) {
    return findWorkingDay(from, daysOff, 1);
}

function findWorkingDay(from, daysOff, step) {

    var date = from.clearTime();

    while (containsDate(daysOff, date)) {
        date.addDays(step);
    }

    return date;
}

// unfortunately, we have to traverse the whole array...
function containsDate(daysOff, date) {
    var found = false;
    angular.forEach(daysOff, function(dayOff) {
        if (datesEqualIgnoringTime(dayOff, date)) {
            found = true;
            return;
        }
    });
    return found;
}

function datesEqualIgnoringTime(date1, date2) {
    return dateWithoutTime(date1).getTime() === dateWithoutTime(date2).getTime();
}

function parseDate(string) {
    // if string is already a date
    if (typeof string == "object" || !string) {
        return string;
    }
    var ymd = string.split('-');
    return new Date(ymd[0], ymd[1]-1, ymd[2]);
}

function min(lhs, rhs) { // for date comparison
    return (lhs > rhs ? lhs : rhs);
}

String.prototype.contains = function(it) { return this.indexOf(it) != -1; };

function todayWithoutHours() {
    return Date.today();
}

function dateWithoutTime(date) {
    return new Date(date).clearTime();
}

function tomorrowDateWithoutTime() {
    return todayWithoutHours().addDays(1);
}

var dateInDatesInterval = function (date, intervalStart, intervalEnd) {
    return (dateWithoutTime(date) >= dateWithoutTime(intervalStart) && dateWithoutTime(date) <= dateWithoutTime(intervalEnd));
};

var filterCheckboxes = function (statuses) {
    var results = [];

    angular.forEach(statuses, function (status) {
        if (status.checked) {
            results.push(status.name);
        }
    });

    return results;
};

function startDateToDate(startDate) {
    return new Date(startDate.year,
                    startDate.monthValue - 1,
                    startDate.dayOfMonth,
                    startDate.hour,
                    startDate.minute,
                    startDate.second,
                    startDate.nano / 1E6);
}

function ascendingOrderComparator(a, b) {
    return a - b;
}

function generateDaysArrayExclusive(from, to) {
    var array = [];
    var followingDay = (new Date(from)).addDays(1).clearTime();
    var limit  = to.clearTime();
    while(followingDay < limit){
        array.push(new Date(followingDay));
        followingDay.addDays(1);
    }
    return array;
}

function mergeArrayOfArrays(array){
    return [].concat.apply([], array);
}

