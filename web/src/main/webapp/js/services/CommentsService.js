'use strict';

services.factory('CommentsService', ['$resource', function ($resource) {
    return $resource('/rest/tasks/:id/comments', {id: "@id"}, {
        getComments: {
            method: 'GET',
            isArray: true
        },
        saveComment: {
            method: 'POST'
        }
    });
}]);