'use strict';

services.factory('TaskPropertiesService', ['$resource', function ($resource) {
    var resource = $resource('', {}, {
            getPossiblePriorities: {
                method: 'GET',
                isArray: true,
                url: '/rest/utils/task-priorities'
            },
            getPossibleTypes: {
                method: 'GET',
                isArray: true,
                url: '/rest/utils/task-types'
            },
            getPossibleStatuses: {
                method: 'GET',
                isArray: true,
                url: '/rest/utils/task-statuses'
            }
        }
    );

    return {
        getPossiblePriorities: function (params, successCallback, failCallback) {
            return resource.getPossiblePriorities(params, function (successResult) {
                    successCallback(unwrap(successResult));
                },
                failCallback)
        },
        getPossibleTypes: function (params, successCallback, failCallback) {
            return resource.getPossibleTypes(params, function (successResult) {
                    successCallback(unwrap(successResult));
                },
                failCallback)
        },
        getPossibleStatuses: function (params, successCallback, failCallback) {
            return resource.getPossibleStatuses(params, function (successResult) {
                    successCallback(unwrap(successResult));
                },
                failCallback)
        }
    }
}]);


