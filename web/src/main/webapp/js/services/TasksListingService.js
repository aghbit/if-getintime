"use strict";

services.factory('TaskListingService', function ($resource) {
    return $resource('/rest/tasks', {}, {
        getTasks: {
            method: 'GET',
            params: {from: '@from' , to: '@to' },
            isArray: true
        },
        getSubordinatesTasks: {
            url: '/rest/tasks/subordinates',
            method: 'GET',
            params: {from: '@from', to: '@to'},
            isArray: true
        }
    });
});

