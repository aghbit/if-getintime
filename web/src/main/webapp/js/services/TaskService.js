"use strict";

services.factory('TaskService', function ($resource) {
    return $resource('rest/tasks/:id', {}, {
        create: {
            method: 'POST'
        },
        getTask: {
            method: 'GET'
        },
        getSubtasks: {
            method: 'GET',
            url: 'rest/tasks/:id/subtasks',
            isArray: true
        },
        getSiblingtasks: {
            method: 'GET',
            url: 'rest/tasks/:id/siblingtasks',
            isArray: true
        },
        updateTask: {
            method: 'PUT'
        }
    })
});
