'use strict';

services.service('ListingTasksStateService', [function () {
    this.myTasksView = true;

    this.lastViewName = null;

    this.updateCurrentViewName = function (currentViewName) {
        this.myTasksView = currentViewName === "tasks_list.my-tasks";
        this.lastViewName = currentViewName;
    }

    this.hasViewChanged = function(currentViewName) {
        return this.lastViewName != currentViewName;
    }

}]);
