"use strict";

services.factory('EmployeesService', function ($resource) {
    return $resource('/rest/employees/:id/:param', {}, {
        query: {
            method: 'GET',
            params: {},
            isArray: true
        },
        subordinates: {
            method: 'GET',
            params: {param: 'subordinates'},
            isArray: true
        }
    })
});

