/**
 * Created by Grzesiek on 11.06.14.
 */
services.service('TasksDateFilterRangeService', function () {

    var dateRange = {
        from: new Date(),
        to: new Date()
    }
    var service = {
        setFromDate: function (fromDate) {
            dateRange.from = fromDate;
        },
        setToDate: function (toDate) {
            dateRange.to = toDate;
        },
        getDateRange: function () {
            return dateRange;
        },
        setDateRange: function (newDateRange) {
            if (newDateRange.from != null) {
                dateRange.from = newDateRange.from;
            }
            if (newDateRange.to != null) {
                dateRange.to = newDateRange.to;
            }
        }
    }

    return service;
});