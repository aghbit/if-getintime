/**
 * Created by Konrad on 2014-12-06.
 */
services.factory('SubTaskService',['$stateParams','TaskService',function($stateParams, TaskService){
    var subtasks = null;
    return{
        updateNumberOfSubtasks: function(){
            subtasks = TaskService.getSubtasks({id: $stateParams.id},
                function(successResult) {
                },
                function(errorResult) {
                    $dialogs.error( "Error", errorResult.data.error);
                })
        },
        getSubTasks: function(){
            return subtasks;
        }
    }
}])
