'use strict';

services.factory ('CyclicTaskDetailsService', ['$resource', function ( $resource ) {

    return $resource ('/rest/tasks/cyclic/:id/:type', {id: "@id"}, {
        getTaskById: {
            method: 'GET',
            isArray : false
        },
        getTasksByIdWithDateRange: {
            method:'GET',
            isArray: true,
            params:{type: 'tasks'}
        }
    });

}]);