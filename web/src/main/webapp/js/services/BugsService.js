"use strict";

services.factory('BugsService', function ($resource) {
    return $resource('rest/support-ticket', {}, {
        report: {
            method: 'POST'
        }
    })
});