"use strict";

services.factory('CyclicTaskDateFilterRangeService', function () {
    var dateRange = {
        from : new Date(),
        to : new Date().addMonths(1),
        min : new Date(),
        max: new Date().addYears(1)
    }
    var service = {
        setFromDate : function(fromDate){
            dateRange.from = fromDate;
        },
        setToDate : function(toDate){
            dateRange.to = toDate;
        },
        getDateRange : function(){
            return dateRange;
        }

    }

    return service;
});