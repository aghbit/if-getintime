'use strict';

services.factory('AuthenticationService', ['$http', 'SessionService', function ($http, SessionService) {
    return {
        currentUser: function (successCallback, errorCallback) {
            return SessionService.currentUser(successCallback, errorCallback);
        },

        login: function (credentials, callback) {
            $http.post('/rest/login', credentials).then(function (result) {
                if (result.data.loggedIn) {
                    SessionService.create(result.data.employee);
                }

                callback(SessionService.currentUser() != null);

            });
        },

        logout: function (callback) {
            $http.get('/rest/logout').then(function (result) {

                if (Boolean(result.data)) {
                    SessionService.destroy();
                    callback(SessionService.currentUser());
                }
            });
        },
        isAuthenticated: function () {
            return !!this.currentUser();
        },
        isAuthorized: function (authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }

            return (this.isAuthenticated() &&
                authorizedRoles.indexOf(SessionService.currentUser().type) !== -1);
        },
        isCurrentlyLogged: function (employee) {
            return this.currentUser().id === employee.id;
        }
    }
}]);
