'use strict';

services.factory('AuthenticationMockService', function ($resource) {
    return $resource('/rest/authentication/current-user-mock/', {}, {
        currentUser: {
            method: 'GET',
            url: '',
            params: {}
        },

        setCurrentUser: {
            method: 'PUT',
            url: '/rest/authentication/current-user-mock/:id',
            params: {
                id: '@id'}
        }
    })
});
