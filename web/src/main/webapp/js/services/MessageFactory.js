'use strict';

services.factory('MessageFactory', function ($resource) {
    return $resource('/rest/message', {}, {
        query: {
            method: 'GET',
            params: {},
            isArray: false
        }
    })
});