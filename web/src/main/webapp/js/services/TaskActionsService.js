"use strict";

services.factory('TaskActionsService', function ($resource) {
    return $resource('', {id: '@id'}, {
        start: {
            method: 'POST',
            url: '/rest/tasks/:id/start'
        },
        stop: {
            method: 'PUT',
            url: '/rest/tasks/:id/stop'
        },
        finish: {
            method: 'POST',
            url: '/rest/tasks/:id/finish'
        },
        logTime: {
            method: 'PUT',
            url: '/rest/tasks/:id/time/:time',
            params: {
                time: '@time'
            }
        },
        logPercent: {
            method: 'PUT',
            url: '/rest/tasks/:id/work-percent/:percent',
            params: {
                percent: '@percent'
            }
        },
        logPercentAndTime: {
            method: 'PUT',
            url: '/rest/tasks/:id/time-and-progress/:timeInMinutes/:percent',
            params: {
                percent: '@percent',
                timeInMinutes: '@timeInMinutes'
            }
        },
        cancelTask: {
            method: 'PUT',
            url: 'rest/tasks/:id/cancel'
        },
        addSubtask: {
            method: 'PUT',
            url: 'rest/tasks/:id/subtask'
        }
    });
});