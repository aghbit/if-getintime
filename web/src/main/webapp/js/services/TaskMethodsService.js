"use strict";

services.factory('TaskMethodsService', function () {
    return {
        isEstimatedTimeExceeded: function (task) {
            if (task == null || task.taskProperties == null || task.taskProperties.estimatedTime == null) {
                return false;
            }

            function isTaskNotDone() {
                return task.status !== 'DONE';
            }

            function isStandardEstimatedTimeExceeded() {
                if (task.loggedTime == null) {
                    return false;
                }

                return task.loggedTime > task.taskProperties.estimatedTime;
            }

            function isActiveTaskEstimatedTimeExceeded() {
                if (task.workingTime == null || task.workingTime.allSeconds == null) {
                    return false;
                }

                return task.workingTime.allSeconds > task.taskProperties.estimatedTime * 60;
            }

            return isTaskNotDone() && (isStandardEstimatedTimeExceeded() || isActiveTaskEstimatedTimeExceeded());
        }
    }
});