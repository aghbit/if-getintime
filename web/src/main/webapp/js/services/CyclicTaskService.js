'use strict';

services.factory('CyclicTaskService', ['$resource', function ($resource) {
    return $resource('/rest/tasks/cyclic', {}, {
        }
    )
}]);
