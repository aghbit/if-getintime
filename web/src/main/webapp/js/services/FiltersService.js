"use strict";

services.factory('FiltersService', function ($resource) {
    return $resource('/rest/employees/:id/filters/:filterId', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            save: {
                method: 'POST'
            },
            delete: {
                method: 'DELETE'
            }
        })
});

