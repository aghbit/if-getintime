services.factory('SessionService', ['$cookieStore', '$http', function ($cookieStore, $http) {
    this.initialized = false;

    this.create = function (data) {
        $cookieStore.put('userData', data);
        this.userData = data;
    };

    this.destroy = function () {
        this.create(null);
    };

    this.currentUser = function (successCallback, errorCallback) {
        if (!this.initialized) {
            var storedData = $cookieStore.get('userData');
            if (storedData != undefined) {
                this.create(storedData);
            } else {
                $http.get('/rest/authentication/current-user',function (data) {
                    this.create(data.result);
                }).error(function(data) {
                        if (errorCallback !== undefined) {}
                        errorCallback(data);
                });
            }
            this.initialized = true;
        }
        if ( successCallback !== undefined) {
            successCallback(this.userData);
        }
        return this.userData;
    }

    return this;
}]);
