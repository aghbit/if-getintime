'use strict';

services.factory('DayOffService', ['$http', function ($http) {
    return {
        getDaysOff: function(from, to) {
            return $http.get('/rest/dayoff/' + dateToYMD(from) + '/' + dateToYMD(to)).then(function (response) {
                var dates = [];
                angular.forEach(response.data, function(dateString) {
                    dates.push(parseDate(dateString));
                });
                return dates;
            });
        }
    }
}]);