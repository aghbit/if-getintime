'use strict';

// Declare app level module which depends on filters, and services
var gitApp = angular.module('getintime', [
    'ui.router',
    'ngCookies',
    'http-auth-interceptor',
    'getintime.filters',
    'getintime.services',
    'getintime.directives',
    'getintime.controllers'
]);

gitApp.config(function ($stateProvider, $urlRouterProvider, USER_ROLES) {
    $urlRouterProvider.otherwise("/tasks");

    $urlRouterProvider.when('/tasks','/tasks/my-tasks');

    $stateProvider
        .state('tasks_list', {
            url: '/tasks',
            views: {
                '': {
                    templateUrl: 'partials/views/tasks_listing.html',
                    controller: 'TasksListingController',
                    data: {
                        authorizedRoles: [USER_ROLES.normal, USER_ROLES.manager, USER_ROLES.director]
                    }
                },
                'active_task_listing@tasks_list': {
                    templateUrl: 'partials/views/active_task.html',
                    controller: 'ActiveTaskController'
                }
            }
        })
        .state('tasks_list.my-tasks', {
            url: '/my-tasks',
            templateUrl: 'partials/views/my_tasks.html',
            controller: "MyTasksTableController",
            data: {
                authorizedRoles: [USER_ROLES.normal, USER_ROLES.manager, USER_ROLES.director]
            }
        })
        .state('tasks_list.subordinates-tasks', {
            url: '/subordinates-tasks',
            templateUrl: 'partials/views/subordinates_tasks.html',
            controller: "SubordinatesTasksTableController",
            data: {
                authorizedRoles: [ USER_ROLES.manager, USER_ROLES.director]
            }
        })

        .state('task_create', {
            url: '/task',
            templateUrl: 'partials/views/task.html',
            controller: 'NewStandardTaskController',
            data: {
                authorizedRoles: [ USER_ROLES.manager, USER_ROLES.director]
            }
        })
        .state('task_cyclic_create', {
            url: '/task/cyclic',
            templateUrl: 'partials/views/new_cyclic_task_form.html',
            controller: 'NewCyclicTaskController',
            data: {
                authorizedRoles: [ USER_ROLES.manager, USER_ROLES.director]
            }
        })
        .state('task_details', {
            url: '/task/:id',
            templateUrl: 'partials/views/task_details.html',
            controller: 'TaskDetailsController',
            data: {
                authorizedRoles: [  USER_ROLES.normal, USER_ROLES.manager, USER_ROLES.director]
            }
        })
        .state('mocked_login', {
            url: '/mockedLogin',
            templateUrl: 'partials/views/mocked_login.html',
            controller: 'MockedLoginController',
            data: {
                authorizedRoles: [  USER_ROLES.normal, USER_ROLES.manager, USER_ROLES.director]
            }
        })

        .state('admin-panel', {
            url: '/admin-panel',
            templateUrl: 'partials/views/admin_panel.html',
            controller: 'AdminPanelController',
            data: {
                authorizedRoles: [USER_ROLES.manager, USER_ROLES.director]
            }
        })
        .state('admin-panel.new-cyclic-task', {
            url: '/new-cyclic-task',
            templateUrl: 'partials/views/new_cyclic_task_form.html',
            controller: 'NewCyclicTaskController'
        })
        .state('admin-panel.tasks_cyclic_list', {
            url: '/tasks/cyclic',
            templateUrl: 'partials/views/cyclic_tasks_listing.html',
            controller: 'CyclicTaskController'
        })
        .state('admin-panel.tasks_cyclic_details', {
            url: '/task/cyclic/:id',
            templateUrl: 'partials/views/cyclic_task_details.html',
            controller: 'CyclicTaskDetailsController'
        })

}).run(function ($rootScope, AuthenticationService, SessionService, $modal, $dialogs) {
    $rootScope.showView = true;
    $rootScope.userData = AuthenticationService.currentUser();
    $rootScope.inLoginAction = false;


    $rootScope.$on('event:auth-loginRequired', function(rejection, nextStateName) {
        $rootScope.showView = false;
        $rootScope.userData =  AuthenticationService.currentUser();
        if(!$rootScope.inLoginAction) {
            $rootScope.inLoginAction = true;
            $modal.open({
                templateUrl: 'partials/views/login_form.html',
                controller: 'LoginCtrl',
                backdrop: 'static',
                resolve:{
                    nextStateName : function(){
                        return nextStateName;
                    }
                }
            });
        }
    });

    $rootScope.$on('event:auth-notAuthorized', function(rejection) {
        $dialogs.error('Brak dostępu', 'Nie posiadasz uprawnień aby wykonać akcję.');
    });

    $rootScope.$on('event:auth-loginConfirmed', function(event, data){
        $rootScope.showView = true;
        $rootScope.userData = data;
        $rootScope.inLoginAction = false;
    });

    $rootScope.$on('event:auth-loginCancelled', function(event, data){
        $rootScope.showView = false;
        $rootScope.userData = data;
        $rootScope.$broadcast('event:auth-loginRequired');
    });

    $rootScope.$on('$stateChangeStart', function (event, nextState) {
        if(nextState.data == undefined){
            return;
        }

        var authorizedRoles = nextState.data.authorizedRoles;
        if (!AuthenticationService.isAuthorized(authorizedRoles)) {
            event.preventDefault();
            if (AuthenticationService.isAuthenticated()) {
                $rootScope.$broadcast('event:auth-notAuthorized');
            } else {
                $rootScope.$broadcast('event:auth-loginRequired', nextState.name);
            }
        }
    });
})
.constant('USER_ROLES', {
    normal: 'NORMAL',
    manager: 'MANAGER',
    director: 'DIRECTOR'
});