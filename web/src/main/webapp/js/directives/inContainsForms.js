"use strict";
directives.directive('inContainsForms', [
    function () {
        return {
            link: function (scope, element) {
                $(element).find('input', 'a.anchor-checkbox', '.input-group.date-picker-group').click(function (e) {
                    e.stopPropagation();
                });
            }
        }
    }])