'use strict';

directives.directive('inSecured', ['AuthenticationService', function (AuthenticationService) {
    return {
        link: function ($scope, $element) {
            var run = function(){
                var roles = $element.attr('roles').split(',');
                roles = roles.map(function(role) { return role.trim();});
                if(AuthenticationService.isAuthorized(roles)){
                    $element.removeClass('ng-hide');
                }else{
                    $element.addClass('ng-hide');
                }
            }

            $scope.$on('event:auth-loginConfirmed', function(){
                run();
            });
            $scope.$on('event:auth-loginCancelled', function(){
                run();
            });

            run();
        }
    }
}]);