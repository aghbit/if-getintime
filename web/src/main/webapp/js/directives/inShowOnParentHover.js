directives.directive('inShowOnParentHover', [
    function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                $(element).hide();

                $(element).parent().bind('mouseover', function() {
                    $(element).show();
                });

                $(element).parent().bind('mouseout', function() {
                    $(element).hide();
                });
            }
        }
    }
]);