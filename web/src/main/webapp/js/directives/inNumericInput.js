'use strict';

directives.directive('inNumericInput', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {

            if (!ngModel || ngModel == undefined) {
                return; // do nothing if no ng-model
            }

            ngModel.$parsers.push(function (inputValue) {
                if (ngModel.$isEmpty(inputValue)) return '';
                var numberOnlyInput = inputValue.toString().replace(/[^0-9]/g, '');
                if (numberOnlyInput != inputValue) {
                    ngModel.$setViewValue(numberOnlyInput);
                    ngModel.$render();
                }
                return numberOnlyInput;
            });
        }
    }
});