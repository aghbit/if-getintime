'use strict';

directives.directive('inDaysOfMonthDisplayer', [function () {
    return {
        restrict: 'E',
        template: '<span>{{daysOfMonthSorted.length > 0 ? (daysOfMonthSorted | joinBy:", ") : "-"}}</span>',
        scope: {
            daysOfMonth: '='
        },
        link: function (scope) {
            scope.$watch('daysOfMonth', function() {
                if(scope.daysOfMonth === undefined)return;
                scope.daysOfMonthSorted = scope.daysOfMonth.slice(0);
                scope.daysOfMonthSorted.sort(ascendingOrderComparator);
            });
        }
    }
}]);