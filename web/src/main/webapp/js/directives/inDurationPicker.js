'use strict';

// Duration (hours & minutes) picker.
// Based on Timepicker directive from UI Bootstrap (ui.bootstrap.timepicker).

directives

    .constant('durationPickerConfig', {
        hourStep: 1,
        minuteStep: 10,
        readonlyInput: false,
        mousewheel: false,
        hoursInputLength: undefined,
        allowZero: false
    })

    .directive('inDurationPicker', ['$parse', '$log', 'durationPickerConfig', function ($parse, $log, durationPickerConfig) {
        return {
            restrict: 'EA',
            require: '?^ngModel',
            replace: true,
            scope: {},
            templateUrl: 'partials/directives/in_duration_picker.html',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return; // do nothing if no ng-model
                }

                var hourStep = durationPickerConfig.hourStep;
                if (attrs.hourStep) {
                    scope.$parent.$watch($parse(attrs.hourStep), function (value) {
                        hourStep = parseInt(value, 10);
                    });
                }

                var minuteStep = durationPickerConfig.minuteStep;
                if (attrs.minuteStep) {
                    scope.$parent.$watch($parse(attrs.minuteStep), function (value) {
                        minuteStep = parseInt(value, 10);
                    });
                }

                var allowZero = durationPickerConfig.allowZero;
                if (attrs.allowZero) {
                    scope.$parent.$watch($parse(attrs.allowZero), function (value) {
                        allowZero = value;
                        refresh();
                    });
                }

                var hoursSelected;
                var minutesSelected;

                function getDurationSelected() {
                    return hoursSelected * 60 + minutesSelected;
                }

                function getDuration(hours, minutes) {
                    return hours * 60 + minutes;
                }

                function setHoursAndMinutesSelected(duration) {
                    if (duration <= 0) {
                        hoursSelected = minutesSelected = 0;
                    } else {
                        hoursSelected = completeHours(duration);
                        minutesSelected = remainingMinutes(duration);
                    }
                }

                function completeHours(duration) {
                    return ~~(duration / 60); // integer division hack
                }

                function remainingMinutes(duration) {
                    return duration % 60;
                }

                function isDurationValid(hours, minutes) {
                    return angular.isDefined(hours) && angular.isDefined(minutes) && (allowZero || getDuration(hours, minutes) != 0)
                }

                scope.hoursInputLength = durationPickerConfig.hoursInputLength;
                if (attrs.hoursInputLength) {
                    scope.$parent.$watch($parse(attrs.hoursInputLength), function (value) {
                        scope.hoursInputLength = parseInt(value, 10);
                    });
                }

                if (ngModel.$error.duration) {
                    // Evaluate from template
                    var hours = getHoursFromTemplate(), minutes = getMinutesFromTemplate();
                    if (isDurationValid(hours, minutes)) {
                        hoursSelected = hours;
                        refresh();
                    }
                } else {
                    updateTemplate();
                }

                function getHoursFromTemplate() {
                    var hours = parseInt(scope.hours, 10);
                    var valid = hours >= 0;
                    if (!valid) {
                        return undefined;
                    }

                    return hours;
                }

                function getMinutesFromTemplate() {
                    var minutes = parseInt(scope.minutes, 10);
                    return (minutes >= 0 && minutes < 60) ? minutes : undefined;
                }

                function pad(value) {
                    return (angular.isDefined(value) && value.toString().length < 2) ? ('0' + value) : value;
                }

                // Input elements
                var inputs = element.find('input'), hoursInputEl = inputs.eq(0), minutesInputEl = inputs.eq(1);

                // Respond on mousewheel spin
                var mousewheel = (angular.isDefined(attrs.mousewheel)) ? scope.$eval(attrs.mousewheel) : durationPickerConfig.mousewheel;
                if (mousewheel) {
                    var isScrollingUp = function (e) {
                        if (e.originalEvent) {
                            e = e.originalEvent;
                        }
                        //pick correct delta variable depending on event
                        var delta = (e.wheelDelta) ? e.wheelDelta : -e.deltaY;
                        return (e.detail || delta > 0);
                    };

                    hoursInputEl.bind('mousewheel wheel', function (e) {
                        scope.$apply((isScrollingUp(e)) ? scope.incrementHours() : scope.decrementHours());
                        e.preventDefault();
                    });

                    minutesInputEl.bind('mousewheel wheel', function (e) {
                        scope.$apply((isScrollingUp(e)) ? scope.incrementMinutes() : scope.decrementMinutes());
                        e.preventDefault();
                    });
                }

                scope.readonlyInput = (angular.isDefined(attrs.readonlyInput)) ? scope.$eval(attrs.readonlyInput) : durationPickerConfig.readonlyInput;
                if (!scope.readonlyInput) {
                    var invalidate = function (invalidHours, invalidMinutes) {
                        ngModel.$setViewValue(getDurationSelected());
                        ngModel.$setValidity('duration', false);
                        if (angular.isDefined(invalidHours)) {
                            scope.invalidHours = invalidHours;
                        }
                        if (angular.isDefined(invalidMinutes)) {
                            scope.invalidMinutes = invalidMinutes;
                        }
                    };

                    scope.updateHours = function () {
                        var hours = getHoursFromTemplate();

                        if (angular.isDefined(hours)) {
                            if (isDurationValid(hours, minutesSelected)) {
                                hoursSelected = hours;
                                refresh();
                            } else {
                                invalidate(true, true);
                            }
                        } else {
                            invalidate(true);
                        }
                    };

                    scope.updateMinutes = function () {
                        var minutes = getMinutesFromTemplate();

                        if (angular.isDefined(minutes)) {
                            if (isDurationValid(hoursSelected, minutes)) {
                                minutesSelected = minutes;
                                refresh('m');
                            } else {
                                invalidate(true, true);
                            }
                        } else {
                            invalidate(undefined, true);
                        }
                    };

                    minutesInputEl.bind('blur', function (e) {
                        if (!scope.invalidMinutes && scope.minutes < 10) {
                            scope.$apply(function () {
                                scope.minutes = pad(scope.minutes);
                            });
                        }
                    });
                } else {
                    scope.updateHours = angular.noop;
                    scope.updateMinutes = angular.noop;
                }

                ngModel.$render = function () {
                    var duration = ngModel.$modelValue;

                    if (isNaN(duration) || duration < (allowZero ? 0 : 1)) {
                        ngModel.$setValidity('duration', false);
                    } else {
                        setHoursAndMinutesSelected(duration);
                        makeValid();
                        updateTemplate();
                    }
                };

                function refresh(keyboardChange) {
                    makeValid();
                    ngModel.$setViewValue(getDurationSelected());
                    updateTemplate(keyboardChange);
                    if (!isDurationValid(hoursSelected, minutesSelected)) {
                        invalidate(true, true);
                    }
                }

                scope.$watch( function() {
                    return scope.hours;
                }, function(newValue, oldValue) {
                    if (!angular.isNumber(newValue)) {
                        scope.hours = oldValue;
                        refresh();
                    }
                });

                scope.$watch( function() {
                    return scope.minutes;
                }, function(newValue, oldValue) {
                    if (!angular.isNumber(newValue)) {
                        scope.minutes = oldValue;
                        refresh();
                    }
                });

                function makeValid() {
                    ngModel.$setValidity('duration', true);
                    scope.invalidHours = false;
                    scope.invalidMinutes = false;
                }

                function updateTemplate(keyboardChange) {
                    scope.hours = hoursSelected;
                    scope.minutes = keyboardChange === 'm' ? minutesSelected : pad(minutesSelected);
                }

                function addMinutes(minutes) {
                    setHoursAndMinutesSelected(getDurationSelected() + minutes);
                    refresh();
                }

                scope.incrementHours = function () {
                    addMinutes(hourStep * 60);
                };
                scope.decrementHours = function () {
                    if (hoursSelected != 0) {
                        addMinutes(-hourStep * 60);
                    }
                };
                scope.incrementMinutes = function () {
                    addMinutes(minuteStep);
                };
                scope.decrementMinutes = function () {
                    addMinutes(-minuteStep);
                };

                var duration = ngModel.$modelValue;
                if (isNaN(duration) || duration < (allowZero ? 0 : 1)) {
                    ngModel.$setValidity('duration', false);
                    setHoursAndMinutesSelected(0);
                } else {
                    setHoursAndMinutesSelected(duration);
                }

                refresh();
            }
        };
    }]);
