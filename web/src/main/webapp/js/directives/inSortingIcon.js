'use strict'

directives.directive('inSortingIcon', function () {
    return {
        restrict: 'E',
        scope: {
            sortOrder: '='
        },
        template: '<i ng-class="iconStyle"></i>',
        link: function (scope) {
            scope.$watch(function() {
                return scope.sortOrder;
            }, function () {
                switch (scope.sortOrder) {
                    case 'ascending':
                        scope.iconStyle = 'glyphicon glyphicon-sort-by-attributes';
                        break;
                    case 'descending':
                        scope.iconStyle = 'glyphicon glyphicon-sort-by-attributes-alt';
                        break;
                    default :
                        scope.iconStyle = 'glyphicon glyphicon-sort';
                }
            });
        }
    }
});