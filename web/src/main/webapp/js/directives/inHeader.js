/**
 * Created by Jacek Żyła on 16.03.14.
 */
'use strict';

directives.directive('inHeader', ['AuthenticationService', '$state', '$stateParams', 'authService',
    function (AuthenticationService, $state, $stateParams, authService) {
        return {
            restrict: 'E',
            scope: {
                user: '=',
                showView:'='
            },
            templateUrl: '/partials/directives/in_header.html',
            link: function ($scope) {
                $scope.$watch('user', function () {
                    $scope.showData = ($scope.user != undefined);
                });

            },
            controller: function ($scope, $modal, $rootScope) {
                $scope.logout = function () {
                    AuthenticationService.logout(function(currentUser){
                        authService.loginCancelled(currentUser);
                    });
                };

                $scope.openAddTaskModal = function () {
                    var addTaskModal = $modal.open({
                        templateUrl: '/partials/views/new_standard_task_form.html',
                        controller: 'NewStandardTaskController'
                    });

                    addTaskModal.result.then(function () {
                        // refresh the view
                        $state.transitionTo($state.current, $stateParams, {
                            reload: true,
                            inherit: false,
                            notify: true
                        });
                    });
                };

                $scope.openAdminPanel = function () {
                    $state.transitionTo('admin-panel');
                };

                $scope.logoClicked = function () {
                    $rootScope.$broadcast('event:GetInTimeLogoClicked');
                }
            }
        }
    }
]);
