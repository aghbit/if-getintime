'use strict';

directives.directive('inPercentageInput', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/directives/in_percentage_input.html',
        scope: {
            percentageVariable: "="
        },
        controller: function ($scope) {
            $scope.showPercentageError = function () {
                if ($scope.percentageVariable == undefined || $scope.percentageVariable == null || $scope.percentageVariable < 0 || $scope.percentageVariable > 100) {
                    return true;
                }
                return false;
            }
            $scope.displayAutofinishWarning = function() {
                return $scope.percentageVariable == 100;
            }
        }
    }
});