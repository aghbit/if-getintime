'use strict';

directives.directive('inFocus', function () {
    return {
        link: function (scope, element) {
            element[0].focus();
        }
    }
});