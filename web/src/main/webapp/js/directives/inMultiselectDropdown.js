/**
 * Created by Jacek Żyła on 18.03.14.
 */
"use strict";
directives.directive('inMultiselectDropdown', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, ngModel) {

            scope.$watch(function() {return ngModel.$modelValue;}, function (newVal, oldVal) {

                if (newVal !== undefined) {

                    scope.filtersLoaded = true;
                    element = $(element); // Get the element as a jQuery element

                    element.multiselect({
                        buttonClass: 'btn btn-default',
                        buttonWidth: 'auto',
                        buttonContainer: '<div class="btn-group" />',
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        buttonText: function (options) {
                            if (options.length == 0) {
                                return element.data()['placeholder'] + ' <b class="caret"></b>';
                            } else if (options.length == 1) {
                                return element.data()['placeholder'] + ': ' + options[0].text
                                    + ' <b class="caret"></b>';
                            } else {
                                return element.data()['placeholder'] + ': ' + options[0].text + ',...'
                                    + ' <b class="caret"></b>';
                            }
                        },
                        onChange: function (optionElement, checked) {
                            scope.$apply(function () {

                                var modelValue = ngModel.$modelValue;

                                var optionText = optionElement[0].text;
                                var optionIndex = modelValue.indexOf(optionText);

                                if (checked) {
                                    modelValue.push(optionElement[0].text);
                                } else {
                                    modelValue.splice(optionIndex, 1);
                                }
                            })
                        }
                    });

                    scope.$watch(function () {
                        return element[0].length;
                    }, function () {
                        element.multiselect('rebuild');
                    });
                }
            })
        }
    }
});