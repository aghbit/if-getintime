'use strict';

directives.directive('inTextChunk', [function () {
    var template =
        '<div> ' +
            '<alert type="success" close="onDelete($index)">' +
            '{{content}}' +
            '</alert>' +
            '</div>';

    return {
        restrict: 'E',
        template: template,
        scope: {
            content: '@',
            onDelete: '&'
        }
    }
}]);