'use strict';

directives.directive('inDatePickerInput', ['$timeout','$compile', function ($timeout, $compile) {
    return {
        restrict: 'E',
        templateUrl: 'partials/directives/in_date_picker_input.html',
        scope: {
            minDate: '=',
            maxDate: '=',
            pickedDate: "=",
            inputName: "=",
            disabledDays: "="
        },
        controller: function ($scope, $element, $attrs) {
            // NOTE: In order to refresh days off (e.g. after getting them from server), you have to change
            // pickedDate, minDate or maxDate value. The function below will be called again then.
            $scope.disabled = function (date, mode) {
                if (mode !== 'day') {
                    return false;
                } else {
                    return ($scope.disabledDays!== undefined && containsDate($scope.disabledDays, date));
                }
            };

            $scope.open = function (event) {
                event.preventDefault();
                event.stopPropagation();
                $scope.opened = !$scope.opened;

            };

            $scope.dateOptions = {
//                Monday is first from left
                'starting-day': 1,
                'year-format': "'yy'"
            };
            $scope.format = 'd MMMM yyyy';

            $scope.isCollapsed = true;
            $scope.name = $scope.inputName!=null && $scope.inputName!=undefined && $scope.inputName != "" ? $scope.inputName : "inDatePicker";
        }
    }
}]);