'use strict';

directives.directive('inDaysOfWeekDisplayer', [function () {
    return {
        restrict: 'E',
        template: '<span>{{daysOfWeekSorted.length > 0 ? (daysOfWeekSorted | joinBy:", ") : "-"}}</span>',
        scope: {
            daysOfWeek: '='
        },
        link: function (scope) {
            var daysOfWeekMapper = {
                'Pon': 1,
                'Wt': 2,
                'Śr': 3,
                'Czw': 4,
                'Pią': 5,
                'Sob': 6,
                'Nie': 7
            };

            var daysOfWeekComparator = function (lhs, rhs) {
                return daysOfWeekMapper[lhs] - daysOfWeekMapper[rhs];
            };

            scope.$watch('daysOfWeek', function () {
                if(scope.daysOfWeek === undefined)return;
                scope.daysOfWeekSorted = scope.daysOfWeek.slice(0);
                scope.daysOfWeekSorted.sort(daysOfWeekComparator);
            });
        }
    }
}]);