'use strict';

directives.directive('inTaskStatus', function () {
    return {
        restrict: 'E',
        scope: {
            showStartActions: '=',
            showStopActions: "=",
            startAction: "&",
            stopAction: "&",
            doneAction: "&",
            taskStatus: "="
        },
        templateUrl: 'partials/directives/status.html',

        link: function (scope, element, attrs) {
        }
    }
});