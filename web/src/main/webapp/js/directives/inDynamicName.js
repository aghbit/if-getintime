"use strict";
// used to load dynamically name attribute to an element for form validation
// based on http://stackoverflow.com/questions/21455695/angularjs-dynamic-form-field-validation
directives.directive("inDynamicName", function ($compile) {
    return {
        restrict: "A",
        terminal: false,
        priority: 10000,
        link: function (scope, element, attrs) {
            if (element.attr('name-compiled') == undefined) {
                element.attr('name', scope.$eval(attrs["inDynamicName"]));
                element.attr('name-compiled', "true");
                element.removeAttr("in-dynamic-name");
                $compile(element)(scope);
            }
        }
    };
});