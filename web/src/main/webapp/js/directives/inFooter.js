/**
 * Created by Jacek Żyła on 16.03.14.
 */
'use strict';

directives.directive('inFooter', [
    function () {
        return {
            restrict: 'E',
            scope: {
                user: '='
            },
            templateUrl: '/partials/directives/in_footer.html',
            link: function ($scope) {
                $scope.$watch('user', function () {
                    $scope.showData = ($scope.user != undefined);
                });

            },
            controller: function ($scope, $modal) {
                $scope.openReportBugModal = function () {
                    var reportBugModal = $modal.open({
                        templateUrl: '/partials/views/report_bug_form.html',
                        controller: 'ReportBugController',
                        resolve: {
                            user: function () {
                                return $scope.user;
                            }
                        }
                    });;
                };
            }
        }
    }
]);
