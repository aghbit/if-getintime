'use strict';

describe('durationFilter', function () {
    beforeEach(module('getintime.filters'));

    it('should exist', inject(function ($filter) {
        expect($filter('duration')).not.toBeNull();
    }));

    it('should convert null durations to dashes',
        inject(function (durationFilter) {
            expect(durationFilter(null)).toBe('\u2013');
        }));

    it('should convert durations in minutes to hour-minute duration strings',
        inject(function (durationFilter) {
            expect(durationFilter(123)).toBe('2:03');
            expect(durationFilter(2909)).toBe('48:29');
        }));
});