"use strict";

var earlierDate = new Date(2010, 5, 12);
var isCyclicGrouping=true;
var notCyclicGrouping=false;

var TASKS = [
    { ownerDeadline: new Date(2009, 7, 27), assigneeDeadline: new Date(2011, 7, 27), type: 'CYCLIC', cyclicTaskId: 0, overdue: true },
    { ownerDeadline: earlierDate, assigneeDeadline: earlierDate, type: 'CYCLIC', cyclicTaskId: 0, overdue: false },
    { ownerDeadline: new Date(2011, 9, 20), assigneeDeadline: new Date(2011, 9, 20), type: 'CYCLIC', cyclicTaskId: 1, overdue: false },
    { ownerDeadline: earlierDate, assigneeDeadline: earlierDate, type: 'CYCLIC', cyclicTaskId: 1, overdue: false },
    { ownerDeadline: new Date(2014, 7, 27), assigneeDeadline: new Date(2011, 7, 27), type: 'CYCLIC', cyclicTaskId: 0, overdue: false },
    { ownerDeadline: earlierDate, assigneeDeadline: earlierDate, type: 'STANDARD', cyclicTaskId: null, overdue: false}
];

describe('cyclicTasksGroupingFilter', function () {
    beforeEach(module('getintime.filters'));

    it('if cyclic task is not overdue it should group cyclic task with the same parent task', inject(function ($filter) {
        expect($filter('cyclicTasksGroupingFilter')(TASKS, isCyclicGrouping).length).toBe(4);
    }));

    it('should return not grouped cyclic task', inject(function ($filter) {
        expect($filter('cyclicTasksGroupingFilter')(TASKS, notCyclicGrouping).length).toBe(6);
    }));

    it('should return the earliest instance of cyclic not overdue task', inject(function ($filter) {
        var result = $filter('cyclicTasksGroupingFilter')(TASKS, isCyclicGrouping);
        expect(result[0].ownerDeadline).toEqual(new Date(2009, 7, 27))
        expect(result[1].ownerDeadline).toEqual(earlierDate)
        expect(result[2].ownerDeadline).toEqual(earlierDate)
        expect(result[3].ownerDeadline).toEqual(earlierDate)
    }));
});