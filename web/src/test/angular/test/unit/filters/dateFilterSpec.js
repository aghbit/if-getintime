"use strict";

var MOCKED_TASKS = [
    { ownerDeadline: { date: new Date(2010, 5, 12)}, assigneeDeadline: { date: new Date(2010, 5, 12)}, status: 'DONE'},
    { ownerDeadline: { date: new Date(2011, 7, 27)}, assigneeDeadline: { date: new Date(2011, 7, 27)}, status: 'DONE'},
    { ownerDeadline: { date: new Date(2011, 7, 28)}, assigneeDeadline: { date: new Date(2011, 7, 28)}, status: 'DONE'},
    { ownerDeadline: { date: new Date(2011, 9, 20)}, assigneeDeadline: { date: new Date(2011, 9, 20)}, status: 'DONE'},
    { ownerDeadline: { date: new Date(2013, 1, 11)}, assigneeDeadline: { date: new Date(2013, 1, 11)}, status: 'DONE'}
];

var DATES = {
    dateFrom: new Date(2011, 7, 20),
    dateTo: new Date(2011, 8, 12)
};

describe('date Filter ', function () {

    beforeEach(module('getintime.filters'));

    it('should exist ', inject(function ($filter) {
        expect($filter('dateFilter')).not.toBeNull();
    }));

    it('should leave dates only from given interval ', inject(function ($filter) {
        expect($filter('dateFilter')(MOCKED_TASKS, DATES).length).toBe(2);
    }));
});