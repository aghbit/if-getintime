describe('Task Methods Service', function() {
    var TaskMethodsService, taskWithExceededWorkTime;

    beforeEach(module('getintime.services'));
    beforeEach(inject(function ($injector) {
        TaskMethodsService = $injector.get("TaskMethodsService");

        taskWithExceededWorkTime = {taskProperties: {estimatedTime: 120}, loggedTime: 130, status: 'STOPPED'};
    }));

    describe("is estimate overdue", function () {

        it("returns false when task is null or undefined", function () {
            expect(TaskMethodsService.isEstimatedTimeExceeded(null)).toBeFalsy();
            expect(TaskMethodsService.isEstimatedTimeExceeded(undefined)).toBeFalsy();
        });

        it("returns false when task estimated time is null", function () {
            var task = {taskProperties: { estimatedTime: null}};
            expect(TaskMethodsService.isEstimatedTimeExceeded(task)).toBeFalsy();
        });

        it("returns false when task has estimated time but no logged time ", function () {
            var task = {taskProperties: {estimatedTime: 120}, loggedTime: null};
            expect(TaskMethodsService.isEstimatedTimeExceeded(task)).toBeFalsy();
        });

        it("returns false if task is already done", function () {
            taskWithExceededWorkTime.status = 'DONE';
            expect(TaskMethodsService.isEstimatedTimeExceeded(taskWithExceededWorkTime)).toBeFalsy();
        });

        it("returns false if not done but estimatedTime is smaller than loggedTime", function () {
            taskWithExceededWorkTime.loggedTime = taskWithExceededWorkTime.taskProperties.estimatedTime - 1;
            expect(TaskMethodsService.isEstimatedTimeExceeded(taskWithExceededWorkTime)).toBeFalsy();
        });

        it("returns true if not done and logged time is bigger than estimated", function () {
            expect(TaskMethodsService.isEstimatedTimeExceeded(taskWithExceededWorkTime)).toBeTruthy();
        });

    });

});
