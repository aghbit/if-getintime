"use strict";

describe('NewStandardTask Controller ', function () {
    var USER_ROLES = {
        normal: 'NORMAL',
        manager: 'MANAGER',
        director: 'DIRECTOR'
    };

    var PRIORITIES = [1, 2, 3, 5, 7];

    var USERS = [
        {"id": 1, "firstName": "Detektyw", "secondName": "Mock"},
        {"id": 2, "firstName": "John", "secondName": "Doe"}
    ];

    var DAYS_OFF = [Date.today(), Date.today().addDays(1), Date.today().addYears(1)];

    var scope, TaskServiceMock, modalInstance;

    var TaskPropertiesServiceMock = {getPossiblePriorities: []};

    var AuthenticationServiceMock = {currentUser: function () {}};

    var EmployeesServiceMock = {subordinates: function () {}};

    var DayOffServiceMock;

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        TaskServiceMock = jasmine.createSpyObj("TaskService", ["create"]);

        EmployeesServiceMock = jasmine.createSpyObj('EmployeesService', ['subordinates']);
        EmployeesServiceMock.subordinates.andCallFake(function (obj, successCallback) {
            successCallback(USERS);
        });

        TaskPropertiesServiceMock = jasmine.createSpyObj('TaskPropertiesService', ['getPossiblePriorities']);
        TaskPropertiesServiceMock.getPossiblePriorities.andCallFake(function (obj, successCallback) {
            successCallback(PRIORITIES);
        });

        spyOn(AuthenticationServiceMock, 'currentUser').andReturn(USERS[0]);

        DayOffServiceMock = jasmine.createSpyObj('DayOffService', ['getDaysOff', 'then']);
        DayOffServiceMock.getDaysOff.andCallFake(function() {
            return DayOffServiceMock;
        });
        DayOffServiceMock.then.andCallFake(function (callback) {
            callback(DAYS_OFF);
        });

        $controller('NewStandardTaskController', {
            $scope: scope,
            $modalInstance: modalInstance,
            TaskService: TaskServiceMock,
            EmployeesService: EmployeesServiceMock,
            AuthenticationService: AuthenticationServiceMock,
            TaskPropertiesService: TaskPropertiesServiceMock,
            DayOffService: DayOffServiceMock,
            USER_ROLES: USER_ROLES
        });
    }));

    it('should get current user from service', function () {
        expect(AuthenticationServiceMock.currentUser).toHaveBeenCalled();
        expect(AuthenticationServiceMock.currentUser.callCount).toBe(1);
    });

    describe('should prepare new standard task end put it into scope. It includes:', function () {
        it('get days off from service', function () {
            expect(DayOffServiceMock.getDaysOff).toHaveBeenCalled();
            expect(DayOffServiceMock.getDaysOff.callCount).toBe(1);
        });

        it('get possible assignees from service', function () {
            expect(EmployeesServiceMock.subordinates).toHaveBeenCalled();
            expect(EmployeesServiceMock.subordinates.callCount).toBe(1);
        });

        it('get possible priorities for service', function () {
            expect(TaskPropertiesServiceMock.getPossiblePriorities).toHaveBeenCalled();
            expect(TaskPropertiesServiceMock.getPossiblePriorities.callCount).toBe(1);
        });

        it('put possible priorities to scope', function () {
            expect(scope.serverData.possiblePriorities).toBeDefined();
            expect(scope.serverData.possiblePriorities).toEqual(PRIORITIES);
        });

        it('put days off into scope', function() {
            expect(scope.serverData.daysOff).toBeDefined();
            expect(scope.serverData.daysOff).toEqual(DAYS_OFF);
        });

        it('set initial task priority', function () {
            expect(scope.task.taskProperties.priority).toBeDefined();
        });

        it('set task type to STANDARD ', function () {
            expect(scope.task.type).toEqual('STANDARD');
        });

        it('set task status to TODO ', function () {
            expect(scope.task.status).toEqual('TODO');
        });

        it('prepare new standard task and in into scope', function () {
            expect(scope.task).toBeDefined();
        });

        it('sets estimated time to not required', function () {
            expect(scope.helpers.withEstimatedTime).toBeFalsy();
        });
    });

    describe('should have proper setters defined.', function () {
        it('task assignee setter', function () {

            scope.setAssignee(USERS[0]);
            expect(scope.task.taskProperties.assigned).toEqual(USERS[0]);

            scope.setAssignee(USERS[1]);
            expect(scope.task.taskProperties.assigned).toEqual(USERS[1]);
        });

        it('task priority setter', function () {

            scope.setPriority(PRIORITIES[0]);
            expect(scope.task.taskProperties.priority).toEqual(PRIORITIES[0]);

            scope.setPriority(PRIORITIES[1]);
            expect(scope.task.taskProperties.priority).toEqual(PRIORITIES[1]);
        })
    });

    it('should set inDurationPicker directive properties', function () {
        expect(scope.helpers.hourStep).toEqual(1);
        expect(scope.helpers.minuteStep).toEqual(10);
    });

    it('should call TaskFactory create method on creating new task', function () {
        scope.createNewStandardTask();

        expect(TaskServiceMock.create).toHaveBeenCalled();
        expect(TaskServiceMock.create.callCount).toBe(1);
    });

    describe('should control picked deadlines. Cases', function () {
        it('should set assignee minimum deadline to soonest working day', function() {
            expect(scope.assigneeMinimumDeadline).toEqual(Date.today().addDays(2));
        });

        it('should set owner maximum deadline to a working day year later', function() {
            expect(scope.ownerMaximumDeadline).toEqual(Date.today().addYears(1).addDays(-1));
        });

        it('should initially set owner minimum deadline to task assignee deadline', function () {
            expect(scope.ownerMinimumDeadline).toEqual(scope.task.assigneeDeadline);
        });

        it('should initially set assignee maximum deadline to task owner deadline', function () {
            expect(scope.assigneeMaximumDeadline).toEqual(scope.task.ownerDeadline);
        });
    });
});

