'use strict';

describe('NewCyclicTask Controller', function () {

    var SUBORDINATES = ['Jack', 'Mike', 'Chris', 'Paul'];
    var PRIORITIES = [1, 2, 3, 5, 8];
    var CURRENT_USER_ID = 1;
    var DAYS_OFF = [Date.today(), Date.today().addDays(1), Date.today().addYears(1)];

    var instance, scope;

    var stateMock = {transitionTo: function () {}};
    var AuthenticationServiceMock = {currentUser: function () {}};
    var EmployeesServiceMock;
    var TaskPropertiesServiceMock;
    var DayOffServiceMock;

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($rootScope, $controller) {

        scope = $rootScope.$new();

        scope.serverData = {};

        spyOn(stateMock, 'transitionTo');

        EmployeesServiceMock = jasmine.createSpyObj('EmployeesService', ['subordinates']);
        EmployeesServiceMock.subordinates.andCallFake(function (obj, successCallback, errorCallback) {
            successCallback(SUBORDINATES);
        });

        TaskPropertiesServiceMock = jasmine.createSpyObj('TaskPropertiesService', ['getPossiblePriorities']);
        TaskPropertiesServiceMock.getPossiblePriorities.andCallFake(function (obj, successCallback) {
            successCallback(PRIORITIES);
        });

        spyOn(AuthenticationServiceMock, 'currentUser').andCallFake(function () {
            return CURRENT_USER_ID;
        });

        DayOffServiceMock = jasmine.createSpyObj('DayOffService', ['getDaysOff', 'then']);
        DayOffServiceMock.getDaysOff.andCallFake(function() {
            return DayOffServiceMock;
        });
        DayOffServiceMock.then.andCallFake(function (callback) {
            callback(DAYS_OFF);
        });

        instance = $controller('NewCyclicTaskController', {
            $scope: scope,
            $state: stateMock,
            EmployeesService: EmployeesServiceMock,
            TaskPropertiesService: TaskPropertiesServiceMock,
            AuthenticationService: AuthenticationServiceMock,
            DayOffService: DayOffServiceMock
        })
    }));

    describe('should call', function () {
        it('EmployeeService for user subordinates', function () {
            expect(EmployeesServiceMock.subordinates).toHaveBeenCalled();
        });

        it('TaskPropertiesService for possible task priorities', function () {
            expect(TaskPropertiesServiceMock.getPossiblePriorities).toHaveBeenCalled();
        });

        it('AuthenticationService for current user id', function () {
            expect(AuthenticationServiceMock.currentUser).toHaveBeenCalled();
        });

        it('DayOffService for days off', function () {
            expect(DayOffServiceMock.getDaysOff).toHaveBeenCalled();
        });
    });

    describe('should set', function () {
        it('standard task properties', function () {
            expect(scope.input.name).toEqual("");
            expect(scope.input.description).toEqual("");
            expect(scope.input.createdBy).toEqual({});
            expect(scope.input.assigned).toEqual('Jack');
            expect(scope.input.priority).toEqual(PRIORITIES[0]);
            expect(scope.input.estimatedTime).toBe(0);
        });

        it('from date', function () {
            expect(scope.input.from).toEqual(Date.today().addDays(2));
        });

        it('to date', function () {
            expect(scope.input.to).toEqual(Date.today().addYears(1).addDays(-1));
        });

        it('five possible working days for weekly repeating', function () {
            expect(scope.input.weekly.length).toBe(5);
        });
    });

    describe('should put server data into scope', function() {
        it('priorities', function () {
            expect(scope.serverData.priorities).toEqual(PRIORITIES);
        });

        it('minimum date', function() {
            expect(scope.serverData.minDate).toEqual(Date.today().addDays(2));
        });

        it('maximum date', function() {
            expect(scope.serverData.maxDate).toEqual
        });

        it('days off', function() {
            expect(scope.serverData.daysOff).toEqual(DAYS_OFF);
        });
    });

    it('should set helpers variables', function () {
        expect(scope.helpers.isWeeklyOpen).toBeFalsy();
        expect(scope.helpers.every).toBeUndefined();
        expect(scope.helpers.hourStep).toEqual(1);
        expect(scope.helpers.minuteStep).toEqual(10);
        expect(scope.helpers.allowZero).toBeFalsy();
    });

    describe('should have properly working setters for', function () {
        it('change task assignee', function () {
            var newAssignee = 'Anakin';

            expect(scope.input.assigned).not.toBe(newAssignee);
            scope.setAssignee(newAssignee);
            expect(scope.input.assigned).toBe(newAssignee);
        });

        it('change task priority', function () {
            var newPriority = PRIORITIES[2];

            expect(scope.input.priority).not.toBe(newPriority);
            scope.setPriority(newPriority);
            expect(scope.input.priority).toBe(newPriority);
        });
    });

    it('should have method for control adding monthly days', function () {
        expect(scope.toggleIsPresent).toBeDefined();
    });
});