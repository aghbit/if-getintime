/**
 * Created by Grzesiek on 20.03.14.
 */
"use strict";

describe('LogProgress Controller ', function () {
    var scope, modalInstance, dialogs, location, TaskActionsServiceMock, TaskListingServiceMock, taskId, progressPercentage;

    beforeEach(module('getintime.controllers'));

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        taskId = 1;
        progressPercentage = 35;


        TaskActionsServiceMock = jasmine.createSpyObj("TaskActionsService", ["logPercentAndTime"]);
        dialogs = jasmine.createSpyObj('$dialogs', ['error']);
        modalInstance = jasmine.createSpyObj('modalInstance', ['close', 'dismiss']);
        location = jasmine.createSpyObj('$location', ['path']);
        TaskListingServiceMock = jasmine.createSpyObj('TaskListingService', ['query']);

        $controller('LogProgressController', {
            $scope: scope,
            $modalInstance: modalInstance,
            $dialogs: dialogs,
            $location: location,
            TaskActionsService: TaskActionsServiceMock,
            TaskListingService: TaskListingServiceMock,
            taskId: taskId,
            progressPercentage: progressPercentage
        });

    }));

    it("should set scope task.id", function () {
        expect(scope.task.id).toEqual(taskId);
    });

    it("should set progress properties to empty", function () {
        expect(scope.progress).toEqual({
            timeInMinutesToLog: 0,
            percentToLog: progressPercentage
        });
    });

    it("should set duration picker settings", function () {
        expect(scope.settings).toEqual({
            hourStep: 1,
            minuteStep: 5,
            allowZero: false
        });
    });

    it("should disable logging progress when no minutes inserted", function () {
        expect(scope.canLogProgress(taskId, 0, 12)).toBeFalsy();
    });

    it("should disable logging progress when no work % done inserted", function () {
        expect(scope.canLogProgress(taskId, 23, 0)).toBeFalsy();
    });


    it("should call $modalInstance.dismiss with 'cancel' parameter", function () {
        scope.cancel();
        expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
    });

    it("should close modal instance", function () {
        scope.ok();
        expect(modalInstance.close).toHaveBeenCalled();
    });

    it('should call TaskActionsService logTime method with parameters', function () {
        var taskId = 10;
        var timeInMinutesToLog = 10;
        scope.logProgress(taskId, timeInMinutesToLog, progressPercentage);

        expect(TaskActionsServiceMock.logPercentAndTime).toHaveBeenCalledWith({id: taskId, timeInMinutes: timeInMinutesToLog, percent: progressPercentage }, jasmine.any(Function), jasmine.any(Function));
    });
});

