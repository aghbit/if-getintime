/**
 * Created by Grzesiek on 23.04.14.
 */
"use strict";

describe('AddSubTaskController', function () {
    var scope, location, modalInstance, dialogs,
        TaskActionsServiceMock, EmployeesServiceMock, AuthenticationServiceMock, TaskPropertiesServiceMock, DayOffServiceMock,
        parentTask, today;


    var PRIORITIES = [1, 2, 3, 5, 7];

    var USERS = [
        {"id": 1, "firstName": "Detektyw", "secondName": "Mock"},
        {"id": 2, "firstName": "John", "secondName": "Doe"}
    ];

    var DAYS_OFF = [Date.today(), Date.today().addDays(1)];

    var currentUser = USERS[0];

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($controller, $rootScope) {

        scope = $rootScope.$new();

        location = jasmine.createSpyObj('$location', ['path']);
        modalInstance = jasmine.createSpyObj('modalInstance', ['close', 'dismiss']);
        dialogs = jasmine.createSpyObj('$dialogs', ['error', 'notify']);
        TaskActionsServiceMock = jasmine.createSpyObj('TaskActionsService', ['addSubtask']);

        EmployeesServiceMock = jasmine.createSpyObj('EmployeesService', ['subordinates']);
        EmployeesServiceMock.subordinates.andCallFake(function (obj, successCallback) {
            successCallback(USERS);
        });

        TaskPropertiesServiceMock = jasmine.createSpyObj('TaskPropertiesService', ['getPossiblePriorities']);
        TaskPropertiesServiceMock.getPossiblePriorities.andCallFake(function (obj, successCallback) {
            successCallback(PRIORITIES);
        });

        AuthenticationServiceMock = jasmine.createSpyObj('AuthenticationService',['currentUser']);
        AuthenticationServiceMock.currentUser.andReturn(currentUser);

        DayOffServiceMock = jasmine.createSpyObj('DayOffService', ['getDaysOff', 'then']);
        DayOffServiceMock.getDaysOff.andCallFake(function() {
            return DayOffServiceMock;
        });
        DayOffServiceMock.then.andCallFake(function (callback) {
            callback(DAYS_OFF);
        });

        parentTask = {
            id: 10,
            taskProperties: {
                createdBy: 'Jacek',
                assigned: 'Grzesiek'
            },
            assigneeDeadline: { date: '2013-05-10' },
            ownerDeadline: { date: '2013-05-12' }
        };

        $controller('AddSubTaskController', {
            $scope: scope,
            $location: location,
            $modalInstance: modalInstance,
            $dialogs: dialogs,
            TaskActionsService: TaskActionsServiceMock,
            EmployeesService: EmployeesServiceMock,
            AuthenticationService: AuthenticationServiceMock,
            TaskPropertiesService: TaskPropertiesServiceMock,
            DayOffService: DayOffServiceMock,
            parentTask: parentTask
        });

    }));

    it("should get days off from service", function () {
        expect(DayOffServiceMock.getDaysOff).toHaveBeenCalled();
        expect(DayOffServiceMock.getDaysOff.callCount).toBe(1);
    });

    it("should put days off into scope", function() {
        expect(scope.serverData.daysOff).toEqual(DAYS_OFF);
    });

    it("should set subtask createdBy", function () {
        expect(scope.task.taskProperties.createdBy).toEqual(parentTask.taskProperties.createdBy);
    });

    it("should create new subtask", function () {
        scope.addSubtask();
        expect(TaskActionsServiceMock.addSubtask).toHaveBeenCalled();
    });

    it("should set subtask owner deadline to parent task owner deadline", function () {
        expect(scope.task.ownerDeadline.date).toEqual(parseDate(parentTask.ownerDeadline.date));
    });

    it("should set subtask assignee deadline to parent task assignee deadline", function () {
        expect(scope.task.assigneeDeadline.date).toEqual(parseDate(parentTask.assigneeDeadline.date));
    });

    it("should set subtask owner deadline min date to assignee deadline", function () {
        expect(scope.ownerMinimumDeadline).toEqual(scope.task.assigneeDeadline.date);
    });

    it("should set subtask owner deadline max date to parent task owner deadline", function () {
        expect(scope.ownerMaximumDeadline).toEqual(parseDate(parentTask.ownerDeadline.date));
    });

    it("should set subtask assignee deadline min date to soonest working day", function () {
        expect(scope.assigneeMinimumDeadline).toEqual(Date.today().addDays(2));
    });
});

