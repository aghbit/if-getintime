/**
 * Created by Piotr on 2014-11-30.
 */

"use strict";


describe('TaskStop Controller', function () {
    var scope, modalInstance, dialogs, location, TaskActionsServiceMock, TaskListingServiceMock, taskId, task, progressPercentage;

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        taskId = 1;
        progressPercentage = 35;


        TaskActionsServiceMock = jasmine.createSpyObj("TaskActionsService", ["logPercentAndTime"]);
        dialogs = jasmine.createSpyObj('$dialogs', ['error']);
        modalInstance = jasmine.createSpyObj('modalInstance', ['close', 'dismiss']);
        location = jasmine.createSpyObj('$location', ['path']);
        TaskListingServiceMock = jasmine.createSpyObj('TaskListingService', ['query']);
        task  = {taskProperties: {estimatedTime: undefined}, loggedTime: 2,
            workingTime: jasmine.createSpyObj('WorkingTime', ['updateTime']),  status: 'ACTIVE'};
        task.workingTime.allSeconds = 60;

        $controller('TaskStopController', {
            $scope: scope,
            $modalInstance: modalInstance,
            $dialogs: dialogs,
            $location: location,
            TaskActionsService: TaskActionsServiceMock,
            TaskListingService: TaskListingServiceMock,
            taskId: taskId,
            task: task,
            progressPercentage: progressPercentage
        });


    }));

    it('should put default percentage as before when task is not estimated', function () {
        expect(scope.percentToLog)
            .toEqual(progressPercentage);
    })

    it('should put default percentage as workingTime/EstimatedTime when task is estimated', function () {
        task.taskProperties.estimatedTime = 10;
        scope.percentToLog = scope.generateDefaultPercentage();
        expect(scope.percentToLog)
            .toBe(10);
    })

    it('should round workingTime/EstimatedTime to integer', function () {
        task.workingTime.allSeconds = 63;
        task.taskProperties.estimatedTime = 10;
        scope.percentToLog = scope.generateDefaultPercentage();
        expect(scope.percentToLog)
            .toBe(10);
    })

    it('should not calculate workingTime/EstimatedTime anymore when user once changed default percentage', function () {
        task.taskProperties.estimatedTime = 10;
        task.percentage = 46;
        scope.percentToLog = scope.generateDefaultPercentage();
        expect(scope.percentToLog)
            .toBe(progressPercentage);
    })
});