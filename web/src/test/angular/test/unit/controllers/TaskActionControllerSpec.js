'use strict';

var TASK = {
    id: 1
};

var TASKS = [TASK];

var PERCENTAGE = 42;

describe('TaskActionController', function () {
    var instance, scope, TaskActionServiceMock, DialogsMock, modal,TaskServiceMock;
    beforeEach(module('getintime.controllers'));

    beforeEach(inject(function ($injector, $controller, $rootScope) {
        scope = $rootScope.$new;

        TaskActionServiceMock = jasmine.createSpyObj('TaskActionService', ['start', 'stop', 'finish']);
        TaskServiceMock = jasmine.createSpyObj('TaskService', ['getTask']);
        DialogsMock = jasmine.createSpyObj('$dialogs', ['notify', 'error']);
        modal = $injector.get('$modal');
        spyOn(modal, 'open').andCallThrough();

        instance = $controller('TaskActionController', {
            $scope: scope,
            TaskService: TaskServiceMock,
            TaskActionsService: TaskActionServiceMock,
            $dialogs: DialogsMock,
            $modal: modal
        });
    }));

    /* smoke tests */
    it('should be define', function() {
        expect(instance).toBeDefined();
    });

    describe('should have api methods defined:', function() {
        it('addSubtask', function () {
            expect(scope.addSubTask).toBeDefined();
        });

        it('start', function () {
            expect(scope.startWorkingOnTask).toBeDefined();
        });

        it('stop', function () {
            expect(scope.openStopWorkingModal).toBeDefined()
        });

        it('edit', function () {
            expect(scope.editTask).toBeDefined();
        });

        it('finish', function () {
            expect(scope.finishWorkingOnTask).toBeDefined();
        });

        it('logProgress', function () {
            expect(scope.openLogProgressModal).toBeDefined();
        });
    });

    describe('should provide start task feature.', function () {
        it('On user start call should call TaskActionService', function () {
            scope.startWorkingOnTask(TASK.id);
            expect(TaskActionServiceMock.start).toHaveBeenCalled();
        });
    });

    describe('should provide stop task feature', function() {
        it('On user stop call should open modal with input for percentage', function () {
            scope.openStopWorkingModal(TASK.id, PERCENTAGE);
            expect(modal.open).toHaveBeenCalled();
        });
    });

    describe('should provide log progress feature', function () {
        it('On user log progress call should open modal', function () {
            scope.openLogProgressModal(TASK.id, PERCENTAGE);
            expect(modal.open).toHaveBeenCalled();
        });
    });

    describe('should provide finish task feature', function () {
        it('On user finish task call should call TaskService to get task', function () {
            scope.finishWorkingOnTask(TASK.id);
            expect(TaskServiceMock.getTask).toHaveBeenCalled();
        });
    });

    describe('should provide add subtask feature', function () {
        it('On user add subtask call should open modal', function () {
            scope.tasks = TASKS;
            scope.addSubTask(TASK.id);
            expect(modal.open).toHaveBeenCalled();
        });
    });

    describe('should provide edit task feature.', function () {
        it('On user edit task call should open modal ', function () {
            scope.editTask(TASK.id);
            expect(modal.open).toHaveBeenCalled();
        });
    });
});

