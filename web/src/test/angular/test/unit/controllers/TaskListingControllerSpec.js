"use strict";

var USERS = [
    {"id": 1, "firstName": "Detektyw", "secondName": "Mock"},
    {"id": 2, "firstName": "John", "secondName": "Doe"}
];

describe('TaskListing Controller ', function () {
    var instance, scope, TaskListingServiceMock, TaskActionsServiceMock;

    var AuthenticationServiceMock = {currentUser: function () {
    }};

    var tasks = [];
    var sortingEvent = {};

    beforeEach(module('getintime.controllers'));

    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        tasks = [
            {id: 1, beingWorkedOn: true},
            {id: 2, beingWorkedOn: false}
        ];

        TaskListingServiceMock = jasmine.createSpyObj('TaskListingService', ['getTasks']);
        TaskActionsServiceMock = jasmine.createSpyObj("TaskActionsService", ["query", "start", "logTime"]);
        TaskListingServiceMock.getTasks.andCallFake(function (obj, successCallback) {
            successCallback(tasks);
        });

        spyOn(AuthenticationServiceMock, 'currentUser').andReturn(USERS[0]);

        instance = $controller('TasksListingController', {
            $scope: scope,
            TaskListingService: TaskListingServiceMock,
            TaskActionsService: TaskActionsServiceMock,
            AuthenticationService: AuthenticationServiceMock
        });

        scope.currentColumnOrderBy = ['term.date', '-priority'];
        sortingEvent.shiftKey = false;

        $rootScope.$broadcast('UPDATE_TASKS_LIST_EVENT', {from: "2014-01-01", to: "2014-01-03"});
        $rootScope.$broadcast('FILTERS_LOADED_EVENT', {from: "2014-01-01", to: "2014-01-03"});

    }));

    it('should get tasks on updating tasks list', function () {
        expect(TaskListingServiceMock.getTasks).toHaveBeenCalled();
    });

    it('should get tasks when filters loaded', function ($rootScope) {
        expect(TaskListingServiceMock.getTasks).toHaveBeenCalled();
    });

    it('should set "sort-attributes" sorting column glyph', function () {
        expect(scope.sortingColumnButtonClass('priority')).toEqual('descending');
    });

    it('should set "sort" sorting column glyph', function () {
        expect(scope.sortingColumnButtonClass('deadline')).toEqual('notSorted');
    });

    it('should set "sort-by-attributes-alt" sorting column glyph', function () {
        expect(scope.sortingColumnButtonClass('term.date')).toEqual('ascending');
    });

    it("should change existing ascending sorting to descending one", function () {
        var columnName = 'term.date';
        scope.currentColumnOrderBy = [columnName];
        scope.applyColumnOrderBy(sortingEvent, columnName);
        expect(scope.currentColumnOrderBy.indexOf('-' + columnName)).toEqual(0);
        expect(scope.currentColumnOrderBy.indexOf(columnName)).toEqual(-1);
    });

    it("should change existing descending sorting to ascending one", function () {
        var columnName = 'term.date';
        scope.currentColumnOrderBy = ['-' + columnName];
        scope.applyColumnOrderBy(sortingEvent, columnName);
        expect(scope.currentColumnOrderBy.indexOf(columnName)).toEqual(0);
        expect(scope.currentColumnOrderBy.indexOf('-' + columnName)).toEqual(-1);
    });

    it("should add new sorting column name to active sorting order list", function () {
        var columnName = 'deadline';
        sortingEvent.shiftKey = true;
        scope.currentColumnOrderBy = ['-term'];
        scope.applyColumnOrderBy(sortingEvent, columnName);
        expect(scope.currentColumnOrderBy.indexOf(columnName)).toEqual(1);
        expect(scope.currentColumnOrderBy.indexOf('-' + columnName)).toEqual(-1);
    });

    it("should change multi-column sorting single column descending to ascending", function () {
        var columnName = 'term.date';
        sortingEvent.shiftKey = true;
        scope.currentColumnOrderBy = ['property1', '-' + columnName];
        scope.applyColumnOrderBy(sortingEvent, columnName);
        expect(scope.currentColumnOrderBy.indexOf(columnName)).toEqual(1);
        expect(scope.currentColumnOrderBy.indexOf('-' + columnName)).toEqual(-1);
    });

    it("should change multi-column sorting single column ascending to descending", function () {
        var columnName = 'term.date';
        sortingEvent.shiftKey = true;
        scope.currentColumnOrderBy = ['property1', columnName];
        scope.applyColumnOrderBy(sortingEvent, columnName);
        expect(scope.currentColumnOrderBy.indexOf('-' + columnName)).toEqual(1);
        expect(scope.currentColumnOrderBy.indexOf(columnName)).toEqual(-1);
    });

    it("should set single column sorting from multi-column sorting", function () {
        var columnName = 'term.date';
        scope.currentColumnOrderBy = ['property1', 'property2'];
        scope.applyColumnOrderBy(sortingEvent, columnName);
        expect(scope.currentColumnOrderBy.indexOf(columnName)).toEqual(0);
        expect(scope.currentColumnOrderBy.length).toEqual(1);
    });

    it("should return task overdue value", function () {
        var task = {overdue: true};
        expect(scope.shouldAlreadyBeCompleted(task)).toEqual(task.overdue);
    });

    describe("displaying available users actions based on task", function () {
        describe("start working on task", function () {
            it("should be disabled when already in progress", function () {
                var task = {status: "TODO"};
                scope.tasks[1].status = 'IN_PROGRESS';
                scope.$broadcast('UPDATE_TASKS_LIST_EVENT', {from: "2014-01-01", to: "2014-01-03"});
                expect(scope.canStartWorkingOnTask(task)).toBeFalsy();
            });

            it("should be disabled when task is cancelled", function () {
                var task = {status: "CANCELLED"};
                expect(scope.canStartWorkingOnTask(task)).toBeFalsy();
            });

            it("should be disabled when already done", function () {
                var task = {status: "DONE"};
                expect(scope.canStartWorkingOnTask(task)).toBeFalsy();
            });

            it("should be enabled otherwise", function () {
                var task = {status: "TODO"};
                expect(scope.canStartWorkingOnTask(task)).toBeTruthy();
                task = {status: "STOP"};
                expect(scope.canStartWorkingOnTask(task)).toBeTruthy();
            });
        });

        describe("stop working on task", function () {
            it("should be enabled when in progress", function () {
                var task = {status: "IN_PROGRESS"};
                expect(scope.canStopWorkingOnTask(task)).toBeTruthy();
            });
        });

        describe("log time to task", function () {
            it("should be disabled when already finished", function () {
                var task = {status: 'DONE'};
                expect(scope.canLogTimeToTask(task)).toBeFalsy();
            });
            it("should be disabled when cancelled", function () {
                var task = {status: 'CANCELLED'};
                expect(scope.canLogTimeToTask(task)).toBeFalsy();
            });
        });

        describe("finish task", function () {
            it("should be disabled when already done", function () {
                var task = {status: 'DONE'};
                expect(scope.canFinishWorkingOnTask(task)).toBeFalsy();
            });
            it("should be disabled when cancelled", function () {
                var task = {status: 'CANCELLED'};
                expect(scope.canFinishWorkingOnTask(task)).toBeFalsy();
            });
            it("should be disabled when not started", function () {
                var task = {status: 'TODO'};
                expect(scope.canFinishWorkingOnTask(task)).toBeFalsy();
            });
        });

        describe("stop working on task", function () {
            it("should be disabled when already done", function () {
                var task = {status: 'DONE'};
                expect(scope.canStopWorkingOnTask(task)).toBeFalsy();
            });
            it("should be disabled when cancelled", function () {
                var task = {status: 'CANCELLED'};
                expect(scope.canStopWorkingOnTask(task)).toBeFalsy();
            });
            it("should be disabled when not started", function () {
                var task = {status: 'TODO'};
                expect(scope.canStopWorkingOnTask(task)).toBeFalsy();
            });
            it("should be disabled when not started", function () {
                var task = {status: 'STOPPED'};
                expect(scope.canStopWorkingOnTask(task)).toBeFalsy();
            });
        });

        describe("cancel task", function () {
            it("should be disabled when already cancelled", function () {
                var task = {status: 'CANCELLED'};
                expect(scope.canCancelTask(task)).toBeFalsy();
            });
            it("should be disabled when already done", function () {
                var task = {status: 'DONE'};
                expect(scope.canCancelTask(task)).toBeFalsy();
            });
        });

        describe("done tasks", function () {
            it("should not be visible when filter task status 'DONE' is unchecked", function () {
                scope.predicatesParent.taskPredicate = {statuses: ['IN_PROGRESS']};
                expect(scope.areCompletedTasksVisible()).toBeFalsy();

            });
            it("should be visible when filter task status 'DONE' is checked", function () {
                scope.predicatesParent.taskPredicate = {statuses: ['IN_PROGRESS', 'DONE']};
                expect(scope.areCompletedTasksVisible()).toBeTruthy();
            })
        });

        describe("add subtask", function () {
            it("should be disabled when already finished", function () {
                var task = {status: 'DONE'};
                expect(scope.canAddSubtaskToTask(task)).toBeFalsy();
            });
        });

    });
});

