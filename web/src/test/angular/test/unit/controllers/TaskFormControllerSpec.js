"use strict";

describe('TaskForm Controller ', function () {
    var scope;

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();

        $controller('TaskFormCtrl', {
            $scope: scope,
            $timeout: undefined
        });
    }));

    it('should disable task creation when assignee deadline is after owner deadline', function () {
        scope.task.assigneeDeadline = new Date(2);
        scope.task.ownerDeadline = new Date(1);

        expect(scope.assigneeDeadlineIsAfterOwnerDeadline()).toBeTruthy();
    });

    it('should enable task creation when assignee deadline is not after owner deadline', function () {
        scope.task.assigneeDeadline = new Date(2);
        scope.task.ownerDeadline = new Date(2);

        expect(scope.assigneeDeadlineIsAfterOwnerDeadline()).toBeFalsy();
    });
});

