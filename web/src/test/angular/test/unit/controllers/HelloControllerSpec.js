"use strict";

var RESPONSE = {
    "message": "--Test Hello from rest service--"
};

describe('Hello Controller ', function () {
    var scope;

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($controller, $httpBackend, $rootScope) {
        scope = $rootScope.$new();

        $httpBackend.whenGET('/rest/message')
            .respond(RESPONSE);
        $controller('HelloCtrl', {$scope: scope});

        $httpBackend.flush();
    }));

    it('should put message from backend and put into scope', function () {
        expect(scope.messages.welcomeMessage)
            .toEqual('--Test Hello from rest service--');
    })
});