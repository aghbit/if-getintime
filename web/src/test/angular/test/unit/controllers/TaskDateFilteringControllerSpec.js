"use strict";

describe('TaskDateFiltering Controller ', function () {
    var scope, timeout;

    beforeEach(module('getintime.controllers'));

    beforeEach(inject(function ($controller, $rootScope, $timeout) {
        scope = $rootScope.$new();
        timeout = $timeout;

        scope.dates = {};

        $controller('TaskDateFilteringCtrl', {
            $scope: scope,
            $timeout: timeout
        });

    }));

    it('should set no date filters at beginning', function () {
        expect(scope.dateFilterRange).toEqual({id: 0});
    });

    it('should clear "date from" filtering', function () {
        scope.dateFrom = '11-11-2011';
        scope.clearDateFrom();
        expect(scope.dateFrom).toBeUndefined();
    });

    it('should clear "date to" filtering', function () {
        scope.dateTo = '11-11-2011';
        scope.clearDateTo();
        expect(scope.dateTo).toBeUndefined();
    });

    it('should open date from window', function () {
        scope.openDateFrom();
        timeout.flush();
        expect(scope.fromOpened).toBeTruthy();
    });

    it('should open date to window', function () {
        scope.openDateTo();
        timeout.flush();
        expect(scope.toOpened).toBeTruthy();
    });

    it("should set minDate, dates.from and radioButtonsDateRangeFilterApplied.from after $scope.dateFrom changed", function () {
        var simpleDate = '11-11-2011';
        scope.dateFrom = simpleDate;
        scope.$apply(); // needed for $watch to work
        expect(scope.minDate).toEqual(simpleDate);
        expect(scope.dates.from).toEqual(simpleDate);
        expect(scope.radioButtonsDateRangeFilterApplied.from).toBeFalsy();
    });

    it("should set dateFilterRange.id to 0 when dateFrom changed not by date filters radio buttons", function () {
        var simpleDate = '11-11-2011';
        scope.radioButtonsDateRangeFilterApplied.from = false;
        scope.dateFrom = simpleDate;
        expect(scope.dateFilterRange.id).toEqual(0);
    });

    it("should NOT set dateFilterRange.id to 0 when dateFrom changed by date filters radio buttons", function () {
        var simpleDate = '11-11-2011';
        var dateFilterRangeId = 5;
        scope.dateFilterRange.id = dateFilterRangeId;
        scope.radioButtonsDateRangeFilterApplied.from = true;
        scope.radioButtonsDateRangeFilterApplied.to = true;
        scope.dateFrom = simpleDate;
        scope.$apply();
        expect(scope.dateFilterRange.id).toEqual(dateFilterRangeId);
    });

    it("should set maxDate, dates.to and radioButtonsDateRangeFilterApplied.to after $scope.dateTo changed", function () {
        var simpleDate = '11-11-2011';
        scope.dateTo = simpleDate;
        scope.$apply(); // needed for $watch to work
        expect(scope.maxDate).toEqual(simpleDate);
        expect(scope.dates.to).toEqual(simpleDate);
        expect(scope.radioButtonsDateRangeFilterApplied.to).toBeFalsy();
    });

    it("should set dateFilterRange.id to 0 when dateTo changed not by date filters radio buttons", function () {
        var simpleDate = '11-11-2011';
        scope.radioButtonsDateRangeFilterApplied.to = false;
        scope.dateTo = simpleDate;
        expect(scope.dateFilterRange.id).toEqual(0);
    });

    it("should set $scope.radioButtonsDateRangeFilterApplied to false at start", function () {
        expect(scope.radioButtonsDateRangeFilterApplied).toEqual({from: false, to: false});
    });

    it("should set radioButtonsDateRangeFilterApplied to true when filters applied", function () {
        var filterId = 1;
        scope.applyDateFilter(filterId);
        expect(scope.radioButtonsDateRangeFilterApplied).toEqual({from: true, to: true});
    });

    it("should reset radio buttons date filters when unselected previously selected button", function () {
        var filterId = 2;
        scope.dateFilterRange.id = filterId;
        scope.applyDateFilter(filterId);
        expect(scope.dateFrom).toBeUndefined();
        expect(scope.dateTo).toBeUndefined();
        expect(scope.dateFilterRange.id).toEqual(0);
    });


    it("should radio buttons date filters when filter applied with wrong id ", function () {
        var filterId = 5;
        scope.dateFilterRange.id = undefined;
        scope.applyDateFilter(filterId);
        expect(scope.dateFrom).toBeUndefined();
        expect(scope.dateTo).toBeUndefined();
        expect(scope.dateFilterRange.id).toEqual(0);
    });

    it("should set filter at today's date", function () {
        var filterId = 1;
        var today = Date.today();
        scope.applyDateFilter(filterId);
        expect(scope.dateFrom).toEqual(today);
        expect(scope.dateTo).toEqual(today);
    });

    it("should set filters at this month", function () {
        var filterId = 3;
        scope.applyDateFilter(filterId);
        expect(scope.dateFrom).toEqual(Date.today().moveToFirstDayOfMonth());
        expect(scope.dateTo).toEqual(Date.today().moveToLastDayOfMonth());
    });

});

