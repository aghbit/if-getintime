'use strict';

describe('Comment Controller ', function () {
    var instance, scope, stateParamsMock, $dialogs, CommentsServiceMock;

    stateParamsMock = {
        id: 1
    };

    var COMMENTS = [
        {text: 'Komentarz', employee: {id: 1, firstName: 'Krzysztof', secondName: 'Slonka'}, time: '25.03.2014 18:01'},
        {text: 'Komentarz', employee: {id: 1, firstName: 'Krzysztof', secondName: 'Slonka'}, time: '25.03.2014 18:01'}
    ];

    beforeEach(module('getintime.controllers'));
    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        scope.input = {};
        scope.clearInput = jasmine.createSpy();

        var AuthenticationServiceMock = jasmine.createSpyObj("AuthenticationService", ["currentUser"]);
        AuthenticationServiceMock.currentUser = jasmine.createSpy().andReturn({"id": 1, "firstName": "Grzesiek", "secondName": "Miejski"});

        CommentsServiceMock = jasmine.createSpyObj("CommentsService", ["getComments", "saveComment"]);
        CommentsServiceMock.getComments = jasmine.createSpy().andReturn(COMMENTS);

        $dialogs = jasmine.createSpyObj("$dialogs", ["error"]);

        instance = $controller('TaskCommentController', {
            $scope: scope,
            $stateParams: stateParamsMock,
            AuthenticationService: AuthenticationServiceMock,
            CommentsService: CommentsServiceMock,
            $dialogs: $dialogs
        })
    }));

    describe('TaskCommentController', function () {
        it('must retrieve new comment', function () {
            expect(scope.comments.length).toBe(2);
        });

        it('must create new comment', function () {
            scope.input.text = 'Komentarz';
            scope.addTaskComment();
            expect(CommentsServiceMock.saveComment).toHaveBeenCalledWith({id: stateParamsMock.id},
                scope.newComment, jasmine.any(Function), jasmine.any(Function)
            );
        });
    });
});