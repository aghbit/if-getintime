"use strict";

describe('gitFocus directive ', function () {
    var scope, input, element;
     
    beforeEach(module('getintime.directives'));
    beforeEach(inject(function ($rootElement, $compile, $rootScope) {
     
        scope = $rootScope.$new();
         
        element = $rootElement.html('<input type="text" in-focus>');
         
        angular.element(document.body).append(element);
         
        $compile(element)(scope);
         
        scope.$apply();
         
        input = element.find('input'); 
    }));
     
    afterEach(inject(function ($rootElement) {
        $rootElement.remove();
    }));
     
    it('should cause element to be focused ', function () {
        expect(input[0]).toEqual(document.activeElement);
    });
});