"use strict";

describe('inNumericInput directive ', function () {
    var scope, input, element;

    var startingNumberValue = '11';

    beforeEach(module('getintime.directives'));
    beforeEach(inject(function ($rootElement, $compile, $rootScope) {

        scope = $rootScope.$new();
        scope.arg = {value :startingNumberValue};
        element = $rootElement.html('<input in-numeric-input ng-model="arg.value"/>');

        $compile(element)(scope);

        input = element.find('input')
        scope.$apply();
    }));

    afterEach(inject(function ($rootElement) {
        $rootElement.remove();
    }));

    it('should set starting ng-model value correctly', function () {
        expect(input.val()).toEqual(startingNumberValue);
    });

    it("should redo text change if it was not numeric value", function () {
        scope.arg.value = startingNumberValue+'d'
        expect(input.val()).toEqual(startingNumberValue);
    });
});