"use strict";

describe('inSortIcon directive ', function () {
    var scope, icon, element;

    beforeEach(module('getintime.directives'));
    beforeEach(inject(function ($rootElement, $compile, $rootScope) {

        scope = $rootScope.$new();
        scope.sortOrder = "ascending";

        element = angular.element('<in-sorting-icon sort-order="sortOrder"/>');

        $compile(element)(scope);

        icon = element.find('i')
    }));

    afterEach(inject(function ($rootElement) {
        $rootElement.remove();
    }));

    it('should set class to ascending', function () {
        scope.$apply();
        expect(icon.hasClass('glyphicon glyphicon-sort-by-attributes')).toBeTruthy();
    });

    it("should set class to descending", function () {
        scope.sortOrder = "descending";
        scope.$apply();
        expect(icon.hasClass('glyphicon glyphicon-sort-by-attributes-alt')).toBeTruthy();
    });

    it("should set no sorting icon class", function () {
        scope.sortOrder = "notSorted";
        scope.$apply();
        expect(icon.hasClass('glyphicon glyphicon-sort')).toBeTruthy();
    });

});