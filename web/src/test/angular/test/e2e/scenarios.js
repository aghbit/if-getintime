'use strict';

describe('Get In Time ', function () {

    describe('connection with backend', function () {
        it('should be established', function () {
            browser().navigateTo('index.html#/hello');

            expect(using('.alert:first').binding('welcomeMessage'))
                .toEqual('--Hello from rest service--');
        })
    });

    describe('user ', function () {
        it('should be redirected to "/hello" from any not matching address', function () {
            browser().navigateTo('index.html#/non/matching/address');
            expect(browser().location().url())
                .toBe('/hello');
        });

//        standard task addition
        describe('trying to add new standard task', function() {

            beforeEach(function() {
                browser().navigateTo('index.html#/task');
            });

            it('should not be allowed to add task with empty name', function () {
                expect(element(':button.btn.btn-primary.btn-large:disabled').count())
                    .toEqual(1);
            });

            it('should be allowed to add task with defined name and date', function () {
                input('task.name').enter('Some task');
//              date is set automatically. Attention: don't run this test on weekends - it fails.

                expect(element(':button.btn.btn-primary.btn-large:enabled').count())
                    .toBe(1);
            });
        });
    });
});


