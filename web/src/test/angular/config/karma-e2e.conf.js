module.exports = function(config){
    config.set({

    basePath : '../',

    files : [
        'test/e2e/**/*.js'
    ],

    reporter : 'progress',

    port : 8080,

    runnerPort : 9876,

    colors : true,

//    urlRoot : '/_karma_/',

    autoWatch : false,

    browsers : ['Chrome'],

    frameworks: ['ng-scenario'],

    singleRun : true,

    proxies : {
      '/': 'http://localhost:8080/'
    },

    plugins : [
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-ng-scenario'
            ],

    junitReporter : {
      outputFile: 'test_out/e2e.xml',
      suite: 'e2e'
    },

    logLevel : config.LOG_DEBUG

})};

