module.exports = function (config) {
    config.set({
        basePath: '../../../',
        preprocessors : {
            '**/js/*.js': 'coverage',
            'main/webapp/partials/**/*.html': 'html2js'
        },
        files: [
            'main/webapp/lib/jquery-1.11.0.min.js',
            'main/webapp/lib/angular/angular.js',
            'main/webapp/lib/angular/angular-*.js',
            'main/webapp/lib/bootstrap/*.js',
            'main/webapp/lib/angular-dialogs/*.js',
            'main/webapp/lib/date.js',
            'main/webapp/js/services/services.js',
            'main/webapp/js/controllers/controllers.js',
            'main/webapp/js/filters/filters.js',
            'main/webapp/js/directives/directives.js',
            'main/webapp/js/**/*.js',
            'main/webapp/partials/**/*.html',

            'test/angular/test/unit/**/*.js',
            'main/webapp/lib/date.js'
        ],

        exclude: [
            'main/webapp/lib/angular/angular-loader.js',
            'main/webapp/lib/angular/*.min.js',
            'main/webapp/lib/angular/angular-scenario.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['Chrome'],

        plugins: [
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-html2js-preprocessor'
        ],

        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        },

        ngHtml2JsPreprocessor: {
//            cacheIdFromPath : function(filepath) {
//                return filepath.substr(filepath.indexOf("angular")+8);
//            },
            stripPrefix: 'main/webapp/',
            // setting this option will create only a single module that contains templates
            // from all the files, so you can load them all with module('templates'),
            moduleName: 'foo'
        },
        logLevel: config.LOG_DEBUG

    });
};