# README #

Web application for organizing time and distributing tasks among employees in one of GetIn Bank departments.

Idea Factory BIT AGH Scientific Group project.

Technologies used: 

* JAVA 8
* Spring(Core, MVC, Security)
* Hibernate
* AngularJS


# LICENSE#

The software is licensed under Creative Commons Attribution-NonCommercial 4.0 International.

Full text of the license can be found on official website as well as in LICENSE file:

[https://creativecommons.org/licenses/by-nc/4.0/legalcode](https://creativecommons.org/licenses/by-nc/4.0/legalcode)