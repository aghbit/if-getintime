package ideafactory.getintime.persistence.dao.task.cyclic;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.persistence.dao.genericdao.TransactionalGenericDAO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Repository
public class CyclicTaskDAO extends TransactionalGenericDAO<CyclicTask, Long> implements
        ICyclicTaskDAO {
    @Override
    public List<CyclicTask> findAll() {
        List<CyclicTask> cyclicTaskList = super.findAll();
        for (CyclicTask cyclicTask : cyclicTaskList) {
            cyclicTask.getTasks().size();
        }
        return cyclicTaskList;
    }
}
