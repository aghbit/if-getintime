package ideafactory.getintime.persistence.dao.task.cyclic.template;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;
import ideafactory.getintime.persistence.dao.IGenericDAO;

public interface ITemplateTaskDAO extends IGenericDAO<TemplateTask, Long> {
}
