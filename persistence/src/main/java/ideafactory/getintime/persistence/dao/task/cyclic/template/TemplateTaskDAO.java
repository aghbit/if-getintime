package ideafactory.getintime.persistence.dao.task.cyclic.template;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;
import ideafactory.getintime.persistence.dao.genericdao.TransactionalGenericDAO;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class TemplateTaskDAO extends TransactionalGenericDAO<TemplateTask, Long> implements
        ITemplateTaskDAO {
    
    @Override
    @Transactional
    public List<TemplateTask> findAll() {
        List<TemplateTask> templateTaskList = super.findAll();
        for (TemplateTask templateTask : templateTaskList) {
            templateTask.getDaysOfMonth().size();
            templateTask.getDaysOfWeek().size();
        }
        return templateTaskList;
    }
}
