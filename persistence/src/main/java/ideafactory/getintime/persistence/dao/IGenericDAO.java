package ideafactory.getintime.persistence.dao;

import com.googlecode.genericdao.search.ExampleOptions;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;
import org.hibernate.Session;

import java.util.List;

/**
 * @author Michal Partyka
 */
public interface IGenericDAO<T, ID> {
    int count(ISearch search);
    
    T find(ID id);
    
    T[] find(ID... ids);
    
    List<T> findAll();
    
    void flush();
    
    T getReference(ID id);
    
    T[] getReferences(ID... ids);
    
    boolean isAttached(T entity);
    
    void refresh(T... entities);
    
    boolean remove(T entity);
    
    void remove(T... entities);
    
    void removeByIds(ID... ids);
    
    boolean removeById(ID id);
    
    boolean save(T entity);
    
    boolean[] save(T... entities);
    
    <RT> List<RT> search(ISearch search);
    
    <RT> SearchResult<RT> searchAndCount(ISearch search);
    
    <RT> RT searchUnique(ISearch search);
    
    Filter getFilterFromExample(T example);
    
    Filter getFilterFromExample(T example, ExampleOptions options);
    
    T reattach(T entity);
    
    List<T> findByExample(T example);
    
    Session getSession();
}