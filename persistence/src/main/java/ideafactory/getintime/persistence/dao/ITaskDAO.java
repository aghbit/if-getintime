package ideafactory.getintime.persistence.dao;

import ideafactory.getintime.domain.model.workingmodel.task.Task;

/**
 * @author Grzegorz Miejski on 13.12.13.
 */
public interface ITaskDAO extends IGenericDAO<Task, Long> {
}