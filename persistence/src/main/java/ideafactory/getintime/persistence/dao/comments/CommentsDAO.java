package ideafactory.getintime.persistence.dao.comments;

import ideafactory.getintime.domain.model.workingmodel.task.TaskComment;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.genericdao.TransactionalGenericDAO;
import org.springframework.stereotype.Repository;

/**
 * @author partyks
 */
@Repository
public class CommentsDAO extends TransactionalGenericDAO<TaskComment, Long> implements Comments {
}
