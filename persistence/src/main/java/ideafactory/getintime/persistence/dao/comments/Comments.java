package ideafactory.getintime.persistence.dao.comments;

import ideafactory.getintime.domain.model.workingmodel.task.TaskComment;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.IGenericDAO;

/**
 * @author partyks
 */
public interface Comments extends IGenericDAO<TaskComment, Long> {
}
