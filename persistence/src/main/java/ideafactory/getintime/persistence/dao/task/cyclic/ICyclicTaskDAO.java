package ideafactory.getintime.persistence.dao.task.cyclic;

import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.persistence.dao.IGenericDAO;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public interface ICyclicTaskDAO extends IGenericDAO<CyclicTask, Long> {
}
