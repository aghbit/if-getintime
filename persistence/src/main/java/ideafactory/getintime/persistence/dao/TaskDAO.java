package ideafactory.getintime.persistence.dao;

import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.persistence.dao.genericdao.TransactionalGenericDAO;
import org.springframework.stereotype.Repository;

/**
 * @author Grzegorz Miejski on 13.12.13.
 */
@Repository
public class TaskDAO extends TransactionalGenericDAO<Task, Long> implements ITaskDAO {
}