package ideafactory.getintime.persistence.dao.employees;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.IGenericDAO;

/**
 * @author partyks
 */
public interface IEmployeeDAO extends IGenericDAO<Employee, Long> {
}
