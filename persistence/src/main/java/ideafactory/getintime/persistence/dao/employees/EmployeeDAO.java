package ideafactory.getintime.persistence.dao.employees;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.persistence.dao.genericdao.TransactionalGenericDAO;
import org.springframework.stereotype.Repository;

/**
 * @author partyks
 */
@Repository
public class EmployeeDAO extends TransactionalGenericDAO<Employee, Long> implements IEmployeeDAO {
}
