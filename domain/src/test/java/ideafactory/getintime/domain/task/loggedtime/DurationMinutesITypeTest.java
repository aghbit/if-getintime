package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;
import org.hibernate.usertype.UserType;
import org.junit.Before;
import org.junit.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Piotr Góralczyk
 */
public abstract class DurationMinutesITypeTest {
    protected UserType durationMinutesIType;
    protected Class<?> returnedClass;
    
    protected abstract IDurationMinutes getNewInstance(long minutes);
    
    @Before
    public abstract void setUp() throws Exception;
    
    @Test
    public void testReturnedClass() throws Exception {
        assertThat(durationMinutesIType.returnedClass()).isEqualTo(returnedClass);
    }
    
    @Test
    public void testNullSafeGetWithNullValue() throws Exception {
        PreparedStatement resultSet = mock(PreparedStatement.class);
        
        durationMinutesIType.nullSafeSet(resultSet, null, 0, null);
        
        verify(resultSet).setNull(0, Types.BIGINT);
    }
    
    @Test
    public void testNullSafeGet() throws Exception {
        PreparedStatement resultSet = mock(PreparedStatement.class);
        
        durationMinutesIType.nullSafeSet(resultSet, getNewInstance(10L), 0, null);
        
        verify(resultSet).setLong(0, 10L);
    }
    
    @Test
    public void testNullSafeSet() throws Exception {
        ResultSet resultSet = mock(ResultSet.class);
        
        when(resultSet.getLong("name")).thenReturn(20L);
        
        Object o = durationMinutesIType.nullSafeGet(resultSet, new String[] { "name" }, null, null);
        
        assertThat(o).isEqualTo(getNewInstance(20L));
    }
    
    @Test
    public void testIsMutable() throws Exception {
        assertThat(durationMinutesIType.isMutable()).isFalse();
    }
}
