package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;

/**
 * @author Piotr Góralczyk
 */
public class EstimatedTimeTypeTest extends DurationMinutesITypeTest {
    @Override
    protected IDurationMinutes getNewInstance(long minutes) {
        return new EstimatedTime(minutes);
    }
    
    @Override
    public void setUp() throws Exception {
        returnedClass = EstimatedTime.class;
        durationMinutesIType = new EstimatedTimeType();
    }
}
