package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.LoggedTime;

/**
 * @author Michal Partyka
 */
public class LoggedTimeTypeTest extends DurationMinutesITypeTest {
    @Override
    protected IDurationMinutes getNewInstance(long minutes) {
        return new LoggedTime().addMinutes(minutes);
    }
    
    @Override
    public void setUp() throws Exception {
        returnedClass = LoggedTime.class;
        durationMinutesIType = new LoggedTimeType();
    }
}
