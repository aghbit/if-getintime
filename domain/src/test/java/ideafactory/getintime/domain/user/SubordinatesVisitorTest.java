package ideafactory.getintime.domain.user;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.model.workingmodel.user.EmployeeFactory;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Michal Partyka
 */
public class SubordinatesVisitorTest {
    private SubordinatesVisitor subordinatesVisitor;
    
    @Before
    public void setUp() throws Exception {
        subordinatesVisitor = new SubordinatesVisitor();
    }
    
    @Test
    public void testVisit() throws Exception {
        Employee employee = EmployeeFactory.createEmployee(1L);
        Employee employee2 = EmployeeFactory.createEmployee(2L);
        Employee employee3 = EmployeeFactory.createEmployee(3L);
        
        subordinatesVisitor.visit(employee);
        subordinatesVisitor.visit(employee2);
        subordinatesVisitor.visit(employee3);
        
        assertThat(subordinatesVisitor.getSubordinates()).hasSize(3);
        assertThat(subordinatesVisitor.getSubordinates()).containsExactly(employee, employee2,
                employee3);
    }
    
}
