package ideafactory.getintime.domain.model.workingmodel.task.cyclic;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import de.jollyday.HolidayManager;
import ideafactory.geintime.commons.dayoff.DayOffManager;
import ideafactory.geintime.commons.dayoff.WeekendDayOffManager;
import ideafactory.geintime.commons.preconditions.IllegalStateStatusException;
import ideafactory.geintime.commons.preconditions.NullPointerStatusException;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.model.workingmodel.task.TaskPropertiesBuilder;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.task.cyclic.CyclicTaskBuilder;
import org.junit.Before;
import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Set;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.hasEntry;

/**
 * @author Michal Partyka
 */
public class CyclicTaskBuilderTest {
    private static final String NAME = "taskName";
    private static final String DESCRIPTION = "taskDesc";
    
    private static final TaskDescription TASK_EXAMPLE = new TaskDescription(NAME, DESCRIPTION);
    
    private static final TemplateTask TEMPLATE_TASK_EXAMPLE = new TemplateTaskBuilder()
            .withTaskDescription(TASK_EXAMPLE)
            .withTaskPriorities(new TaskPropertiesBuilder().build()).everyDayOfMonth(1, 5, 10)
            .build();
    
    private static final String HOLIDAY_FILE_SUFFIX = "test";
    private static final HolidayManager HOLIDAY_MANAGER = HolidayManager
            .getInstance(HOLIDAY_FILE_SUFFIX);
    private static final DayOffManager DAY_OFF_MANAGER = new WeekendDayOffManager(HOLIDAY_MANAGER);
    
    private CyclicTaskBuilder instance;
    
    @Before
    public void setUp() throws Exception {
        instance = new CyclicTaskBuilder();
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldHaveTaskTemplateForCreation() throws Exception {
        instance.withDayOffManager(DAY_OFF_MANAGER).build();
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldHaveDayOffManagerForCreation() throws Exception {
        instance.withTemplateTask(TEMPLATE_TASK_EXAMPLE).build();
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionIfToDateIsAfterFromDate() throws Exception {
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK_EXAMPLE)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2013, 1, 1))
                .to(LocalDate.of(2012, 12, 12)).build();
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionIfFromDateIsDayOff() throws Exception {
        instance.withTemplateTask(TEMPLATE_TASK_EXAMPLE).withDayOffManager(DAY_OFF_MANAGER)
                .from(LocalDate.of(2014, 5, 1)).to(LocalDate.of(2014, 5, 5)).build();
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionIfGetDayOffMovedTermsIsCalledBeforeBuild() throws Exception {
        instance.withTemplateTask(TEMPLATE_TASK_EXAMPLE).withDayOffManager(DAY_OFF_MANAGER)
                .from(LocalDate.of(2014, 5, 2)).to(LocalDate.of(2014, 5, 20));
        instance.getDayOffMovedTerms();
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionIfGetEndOfMonthMovedTermsIsCalledBeforeBuild() throws Exception {
        instance.withTemplateTask(TEMPLATE_TASK_EXAMPLE).withDayOffManager(DAY_OFF_MANAGER)
                .from(LocalDate.of(2014, 5, 2)).to(LocalDate.of(2014, 5, 20));
        instance.getEndOfMonthMovedTerms();
    }
    
    @Test
    public void shouldBuildCyclicTaskFromGivenTask() throws Exception {
        CyclicTask cyclicTask = instance.from(LocalDate.of(2014, 5, 5))
                .to(LocalDate.of(2015, 5, 5)).withTemplateTask(TEMPLATE_TASK_EXAMPLE)
                .withDayOffManager(DAY_OFF_MANAGER).build();
        
        assertThat(cyclicTask.getTemplateTask().getTaskDescription().getDescription()).isEqualTo(
                DESCRIPTION);
    }
    
    @Test
    public void shouldCreateCyclicTaskAccordingToDaysOfMonthWithDaysOffMoved() throws Exception {
        LocalDate from = LocalDate.of(2013, 10, 1);
        LocalDate to = LocalDate.of(2013, 12, 30);
        Term term1 = new Term(LocalDate.of(2013, 10, 1));
        Term term2Moved = new Term(LocalDate.of(2013, 10, 4)); // 5.10.2013 is
                                                               // Saturday
        Term term3 = new Term(LocalDate.of(2013, 10, 10));
        Term term4 = new Term(LocalDate.of(2013, 10, 15));
        Term term5Moved = new Term(LocalDate.of(2013, 10, 31)); // 1.11.2013 is
                                                                // a holiday
        Term term6 = new Term(LocalDate.of(2013, 11, 5));
        Term term7Moved = new Term(LocalDate.of(2013, 11, 8)); // 10.11.2013 is
                                                               // Sunday
        Term term8 = new Term(LocalDate.of(2013, 11, 15));
        Term term9Moved = new Term(LocalDate.of(2013, 11, 29)); // 1.12.2013 is
                                                                // Sunday
        Term term10 = new Term(LocalDate.of(2013, 12, 5));
        Term term11 = new Term(LocalDate.of(2013, 12, 10));
        Term term12Moved = new Term(LocalDate.of(2013, 12, 13)); // 15.12.2013
                                                                 // is Sunday
        
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfMonth(Lists.newArrayList(1, 5, 10, 15)).build();
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(from).to(to).build();
        
        assertThat(cyclicTask.getTasks()).hasSize(12);
        assertThat(cyclicTask.getTasks()).onProperty("assigneeDeadline").contains(term1,
                term2Moved, term3, term4, term5Moved, term6, term7Moved, term8, term9Moved, term10,
                term11, term12Moved);
        assertThat(cyclicTask.getTasks()).onProperty("ownerDeadline").contains(term1, term2Moved,
                term3, term4, term5Moved, term6, term7Moved, term8, term9Moved, term10, term11,
                term12Moved);
    }
    
    @Test
    public void shouldCreateCyclicTaskAccordingToDaysOfMonthWithEndOfMonthTermsMoved()
            throws Exception {
        Term term1Moved = new Term(LocalDate.of(2012, 02, 29)); // 30.02.2012
                                                                // and
                                                                // 31.02.2012
                                                                // don't exist
        Term term2Moved = new Term(LocalDate.of(2012, 03, 30)); // 31.03.2012
                                                                // is a
                                                                // Saturday
        Term term3Moved = new Term(LocalDate.of(2012, 04, 30)); // 31.04.2012
                                                                // doesn't
                                                                // exist
        Term term7 = new Term(LocalDate.of(2012, 05, 30));
        Term term8 = new Term(LocalDate.of(2012, 05, 31));
        
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build()).everyDayOfMonth(30, 31)
                .build();
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2012, 02, 15))
                .to(LocalDate.of(2012, 06, 15)).build();
        
        assertThat(cyclicTask.getTasks()).hasSize(5);
        assertThat(cyclicTask.getTasks()).onProperty("assigneeDeadline").contains(term1Moved,
                term2Moved, term3Moved, term7, term8);
        assertThat(cyclicTask.getTasks()).onProperty("ownerDeadline").contains(term1Moved,
                term2Moved, term3Moved, term7, term8);
    }
    
    @Test
    public void shouldNotCreateDoubledTasksFromDaysOfMonth() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfMonth(1, 1, 1, 1).build();
        
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2013, 10, 1))
                .to(LocalDate.of(2013, 10, 30)).build();
        
        assertThat(cyclicTask.getTasks()).hasSize(1);
    }
    
    @Test
    public void shouldCreateCyclicTaskAccordingToDaysOfWeekWithDaysOffMoved() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfWeek(DayOfWeek.MONDAY, DayOfWeek.THURSDAY, DayOfWeek.SUNDAY).build();
        
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2014, 4, 28))
                .to(LocalDate.of(2014, 5, 5)).build();
        Term term1 = new Term(LocalDate.of(2014, 4, 28));
        Term term2Moved = new Term(LocalDate.of(2014, 4, 30)); // Thursday
                                                               // 1.05.2014 is a
                                                               // holiday
        Term term3Moved = new Term(LocalDate.of(2014, 5, 2)); // Sunday
                                                              // 4.05.2014
        Term term4 = new Term(LocalDate.of(2014, 5, 5));
        
        assertThat(cyclicTask.getTasks()).hasSize(4);
        assertThat(cyclicTask.getTasks()).onProperty("assigneeDeadline").contains(term1,
                term2Moved, term3Moved, term4);
        assertThat(cyclicTask.getTasks()).onProperty("ownerDeadline").contains(term1, term2Moved,
                term3Moved, term4);
    }
    
    @Test
    public void shouldNotCreateDoubledTasksFromDaysOfWeek() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfWeek(DayOfWeek.MONDAY, DayOfWeek.MONDAY, DayOfWeek.MONDAY).build();
        
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2013, 10, 7))
                .to(LocalDate.of(2013, 10, 11)).build();
        
        assertThat(cyclicTask.getTasks()).hasSize(1);
    }
    
    @Test
    public void shouldNotCreateDoubledTasksEvenIfConstructedFromBothWeekDayAndMonthDay()
            throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfWeek(DayOfWeek.MONDAY, DayOfWeek.THURSDAY)
                .everyDayOfMonth(18, 19, 21, 25).build();
        
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2013, 11, 18))
                .to(LocalDate.of(2013, 11, 26)).build();
        Term term1 = new Term(LocalDate.of(2013, 11, 18));
        Term term4 = new Term(LocalDate.of(2013, 11, 19));
        Term term2 = new Term(LocalDate.of(2013, 11, 21));
        Term term3 = new Term(LocalDate.of(2013, 11, 25));
        
        assertThat(cyclicTask.getTasks()).hasSize(4);
        assertThat(cyclicTask.getTasks()).onProperty("assigneeDeadline").contains(term1, term2,
                term3, term4);
        assertThat(cyclicTask.getTasks()).onProperty("ownerDeadline").contains(term1, term2, term3,
                term4);
    }
    
    @Test
    public void shouldNotCreateDoubledTasksIfTasksNeedToBeMoved() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfMonth(30, 31, 1, 3, 4).build();
        
        CyclicTask cyclicTask = instance.withTemplateTask(TEMPLATE_TASK)
                .withDayOffManager(DAY_OFF_MANAGER).from(LocalDate.of(2014, 4, 30))
                .to(LocalDate.of(2014, 5, 5)).build();
        Term term1Moved = new Term(LocalDate.of(2014, 4, 30)); // 31.04.2014
                                                               // doesn't exist;
                                                               // 1.05.2014 is a
                                                               // holiday
        Term term2Moved = new Term(LocalDate.of(2014, 5, 2)); // 3.05.2014 and
                                                              // 4.05.2014 are
                                                              // Saturday and
                                                              // Sunday
        
        assertThat(cyclicTask.getTasks()).hasSize(2);
        assertThat(cyclicTask.getTasks()).onProperty("assigneeDeadline").contains(term1Moved,
                term2Moved);
        assertThat(cyclicTask.getTasks()).onProperty("ownerDeadline").contains(term1Moved,
                term2Moved);
    }
    
    @Test
    public void shouldRecordTasksMovedBecauseOfADayOff() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfMonth(30, 1, 3, 4).everyDayOfWeek(DayOfWeek.THURSDAY).build();
        
        instance.withTemplateTask(TEMPLATE_TASK).withDayOffManager(DAY_OFF_MANAGER)
                .from(LocalDate.of(2014, 4, 30)).to(LocalDate.of(2014, 5, 5)).build();
        Term term1 = new Term(LocalDate.of(2014, 5, 1)); // holiday
        Term term2 = new Term(LocalDate.of(2014, 5, 3)); // Saturday
        Term term3 = new Term(LocalDate.of(2014, 5, 4)); // Sunday
        
        Set<Term> term1Set = Sets.newHashSet(term1);
        Set<Term> term23Set = Sets.newHashSet(term2, term3);
        
        Term term1Moved = new Term(LocalDate.of(2014, 4, 30));
        Term term23Moved = new Term(LocalDate.of(2014, 5, 2));
        
        assertThat(instance.getDayOffMovedTerms().entrySet()).hasSize(2);
        org.junit.Assert.assertThat(instance.getDayOffMovedTerms(),
                allOf(hasEntry(term1Moved, term1Set), hasEntry(term23Moved, term23Set)));
    }
    
    @Test
    public void shouldRecordTasksMovedBecauseOfEndOfMonth() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build())
                .everyDayOfMonth(29, 30, 31).everyDayOfWeek(DayOfWeek.THURSDAY, DayOfWeek.FRIDAY)
                .build();
        
        instance.withTemplateTask(TEMPLATE_TASK).withDayOffManager(DAY_OFF_MANAGER)
                .from(LocalDate.of(2015, 2, 27)).to(LocalDate.of(2015, 5, 27)).build();
        
        Term term1Moved = new Term(LocalDate.of(2015, 2, 28));
        Term term2Moved = new Term(LocalDate.of(2015, 4, 30));
        
        Set<Integer> term1DaysOfMonth = Sets.newHashSet(29, 30, 31);
        Set<Integer> term2DaysOfMonth = Sets.newHashSet(31);
        
        assertThat(instance.getEndOfMonthMovedTerms().entrySet()).hasSize(2);
        org.junit.Assert.assertThat(
                instance.getEndOfMonthMovedTerms(),
                allOf(hasEntry(term1Moved, term1DaysOfMonth),
                        hasEntry(term2Moved, term2DaysOfMonth)));
    }
    
    @Test
    public void shouldNotRecordTasksNotMoved() throws Exception {
        final TemplateTask TEMPLATE_TASK = new TemplateTaskBuilder()
                .withTaskDescription(TASK_EXAMPLE)
                .withTaskPriorities(new TaskPropertiesBuilder().build()).everyDayOfMonth(5, 7)
                .everyDayOfWeek(DayOfWeek.THURSDAY).build();
        
        instance.withTemplateTask(TEMPLATE_TASK).withDayOffManager(DAY_OFF_MANAGER)
                .from(LocalDate.of(2014, 5, 2)).to(LocalDate.of(2014, 5, 11)).build();
        
        assertThat(instance.getDayOffMovedTerms().entrySet()).isEmpty();
        assertThat(instance.getEndOfMonthMovedTerms().entrySet()).isEmpty();
    }
}
