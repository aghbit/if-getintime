package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

import ideafactory.geintime.commons.preconditions.IllegalStateStatusException;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Piotr Góralczyk
 */
public class EstimatedTimeTest {
    private EstimatedTime instance;
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionIfMinutesAreBelowZero() throws Exception {
        final long minutes = -1L;
        
        instance = new EstimatedTime(minutes);
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionIfMinutesAreZero() throws Exception {
        final long minutes = 0L;
        
        instance = new EstimatedTime(minutes);
    }
    
    @Test
    public void shouldCreateEstimatedTimeFromMinutes() throws Exception {
        final long minutes = 123L;
        
        instance = new EstimatedTime(minutes);
        
        assertThat(instance.getMinutes()).isEqualTo(minutes);
    }
}
