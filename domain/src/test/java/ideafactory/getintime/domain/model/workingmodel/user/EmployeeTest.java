package ideafactory.getintime.domain.model.workingmodel.user;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import ideafactory.geintime.commons.preconditions.IllegalStateStatusException;
import ideafactory.geintime.commons.preconditions.NullPointerStatusException;
import ideafactory.getintime.domain.exception.NoRequiredRoleException;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;
import ideafactory.getintime.domain.visitorapi.IEmployeeVisitor;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Michal Partyka
 */
public class EmployeeTest {
    private static final long MINUTES_TO_LOG = 10;
    private static final int WORK_PERCENTAGE = 20;
    private final long INSTANCE_ID = 1L;
    private Employee instance;
    
    private Task task;
    
    @Before
    public void setUp() throws Exception {
        instance = EmployeeFactory.createEmployee(INSTANCE_ID);
        task = mock(Task.class);
    }
    
    @Test
    public void shouldBeAComposite() throws Exception {
        Employee employee = EmployeeFactory.createEmployee(2L);
        instance.addSubordinate(employee);
        
        Iterator<Employee> iterator = instance.iterator();
        
        assertThat(Lists.newArrayList(iterator)).containsExactly(employee);
    }
    
    @Test
    public void shouldStoreTwoSameEmployeesAsOne() throws Exception {
        Employee employee = EmployeeFactory.createEmployee(2L);
        instance.addSubordinate(employee);
        instance.addSubordinate(employee);
        
        Iterator<Employee> iterator = instance.iterator();
        
        assertThat(Lists.newArrayList(iterator)).hasSize(1);
    }
    
    @Test
    public void shouldBeVisitableComposite() throws Exception {
        IEmployeeVisitor visitor = mock(IEmployeeVisitor.class);
        Employee employee = EmployeeFactory.createEmployee(3L);
        instance.addSubordinate(employee);
        
        instance.accept(visitor);
        
        verify(visitor).visit(instance);
        verify(visitor).visit(employee);
    }
    
    @Test
    public void shouldReturnCurrentWorkingTask() throws Exception {
        Task task = mock(Task.class);
        when(task.isBeingWorkedOn()).thenReturn(true);
        instance.setTasks(Sets.newHashSet(task));
        
        assertThat(instance.getCurrenWorkingTask().get()).isEqualTo(task);
    }
    
    @Test
    public void shouldReturnEmptyOptionalWhenNotWorkingOnTask() throws Exception {
        
        Task task = mock(Task.class);
        when(task.isBeingWorkedOn()).thenReturn(false);
        
        instance.setTasks(Sets.newHashSet(task));
        
        assertThat(instance.getCurrenWorkingTask().isPresent()).isFalse();
    }
    
    @Test
    public void shouldReturnSubordinatesTasks() throws Exception {
        final int numberOfSubordinates = 6;
        List<Employee> employees = new ArrayList<>(numberOfSubordinates);
        List<Task> tasks = new ArrayList<>(numberOfSubordinates);
        
        for (int i = 0; i < numberOfSubordinates; i++) {
            employees.add(EmployeeFactory.createEmployee((long) (i + 2)));
            tasks.add(mock(Task.class));
            employees.get(i).setTasks(new HashSet<>(Arrays.asList(tasks.get(i))));
            
        }
        instance.setTasks(new HashSet<>(Arrays.asList(mock(Task.class))));
        
        instance.addSubordinate(employees.get(0));
        instance.addSubordinate(employees.get(1));
        employees.get(0).addSubordinate(employees.get(2));
        employees.get(0).addSubordinate(employees.get(3));
        
        employees.get(1).addSubordinate(employees.get(4));
        employees.get(1).addSubordinate(employees.get(5));
        
        assertThat(instance.getSubordinatesTasks()).hasSize(numberOfSubordinates);
        for (Task task : tasks) {
            assertThat(instance.getSubordinatesTasks()).contains(task);
        }
        
    }
    
    @Test
    public void shouldReturnTrueIfContainsSelectedRole() throws Exception {
        // given
        EmployeeType employeeType = EmployeeType.DIRECTOR;
        instance.setType(employeeType);
        // when
        boolean hasRole = instance.hasAnyRole(employeeType);
        // then
        assertThat(hasRole).isTrue();
    }
    
    @Test
    public void shouldReturnFalseIfNotHavingSelectedRole() throws Exception {
        // given
        instance.setType(EmployeeType.DIRECTOR);
        // when
        boolean hasRole = instance.hasAnyRole(EmployeeType.MANAGER);
        // then
        assertThat(hasRole).isFalse();
    }
    
    @Test
    public void shouldReturnTrueIfRoleIsOneOfSpecified() throws Exception {
        // given
        EmployeeType employeeType = EmployeeType.DIRECTOR;
        instance.setType(employeeType);
        // when
        boolean hasRole = instance.hasAnyRole(employeeType, EmployeeType.MANAGER);
        // then
        assertThat(hasRole).isTrue();
    }
    
    @Test
    public void shouldStartWorkingOnTask() throws Exception {
        // given
        when(task.getAssigned()).thenReturn(instance);
        // when
        instance.startWorkingOnTask(task);
        // then
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenStartWorkingOnNotYourTask() throws Exception {
        // given
        
        // when
        instance.startWorkingOnTask(task);
        // then
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenStartWorkingOnNullTask() throws Exception {
        // given
        
        // when
        instance.startWorkingOnTask(null);
        // then
        
    }
    
    @Test
    public void shouldStopWorkingOnTask() throws Exception {
        // given
        when(task.getAssigned()).thenReturn(instance);
        // when
        instance.stopWorkingOnTask(task, LocalDateTime.now());
        // then
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenStopWorkingOnNotYourTask() throws Exception {
        // given
        
        // when
        instance.stopWorkingOnTask(task, LocalDateTime.now());
        // then
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenStopWorkingOnNullTask() throws Exception {
        // given
        
        // when
        instance.stopWorkingOnTask(null, LocalDateTime.now());
        // then
        
    }
    
    @Test
    public void shouldFinishWorkingOnTask() throws Exception {
        // given
        when(task.getAssigned()).thenReturn(instance);
        // when
        instance.doneWorkingOnTask(task, LocalDateTime.now());
        // then
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenFinishWorkingOnNotYourTask() throws Exception {
        // given
        
        // when
        instance.doneWorkingOnTask(task, LocalDateTime.now());
        // then
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenFinishWorkingOnNullTask() throws Exception {
        // given
        
        // when
        instance.doneWorkingOnTask(null, LocalDateTime.now());
        // then
    }
    
    @Test(expected = NoRequiredRoleException.class)
    public void shouldThrowExceptionWhenCancellingTaskAsNormalUser() throws Exception {
        // given
        instance.setType(EmployeeType.NORMAL);
        // when
        instance.cancelTask(task, LocalDateTime.now());
        // then
    }
    
    @Test
    public void shouldCancelTask() throws Exception {
        // given
        instance.setType(EmployeeType.DIRECTOR);
        // when
        instance.cancelTask(task, LocalDateTime.now());
        // then
        verify(task).cancelTask(any(LocalDateTime.class));
    }
    
    @Test
    public void shouldLogTimeToTask() throws Exception {
        // given
        when(task.getAssigned()).thenReturn(instance);
        // when
        instance.logTime(task, MINUTES_TO_LOG);
        // then
        verify(task).logTimeForAssignee(any(Consumer.class));
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenLoggingTimeToNullTask() throws Exception {
        // given
        
        // when
        instance.logTime(null, MINUTES_TO_LOG);
        // then
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenLoggingTimeToNotYourTask() throws Exception {
        // given
        
        // when
        instance.logTime(task, MINUTES_TO_LOG);
        // then
    }
    
    @Test
    public void shouldUpdateWorkProgressOfTask() throws Exception {
        // given
        when(task.getAssigned()).thenReturn(instance);
        // when
        instance.updateWorkProgressPercentage(task, WORK_PERCENTAGE);
        // then
        verify(task).updateWorkProgressPercentage(WORK_PERCENTAGE);
    }
    
    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenUpdatingProgressPercentageOfNullTask() throws Exception {
        // given
        
        // when
        instance.updateWorkProgressPercentage(null, WORK_PERCENTAGE);
        // then
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenUpdatingPercentageOfNotYourTask() throws Exception {
        // given
        
        // when
        instance.updateWorkProgressPercentage(task, WORK_PERCENTAGE);
        // then
    }
    
}
