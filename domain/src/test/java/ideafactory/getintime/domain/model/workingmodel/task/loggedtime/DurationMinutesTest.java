package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Piotr Góralczyk
 */
public class DurationMinutesTest {
    private DurationMinutes instance;
    private static final long minutes = 123L;
    
    @Before
    public void setUp() throws Exception {
        instance = new DurationMinutes(minutes);
    }
    
    @Test
    public void shouldCreateDurationMinutesFromMinutes() throws Exception {
        assertThat(instance.getMinutes()).isEqualTo(minutes);
    }
    
    @Test
    public void addingMinutesShouldBePossible() throws Exception {
        final long minutesToAdd = 10L;
        
        final DurationMinutes instanceAddedTo = instance.plusMinutes(minutesToAdd);
        
        assertThat(instanceAddedTo.getMinutes()).isEqualTo(minutes + minutesToAdd);
    }
}
