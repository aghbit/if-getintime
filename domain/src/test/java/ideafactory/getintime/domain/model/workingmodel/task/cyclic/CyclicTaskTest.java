package ideafactory.getintime.domain.model.workingmodel.task.cyclic;

import com.google.common.collect.Lists;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.model.workingmodel.task.TaskProperties;
import ideafactory.getintime.domain.model.workingmodel.task.TaskPropertiesBuilder;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Michal Partyka
 */
public class CyclicTaskTest {
    private final String DESCRIPTION = "Ala has a cat";
    private final String NAME = "do some report";
    private CyclicTask instance;
    
    @Before
    public void setUp() throws Exception {
        TaskDescription taskDescription = new TaskDescription(NAME, DESCRIPTION);
        TaskProperties properties = new TaskPropertiesBuilder().build();
        TemplateTask templateTask = new TemplateTaskBuilder().withTaskDescription(taskDescription)
                .withTaskPriorities(properties).build();
        instance = new CyclicTask(templateTask);
    }
    
    @Test
    public void shouldAddedTasksBeAggregated() throws Exception {
        Term deadline1 = new Term(LocalDate.ofEpochDay(10L));
        Term deadline2 = new Term(LocalDate.ofEpochDay(20L));
        
        instance.addAll(Lists.newArrayList(deadline1, deadline2));
        
        assertThat(instance.getTasks()).hasSize(2);
        assertThat(instance.getTasks()).onProperty("description").containsOnly(DESCRIPTION,
                DESCRIPTION);
        assertThat(instance.getTasks()).onProperty("assigneeDeadline").containsOnly(deadline1,
                deadline2);
        assertThat(instance.getTasks()).onProperty("ownerDeadline").containsOnly(deadline1,
                deadline2);
    }
}
