package ideafactory.getintime.domain.model.workingmodel.task;

import com.google.common.collect.Lists;
import ideafactory.geintime.commons.preconditions.IllegalStateStatusException;
import ideafactory.geintime.commons.preconditions.NullPointerStatusException;
import ideafactory.getintime.domain.exception.ProgressPercentageException;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.LoggedTime;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Percentage;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.model.workingmodel.task.workingstartdate.WorkingStartDate;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.model.workingmodel.user.EmployeeFactory;
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;
import ideafactory.getintime.domain.task.TaskBuilder;
import ideafactory.getintime.domain.task.properties.TaskStatus;
import ideafactory.getintime.domain.task.properties.TaskType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Michal Partyka
 */
@RunWith(value = MockitoJUnitRunner.class)
public class TaskTest {
    private final long INSTANCE_ID = 1L;
    private Task instance;

    @Mock
    private Employee createdByMock;

    @Mock
    private Term assigneeDeadline;

    @Mock
    private Term ownerDeadline;

    @Mock
    private WorkingStartDate workingStartDate;

    private static final int TIME_WORKING_MINUTES = 100;
    private static final long MINUTES_TO_ADD = 10;
    private static final Consumer<LoggedTime> LOGGED_TIME_CONSUMER = x -> x.addMinutes(MINUTES_TO_ADD);

    private LocalDateTime CURRENT_TIME;
    private LocalDate CURRENT_DATE;
    private Duration DEFAULT_DURATION;

    private Employee assignee;
    private int PROGRESS_PERCENTAGE;
    private final String TEST_DESCRIPTION = "description";
    private final String TASK_NAME = "name";
    private EstimatedTime estimatedTimeMock;

    private Task subtask;
    private Employee currentUser;

    public TaskTest() {
    }

    @Before
    public void setUp() throws Exception {
        CURRENT_TIME = LocalDateTime.of(2000, 1, 2, 3, 20);
        CURRENT_DATE = CURRENT_TIME.toLocalDate();
        given(ownerDeadline.getDate()).willReturn(CURRENT_TIME.plusDays(2).toLocalDate());
        given(assigneeDeadline.getDate()).willReturn(CURRENT_TIME.plusDays(1).toLocalDate());

        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setAssigneeDeadline(assigneeDeadline).setOwnerDeadline(ownerDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        DEFAULT_DURATION = Duration.ofMinutes(TIME_WORKING_MINUTES);
        assignee = new Employee();
        assignee.setId(1L);
        PROGRESS_PERCENTAGE = 10;
        subtask = mock(Task.class);
        currentUser = new Employee();
        currentUser.setId(2L);
    }

    private Task createDefaultTaskWithId(Long id) {
        return new TaskBuilder().setId(id).setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setAssigneeDeadline(assigneeDeadline).setOwnerDeadline(ownerDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME)
                .setEstimatedTime(estimatedTimeMock).createTask();
    }

    @Test
    public void shouldNotRecordLoggedTimeOnCreation() {
        assertEquals(0, instance.getLoggedTimes().size());
    }

    @Test
    public void shouldRecordLoggedTime() {
        Employee assignee1 = new Employee();
        assignee1.setId(1L);

        Employee assignee2 = new Employee();
        assignee2.setId(2L);

        Consumer<LoggedTime> loggedTimeAdder = x -> x.addMinutes(MINUTES_TO_ADD);

        instance.assign(assignee1);
        instance.logTimeForAssignee(loggedTimeAdder);
        instance.assign(assignee2);
        instance.logTimeForAssignee(loggedTimeAdder);
        instance.assign(assignee1);
        instance.logTimeForAssignee(loggedTimeAdder);
        instance.assign(null);

        assertThat(instance.getLoggedTimes()).hasSize(2);
        assertEquals(2 * MINUTES_TO_ADD, instance.getLoggedTimes().get(assignee1).getMinutes());
        assertEquals(MINUTES_TO_ADD, instance.getLoggedTimes().get(assignee2).getMinutes());
    }

    @Test
    public void shouldBeAComposite() throws Exception {
        Task task = createDefaultTaskWithId(1L);
        instance.addSubTask(task);

        Iterator<Task> iterator = instance.iterator();

        List<Task> subTasks = Lists.newArrayList(iterator);
        assertThat(subTasks).hasSize(1);
        assertThat(subTasks).containsExactly(task);
    }

    @Test
    public void shouldStoreDoubleTasksAsSingleTask() throws Exception {
        Task task = createDefaultTaskWithId(2L);
        Task task2 = createDefaultTaskWithId(2L);
        instance.addSubTask(task);
        instance.addSubTask(task2);

        Iterator<Task> iterator = instance.iterator();

        List<Task> subTasks = Lists.newArrayList(iterator);
        assertThat(subTasks).hasSize(1);
    }

    @Test
    public void shouldSetWorkingStartDateOnStart() throws Exception {
        Employee employee = mock(Employee.class);
        instance.assign(employee);

        when(employee.getCurrenWorkingTask()).thenReturn(Optional.empty());

        instance.startWorking();

        assertThat(instance.getWorkingStartDateOptional()).isNotNull();
    }

    @Test
    public void shouldChangeStatusToStoppedWhenLoggedTime() throws Exception {
        instance.assign(assignee);

        instance.logTimeForAssignee((x) -> x.addMinutes(20));

        assertThat(instance.getStatus()).isEqualTo(TaskStatus.STOPPED);
    }

    @Test
    public void shouldAssigningWorkProperly() throws Exception {
        Employee employee = EmployeeFactory.createEmployee(10L);

        instance.assign(EmployeeFactory.createEmployee(10L));

        assertThat(instance.getAssigned().getId()).isEqualTo(employee.getId());
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenAlreadyWorkingOnTask() throws Exception {
        Employee employee = mock(Employee.class);
        instance.assign(employee);
        Task currentWorkingTask = mock(Task.class);
        when(employee.isWorkingOnTask()).thenReturn(true);
        when(employee.getCurrenWorkingTask()).thenReturn(Optional.of(currentWorkingTask));

        instance.startWorking();

    }

    @Test
    public void shouldSetStateToWorkingDuringStart() throws Exception {
        Employee employee = mock(Employee.class);
        instance.assign(employee);
        when(employee.getCurrenWorkingTask()).thenReturn(Optional.empty());

        instance.startWorking();

        assertThat(instance.getStatus()).isEqualTo(TaskStatus.IN_PROGRESS);

    }

    @Test
    public void shouldSetToDoStatusAfterTaskCreation() {
        assertThat(instance.getStatus()).isEqualTo(TaskStatus.TODO);
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenStartingUnassignedTask() {
        instance.startWorking();
    }

    @Test
    public void shouldSetNewWorkingStartDateDuringWorkStart() {
        instance.assign(assignee);

        instance.startWorking();

        assertThat(instance.getWorkingStartDateOptional().isPresent()).isTrue();
    }

    @Test
    public void shouldChangeStatusToStopped() throws ProgressPercentageException {
        Employee employee = mock(Employee.class);
        instance.setStatus(TaskStatus.IN_PROGRESS);
        instance.assign(employee);
        instance.setWorkingStartDate(workingStartDate);
        when(workingStartDate.getTimeDifferenceFromDate(CURRENT_TIME)).thenReturn(DEFAULT_DURATION);

        instance.stopWorking(CURRENT_TIME);

        assertThat(instance.getStatus()).isEqualTo(TaskStatus.STOPPED);
    }

    @Test
    public void shouldSetWorkingStartDateToNull() throws ProgressPercentageException {
        Employee employee = mock(Employee.class);
        instance.setStatus(TaskStatus.IN_PROGRESS);
        instance.assign(employee);
        instance.setWorkingStartDate(workingStartDate);
        when(workingStartDate.getTimeDifferenceFromDate(CURRENT_TIME)).thenReturn(DEFAULT_DURATION);

        instance.stopWorking(CURRENT_TIME);

        assertThat(instance.getWorkingStartDateOptional().isPresent()).isFalse();
    }

    @Test
    public void shouldAddWorkingTimeToLoggedTime() throws ProgressPercentageException {
        Employee employee = mock(Employee.class);
        instance.setStatus(TaskStatus.IN_PROGRESS);
        instance.assign(employee);
        instance.setWorkingStartDate(workingStartDate);
        long previousLoggedMinutes = instance.getAssigneeLoggedTime().getMinutes();
        when(workingStartDate.getTimeDifferenceFromDate(CURRENT_TIME)).thenReturn(DEFAULT_DURATION);

        instance.stopWorking(CURRENT_TIME);

        assertThat(instance.getAssigneeLoggedTime().getMinutes()).isEqualTo(
                previousLoggedMinutes + DEFAULT_DURATION.toMinutes());
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenStoppingNotInProgressTask()
            throws ProgressPercentageException {
        Employee employee = mock(Employee.class);

        instance.assign(employee);

        instance.stopWorking(CURRENT_TIME);
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenFinishingNotStartedTask() throws Exception {
        Employee employee = mock(Employee.class);
        instance.assign(employee);

        instance.setStatus(TaskStatus.TODO);

        instance.doneWorkingOnActiveTask(CURRENT_TIME);
    }

    @Test
    public void shouldChangeStatusToDone() throws Exception {
        Employee employee = mock(Employee.class);
        instance.setStatus(TaskStatus.IN_PROGRESS);
        instance.assign(employee);
        instance.setWorkingStartDate(workingStartDate);
        when(workingStartDate.getTimeDifferenceFromDate(CURRENT_TIME)).thenReturn(DEFAULT_DURATION);

        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        assertThat(instance.getStatus()).isEqualTo(TaskStatus.DONE);
    }

    @Test
    public void shouldAddWorkingTimeToLoggedTimeWhenEndingTask() throws Exception {
        Employee employee = mock(Employee.class);
        instance.setStatus(TaskStatus.IN_PROGRESS);
        instance.assign(employee);
        instance.setWorkingStartDate(workingStartDate);
        long previousLoggedMinutes = instance.getAssigneeLoggedTime().getMinutes();
        when(workingStartDate.getTimeDifferenceFromDate(CURRENT_TIME)).thenReturn(DEFAULT_DURATION);

        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        assertThat(instance.getAssigneeLoggedTime().getMinutes()).isEqualTo(
                previousLoggedMinutes + DEFAULT_DURATION.toMinutes());
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenStartingFinishedTask() throws Exception {
        Employee employee = mock(Employee.class);
        instance.assign(employee);
        instance.setStatus(TaskStatus.DONE);

        instance.startWorking();
    }

    @Test
    public void shouldAddLoggedTimeForTask() throws Exception {
        // given
        instance.assign(assignee);

        LoggedTime loggedTime = new LoggedTime();
        instance.setAssigneeLoggedTime(loggedTime);
        long initiallyLogged = loggedTime.getMinutes();

        final long FINALLY_LOGGED = initiallyLogged + MINUTES_TO_ADD;
        Consumer<LoggedTime> loggedTimeAdder = x -> x.addMinutes(MINUTES_TO_ADD);

        // when
        instance.logTimeForAssignee(loggedTimeAdder);

        // then
        assertEquals(FINALLY_LOGGED, loggedTime.getMinutes());
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenLoggingTimeToUnassignedTask() throws Exception {
        //given
        instance.assign(null);

        Consumer<LoggedTime> loggedTimeAdder = x -> x.addMinutes(anyInt());

        //when
        instance.logTimeForAssignee(loggedTimeAdder);
    }

    @Test
    public void shouldSetProgressPercentTo100WhenFinishingWorkingOnTask() throws Exception {

        // given
        instance.assign(assignee);
        int percentageValue = 100;
        instance.startWorking();

        // when
        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        // then
        assertThat(instance.getProgressPercentage().getPercentageValue())
                .isEqualTo(percentageValue);
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenLoggingWorkToFinishedTask() throws Exception {
        //given
        instance.assign(assignee);
        instance.startWorking();

        //when
        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        Consumer<LoggedTime> loggedTimeAdder = x -> x.addMinutes(TIME_WORKING_MINUTES);
        instance.logTimeForAssignee(loggedTimeAdder);
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenAssigneeDeadlineIsAfterOwnerDeadline() throws Exception {
        // given
        given(ownerDeadline.getDate()).willReturn(CURRENT_TIME.plusDays(1).toLocalDate());
        given(assigneeDeadline.getDate()).willReturn(CURRENT_TIME.plusDays(2).toLocalDate());

        // when
        createDefaultTaskWithId(1L);
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldNotUpdateProgressPercentageWhenNotAssignedToaAnybody() throws Exception {
        // given

        // when
        instance.updateWorkProgressPercentage(PROGRESS_PERCENTAGE);
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenUpdatingProgressPercentageOfDoneTask() throws Exception {

        // given
        instance.assign(assignee);
        // when
        instance.startWorking();
        instance.doneWorkingOnActiveTask(CURRENT_TIME);
        instance.updateWorkProgressPercentage(PROGRESS_PERCENTAGE);
        // then

    }

    @Test
    public void shouldSetNewProgressPercentage() throws Exception {

        // given
        instance.assign(assignee);

        // when
        instance.updateWorkProgressPercentage(PROGRESS_PERCENTAGE);
        // then
        assertThat(instance.getProgressPercentage().getPercentageValue()).isEqualTo(
                PROGRESS_PERCENTAGE);
    }

    @Test(expected = ProgressPercentageException.class)
    public void shouldThrowExceptionWhenProgressPercentageIsBiggerThan100() throws Exception {

        // given
        instance.assign(assignee);
        int toBigPercentage = 101;
        // when
        instance.updateWorkProgressPercentage(toBigPercentage);
        // then
    }

    @Test(expected = ProgressPercentageException.class)
    public void shouldThrowExceptionWhenProgressPercentageIsNotPositive() throws Exception {

        // given
        instance.assign(assignee);
        int negativePercentageValue = -1;
        // when
        instance.updateWorkProgressPercentage(negativePercentageValue);
        // then
    }

    @Test
    public void shouldSetOverdueToTrueWhenDeadlineBeforeTodayDate() throws Exception {
        // given
        LocalDate deadline = LocalDate.now().minusDays(1);
        Term assigneeTerm = mock(Term.class);
        when(assigneeTerm.getDate()).thenReturn(deadline);
        Term ownerTerm = mock(Term.class);
        when(ownerTerm.getDate()).thenReturn(deadline.plusDays(1));
        // when

        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setAssigneeDeadline(assigneeTerm).setOwnerDeadline(ownerTerm)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        // then
        assertThat(instance.isOverdue()).isEqualTo(true);
    }

    @Test
    public void shouldSetOverdueToFalseWhenDeadlineAfterTodayDate() throws Exception {

        // given
        LocalDate deadline = LocalDate.now().plusDays(1);
        Term assigneeTerm = mock(Term.class);
        when(assigneeTerm.getDate()).thenReturn(deadline);
        Term ownerTerm = mock(Term.class);
        when(ownerTerm.getDate()).thenReturn(deadline.plusDays(2));

        // when
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setAssigneeDeadline(assigneeTerm).setOwnerDeadline(ownerTerm)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();

        // then
        assertThat(instance.isOverdue()).isEqualTo(false);
    }

    @Test
    public void shouldChangeStatusToCancelledAfterBeingCancelled() {
        // given
        instance.assign(assignee);
        assignee.setType(EmployeeType.MANAGER);
        // when
        instance.cancelTask(LocalDateTime.now());
        // then
        assertThat(instance.getStatus()).isEqualTo(TaskStatus.CANCELLED);
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenStartingCancelledTask() throws Exception {
        // given
        instance.assign(assignee);
        assignee.setType(EmployeeType.DIRECTOR);
        instance.cancelTask(LocalDateTime.now());
        // when
        instance.startWorking();
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenLoggingTimeToCancelledTask() {
        // given
        instance.assign(assignee);
        assignee.setType(EmployeeType.MANAGER);
        instance.cancelTask(LocalDateTime.now());
        Consumer<LoggedTime> loggedTimeAdder = x -> x.addMinutes(TIME_WORKING_MINUTES);
        // when
        instance.logTimeForAssignee(loggedTimeAdder);
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldNotBeAbleToCancelSameTaskTwice() {
        // given
        instance.assign(assignee);
        assignee.setType(EmployeeType.DIRECTOR);

        instance.cancelTask(LocalDateTime.now());
        // when
        instance.cancelTask(LocalDateTime.now());
        // then
    }

    @Test
    public void shouldSetOverdueToFalseWhenFinishedWorkingOnTask() throws Exception {

        // given
        instance.assign(assignee);
        instance.setOverdue(true);

        // when
        instance.startWorking();
        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        // then
        assertThat(instance.isOverdue()).isEqualTo(false);
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenAssigneeDateIsNull() throws Exception {

        // given

        // when
        Task task = new TaskBuilder().setAssigneeDeadline(null).setName(TASK_NAME).createTask();
        // then

    }

    @Test
    public void shouldSetOwnerDeadlineToEqualAssigneeDeadlineIfNotGiven() throws Exception {

        // given
        Term term = mock(Term.class);
        when(term.getDate()).thenReturn(CURRENT_DATE);
        // when
        Task task = new TaskBuilder().setCreatedBy(createdByMock).setAssigneeDeadline(term)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        // then
        assertThat(task.getOwnerDeadline()).isNotNull();
        assertThat(task.getOwnerDeadline().getDate()).isEqualTo(CURRENT_DATE);
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenAddingSubtaskWithNoAssigneeDeadline() throws Exception {
        // given
        when(subtask.getOwnerDeadline()).thenReturn(new Term(LocalDate.now()));
        // when
        instance.addSubTask(subtask);
        // then
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenAddingSubtaskWithNoOwnerDeadline() throws Exception {
        // given
        when(subtask.getAssigneeDeadline()).thenReturn(new Term(LocalDate.now()));
        // when
        instance.addSubTask(subtask);
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenAddingSubtaskWithOwnerDeadlineAfterSubtaskAssigneeDeadline()
            throws Exception {
        // given
        when(subtask.getAssigneeDeadline()).thenReturn(new Term(LocalDate.now().plusDays(1)));
        when(subtask.getOwnerDeadline()).thenReturn(new Term(LocalDate.now()));
        // when
        instance.addSubTask(subtask);
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenSubtaskOwnerDeadlineAfterParentTaskOwnerDeadline()
            throws Exception {
        // given
        when(subtask.getAssigneeDeadline()).thenReturn(new Term(LocalDate.now()));
        when(subtask.getOwnerDeadline()).thenReturn(new Term(LocalDate.now().plusDays(1)));

        instance.setOwnerDeadline(new Term(LocalDate.now()));

        // when
        instance.addSubTask(subtask);
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenSubtaskAssigneeDeadlineAfterParentTaskAssigneeDeadline()
            throws Exception {
        // given
        when(subtask.getAssigneeDeadline()).thenReturn(new Term(LocalDate.now()));
        when(subtask.getOwnerDeadline()).thenReturn(new Term(LocalDate.now()));

        instance.setAssigneeDeadline(new Term(LocalDate.now().minusDays(1)));

        // when
        instance.addSubTask(subtask);
        // then
    }

    @Test
    public void shouldAddProperSubtaskToParentTask() throws Exception {
        // given
        when(subtask.getAssigneeDeadline()).thenReturn(new Term(LocalDate.now()));
        when(subtask.getOwnerDeadline()).thenReturn(new Term(LocalDate.now()));
        when(subtask.getTaskProperties()).thenReturn(new TaskPropertiesBuilder().build());
        instance.setAssigneeDeadline(new Term(LocalDate.now()));
        instance.setOwnerDeadline(new Term(LocalDate.now()));
        // when
        instance.addSubTask(subtask);
        // then
        assertThat(instance.getSubTasks().size()).isEqualTo(1);
        assertThat(instance.getSubTasks()).contains(subtask);

    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenCurrentUserIsNullDuringWorkStart() throws Exception {
        // given
        currentUser = null;
        // when
        instance.startWorking();
        // then
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenStoppingTaskOnNotYourTask() throws Exception {
        // given
        instance.assign(assignee);
        // when
        instance.stopWorking(CURRENT_TIME);
        // then
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenStoppingTaskWithoutSpecifyingCurrentDate() throws Exception {
        // given

        // when
        instance.stopWorking(null);
        // then

    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenCancellingAlreadyDoneTask() throws Exception {
        //given
        instance.setStatus(TaskStatus.DONE);
        //when
        instance.cancelTask(CURRENT_TIME);
        //then

    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenFinishingNotStoppedTask() throws Exception {
        // given

        // when
        instance.doneWorkingOnStoppedTask();
        // then
    }

    @Test
    public void shouldChangeTaskStatusToDoneWhenFinishingStoppedTask() throws Exception {
        // given
        instance.setStatus(TaskStatus.STOPPED);
        // when
        instance.doneWorkingOnStoppedTask();
        // then
        assertThat(instance.getStatus()).isEqualTo(TaskStatus.DONE);
    }

    @Test
    public void shouldSetProgressPercentageToHundredWhenFinishingStoppedTask() throws Exception {
        // given
        instance.setStatus(TaskStatus.STOPPED);
        // when
        instance.doneWorkingOnStoppedTask();
        // then
        assertThat(instance.getProgressPercentage()).isEqualTo(Percentage.ONE_HUNDRED);
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenAddingSubtaskToFinishedTask() throws Exception {
        //given
        instance.assign(assignee);
        subtask = createDefaultTaskWithId(3L);
        //when
        instance.startWorking();
        instance.doneWorkingOnActiveTask(LocalDateTime.now());
        instance.addSubTask(subtask);
        //then

    }

    @Test
    public void shouldSetOverdueToFalseAfterFinishingStoppedTask() throws Exception {
        // given
        instance.setOverdue(true);
        instance.setStatus(TaskStatus.STOPPED);
        // when
        instance.doneWorkingOnStoppedTask();
        // then
        assertThat(instance.isOverdue()).isFalse();
    }

    @Test
    public void shouldSetOverdueToFalseAfterCancellingTask() throws Exception {
        // given
        instance.setOverdue(true);
        // when
        instance.cancelTask(CURRENT_TIME);
        // then
        assertThat(instance.isOverdue()).isFalse();
    }

    @Test
    public void shouldSetOverdueToFalseAfterUpdatingTaskToNotOverdue() throws Exception {
        //given
        LocalDate oldDeadline = LocalDate.now().minusDays(2);
        Term oldAssigneeTerm = mock(Term.class);
        when(oldAssigneeTerm.getDate()).thenReturn(oldDeadline);
        Term oldOwnerTerm = mock(Term.class);
        when(oldOwnerTerm.getDate()).thenReturn(oldDeadline.plusDays(1));
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setAssigneeDeadline(oldAssigneeTerm).setOwnerDeadline(oldOwnerTerm)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();

        LocalDate newDeadline = LocalDate.now().plusDays(2);
        Term newAssigneeTerm = mock(Term.class);
        when(newAssigneeTerm.getDate()).thenReturn(newDeadline);
        Term newOwnerTerm = mock(Term.class);
        when(newOwnerTerm.getDate()).thenReturn(newDeadline.plusDays(1));

        Task taskUpdate = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setAssigneeDeadline(newAssigneeTerm).setOwnerDeadline(newOwnerTerm)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        taskUpdate.setOverdue(instance.isOverdue());
        //when
        instance.update(taskUpdate);
        // then
        assertThat(instance.isOverdue()).isFalse();
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenPercentageDoneNotGivenDuringReopening() throws Exception {
        //given
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setOwnerDeadline(assigneeDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        instance.assign(assignee);
        instance.doneWorkingOnStoppedTask();

        //when
        instance.reopen(ownerDeadline.getDate(), ownerDeadline.getDate(), null);
    }

    @Test
    public void shouldReopenTask() throws Exception {
        //given
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setOwnerDeadline(assigneeDeadline).setAssigneeDeadline(assigneeDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        Percentage percentageAfterReopening = new Percentage(10);
        instance.assign(assignee);
        instance.startWorking();
        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        //when
        instance.reopen(ownerDeadline.getDate(), ownerDeadline.getDate(), percentageAfterReopening);

        //then
        assertThat(instance.getStatus()).isEqualTo(TaskStatus.STOPPED);
        assertThat(instance.getProgressPercentage()).isEqualTo(percentageAfterReopening);
        assertThat(instance.getAssigned()).isEqualTo(assignee);
        assertThat(instance.getAssigneeDeadline().getDate()).isEqualTo(ownerDeadline.getDate());
        assertThat(instance.getOwnerDeadline().getDate()).isEqualTo(ownerDeadline.getDate());
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenReopeningTaskWith100PercentOfWorkDone() throws Exception {
        //given
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock).setAssignee(assignee)
                .setOwnerDeadline(assigneeDeadline).setAssigneeDeadline(assigneeDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        instance.startWorking();
        instance.doneWorkingOnActiveTask(CURRENT_TIME);

        //when
        instance.reopen(ownerDeadline.getDate(), ownerDeadline.getDate(), Percentage.ONE_HUNDRED);
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenOwnerDeadlineIsNotProvidedWhenReopening() throws Exception {
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setOwnerDeadline(assigneeDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        instance.assign(assignee);
        instance.doneWorkingOnStoppedTask();

        //when
        instance.reopen(null, ownerDeadline.getDate(), new Percentage(10));
    }

    @Test(expected = NullPointerStatusException.class)
    public void shouldThrowExceptionWhenAssigneeDeadlineIsNotProvidedWhenReopening() throws Exception {
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setOwnerDeadline(assigneeDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();
        instance.assign(assignee);
        instance.doneWorkingOnStoppedTask();

        //when
        instance.reopen(ownerDeadline.getDate(), null, new Percentage(10));
    }

    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowExceptionWhenReopningNotCancelledOrDoneTask() throws Exception {
        //given
        instance = new TaskBuilder().setEstimatedTime(estimatedTimeMock).setId(INSTANCE_ID)
                .setType(TaskType.STANDARD).setCreatedBy(createdByMock)
                .setOwnerDeadline(assigneeDeadline).setAssigneeDeadline(assigneeDeadline)
                .setDescription(TEST_DESCRIPTION).setName(TASK_NAME).createTask();

        Percentage percentageAfterReopening = new Percentage(10);
        instance.assign(assignee);
        instance.startWorking();

        //when
        instance.reopen(ownerDeadline.getDate(), ownerDeadline.getDate(), percentageAfterReopening);
    }


}
