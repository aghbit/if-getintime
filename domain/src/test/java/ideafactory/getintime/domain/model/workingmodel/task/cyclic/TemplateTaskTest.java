package ideafactory.getintime.domain.model.workingmodel.task.cyclic;

import com.google.common.collect.Sets;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.model.workingmodel.task.TaskProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.Set;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@RunWith(MockitoJUnitRunner.class)
public class TemplateTaskTest {
    private TemplateTask instance;
    
    @Mock
    private TaskProperties properties;
    
    @Mock
    private TaskDescription description;
    
    @Before
    public void setUp() throws Exception {
    }
    
    @Test
    public void shouldCorrectlyStoreDaysOfWeek() throws Exception {
        Set<DayOfWeek> daysOfWeeks = Sets.newHashSet(DayOfWeek.MONDAY, DayOfWeek.FRIDAY);
        Set<Integer> daysOfMonth = new HashSet<>();
        
        instance = new TemplateTask(description, properties, daysOfMonth, daysOfWeeks);
        
        assertThat(instance.getDaysOfWeek()).containsOnly(DayOfWeek.MONDAY, DayOfWeek.FRIDAY);
    }
    
    @Test
    public void shouldCorrectlyStoreDaysOfMonth() {
        Set<DayOfWeek> daysOfWeeks = new HashSet<>();
        Set<Integer> daysOfMonth = Sets.newHashSet(5, 7, 21);
        
        instance = new TemplateTask(description, properties, daysOfMonth, daysOfWeeks);
        
        assertThat(instance.getDaysOfMonth()).containsOnly(5, 7, 21);
    }
    
    @Test
    public void shouldHaveCopyConstructor() {
        final Set<Task> subTasks = Sets.newHashSet();
        when(properties.getSubTasks()).thenReturn(subTasks);
        when(description.getDescription()).thenReturn("description");
        when(description.getName()).thenReturn("name");
        instance = new TemplateTask(description, properties, Sets.newHashSet(), Sets.newHashSet());
        
        TemplateTask instanceCopy = new TemplateTask(instance);
        
        // needs System.identityHashCode() cause, hashCode() for Set class is
        // overridden.
        final int hash = System.identityHashCode(properties.getSubTasks());
        final int copyHash = System
                .identityHashCode(instanceCopy.getTaskProperties().getSubTasks());
        assertThat(hash).isNotEqualTo(copyHash);
    }
}
