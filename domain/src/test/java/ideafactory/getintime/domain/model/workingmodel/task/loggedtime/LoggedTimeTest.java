package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Michal Partyka
 */
public class LoggedTimeTest {
    
    private LoggedTime instance;
    
    @Before
    public void setUp() throws Exception {
        instance = new LoggedTime();
    }
    
    @Test
    public void shouldBeZeroAfterCreation() throws Exception {
        assertThat(instance.getMinutes()).isEqualTo(0);
    }
    
    @Test
    public void addingMinutesShouldBePossible() throws Exception {
        instance.addMinutes(10L);
        
        assertThat(instance.getMinutes()).isEqualTo(10L);
    }
}
