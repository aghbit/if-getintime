package ideafactory.getintime.domain.model.workingmodel.task;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TaskCommentTest {
    
    TaskComment comment1, comment2;
    
    Employee employee, employee2;
    
    LocalDateTime localDateTime;
    
    private TaskComment createComment(Long id, Employee employee, String text, LocalDateTime time) {
        final TaskComment comment = new TaskComment(employee, text);
        comment.setId(id);
        comment.setTime(time);
        return comment;
    }
    
    @Before
    public void setUp() throws Exception {
        // can't mock equals
        employee = new Employee(1L, "Grzesiek", "Miejski", "username", "password",
                EmployeeType.MANAGER);
        employee2 = new Employee(2L, "Piotr", "Góralczyk", "username", "password",
                EmployeeType.MANAGER);
        
        localDateTime = LocalDateTime.of(2010, 10, 10, 12, 30);
    }
    
    @Test
    public void testEqualsSameIdsAndContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz", localDateTime);
        comment2 = createComment(1L, employee, "Komentarz", localDateTime);
        
        assertTrue(comment1.equals(comment2));
        assertTrue(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsSameIdsAndDifferentContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz 1", localDateTime);
        comment2 = createComment(1L, employee2, "Komentarz 2", localDateTime);
        
        assertTrue(comment1.equals(comment2));
        assertTrue(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsDifferentIdsAndSameContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz", localDateTime);
        comment2 = createComment(2L, employee, "Komentarz", localDateTime);
        
        assertFalse(comment1.equals(comment2));
        assertFalse(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsDifferentIdsAndDifferentContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz 1", localDateTime);
        comment2 = createComment(2L, employee2, "Komentarz 2", localDateTime);
        
        assertFalse(comment1.equals(comment2));
        assertFalse(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsNullIdAndSameContent() throws Exception {
        comment1 = createComment(null, employee, "Komentarz", localDateTime);
        comment2 = createComment(null, employee, "Komentarz", localDateTime);
        
        assertTrue(comment1.equals(comment2));
        assertTrue(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsNullIdAndDifferentContent() throws Exception {
        comment1 = createComment(null, employee, "Komentarz 1", localDateTime);
        comment2 = createComment(null, employee2, "Komentarz 2", localDateTime);
        
        assertFalse(comment1.equals(comment2));
        assertFalse(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsOneIdNullIdAndDifferentContent() throws Exception {
        comment1 = createComment(null, employee, "Komentarz 1", localDateTime);
        comment2 = createComment(1L, employee2, "Komentarz 2", localDateTime);
        
        assertFalse(comment1.equals(comment2));
        assertFalse(comment2.equals(comment1));
    }
    
    @Test
    public void testEqualsOneIdNullIdAndSameContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz", localDateTime);
        comment2 = createComment(null, employee, "Komentarz", localDateTime);
        
        assertFalse(comment1.equals(comment2));
        assertFalse(comment2.equals(comment1));
    }
    
    @Test
    public void testHashcodeSameIdsAndContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz", localDateTime);
        comment2 = createComment(1L, employee, "Komentarz", localDateTime);
        
        assertEquals(comment1.hashCode(), comment2.hashCode());
    }
    
    @Test
    public void testHashcodeSameIdsAndDifferentContent() throws Exception {
        comment1 = createComment(1L, employee, "Komentarz 1", localDateTime);
        comment2 = createComment(1L, employee2, "Komentarz 2", localDateTime);
        
        assertEquals(comment1.hashCode(), comment2.hashCode());
    }
    
    @Test
    public void testHashcodeNullIdAndSameContent() throws Exception {
        comment1 = createComment(null, employee, "Komentarz", localDateTime);
        comment2 = createComment(null, employee, "Komentarz", localDateTime);
        
        assertEquals(comment1.hashCode(), comment2.hashCode());
    }
    
}
