package ideafactory.getintime.domain.model.workingmodel.task.properties;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * @author Michal Partyka
 */
public class TermTest {
    private Term instance;
    private LocalDate LOCALDATE_EXAMPLE = LocalDate.of(2014, 5, 9);
    
    @Before
    public void setUp() throws Exception {
        instance = new Term(LOCALDATE_EXAMPLE);
    }
    
    @Test
    public void equalsShouldWork() throws Exception {
        Term term = new Term(LOCALDATE_EXAMPLE);
        
        boolean equals = instance.equals(term);
        
        assertThat(equals).isTrue();
    }
    
    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionIfDateIsNull() throws Exception {
        Term term = new Term(null);
    }
    
    @Test
    public void toStringShouldReturnProperlyFormattedDateString() {
        String dateString = instance.toString();
        
        assertEquals("2014-05-09", dateString);
    }
}
