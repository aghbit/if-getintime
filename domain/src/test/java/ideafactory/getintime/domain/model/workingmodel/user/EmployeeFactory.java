package ideafactory.getintime.domain.model.workingmodel.user;

import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;

public class EmployeeFactory {
    public static Employee createEmployee(Long id) {
        return new Employee(id, "firstname", "secondname", "username", "password",
                EmployeeType.NORMAL);
    }
}