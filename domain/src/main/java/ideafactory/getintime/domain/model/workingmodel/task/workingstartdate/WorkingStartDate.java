package ideafactory.getintime.domain.model.workingmodel.task.workingstartdate;

import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * @author Grzegorz Miejski on 13.12.13.
 */
@Audited
@Entity
@Table(name = "working_start_date")
public class WorkingStartDate extends AbstractTimerEntity {
    
    @Transient
    public static final LocalDateTime NULL_START_DATE = null;
    
    private LocalDateTime startDate;
    
    private WorkingStartDate() {
        
    }
    
    public WorkingStartDate(Clock clock) {
        startDate = LocalDateTime.now(clock);
        
    }
    
    public LocalDateTime getStartDate() {
        return startDate;
    }
    
    public Duration getTimeDifferenceFromDate(LocalDateTime localDateTime) {
        return Duration.between(startDate, localDateTime);
    }
}
