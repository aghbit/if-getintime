package ideafactory.getintime.domain.model.workingmodel.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import ideafactory.geintime.commons.hash.PasswordHash;
import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.exception.NoRequiredRoleException;
import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.user.filters.Filter;
import ideafactory.getintime.domain.model.workingmodel.user.type.EmployeeType;
import ideafactory.getintime.domain.user.SubordinatesVisitor;
import ideafactory.getintime.domain.visitorapi.IEmployeeVisitor;
import ideafactory.getintime.domain.visitorapi.Visitable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Michal Partyka
 */
@Audited
@Entity
public class Employee extends AbstractTimerEntity implements Visitable, UserDetails {
    private static final String passwordHashAlgorithm = "SHA-512";
    @Column(unique = true)
    private String username;
    @JsonIgnore
    private String password;
    
    @Enumerated(EnumType.STRING)
    private EmployeeType type;
    
    @JsonIgnore
    private boolean isAccountNonLocked = true;
    @JsonIgnore
    private boolean isCredentialsNonExpired = true;
    @JsonIgnore
    private boolean isEnabled = true;
    @JsonIgnore
    private boolean isAccountNonExpired = true;
    
    public Employee() {
    }
    
    @JsonCreator
    public Employee(@JsonProperty("id") Long id, @JsonProperty("firstName") String firstName,
            @JsonProperty("secondName") String secondName,
            @JsonProperty("username") String username, @JsonProperty("password") String password,
            @JsonProperty("type") EmployeeType type) {
        super(id);
        this.firstName = firstName;
        this.secondName = secondName;
        this.username = username;
        this.type = type;
        setPassword(password);
    }
    
    private String firstName;
    private String secondName;
    
    @NotAudited
    @OneToMany(fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Employee> subordinates = new HashSet<>(2);
    
    @NotAudited
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "taskProperties.assigned")
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();
    
    public void addSubordinate(Employee employee) {
        subordinates.add(employee);
    }
    
    @NotAudited
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Filter> filters;
    
    @Override
    public void accept(IEmployeeVisitor visitor) {
        visitor.visit(this);
        for (Employee subordinate : subordinates) {
            subordinate.accept(visitor);
        }
    }
    
    public Iterator<Employee> iterator() {
        return subordinates.iterator();
    }
    
    @JsonIgnore
    public Set<Task> getTasks() {
        return Collections.unmodifiableSet(tasks);
    }
    
    void setTasks(Set<Task> tasks) {
        Preconditions.checkNotNull(tasks);
        this.tasks = tasks;
    }
    
    @JsonIgnore
    private Set<Employee> getSubordinates() {
        return subordinates;
    }
    
    private void setSubordinates(Set<Employee> subordinates) {
        this.subordinates = subordinates;
    }
    
    @JsonIgnore
    public Optional<Task> getCurrenWorkingTask() {
        
        // tasks.stream().filter(x -> x.isBeingWorkedOn()).findFirst();
        
        for (Task task : tasks) {
            if (task.isBeingWorkedOn()) {
                return Optional.of(task);
            }
        }
        return Optional.empty();
    }
    
    @JsonIgnore
    public Set<Task> getSubordinatesTasks() {
        SubordinatesVisitor subordinatesVisitor = new SubordinatesVisitor();
        this.accept(subordinatesVisitor);
        
        Set<Task> subordinatesTasks = new HashSet<>();
        
        for (Employee employee : subordinatesVisitor.getSubordinates()) {
            subordinatesTasks.addAll(employee.getTasks());
        }
        subordinatesTasks.removeAll(this.tasks);
        return subordinatesTasks;
    }
    
    @JsonIgnore
    public boolean isWorkingOnTask() {
        return getCurrenWorkingTask().isPresent();
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getSecondName() {
        return secondName;
    }
    
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
    
    public void addFilter(final Filter filter) {
        Iterables
            .tryFind(
                    filters, input -> input.getFilterName().equals(filter.getFilterName())
            )
            .transform(
                    input -> {
                        filters.remove(input);
                        filters.add(filter);
                        return filter;
                    }
            )
            .or(() -> {
                filters.add(filter);
                return filter;
            });
    }
    
    @JsonIgnore
    public Set<Filter> getFilters() {
        return filters;
    }
    
    public void setFilters(Set<Filter> filters) {
        this.filters = filters;
    }
    
    public void removeFilterWithId(Long filterId) {
        this.filters = Sets.newHashSet(Iterables.filter(
                this.filters, filter -> !filter.getId().equals(filterId)
        ));
    }
    
    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final String employeeRole = type.toString();
        final SimpleGrantedAuthority role = new SimpleGrantedAuthority(employeeRole);
        return Lists.newArrayList(role);
    }
    
    @Override
    @JsonIgnore
    public String getPassword() {
        return password;
    }
    
    @Override
    public String getUsername() {
        return username;
    }
    
    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }
    
    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }
    
    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }
    
    @Override
    @JsonIgnore
    public boolean isEnabled() {
        return isEnabled;
    }
    
    @JsonIgnore
    public static String getPasswordHashAlgorithm() {
        return passwordHashAlgorithm;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    private void setPassword(String password) {
        if (password == null) {
            this.password = "";
        } else {
            this.password = PasswordHash.hash(password, passwordHashAlgorithm);
        }
    }
    
    public EmployeeType getType() {
        return type;
    }
    
    public void setType(EmployeeType type) {
        this.type = type;
    }
    
    public void setAccountNonLocked(boolean accountNonLocked) {
        isAccountNonLocked = accountNonLocked;
    }
    
    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        isCredentialsNonExpired = credentialsNonExpired;
    }
    
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }
    
    public void setAccountNonExpired(boolean accountNonExpired) {
        isAccountNonExpired = accountNonExpired;
    }
    
    @JsonIgnore
    public boolean hasAnyRole(EmployeeType... types) {
        return Arrays.asList(types).contains(type);
    }
    
    private void checkIfAssignedToCurrentUser(Task task, String message) {
        GetintimePreconditions.checkNotNull(task, "Exception.NoTaskFoundException");
        GetintimePreconditions.checkState(this.equals(task.getAssigned()), message,
                "Exception.Task.AssigneeMustEqualLoggedInUser");
    }
    
    public void startWorkingOnTask(Task task) {
        checkIfAssignedToCurrentUser(task, "Cannot start working on not your task!");
        
        task.startWorking();
    }
    
    public void stopWorkingOnTask(Task task, LocalDateTime now) {
        checkIfAssignedToCurrentUser(task, "Cannot stop working on not your task!");
        
        task.stopWorking(now);
    }
    
    public void doneWorkingOnTask(Task task, LocalDateTime now) {
        checkIfAssignedToCurrentUser(task, "Cannot finish working on not your task!");
        if (task.isBeingWorkedOn()) {
            task.doneWorkingOnActiveTask(now);
        } else {
            task.doneWorkingOnStoppedTask();
        }
    }
    
    public void cancelTask(Task task, LocalDateTime now) {
        checkAnyRole(this, "Cannot cancel task as normal user!", EmployeeType.DIRECTOR,
                EmployeeType.MANAGER);
        task.cancelTask(now);
    }
    
    private void checkAnyRole(Employee employee, String errorMessage, EmployeeType... employeeTypes) {
        if (!employee.hasAnyRole(employeeTypes)) {
            throw new NoRequiredRoleException(errorMessage);
        }
    }
    
    public void logTime(Task task, long timeInMinutes) {
        checkIfAssignedToCurrentUser(task, "Cannot log time to not your task!");
        task.logTimeForAssignee((loggedTime) -> loggedTime.addMinutes(timeInMinutes));
    }
    
    public void updateWorkProgressPercentage(Task task, int percentage) {
        checkIfAssignedToCurrentUser(task, "Cannot log time to not your task!");
        task.updateWorkProgressPercentage(percentage);
    }
}
