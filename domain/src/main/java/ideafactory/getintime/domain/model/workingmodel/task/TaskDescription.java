package ideafactory.getintime.domain.model.workingmodel.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Embeddable;

/**
 * Immutable class representing name and description of the task.
 * 
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Embeddable
public class TaskDescription {
    private String name;
    private String description;
    
    public TaskDescription() {
    }
    
    @JsonCreator
    public TaskDescription(@JsonProperty("name") String name,
            @JsonProperty("description") String description) {
        this.name = name;
        this.description = description;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
}
