package ideafactory.getintime.domain.model.workingmodel.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Marcin on 2014-12-13J
 */
public class ParentTaskDescription {
    public static final ParentTaskDescription EMPTY = new ParentTaskDescription(null, null);
    private final Long id;
    private final String name;
    
    @JsonCreator
    public ParentTaskDescription(@JsonProperty("id") Long id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }
    
    public Long getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }
}
