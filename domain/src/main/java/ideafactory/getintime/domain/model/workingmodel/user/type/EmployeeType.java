package ideafactory.getintime.domain.model.workingmodel.user.type;

/**
 * @author Maciej Ciołek
 */
public enum EmployeeType {
    DIRECTOR, MANAGER, NORMAL
}
