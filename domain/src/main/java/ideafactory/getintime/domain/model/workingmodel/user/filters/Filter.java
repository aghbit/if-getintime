package ideafactory.getintime.domain.model.workingmodel.user.filters;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.task.properties.TaskStatus;
import ideafactory.getintime.domain.task.properties.TaskType;
import ideafactory.getintime.domain.user.filters.TimeRange;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.util.List;
import java.util.Set;

/**
 * @author partyks
 */
@Entity
public class Filter extends AbstractTimerEntity {
    private String filterName;
    
    private TaskDescription taskDescription;
    
    @ElementCollection(fetch = FetchType.EAGER)
    private List<TaskStatus> statuses;
    
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<TaskType> types;
    
    private TimeRange timeRange;
    
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Integer> priorities;
    
    private Filter() {
        
    }
    
    @JsonCreator
    public Filter(@JsonProperty("name") String filterName,
            @JsonProperty("taskDescription") TaskDescription taskDescription,
            @JsonProperty("statuses") List<TaskStatus> statuses,
            @JsonProperty("types") Set<TaskType> types, @JsonProperty("date") TimeRange timeRange,
            @JsonProperty("priorities") Set<Integer> priorities) {
        this.filterName = filterName;
        this.taskDescription = taskDescription;
        this.statuses = statuses;
        this.types = types;
        this.timeRange = timeRange;
        this.priorities = priorities;
    }
    
    @JsonProperty("name")
    public String getFilterName() {
        return filterName;
    }
    
    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }
    
    public TaskDescription getTaskDescription() {
        return taskDescription;
    }
    
    public void setTaskDescription(TaskDescription taskDescription) {
        this.taskDescription = taskDescription;
    }
    
    public Set<TaskType> getTypes() {
        return types;
    }
    
    public void setTypes(Set<TaskType> types) {
        this.types = types;
    }
    
    @JsonProperty("date")
    public TimeRange getTimeRange() {
        return timeRange;
    }
    
    @JsonIgnore
    public void setTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
    }
    
    public Set<Integer> getPriorities() {
        return priorities;
    }
    
    public void setPriorities(Set<Integer> priorities) {
        this.priorities = priorities;
    }
    
    public List<TaskStatus> getStatuses() {
        return statuses;
    }
    
    public void setStatuses(List<TaskStatus> statuses) {
        this.statuses = statuses;
    }
}
