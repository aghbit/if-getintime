package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

import ideafactory.getintime.domain.task.loggedtime.DurationMinutesType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Embeddable;

/**
 * @author Michal Partyka
 */
@Embeddable
@TypeDefs(value = { @TypeDef(name = "durationMinutesType", defaultForType = DurationMinutesType.class, typeClass = DurationMinutesType.class) })
public class LoggedTime implements IDurationMinutes {
    @Type(type = "durationMinutesType")
    private DurationMinutes durationMinutes = DurationMinutes.ZERO;
    
    public LoggedTime() {
    }
    
    public LoggedTime(LoggedTime loggedTime) {
        this.durationMinutes = loggedTime.durationMinutes;
    }
    
    @Override
    public long getMinutes() {
        return durationMinutes.getMinutes();
    }
    
    public LoggedTime addMinutes(long minutes) {
        durationMinutes = durationMinutes.plusMinutes(minutes);
        return this;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof LoggedTime))
            return false;
        
        LoggedTime that = (LoggedTime) o;
        
        if (!durationMinutes.equals(that.durationMinutes))
            return false;
        
        return true;
    }
    
    @Override
    public int hashCode() {
        return durationMinutes.hashCode();
    }
    
    @Override
    public String toString() {
        return "Logged time: " + durationMinutes;
    }
}
