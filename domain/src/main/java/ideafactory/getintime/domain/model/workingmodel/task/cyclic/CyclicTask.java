package ideafactory.getintime.domain.model.workingmodel.task.cyclic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.task.properties.TaskType;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Michal Partyka
 */
@Audited
@Entity
public class CyclicTask extends AbstractTimerEntity {
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private TemplateTask templateTask;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "cyclicTask")
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();
    
    public CyclicTask() {
    }
    
    public CyclicTask(TemplateTask templateTask) {
        this.templateTask = templateTask;
    }
    
    public void add(Term deadline) {
        final TemplateTask templateTaskCopy = new TemplateTask(templateTask);
        Task task = new Task(templateTaskCopy);
        task.setOwnerDeadline(deadline);
        task.setAssigneeDeadline(deadline);
        task.setType(TaskType.CYCLIC);
        task.setCyclicTask(this);
        tasks.add(task);
    }
    
    public void addAll(List<Term> deadlines) {
        for (Term deadline : deadlines) {
            add(deadline);
        }
    }
    
    public TemplateTask getTemplateTask() {
        return templateTask;
    }
    
    private void setTemplateTask(TemplateTask templateTask) {
        this.templateTask = templateTask;
    }
    
    @JsonIgnore
    public Set<Task> getTasks() {
        return tasks;
    }
    
    private void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
}