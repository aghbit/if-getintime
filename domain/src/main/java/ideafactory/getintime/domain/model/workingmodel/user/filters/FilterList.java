package ideafactory.getintime.domain.model.workingmodel.user.filters;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.collect.Iterables;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import java.util.Iterator;
import java.util.List;

/**
 * @author partyks
 */
@Embeddable
public class FilterList<T> implements Iterable {
    @ElementCollection
    private List<FilterProperty<T>> filterPropertyList;
    
    @JsonCreator
    public FilterList(List<FilterProperty<T>> filterPropertyList) {
        this.filterPropertyList = filterPropertyList;
    }
    
    @Override
    public Iterator iterator() {
        return Iterables.filter(
                filterPropertyList, tFilterProperty -> tFilterProperty.isChecked()
        ).iterator();
    }
}
