package ideafactory.getintime.domain.model.workingmodel;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Michal Partyka
 */
@MappedSuperclass
public abstract class AbstractTimerEntity {
    
    @Id
    @GeneratedValue
    private Long id;
    
    protected AbstractTimerEntity() {
    }
    
    public AbstractTimerEntity(Long id) {
        this.id = id;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof AbstractTimerEntity))
            return false;
        
        AbstractTimerEntity that = (AbstractTimerEntity) o;
        
        return id != null && id.equals(that.id);
        
    }
    
    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
