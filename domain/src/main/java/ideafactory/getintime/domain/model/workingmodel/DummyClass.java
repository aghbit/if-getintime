package ideafactory.getintime.domain.model.workingmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Michal Partyka
 */
@Entity
public class DummyClass {
    
    @GeneratedValue
    @Id
    private Long id;
    
    private String someStringProperty;
    
    private Long someLongProperty;
    
    private String customNamedProperty;
    
    public String getSomeStringProperty() {
        return someStringProperty;
    }
    
    public void setSomeStringProperty(String someStringProperty) {
        this.someStringProperty = someStringProperty;
    }
    
    public Long getSomeLongProperty() {
        return someLongProperty;
    }
    
    public void setSomeLongProperty(Long someLongProperty) {
        this.someLongProperty = someLongProperty;
    }
    
    @JsonProperty("customProp")
    public String getCustomNamedProperty() {
        return customNamedProperty;
    }
    
    @JsonProperty("customProp")
    public void setCustomNamedProperty(String customNamedProperty) {
        this.customNamedProperty = customNamedProperty;
    }
    
    private Long getId() {
        return id;
    }
    
    private void setId(Long id) {
        this.id = id;
    }
}
