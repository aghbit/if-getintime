package ideafactory.getintime.domain.model.workingmodel.task.workingstartdate.factory;

import ideafactory.getintime.domain.model.workingmodel.task.workingstartdate.WorkingStartDate;

import java.time.Clock;
import java.time.ZoneId;

/**
 * @author Grzegorz Miejski on 08.02.14.
 */
public class WorkingStartDateFactory {
    
    private WorkingStartDateFactory() {
    }
    
    public static WorkingStartDate createCurrentWorkingStartDate() {
        return new WorkingStartDate(Clock.system(ZoneId.systemDefault()));
    }
    
}
