package ideafactory.getintime.domain.model.workingmodel.task.properties;

import ideafactory.getintime.domain.exception.ProgressPercentageException;

import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 * @author Grzegorz Miejski on 20.03.14.
 */
@Embeddable
public class Percentage {
    
    @Transient
    public static Percentage ZERO = new Percentage();
    
    @Transient
    public static Percentage ONE_HUNDRED = get100Percentage();
    
    private int percentageValue;
    
    private Percentage() {
        percentageValue = 0;
    }
    
    private static Percentage get100Percentage() {
        Percentage result = new Percentage();
        result.setPercentageValue(100);
        return result;
    }
    
    public Percentage(int percentageValue) throws ProgressPercentageException {
        
        if (percentageValue < 0 || percentageValue > 100) {
            throw new ProgressPercentageException("Percentage value must be between 0 and 100.",
                    "Exception.Percentage.valueNotInRange");
        }
        this.percentageValue = percentageValue;
    }
    
    public static Percentage getPercentageFromValues(int partialValue, int totalValue)
            throws ProgressPercentageException {
        return new Percentage(partialValue / totalValue);
    }
    
    public int getPercentageValue() {
        return percentageValue;
    }
    
    private void setPercentageValue(int percentage) {
        this.percentageValue = percentage;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        
        Percentage that = (Percentage) o;
        
        if (percentageValue != that.percentageValue)
            return false;
        
        return true;
    }
    
    @Override
    public int hashCode() {
        return percentageValue;
    }
    
    @Override
    public String toString() {
        return "Percentage: " + percentageValue;
    }
}
