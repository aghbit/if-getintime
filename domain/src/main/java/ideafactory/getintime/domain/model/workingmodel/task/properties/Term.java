package ideafactory.getintime.domain.model.workingmodel.task.properties;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import ideafactory.getintime.domain.mappers.LocalDateDeserializer;
import ideafactory.getintime.domain.mappers.LocalDateSerializer;
import ideafactory.getintime.domain.task.loggedtime.LocalDateType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * @author Michal Partyka
 */
@Embeddable
@TypeDefs(value = { @TypeDef(name = "localDateType", defaultForType = LocalDateType.class, typeClass = LocalDateType.class) })
public class Term implements Comparable<Term> {
    
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @Type(type = "localDateType")
    private LocalDate date;
    
    public Term() {
    }
    
    @JsonCreator
    public Term(@JsonProperty("date") LocalDate date) {
        Preconditions.checkNotNull(date);
        this.date = date;
    }
    
    public LocalDate getDate() {
        return date;
    }
    
    private void setDate(LocalDate date) {
        this.date = date;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Term))
            return false;
        
        Term term = (Term) o;
        
        return date.equals(term.date);
        
    }
    
    @Override
    public int hashCode() {
        return date.hashCode();
    }
    
    @Override
    public String toString() {
        return date.toString();
    }
    
    @Override
    public int compareTo(Term that) {
        return date.compareTo(that.date);
    }
}
