package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

import java.io.Serializable;
import java.time.Duration;

/**
 * @author Piotr Góralczyk
 */
public final class DurationMinutes implements Serializable, IDurationMinutes {
    public static final DurationMinutes ZERO = new DurationMinutes(0);
    
    private final Duration duration;
    
    public DurationMinutes(long minutes) {
        duration = Duration.ofMinutes(minutes);
    }
    
    public DurationMinutes plusMinutes(long minutes) {
        return new DurationMinutes(getMinutes() + minutes);
    }
    
    public long getMinutes() {
        return duration.toMinutes();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof DurationMinutes))
            return false;
        
        DurationMinutes that = (DurationMinutes) o;
        
        if (getMinutes() != that.getMinutes())
            return false;
        
        return true;
    }
    
    @Override
    public int hashCode() {
        return Long.valueOf(getMinutes()).hashCode();
    }
    
    @Override
    public String toString() {
        return getMinutes() + " minutes";
    }
}
