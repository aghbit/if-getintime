package ideafactory.getintime.domain.model.workingmodel.task.cyclic;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Sets;
import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.model.workingmodel.task.TaskProperties;
import ideafactory.getintime.domain.model.workingmodel.task.decorators.DayOfMonthDecorator;
import ideafactory.getintime.domain.model.workingmodel.task.decorators.DayOfWeekDecorator;
import org.hibernate.envers.Audited;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.Set;

/**
 * Template for creating CyclicTask.
 * 
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Audited
@Entity
public class TemplateTask extends AbstractTimerEntity {
    
    @Embedded
    private TaskDescription taskDescription;
    
    @Embedded
    private TaskProperties taskProperties;
    
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<DayOfMonthDecorator> daysOfMonth = new HashSet<>();
    
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<DayOfWeekDecorator> daysOfWeek = new HashSet<>();
    
    public TemplateTask() {
    }
    
    /**
     * Copy constructor.
     * 
     * @param templateTask
     *            instance to copy
     */
    public TemplateTask(TemplateTask templateTask) {
        final TaskDescription description = templateTask.getTaskDescription();
        this.taskDescription = new TaskDescription(description.getName(),
                description.getDescription());
        this.taskProperties = new TaskProperties(templateTask.getTaskProperties());
        
        this.daysOfMonth = Sets.newHashSet(parseDayOfMonth(templateTask.getDaysOfMonth()));
        this.daysOfWeek = Sets.newHashSet(parseDayOfWeek(templateTask.getDaysOfWeek()));
    }
    
    @JsonCreator
    public TemplateTask(@JsonProperty("taskDescription") TaskDescription taskDescription,
            @JsonProperty("taskProperties") TaskProperties taskProperties,
            @JsonProperty("daysOfMonth") Set<Integer> daysOfMonth,
            @JsonProperty("daysOfWeek") Set<DayOfWeek> daysOfWeek) {
        this.taskDescription = taskDescription;
        this.taskProperties = taskProperties;
        this.daysOfMonth = parseDayOfMonth(daysOfMonth);
        this.daysOfWeek = parseDayOfWeek(daysOfWeek);
    }
    
    public TaskDescription getTaskDescription() {
        return taskDescription;
    }
    
    public TaskProperties getTaskProperties() {
        return taskProperties;
    }
    
    public Set<Integer> getDaysOfMonth() {
        return getIntegerValues(daysOfMonth);
    }
    
    public Set<DayOfWeek> getDaysOfWeek() {
        return getDaysOfWeekValues(daysOfWeek);
    }
    
    private Set<DayOfWeekDecorator> parseDayOfWeek(Set<DayOfWeek> daysOfWeek) {
        Set<DayOfWeekDecorator> result = new HashSet<>();
        for (DayOfWeek dayOfWeek : daysOfWeek) {
            result.add(new DayOfWeekDecorator(dayOfWeek));
        }
        
        return result;
    }
    
    private Set<DayOfMonthDecorator> parseDayOfMonth(Set<Integer> daysOfMonth) {
        Set<DayOfMonthDecorator> result = new HashSet<>();
        for (Integer integer : daysOfMonth) {
            result.add(new DayOfMonthDecorator(integer));
        }
        
        return result;
    }
    
    private Set<Integer> getIntegerValues(Set<DayOfMonthDecorator> daysOfMonth) {
        Set<Integer> result = new HashSet<>();
        for (DayOfMonthDecorator dayOfMonthDecorator : daysOfMonth) {
            result.add(dayOfMonthDecorator.integerValue());
        }
        
        return result;
    }
    
    private Set<DayOfWeek> getDaysOfWeekValues(Set<DayOfWeekDecorator> daysOfWeek) {
        Set<DayOfWeek> result = new HashSet<>();
        
        for (DayOfWeekDecorator dayOfWeekDecorator : daysOfWeek) {
            result.add(dayOfWeekDecorator.getDayOfWeek());
        }
        
        return result;
    }
}
