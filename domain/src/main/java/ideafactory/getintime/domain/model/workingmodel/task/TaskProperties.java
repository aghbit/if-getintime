package ideafactory.getintime.domain.model.workingmodel.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import ideafactory.getintime.domain.mappers.EstimatedTimeDeserializer;
import ideafactory.getintime.domain.mappers.IDurationMinutesSerializer;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.task.loggedtime.EstimatedTimeType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Embeddable
@TypeDefs(value = { @TypeDef(name = "estimatedTimeType", defaultForType = EstimatedTimeType.class, typeClass = EstimatedTimeType.class) })
@JsonIgnoreProperties(value = { "subTasks" }, ignoreUnknown = true)
public class TaskProperties {
    
    private int priority;
    
    @Type(type = "estimatedTimeType")
    @JsonSerialize(using = IDurationMinutesSerializer.class)
    @JsonDeserialize(using = EstimatedTimeDeserializer.class)
    private EstimatedTime estimatedTime;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private Employee assigned;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private Employee createdBy;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Set<Task> subTasks = new HashSet<>();
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Task parentTask;
    
    private TaskProperties() {
    }
    
    public TaskProperties(int priority, EstimatedTime estimatedTime, Employee assigned,
            Employee createdBy, Set<Task> subTasks) {
        this.priority = priority;
        this.estimatedTime = estimatedTime;
        this.assigned = assigned;
        this.createdBy = createdBy;
        if (subTasks != null) {
            this.subTasks = subTasks;
        }
    }
    
    @JsonCreator
    public TaskProperties(@JsonProperty("priority") int priority,
            @JsonProperty("estimatedTime") EstimatedTime estimatedTime,
            @JsonProperty("assigned") Employee assigned,
            @JsonProperty("createdBy") Employee createdBy,
            @JsonProperty("subTasks") Set<Task> subTasks,
            @JsonProperty("parentTaskDescription") ParentTaskDescription parentTaskDescription) {
        this(priority, estimatedTime, assigned, createdBy, subTasks);
        if (parentTaskDescription.getId() != null) {
            assignParentTask(parentTaskDescription);
        }
    }
    
    private void assignParentTask(ParentTaskDescription parentTaskDescription) {
        Task parentTask = new Task();
        parentTask.setId(parentTaskDescription.getId());
        this.parentTask = parentTask;
    }
    
    public TaskProperties(TaskProperties properties) {
        this.priority = properties.priority;
        this.estimatedTime = properties.estimatedTime;
        this.assigned = properties.assigned;
        this.createdBy = properties.createdBy;
        this.subTasks = Sets.newHashSet(properties.getSubTasks());
        this.parentTask = properties.parentTask;
    }
    
    public int getPriority() {
        return priority;
    }
    
    public void setPriority(int priority) {
        this.priority = priority;
    }
    
    public EstimatedTime getEstimatedTime() {
        return estimatedTime;
    }
    
    public void setEstimatedTime(EstimatedTime estimatedTime) {
        this.estimatedTime = estimatedTime;
    }
    
    public Employee getAssigned() {
        return assigned;
    }
    
    public void setAssigned(Employee assigned) {
        this.assigned = assigned;
    }
    
    public Employee getCreatedBy() {
        return createdBy;
    }
    
    public void setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
    }
    
    public void setSubTasks(Set<Task> subTasks) {
        this.subTasks = subTasks;
    }
    
    public Set<Task> getSubTasks() {
        return subTasks;
    }
    
    public void addSubTask(Task task) {
        subTasks.add(task);
    }
    
    public Iterator<Task> iterator() {
        return subTasks.iterator();
    }
    
    @JsonIgnore
    public Task getParentTask() {
        return parentTask;
    }
    
    public void setParentTask(Task parentTask) {
        this.parentTask = parentTask;
    }
    
    public ParentTaskDescription getParentTaskDescription() {
        if (parentTask != null) {
            return new ParentTaskDescription(parentTask.getId(), parentTask.getName());
        }
        return ParentTaskDescription.EMPTY;
    }
    
}
