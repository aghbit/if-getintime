package ideafactory.getintime.domain.model.workingmodel.task.decorators;

import javax.persistence.Embeddable;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Embeddable
public class DayOfMonthDecorator {
    private Integer dayOfMonth;
    
    private DayOfMonthDecorator() {
    }
    
    public DayOfMonthDecorator(Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }
    
    public Integer integerValue() {
        return dayOfMonth;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        
        DayOfMonthDecorator that = (DayOfMonthDecorator) o;
        
        if (!dayOfMonth.equals(that.dayOfMonth))
            return false;
        
        return true;
    }
    
    @Override
    public int hashCode() {
        return dayOfMonth.hashCode();
    }
}
