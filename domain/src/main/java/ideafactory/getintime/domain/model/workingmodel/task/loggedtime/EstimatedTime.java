package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

import ideafactory.geintime.commons.preconditions.GetintimePreconditions;

/**
 * @author Piotr Góralczyk
 */
public final class EstimatedTime implements IDurationMinutes {
    private final DurationMinutes durationMinutes;
    
    public EstimatedTime(long minutes) {
        GetintimePreconditions.checkAboveZero(minutes, "Estimated time must be above zero.",
                "Exception.EstimatedTime.MustBeAboveZero");
        durationMinutes = new DurationMinutes(minutes);
    }
    
    @Override
    public long getMinutes() {
        return durationMinutes.getMinutes();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof EstimatedTime))
            return false;
        
        EstimatedTime that = (EstimatedTime) o;
        
        if (!durationMinutes.equals(that.durationMinutes))
            return false;
        
        return true;
    }
    
    @Override
    public int hashCode() {
        return durationMinutes.hashCode();
    }
    
    @Override
    public String toString() {
        return "Estimated time: " + durationMinutes;
    }
}
