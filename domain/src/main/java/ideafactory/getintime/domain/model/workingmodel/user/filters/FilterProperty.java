package ideafactory.getintime.domain.model.workingmodel.user.filters;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import javax.persistence.Embeddable;

/**
 * @author partyks
 */
@Embeddable
public class FilterProperty<T> {
    private T name;
    private boolean checked;
    
    private FilterProperty() {
        
    }
    
    @JsonCreator
    public FilterProperty(@JsonProperty("name") T name, @JsonProperty("checked") boolean checked) {
        this.name = name;
        this.checked = checked;
    }
    
    public T getName() {
        return name;
    }
    
    public void setName(T name) {
        this.name = name;
    }
    
    public boolean isChecked() {
        return checked;
    }
    
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        
        FilterProperty that = (FilterProperty) o;
        
        return Objects.equal(isChecked(), that.checked) && Objects.equal(getName(), that.name);
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), isChecked());
    }
}
