package ideafactory.getintime.domain.model.workingmodel.task;

import com.google.common.collect.ImmutableMap;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public class TaskPropertiesBuilder {
    private int priority;
    
    private EstimatedTime estimatedTime;
    
    private Employee assigned;
    
    private Employee createdBy;

    
    private Set<Task> subTasks = new HashSet<>();
    
    public TaskPropertiesBuilder setPriority(int priority) {
        this.priority = priority;
        return this;
    }
    
    public TaskPropertiesBuilder setEstimatedTime(EstimatedTime estimatedTime) {
        this.estimatedTime = estimatedTime;
        return this;
    }
    
    public TaskPropertiesBuilder setAssigned(Employee assigned) {
        this.assigned = assigned;
        return this;
    }
    
    public TaskPropertiesBuilder setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    

    
    public TaskPropertiesBuilder setSubTasks(Set<Task> subTasks) {
        this.subTasks = subTasks;
        return this;
    }
    
    public TaskProperties build() {
        return new TaskProperties(priority, estimatedTime, assigned, createdBy, subTasks);
    }
}
