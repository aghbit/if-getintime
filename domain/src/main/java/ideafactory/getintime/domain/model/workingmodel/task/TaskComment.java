package ideafactory.getintime.domain.model.workingmodel.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.mappers.LocalDateTimeDeserializer;
import ideafactory.getintime.domain.mappers.LocalDateTimeSerializer;
import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

/**
 * Created by slonka on 15.03.14.
 */
@Entity
@Audited
public class TaskComment extends AbstractTimerEntity {
    @ManyToOne
    private Employee employee;
    
    private String text;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime time;
    
    TaskComment() {
        
    }
    
    @JsonCreator
    public TaskComment(@JsonProperty("employee") Employee employee,
            @JsonProperty("text") String text) {
        GetintimePreconditions.checkNotNull(employee, "Exception.TaskComment.NoAuthor");
        GetintimePreconditions.checkNotNull(text, "TaskComment.NoText");
        
        this.employee = employee;
        this.text = text;
        this.time = LocalDateTime.now();
    }
    
    public Employee getEmployee() {
        return employee;
    }
    
    public String getText() {
        return text;
    }
    
    public LocalDateTime getTime() {
        return time;
    }
    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public void setTime(LocalDateTime time) {
        this.time = time;
    }
    
    private boolean sameValues(TaskComment that) {
        if (employee.equals(that.employee) && text.equals(that.text) && time.equals(that.time))
            return true;
        else
            return false;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        
        TaskComment that = (TaskComment) o;
        
        if (this.getId() != null && that.getId() != null) {
            if (this.getId().equals(that.getId())) {
                return true;
            } else {
                return false;
            }
        }
        
        if (that.getId() == null && this.getId() == null) {
            // objects with no ids => compare by fields
            if (sameValues(that)) {
                return true;
            } else {
                return false;
            }
        }
        
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.getId() != null) {
            return super.hashCode();
        } else {
            int result = employee.hashCode();
            result = 31 * result + text.hashCode();
            result = 31 * result + time.hashCode();
            return result;
        }
    }
}
