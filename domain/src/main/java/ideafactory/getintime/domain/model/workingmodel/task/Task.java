package ideafactory.getintime.domain.model.workingmodel.task;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.exception.ProgressPercentageException;
import ideafactory.getintime.domain.hibernate.HibernateEntityIdLazyLoader;
import ideafactory.getintime.domain.mappers.IDurationMinutesSerializer;
import ideafactory.getintime.domain.mappers.LoggedTimeDeserializer;
import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.LoggedTime;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Percentage;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.model.workingmodel.task.workingstartdate.WorkingStartDate;
import ideafactory.getintime.domain.model.workingmodel.task.workingstartdate.factory.WorkingStartDateFactory;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.task.properties.TaskStatus;
import ideafactory.getintime.domain.task.properties.TaskType;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author Michal Partyka
 */

@Audited
@Entity
@JsonIgnoreProperties(value = { "percentage" }, ignoreUnknown = true)
public class Task extends AbstractTimerEntity {
    
    @Embedded
    private TaskProperties taskProperties;
    
    @Embedded
    private TaskDescription taskDescription;
    
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyJoinColumn(name = "employee_id")
    @JsonIgnore
    private Map<Employee, LoggedTime> loggedTimes = new HashMap<>();
    
    @Enumerated(EnumType.STRING)
    private TaskStatus status = TaskStatus.TODO;
    
    @AttributeOverrides(@AttributeOverride(name = "date", column = @Column(name = "assigneedeadline")))
    @Embedded
    private Term assigneeDeadline;
    
    @AttributeOverrides(@AttributeOverride(name = "date", column = @Column(name = "ownerdeadline")))
    @Embedded
    private Term ownerDeadline;
    
    @Enumerated(EnumType.STRING)
    private TaskType type;
    
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private WorkingStartDate workingStartDate;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<TaskComment> comments = new HashSet<>();
    
    @Embedded
    private Percentage progressPercentage = Percentage.ZERO;
    
    private boolean overdue;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    private CyclicTask cyclicTask;
    
    public Task() {
    }
    
    @JsonCreator
    public Task(@JsonProperty("id") Long id,
            @JsonProperty("taskDescription") TaskDescription taskDescription,
            @JsonProperty("taskProperties") TaskProperties taskProperties,
            @JsonProperty("ownerDeadline") Term ownerDeadline,
            @JsonProperty("assigneeDeadline") Term assigneeDeadline,
            @JsonProperty("type") TaskType type) {
        GetintimePreconditions.checkNotNull(assigneeDeadline, "Exception.Task.NoAssigneeDeadline");
        if (ownerDeadline != null) {
            GetintimePreconditions.checkChronologicalDateOrder(assigneeDeadline.getDate(),
                    ownerDeadline.getDate(),
                    "Assignee's deadline cannot be after owner's deadline",
                    "Exception.Task.AssigneeDeadlineAfterOwnerDeadline");
        } else {
            ownerDeadline = new Term(assigneeDeadline.getDate());
        }
        this.taskDescription = taskDescription;
        this.taskProperties = taskProperties;
        this.ownerDeadline = ownerDeadline;
        this.assigneeDeadline = assigneeDeadline;
        this.type = type;
        this.overdue = checkIfOverdue(assigneeDeadline.getDate());
        super.setId(id);
        addLoggedTimeEntry(taskProperties.getAssigned());
    }
    
    public Task(TemplateTask templateTask) {
        this.taskDescription = templateTask.getTaskDescription();
        this.taskProperties = templateTask.getTaskProperties();
        addLoggedTimeEntry(taskProperties.getAssigned());
    }
    
    public void update(Task task) {
        task.taskProperties.setSubTasks(this.getSubTasks());
        this.taskProperties = task.taskProperties;
        this.taskDescription = task.taskDescription;
        this.status = task.status;
        this.assigneeDeadline = task.assigneeDeadline;
        this.ownerDeadline = task.ownerDeadline;
        this.type = task.type;
        this.workingStartDate = task.workingStartDate;
        this.progressPercentage = task.progressPercentage;
        this.overdue = checkIfOverdue(task.assigneeDeadline.getDate());
        addLoggedTimeEntry(task.getAssigned());
    }
    
    public void logTimeForAssignee(Consumer<LoggedTime> consumer) throws IllegalStateException {
        GetintimePreconditions.checkState(getTaskProperties().getAssigned() != null,
                "Task is not assigned to anybody!", "Exception.Task.employeeMustBeAssigned");
        GetintimePreconditions.checkState(getStatus() != TaskStatus.DONE,
                "Cannot log time to finished task!", "Exception.Task.alreadyInDone");
        GetintimePreconditions.checkState(getStatus() != TaskStatus.CANCELLED,
                "Cannot log time to cancelled task!", "Exception.Task.taskCancelled");
        
        consumer.accept(getAssigneeLoggedTime());
        status = TaskStatus.STOPPED;
    }
    
    public void assign(Employee employee) {
        this.getTaskProperties().setAssigned(employee);
        addLoggedTimeEntry(employee);
    }
    
    @JsonIgnore
    public Employee getAssigned() {
        return this.getTaskProperties().getAssigned();
    }
    
    @JsonProperty
    TaskProperties getTaskProperties() {
        return taskProperties;
    }
    
    @JsonProperty
    void setTaskProperties(TaskProperties taskProperties) {
        this.taskProperties = taskProperties;
    }
    
    public TaskDescription getTaskDescription() {
        return taskDescription;
    }
    
    public void setTaskDescription(TaskDescription taskDescription) {
        this.taskDescription = taskDescription;
    }
    
    @JsonIgnore
    public int getPriority() {
        return this.getTaskProperties().getPriority();
    }
    
    public void setPriority(int priority) {
        this.getTaskProperties().setPriority(priority);
    }
    
    @JsonIgnore
    public EstimatedTime getEstimatedTime() {
        return this.getTaskProperties().getEstimatedTime();
    }
    
    public void setEstimatedTime(EstimatedTime estimatedTime) {
        this.getTaskProperties().setEstimatedTime(estimatedTime);
    }
    
    @JsonProperty("loggedTime")
    @JsonSerialize(using = IDurationMinutesSerializer.class)
    public LoggedTime getAssigneeLoggedTime() {
        return loggedTimes.get(getAssigned());
    }
    
    @JsonProperty("loggedTime")
    @JsonDeserialize(using = LoggedTimeDeserializer.class)
    public void setAssigneeLoggedTime(LoggedTime loggedTime) {
        loggedTimes.put(getAssigned(), loggedTime);
    }
    
    @JsonIgnore
    Map<Employee, LoggedTime> getLoggedTimes() {
        return Collections.unmodifiableMap(loggedTimes);
    }
    
    public TaskStatus getStatus() {
        return status;
    }
    
    public void setStatus(TaskStatus status) {
        this.status = status;
    }
    
    public Term getAssigneeDeadline() {
        return assigneeDeadline;
    }
    
    public void setAssigneeDeadline(Term assigneeDeadline) {
        this.assigneeDeadline = assigneeDeadline;
    }
    
    public Term getOwnerDeadline() {
        return ownerDeadline;
    }
    
    public void setOwnerDeadline(Term ownerDeadline) {
        this.ownerDeadline = ownerDeadline;
    }
    
    public TaskType getType() {
        return type;
    }
    
    public void setType(TaskType type) {
        this.type = type;
    }
    
    public boolean isOverdue() {
        return overdue;
    }
    
    public void setOverdue(boolean isOverdue) {
        this.overdue = isOverdue;
    }
    
    @JsonIgnore
    public Employee getCreatedBy() {
        return this.getTaskProperties().getCreatedBy();
    }
    
    private void setCreatedBy(Employee createdBy) {
        this.getTaskProperties().setCreatedBy(createdBy);
    }
    
    @JsonIgnore
    public Optional<WorkingStartDate> getWorkingStartDateOptional() {
        return Optional.ofNullable(workingStartDate);
    }
    
    @Deprecated
    public WorkingStartDate getWorkingStartDate() {
        return workingStartDate;
    }
    
    void setWorkingStartDate(WorkingStartDate workingStartDate) {
        this.workingStartDate = workingStartDate;
    }
    
    public void startWorking() throws IllegalStateException {
        GetintimePreconditions.checkNotNull(getAssigned(), "Exception.Task.employeeMustBeAssigned");
        GetintimePreconditions.checkState(this.getStatus() != TaskStatus.IN_PROGRESS,
                "Task is already in progress", "Exception.Task.alreadyInProgress");
        GetintimePreconditions.checkState(this.getStatus() != TaskStatus.DONE,
                "Task is already done", "Exception.Task.alreadyInDone");
        GetintimePreconditions.checkState(!getTaskProperties().getAssigned().isWorkingOnTask(),
                "Cannot start working on task when already working on one",
                "Exception.Task.alreadyWorkingOn");
        GetintimePreconditions.checkState(this.getStatus() != TaskStatus.CANCELLED,
                "Cannot start working on cancelled task", "Exception.Task.taskCancelled");
        
        workingStartDate = WorkingStartDateFactory.createCurrentWorkingStartDate();
        startProgress();
    }
    
    public void stopWorking(LocalDateTime now) throws IllegalStateException {
        GetintimePreconditions.checkNotNull(now, "Exception.Task.currentDateTimeCannotBeNull");
        GetintimePreconditions.checkState(this.getStatus() == TaskStatus.IN_PROGRESS,
                "Task is not in progress!", "Exception.Task.notInProgress");
        status = TaskStatus.STOPPED;
        
        getAssigneeLoggedTime().addMinutes(
                workingStartDate.getTimeDifferenceFromDate(now).toMinutes());
        
        workingStartDate = null;
    }
    
    private void startProgress() {
        status = TaskStatus.IN_PROGRESS;
    }
    
    public boolean isBeingWorkedOn() {
        return status == TaskStatus.IN_PROGRESS;
    }
    
    @Deprecated
    public void setBeingWorkedOn(boolean beingWorkedOn) {
        // For jackson... ignore...
        // see: https://github.com/FasterXML/jackson-databind/issues/95
    }
    
    public void setName(String name) {
        final TaskDescription newDescription = new TaskDescription(name, getDescription());
        this.setTaskDescription(newDescription);
    }
    
    @JsonIgnore
    public String getName() {
        // TU JEST NULL
        if(this.getTaskDescription() == null) return new String("");
        else {
            return this.getTaskDescription().getName();
        }
    }
    
    @JsonIgnore
    public String getDescription() {
        return this.getTaskDescription().getDescription();
    }
    
    private void setDescription(String description) {
        final TaskDescription newDescription = new TaskDescription(getName(), description);
        
        this.setTaskDescription(newDescription);
    }
    
    @Override
    public String toString() {
        return "Task{" + "taskProperties=" + taskProperties + ", taskDescription="
                + taskDescription + ", loggedTimes=" + loggedTimes + ", status=" + status
                + ", assigneeDeadline=" + assigneeDeadline + ", ownerDeadline=" + ownerDeadline
                + ", type=" + type + ", workingStartDate=" + workingStartDate + ", comments="
                + comments + ", progressPercentage=" + progressPercentage + ", overdue=" + overdue
                + ", cyclicTask=" + cyclicTask + '}';
    }
    
    public void addSubTask(Task subtask) {
        GetintimePreconditions.checkNotNull(subtask.getAssigneeDeadline(),
                "Exception.Task.NoAssigneeDeadline");
        GetintimePreconditions.checkNotNull(subtask.getOwnerDeadline(),
                "Exception.Task.NoOwnerDeadline");
        GetintimePreconditions.checkChronologicalDateOrder(subtask.getAssigneeDeadline().getDate(),
                subtask.getOwnerDeadline().getDate(),
                "Owners deadline cannot be before assignees deadline",
                "Exception.Task.AssigneeDeadlineAfterOwnerDeadline");
        GetintimePreconditions.checkChronologicalDateOrder(subtask.getOwnerDeadline().getDate(),
                this.getOwnerDeadline().getDate(),
                "Owners deadline cannot be after parent owners deadline",
                "Exception.Subtask.OwnerDeadlineAfterParentTaskOwnerDeadline");
        GetintimePreconditions.checkState(status != TaskStatus.DONE,
                "Cannot add subtask to finished task", "Exception.Subtask.TaskAlreadyFinished");
        
        subtask.getTaskProperties().setParentTask(this);
        this.getTaskProperties().addSubTask(subtask);
    }
    
    @JsonIgnore
    public Set<Task> getSubTasks() {
        return this.getTaskProperties().getSubTasks();
    }

    
    public Iterator<Task> iterator() {
        return this.getTaskProperties().iterator();
    }
    
    @JsonIgnore
    public Task getParentTask() {
        return this.getTaskProperties().getParentTask();
    }
    
    public void doneWorkingOnActiveTask(LocalDateTime now) throws IllegalStateException {
        GetintimePreconditions.checkState(this.getStatus() == TaskStatus.IN_PROGRESS,
                "Cannot finish not started task!", "Exception.Task.notInProgress");
        
        status = TaskStatus.DONE;
        
        getAssigneeLoggedTime().addMinutes(
                workingStartDate.getTimeDifferenceFromDate(now).toMinutes());
        
        workingStartDate = null;
        
        progressPercentage = Percentage.ONE_HUNDRED;
        
        overdue = false;
    }
    
    public void doneWorkingOnStoppedTask() {
        GetintimePreconditions.checkState(this.getStatus() == TaskStatus.STOPPED,
                "Cannot finish task", "Exception.Task.notStopped");
        
        status = TaskStatus.DONE;
        
        progressPercentage = Percentage.ONE_HUNDRED;
        
        overdue = false;
    }
    
    public void cancelTask(LocalDateTime now) throws IllegalStateException {
        GetintimePreconditions.checkState(this.getStatus() != TaskStatus.CANCELLED,
                "Task already cancelled!", "Exception.Task.alreadyCancelled");
        GetintimePreconditions.checkState(this.getStatus() != TaskStatus.DONE,
                "Task already done!", "Exception.Task.alreadyInDone");
        
        if (this.getStatus() == TaskStatus.IN_PROGRESS) {
            Duration timeToLog = workingStartDate.getTimeDifferenceFromDate(now);
            getAssigneeLoggedTime().addMinutes(timeToLog.toMinutes());
            
            workingStartDate = null;
        }
        
        this.status = TaskStatus.CANCELLED;
        this.overdue = false;
    }
    
    public Percentage getProgressPercentage() {
        return progressPercentage;
    }
    
    public void updateWorkProgressPercentage(Integer progressPercentage)
            throws ProgressPercentageException {
        GetintimePreconditions.checkNotNull(getAssigned(), "Exception.Task.employeeMustBeAssigned");
        GetintimePreconditions.checkState(status != TaskStatus.DONE, "Task is already done",
                "Exception.Task.alreadyInDone");
        GetintimePreconditions.checkState(status != TaskStatus.CANCELLED, "Task is cancelled",
                "Exception.Task.taskCancelled");
        
        this.progressPercentage = new Percentage(progressPercentage);
        
        if (this.progressPercentage.equals(Percentage.ONE_HUNDRED)) {
            status = TaskStatus.DONE;
        }
        
    }
    
    public void reopen(LocalDate ownerDeadline, LocalDate assigneeDeadline,
            Percentage percentageDoneAfterReopening) {
        GetintimePreconditions.checkNotNull(percentageDoneAfterReopening,
                "Exception.Task.WorkPercentageDoneNotProvided");
        GetintimePreconditions.checkNotNull(ownerDeadline, "Exception.Task.DeadlineNotProvided");
        GetintimePreconditions.checkNotNull(assigneeDeadline, "Exception.Task.DeadlineNotProvided");
        GetintimePreconditions.checkState(
                !percentageDoneAfterReopening.equals(Percentage.ONE_HUNDRED),
                "Cannot reopen task with 100% work done percentage",
                "Exception.Task.ReopeningWrongPercentageDone");
        GetintimePreconditions.checkState(isCancelledOrDone(),
                "Cannot reopen task that is not done or cancelled",
                "Exception.Task.Reopen.InvalidTaskStatus");
        
        setStatus(TaskStatus.STOPPED);
        this.progressPercentage = percentageDoneAfterReopening;
        setAssigneeDeadline(new Term(assigneeDeadline));
        setOwnerDeadline(new Term(ownerDeadline));
    }
    
    private boolean isCancelledOrDone() {
        return status == TaskStatus.CANCELLED || status == TaskStatus.DONE;
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
    
    @JsonIgnore
    public Set<TaskComment> getComments() {
        return comments;
    }
    
    public void addComment(TaskComment taskComment) {
        comments.add(taskComment);
    }
    
    public Long getCyclicTaskId() {
        return HibernateEntityIdLazyLoader.getIdFromLazyObject(cyclicTask);
    }
    
    @JsonIgnore
    public CyclicTask getCyclicTask() {
        return cyclicTask;
    }
    
    public void setCyclicTask(CyclicTask cyclicTask) {
        this.cyclicTask = cyclicTask;
    }
    
    private void addLoggedTimeEntry(Employee employee) {
        if (employee != null) {
            loggedTimes.putIfAbsent(employee, new LoggedTime());
        }
    }
    
    private boolean checkIfOverdue(LocalDate deadlineDate) {
        return LocalDate.now().isAfter(deadlineDate);
    }
}
