package ideafactory.getintime.domain.model.workingmodel.task.loggedtime;

/**
 * @author Piotr Góralczyk
 */
public interface IDurationMinutes {
    long getMinutes();
}
