package ideafactory.getintime.domain.model.workingmodel.task.decorators;

import javax.persistence.Embeddable;
import java.time.DayOfWeek;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Embeddable
public class DayOfWeekDecorator {
    private Integer integer;
    
    private DayOfWeekDecorator() {
    }
    
    public DayOfWeekDecorator(DayOfWeek dayOfWeek) {
        this.integer = dayOfWeek.getValue();
    }
    
    public DayOfWeek getDayOfWeek() {
        return DayOfWeek.of(integer);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        
        DayOfWeekDecorator that = (DayOfWeekDecorator) o;
        
        if (!integer.equals(that.integer))
            return false;
        
        return true;
    }
    
    @Override
    public int hashCode() {
        return integer.hashCode();
    }
}
