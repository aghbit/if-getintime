package ideafactory.getintime.domain.model.workingmodel.task.cyclic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.model.workingmodel.task.TaskProperties;

import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.sort;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
public class TemplateTaskBuilder {
    private TaskDescription taskDescription;
    private TaskProperties taskProperties;
    private Set<Integer> daysOfMonth = new HashSet<>();
    private Set<DayOfWeek> daysOfWeek = new HashSet<>();
    
    public TemplateTaskBuilder withTaskDescription(TaskDescription task) {
        this.taskDescription = task;
        return this;
    }
    
    public TemplateTaskBuilder withTaskPriorities(TaskProperties properties) {
        this.taskProperties = properties;
        return this;
    }
    
    public TemplateTaskBuilder everyDayOfMonth(List<Integer> monthsDays) {
        Preconditions.checkArgument(!monthsDays.isEmpty());
        sort(monthsDays);
        this.daysOfMonth = Sets.newHashSet(monthsDays);
        return this;
    }
    
    public TemplateTaskBuilder everyDayOfMonth(Integer... monthsDays) {
        Preconditions.checkArgument(!(monthsDays.length == 0));
        this.daysOfMonth = Sets.newHashSet(monthsDays);
        return this;
    }
    
    public TemplateTaskBuilder everyDayOfWeek(List<DayOfWeek> dayOfWeek) {
        daysOfWeek = Sets.newHashSet(dayOfWeek);
        return this;
    }
    
    public TemplateTaskBuilder everyDayOfWeek(DayOfWeek... dayOfWeek) {
        daysOfWeek = Sets.newHashSet(dayOfWeek);
        return this;
    }
    
    public TemplateTask build() {
        Preconditions.checkNotNull(taskDescription);
        Preconditions.checkNotNull(taskProperties);
        
        return new TemplateTask(taskDescription, taskProperties, daysOfMonth, daysOfWeek);
    }
}
