package ideafactory.getintime.domain.model.workingmodel.user.permissions;

import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.user.permissions.policy.EditPolicy;
import ideafactory.getintime.domain.user.permissions.policy.ViewPolicy;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

/**
 * @author Michal Partyka
 */
@Audited
@Entity
public class Permission extends AbstractTimerEntity implements ViewPolicy, EditPolicy {
    
    @OneToOne(fetch = FetchType.EAGER)
    private Employee employee;
    
    @Override
    public boolean canEdit(Employee employee) {
        return true; // to be implemented TODO
    }
    
    @Override
    public boolean canView(Employee employee) {
        return true; // to be implemented TODO
    }
}
