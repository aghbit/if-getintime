package ideafactory.getintime.domain.messages;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Simple message wrapper.
 * 
 * @author Lukasz Raduj raduj.lukasz@gmail.com
 */
@XmlRootElement
public class Message {
    private final String message;
    
    public Message(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
}
