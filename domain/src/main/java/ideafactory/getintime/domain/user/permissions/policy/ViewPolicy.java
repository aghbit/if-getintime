package ideafactory.getintime.domain.user.permissions.policy;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

/**
 * @author Michal Partyka
 */
public interface ViewPolicy {
    boolean canView(Employee employee);
}
