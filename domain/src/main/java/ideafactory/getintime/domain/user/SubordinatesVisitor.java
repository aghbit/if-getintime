package ideafactory.getintime.domain.user;

import com.google.common.collect.Lists;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.visitorapi.IEmployeeVisitor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Michal Partyka
 */
public class SubordinatesVisitor implements IEmployeeVisitor {
    private Set<Employee> subordinates = new HashSet<>();
    
    @Override
    public void visit(Employee visitable) {
        subordinates.add(visitable);
    }
    
    public List<Employee> getSubordinates() {
        return Lists.newArrayList(subordinates);
    }
}
