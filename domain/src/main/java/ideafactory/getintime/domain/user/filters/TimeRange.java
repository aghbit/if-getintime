package ideafactory.getintime.domain.user.filters;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ideafactory.getintime.domain.mappers.LocalDateDeserializer;
import ideafactory.getintime.domain.mappers.LocalDateSerializer;

import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * @author partyks
 */
@Embeddable
public class TimeRange {
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate fromDate;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate toDate;
    
    private TimeRange() {
        
    }
    
    @JsonCreator
    public TimeRange(
            @JsonProperty("from") @JsonDeserialize(using = LocalDateDeserializer.class) @JsonSerialize(using = LocalDateSerializer.class) LocalDate fromDate,
            @JsonProperty("to") @JsonDeserialize(using = LocalDateDeserializer.class) @JsonSerialize(using = LocalDateSerializer.class) LocalDate toDate) {
        this.fromDate = fromDate;
        this.toDate = toDate;
    }
    
    @JsonProperty("from")
    public LocalDate getFromDate() {
        return fromDate;
    }
    
    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }
    
    @JsonProperty("to")
    public LocalDate getToDate() {
        return toDate;
    }
    
    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }
}
