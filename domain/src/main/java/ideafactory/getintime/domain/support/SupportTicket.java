package ideafactory.getintime.domain.support;

public class SupportTicket {
    private String reporter;
    private String subject;
    private String description;
    
    public String getReporter() {
        return reporter;
    }
    
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }
    
    public String getSubject() {
        return subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString() {
        return "SupportTicket{" + "reporter='" + reporter + '\'' + ", subject='" + subject + '\''
                + ", description='" + description + '\'' + '}';
    }
}
