package ideafactory.getintime.domain.mappers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.LoggedTime;

import java.io.IOException;

/**
 * @author Maciej Ciołek
 */
public class LoggedTimeDeserializer extends JsonDeserializer<LoggedTime> {
    
    @Override
    public LoggedTime deserialize(JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException,
            JsonProcessingException {
        final Long minutes = jsonParser.getLongValue();
        
        final LoggedTime loggedTime = new LoggedTime();
        return loggedTime.addMinutes(minutes);
    }
}
