package ideafactory.getintime.domain.mappers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;

import java.io.IOException;

/**
 * @author Piotr Góralczyk
 */
public class EstimatedTimeDeserializer extends JsonDeserializer<EstimatedTime> {
    @Override
    public EstimatedTime deserialize(JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException,
            JsonProcessingException {
        return new EstimatedTime(jsonParser.getLongValue());
    }
}
