package ideafactory.getintime.domain.mappers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;

import java.io.IOException;

/**
 * @author Piotr Góralczyk
 */
public class IDurationMinutesSerializer extends JsonSerializer<IDurationMinutes> {
    @Override
    public void serialize(IDurationMinutes durationMinutes, JsonGenerator jsonGenerator,
            SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNumber(durationMinutes.getMinutes());
    }
}
