package ideafactory.getintime.domain.exception;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Grzegorz Miejski on 01.05.14.
 */
public class NoLoggedUserException extends StatusException {
    
    private final static String MESSAGE_TEMPLATE = "No employee is logged in!";
    private final static String ERROR_CODE = "Employee.NoLoggedUser";
    
    public NoLoggedUserException() {
        super(MESSAGE_TEMPLATE, ERROR_CODE);
    }
    
}
