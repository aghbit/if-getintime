package ideafactory.getintime.domain.exception;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Grzegorz Miejski on 03.05.14.
 */
public class NoRequiredRoleException extends StatusException {
    
    private final static String MESSAGE_TEMPLATE = "Logged user doesn't have required roles";
    private final static String ERROR_CODE = "Exception.Task.NoRequiredRole";
    
    public NoRequiredRoleException() {
        super(MESSAGE_TEMPLATE, ERROR_CODE);
    }
    
    public NoRequiredRoleException(String message) {
        super(message, ERROR_CODE);
    }
    
    public NoRequiredRoleException(String message, String errorCode) {
        super(message, errorCode);
    }
    
}
