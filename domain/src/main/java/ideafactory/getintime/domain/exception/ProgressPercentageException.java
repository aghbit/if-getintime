package ideafactory.getintime.domain.exception;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Grzegorz Miejski on 20.03.14.
 */
public class ProgressPercentageException extends StatusException {
    
    public ProgressPercentageException(String message, String errorCode) {
        super(message, errorCode);
    }
    
}
