package ideafactory.getintime.domain.hibernate.envers;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Grzegorz Miejski on 02.05.14.
 */
public class RevisionEntityListener implements RevisionListener {
    
    @Override
    public void newRevision(Object revisionObject) {
        GetintimeRevisionEntity revisionEntity = (GetintimeRevisionEntity) revisionObject;
        
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        if (authentication == null)
            revisionEntity.setUserName("OverdueUpdater");
        else
            revisionEntity.setUserName(authentication.getName());
    }
}
