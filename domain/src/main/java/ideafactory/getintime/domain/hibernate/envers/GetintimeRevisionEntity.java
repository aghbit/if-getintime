package ideafactory.getintime.domain.hibernate.envers;

import com.google.common.base.Objects;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Grzegorz Miejski on 02.05.14.
 */

@Entity(name = "revinfo")
@RevisionEntity(RevisionEntityListener.class)
public class GetintimeRevisionEntity {
    
    @Id
    @GeneratedValue
    @RevisionNumber
    @Column(name = "rev")
    private int id;
    
    @RevisionTimestamp
    @Column(name = "rev_timestamp")
    private long revtstmp;
    
    @Column(name = "rev_username")
    private String userName;
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final GetintimeRevisionEntity other = (GetintimeRevisionEntity) obj;
        return Objects.equal(this.id, other.id);
    }
}
