package ideafactory.getintime.domain.hibernate;

import ideafactory.getintime.domain.model.workingmodel.AbstractTimerEntity;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

/**
 * @author Grzegorz Miejski on 02.06.14.
 */
public class HibernateEntityIdLazyLoader {
    
    public static <T> T getIdFromLazyObject(AbstractTimerEntity entity) {
        if (entity == null) {
            return null;
        }
        
        if (entity instanceof HibernateProxy) {
            LazyInitializer lazyInitializer = ((HibernateProxy) entity)
                    .getHibernateLazyInitializer();
            if (lazyInitializer.isUninitialized()) {
                return (T) lazyInitializer.getIdentifier();
            }
        }
        return (T) entity.getId();
    }
}
