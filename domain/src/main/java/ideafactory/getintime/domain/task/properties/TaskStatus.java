package ideafactory.getintime.domain.task.properties;

/**
 * @author Michal Partyka
 */

/**
 * Enum describing if employee has started working on task generally.<br>
 * </br> Has nothing to do with 'live' time counting.<br>
 * </br> TODO - employee has never worked on this task before<br>
 * </br> DONE - employee has finish the task<br>
 * </br> IN_PROGRESS - employee has started this task in past, but it doesn't
 * mean he/she is working on it right now<br>
 * </br>
 * <p>
 * Connected in some way with TaskState.
 */
public enum TaskStatus {
    TODO, DONE, IN_PROGRESS, STOPPED, CANCELLED
}
