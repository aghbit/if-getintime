package ideafactory.getintime.domain.task.creationproperty;

import org.springframework.stereotype.Component;

/**
 * @author Lukasz Raduj <raduj.lukasz@gmail.com>
 */
@Component
public class TaskCreationProperty<T> {
    private final T value;
    
    public TaskCreationProperty(T value) {
        this.value = value;
    }
    
    public T getValue() {
        return value;
    }
}
