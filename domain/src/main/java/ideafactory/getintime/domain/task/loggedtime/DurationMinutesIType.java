package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

/**
 * @author Piotr Góralczyk
 */
public abstract class DurationMinutesIType implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[] { Types.BIGINT };
    }
    
    @Override
    public boolean equals(Object o, Object o2) throws HibernateException {
        return Objects.equals(o, o2);
    }
    
    @Override
    public int hashCode(Object o) throws HibernateException {
        return o.hashCode();
    }
    
    protected abstract IDurationMinutes createDurationMinutes(long minutes);
    
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] strings,
            SessionImplementor sessionImplementor, Object o) throws HibernateException,
            SQLException {
        final long minutes = resultSet.getLong(strings[0]);
        if (!resultSet.wasNull()) {
            return createDurationMinutes(minutes);
        }
        return null;
    }
    
    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object o, int i,
            SessionImplementor sessionImplementor) throws HibernateException, SQLException {
        if (null == o) {
            preparedStatement.setNull(i, sqlTypes()[0]);
        } else {
            final IDurationMinutes durationMinutes = (IDurationMinutes) o;
            preparedStatement.setLong(i, durationMinutes.getMinutes());
        }
    }
    
    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return o;
    }
    
    @Override
    public boolean isMutable() {
        return false;
    }
    
    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        return (Serializable) o;
    }
    
    @Override
    public Object assemble(Serializable serializable, Object o) throws HibernateException {
        return serializable;
    }
    
    @Override
    public Object replace(Object original, Object o2, Object o3) throws HibernateException {
        return original;
    }
}
