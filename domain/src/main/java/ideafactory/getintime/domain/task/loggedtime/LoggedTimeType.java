package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.LoggedTime;

/**
 * @author Michal Partyka
 */
public class LoggedTimeType extends DurationMinutesIType {
    @Override
    public Class returnedClass() {
        return LoggedTime.class;
    }
    
    @Override
    protected IDurationMinutes createDurationMinutes(long minutes) {
        LoggedTime loggedTime = new LoggedTime();
        loggedTime.addMinutes(minutes);
        return loggedTime;
    }
}
