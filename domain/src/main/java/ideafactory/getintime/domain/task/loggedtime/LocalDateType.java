package ideafactory.getintime.domain.task.loggedtime;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Objects;

/**
 * @author Grzegorz Miejski on 09.04.14.
 */
public class LocalDateType implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[] { Types.DATE };
    }
    
    @Override
    public Class returnedClass() {
        return LocalDate.class;
    }
    
    @Override
    public boolean equals(Object o, Object o2) throws HibernateException {
        return Objects.equals(o, o2);
    }
    
    @Override
    public int hashCode(Object o) throws HibernateException {
        return o.hashCode();
    }
    
    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] strings,
            SessionImplementor sessionImplementor, Object o) throws HibernateException,
            SQLException {
        final Date date = resultSet.getDate(strings[0]);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        final LocalDate localDate = LocalDate.of(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        if (!resultSet.wasNull()) {
            return localDate;
        }
        return null;
    }
    
    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object o, int i,
            SessionImplementor sessionImplementor) throws HibernateException, SQLException {
        if (null == o) {
            preparedStatement.setNull(i, sqlTypes()[0]);
        } else {
            final LocalDate localDate = (LocalDate) o;
            preparedStatement.setDate(i, Date.valueOf(localDate));
        }
    }
    
    @Override
    public Object deepCopy(Object o) throws HibernateException {
        return o;
    }
    
    @Override
    public boolean isMutable() {
        return false;
    }
    
    @Override
    public Serializable disassemble(Object o) throws HibernateException {
        return (Serializable) o;
    }
    
    @Override
    public Object assemble(Serializable serializable, Object o) throws HibernateException {
        return serializable;
    }
    
    @Override
    public Object replace(Object original, Object o2, Object o3) throws HibernateException {
        return original;
    }
}
