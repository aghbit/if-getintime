package ideafactory.getintime.domain.task.cyclic;

import com.google.common.collect.Lists;
import ideafactory.geintime.commons.dayoff.DayOffManager;
import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.CyclicTask;
import ideafactory.getintime.domain.model.workingmodel.task.cyclic.TemplateTask;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * @author Michal Partyka
 */
public class CyclicTaskBuilder {
    private TemplateTask templateTask;
    private DayOffManager dayOffManager;
    private LocalDate from;
    private LocalDate to;
    private Map<Term, Set<Term>> dayOffMovedTerms;
    private Map<Term, Set<Integer>> endOfMonthMovedTerms;
    
    public CyclicTaskBuilder withTemplateTask(TemplateTask templateTask) {
        this.templateTask = templateTask;
        return this;
    }
    
    public CyclicTaskBuilder withDayOffManager(DayOffManager dayOffManager) {
        this.dayOffManager = dayOffManager;
        return this;
    }
    
    public CyclicTaskBuilder from(LocalDate date) {
        this.from = date;
        return this;
    }
    
    public CyclicTaskBuilder to(LocalDate date) {
        this.to = date;
        return this;
    }
    
    public CyclicTask build() {
        GetintimePreconditions.checkNotNull(templateTask,
                "Exception.CyclicTaskBuilder.TemplateTaskNull");
        GetintimePreconditions.checkNotNull(dayOffManager,
                "Exception.CyclicTaskBuilder.DayOffManagerNull");
        GetintimePreconditions.checkState(isBeforeOrEqualTheEnd(from),
                "Cyclic task start date cannot be after end date",
                "Exception.CyclicTaskBuilder.FromDateAfterToDate");
        GetintimePreconditions.checkState(!dayOffManager.isDayOff(from),
                "Cyclic task start date cannot be a day off",
                "Exception.CyclicTaskBuilder.FromDateIsHoliday");
        
        CyclicTask cyclicTask = new CyclicTask(templateTask);
        
        List<Term> terms = new ArrayList<>();
        endOfMonthMovedTerms = new HashMap<>();
        terms.addAll(generateTermsFromDaysOfMonth());
        terms.addAll(generateTermsFromDaysOfWeek());
        
        dayOffMovedTerms = new HashMap<>();
        cyclicTask.addAll(Lists.newArrayList(moveTermsToWorkingDays(terms)));
        return cyclicTask;
    }
    
    private List<Term> moveTermsToWorkingDays(List<Term> terms) {
        Set<Term> workingDayTerms = new HashSet<>();
        for (Term term : terms) {
            if (isDayOff(term)) {
                Term movedTerm = moveDayOffToWorkingDay(term);
                recordDayOffMovedTerm(movedTerm, term);
                workingDayTerms.add(movedTerm);
            } else {
                workingDayTerms.add(term);
            }
        }
        return new ArrayList<>(workingDayTerms);
    }
    
    private boolean isDayOff(Term term) {
        return dayOffManager.isDayOff(term.getDate());
    }
    
    private Term moveDayOffToWorkingDay(Term term) {
        LocalDate date = term.getDate().minusDays(1);
        while (dayOffManager.isDayOff(date)) {
            date = date.minusDays(1);
        }
        return new Term(date);
    }
    
    private void recordDayOffMovedTerm(Term movedTerm, Term term) {
        if (!dayOffMovedTerms.containsKey(movedTerm)) {
            dayOffMovedTerms.put(movedTerm, new HashSet<>());
        }
        dayOffMovedTerms.get(movedTerm).add(term);
    }
    
    private List<Term> generateTermsFromDaysOfWeek() {
        int fromDayOfWeek = LocalDate.from(from).getDayOfWeek().getValue();
        LocalDate pivot = LocalDate.from(from).minusDays(fromDayOfWeek - 1);

        List<Term> terms = new ArrayList<>();
        Set<DayOfWeek> daysOfWeek = templateTask.getDaysOfWeek();
        while (isBeforeOrEqualTheEnd(pivot)) {
            terms.addAll(
                    (Collection<? extends Term>) daysOfWeek
                            .stream()
                            .map(DayOfWeek::ordinal)
                            .map(pivot::plusDays)
                            .filter(this::isEqualOrAfterTheBeginning)
                            .filter(this::isBeforeOrEqualTheEnd)
                            .map(Term::new)
                            .collect(toList())
            );
            pivot = pivot.plusWeeks(1L);
        }
        return terms;
    }
    
    // definitely could be achieved better, if anybody has an idea, go ahead
    // ~partyks
    private List<Term> generateTermsFromDaysOfMonth() {
        //pivot is a date that we are currently at - between (from, to)
        LocalDate pivot = LocalDate.from(from).withDayOfMonth(1);

        List<Term> terms = new ArrayList<>();
        Set<Integer> daysOfMonth = templateTask.getDaysOfMonth();

        while (isBeforeOrEqualTheEnd(pivot)) {
            Set<Integer> daysOfCurrentMonth = moveEndOfMonthDays(daysOfMonth, pivot);
            terms.addAll(daysOfCurrentMonth
                            .stream()
                            .map(pivot::withDayOfMonth)
                            .filter(this::isEqualOrAfterTheBeginning)
                            .filter(this::isBeforeOrEqualTheEnd)
                            .map(Term::new)
                            .collect(toList())
            );
            pivot = pivot.plusMonths(1L);
        }
        return terms;
    }
    
    private Set<Integer> moveEndOfMonthDays(Set<Integer> daysOfMonth, LocalDate currentMonthDate) {
        Set<Integer> daysOfCurrentMonth = new HashSet<>();
        for (int dayOfMonth : daysOfMonth) {
            int lastDayOfCurrentMonth = currentMonthDate.lengthOfMonth();
            if (dayOfMonth > lastDayOfCurrentMonth) {
                daysOfCurrentMonth.add(lastDayOfCurrentMonth);
                Term movedTerm = new Term(currentMonthDate.withDayOfMonth(lastDayOfCurrentMonth));
                recordEndOfMonthMovedTerm(movedTerm, dayOfMonth);
            } else {
                daysOfCurrentMonth.add(dayOfMonth);
            }
        }
        return daysOfCurrentMonth;
    }
    
    private void recordEndOfMonthMovedTerm(Term movedTerm, int dayOfMonth) {
        if (!endOfMonthMovedTerms.containsKey(movedTerm)) {
            endOfMonthMovedTerms.put(movedTerm, new HashSet<>());
        }
        endOfMonthMovedTerms.get(movedTerm).add(dayOfMonth);
    }
    
    private boolean isBeforeOrEqualTheEnd(LocalDate term) {
        return term.isBefore(to) || term.isEqual(to);
    }
    
    private boolean isEqualOrAfterTheBeginning(LocalDate term) {
        return term.isAfter(from) || term.isEqual(from);
    }
    
    public Map<Term, Set<Term>> getDayOffMovedTerms() {
        GetintimePreconditions.checkState(dayOffMovedTerms != null,
                "Cannot get terms moved because of days off for a cyclic task before creating it",
                "Exception.CyclicTaskBuilder.GetDayOffMovedTermsCalledBeforeBuild");
        return Collections.unmodifiableMap(dayOffMovedTerms);
    }
    
    public Map<Term, Set<Integer>> getEndOfMonthMovedTerms() {
        GetintimePreconditions
                .checkState(
                        endOfMonthMovedTerms != null,
                        "Cannot get terms moved because of end of the month for a cyclic task before creating it",
                        "Exception.CyclicTaskBuilder.GetEndOfMonthMovedTermsCalledBeforeBuild");
        return Collections.unmodifiableMap(endOfMonthMovedTerms);
    }
}
