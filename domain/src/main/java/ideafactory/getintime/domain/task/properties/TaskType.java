package ideafactory.getintime.domain.task.properties;

/**
 * @author Michal Partyka
 */
public enum TaskType {
    CYCLIC, STANDARD
}
