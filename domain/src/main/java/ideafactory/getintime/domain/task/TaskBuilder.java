package ideafactory.getintime.domain.task;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import ideafactory.getintime.domain.model.workingmodel.task.Task;
import ideafactory.getintime.domain.model.workingmodel.task.TaskDescription;
import ideafactory.getintime.domain.model.workingmodel.task.TaskProperties;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.task.properties.Term;
import ideafactory.getintime.domain.model.workingmodel.user.Employee;
import ideafactory.getintime.domain.task.properties.TaskType;

import java.time.LocalDate;
import java.util.Map;

public class TaskBuilder {
    private Employee createdBy;
    private Term assigneeDeadline;
    private Term ownerDeadline;
    private TaskType type;
    private String description;
    private String name;
    private int priority;
    private EstimatedTime estimatedTime;
    private Employee assignee;
    private Long id;
    
    public TaskBuilder setCreatedBy(Employee createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    
    public TaskBuilder setAssigneeDeadline(Term assigneeDeadline) {
        this.assigneeDeadline = assigneeDeadline;
        return this;
    }
    
    public TaskBuilder setOwnerDeadline(Term ownerDeadline) {
        this.ownerDeadline = ownerDeadline;
        return this;
    }
    
    public TaskBuilder setType(TaskType type) {
        this.type = type;
        return this;
    }
    
    public TaskBuilder setDescription(String description) {
        this.description = description;
        return this;
    }
    
    public TaskBuilder setName(String name) {
        this.name = name;
        return this;
    }
    
    public TaskBuilder setPriority(int priority) {
        this.priority = priority;
        return this;
    }
    
    public TaskBuilder setEstimatedTime(EstimatedTime estimatedTime) {
        this.estimatedTime = estimatedTime;
        return this;
    }
    
    public TaskBuilder setAssignee(Employee assignee) {
        this.assignee = assignee;
        return this;
    }
    
    public TaskBuilder setId(Long id) {
        this.id = id;
        return this;
    }
    
    public Task createTask() {
        TaskDescription taskDescription = new TaskDescription(name, description);
        TaskProperties taskProperties = new TaskProperties(priority, estimatedTime, assignee,
                createdBy, Sets.newHashSet());
        Task task = new Task(id, taskDescription, taskProperties, ownerDeadline, assigneeDeadline,
                type);
        return task;
    }
}