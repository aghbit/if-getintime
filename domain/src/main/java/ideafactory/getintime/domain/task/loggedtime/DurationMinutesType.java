package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.DurationMinutes;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;

/**
 * @author Michal Partyka
 */
public class DurationMinutesType extends DurationMinutesIType {
    @Override
    public Class returnedClass() {
        return DurationMinutes.class;
    }
    
    @Override
    protected IDurationMinutes createDurationMinutes(long minutes) {
        return new DurationMinutes(minutes);
    }
}
