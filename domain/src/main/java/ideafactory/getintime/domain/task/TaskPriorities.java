package ideafactory.getintime.domain.task;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by yorg on 25.04.14.
 */
public class TaskPriorities {
    
    // TODO: These values should come from config file
    private static final List<Integer> possibleTaskPriorities = Lists.newArrayList(1, 2, 3, 4);
    
    public List<Integer> getPossibleTaskPriorities() {
        return Collections.unmodifiableList(possibleTaskPriorities);
    }
}
