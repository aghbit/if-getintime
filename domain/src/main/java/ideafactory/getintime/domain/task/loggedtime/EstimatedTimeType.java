package ideafactory.getintime.domain.task.loggedtime;

import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.EstimatedTime;
import ideafactory.getintime.domain.model.workingmodel.task.loggedtime.IDurationMinutes;

/**
 * @author Piotr Góralczyk
 */
public class EstimatedTimeType extends DurationMinutesIType {
    @Override
    public Class returnedClass() {
        return EstimatedTime.class;
    }
    
    @Override
    protected IDurationMinutes createDurationMinutes(long minutes) {
        return new EstimatedTime(minutes);
    }
}
