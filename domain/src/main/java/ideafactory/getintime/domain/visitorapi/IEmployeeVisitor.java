package ideafactory.getintime.domain.visitorapi;

import ideafactory.getintime.domain.model.workingmodel.user.Employee;

/**
 * @author Michal Partyka
 */
public interface IEmployeeVisitor {
    void visit(Employee visitable);
}
