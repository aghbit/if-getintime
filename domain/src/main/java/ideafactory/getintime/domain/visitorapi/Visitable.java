package ideafactory.getintime.domain.visitorapi;

/**
 * @author Michal Partyka
 */
public interface Visitable {
    void accept(IEmployeeVisitor visitor);
}
