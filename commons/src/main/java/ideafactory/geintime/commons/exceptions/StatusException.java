package ideafactory.geintime.commons.exceptions;

/**
 * @author Maciej Ciołek
 */
public abstract class StatusException extends RuntimeException implements IStatusException {
    private String errorCode;
    
    protected StatusException(String message) {
        super(message);
    }
    
    protected StatusException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
    
    @Override
    public String getErrorCode() {
        return errorCode;
    }
}
