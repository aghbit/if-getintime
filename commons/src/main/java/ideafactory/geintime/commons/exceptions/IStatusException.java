package ideafactory.geintime.commons.exceptions;

/**
 * @author Maciej Ciołek
 */
public interface IStatusException {
    public String getErrorCode();
}
