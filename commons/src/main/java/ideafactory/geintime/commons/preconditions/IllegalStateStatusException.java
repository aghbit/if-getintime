package ideafactory.geintime.commons.preconditions;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Maciej Ciołek
 */
public class IllegalStateStatusException extends StatusException {
    public IllegalStateStatusException(String message, String errorCode) {
        super(message, errorCode);
    }
}
