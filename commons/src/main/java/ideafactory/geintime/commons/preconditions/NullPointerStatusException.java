package ideafactory.geintime.commons.preconditions;

import ideafactory.geintime.commons.exceptions.StatusException;

/**
 * @author Maciej Ciołek
 */
public class NullPointerStatusException extends StatusException {
    public NullPointerStatusException(String message, String errorCode) {
        super(message, errorCode);
    }
}
