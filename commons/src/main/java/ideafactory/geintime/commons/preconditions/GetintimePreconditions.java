package ideafactory.geintime.commons.preconditions;

import java.time.chrono.ChronoLocalDate;

/**
 * @author Maciej Ciołek
 */
public class GetintimePreconditions {
    public static <T> void checkNotNull(T reference, String errorCode) {
        if (reference == null) {
            throw new NullPointerStatusException("reference " + reference + " is null", errorCode);
        }
    }
    
    public static <T> void checkState(boolean expectedState, String message, String errorCode) {
        if (!expectedState) {
            throw new IllegalStateStatusException(message, errorCode);
        }
    }
    
    public static void checkNotEmpty(String reference, String message, String errorCode) {
        if (reference.isEmpty()) {
            throw new IllegalStateStatusException(message, errorCode);
        }
    }
    
    public static void checkChronologicalDateOrder(ChronoLocalDate lhs, ChronoLocalDate rhs,
            String message, String errorCode) {
        if (lhs.isAfter(rhs)) {
            throw new IllegalStateStatusException(message, errorCode);
        }
    }
    
    public static void checkAboveZero(long number, String message, String errorCode) {
        if (number <= 0) {
            throw new IllegalStateStatusException(message, errorCode);
        }
    }
}
