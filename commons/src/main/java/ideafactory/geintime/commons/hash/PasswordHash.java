package ideafactory.geintime.commons.hash;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Maciej Ciołek
 */
public class PasswordHash {
    public static String hash(String string, String algorithm) {
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(string.getBytes());
            
            byte byteData[] = messageDigest.digest();
            
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                stringBuffer
                        .append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return stringBuffer.toString();
            
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("Supplied algorithm: " + algorithm
                    + " is not supported");
        }
    }
}
