package ideafactory.geintime.commons.dayoff;

import java.time.LocalDate;

public final class JodaTimeConverter {
    public static org.joda.time.LocalDate convertJavaLocalDateToJoda(LocalDate date) {
        return new org.joda.time.LocalDate(date.getYear(), date.getMonthValue(),
                date.getDayOfMonth());
    }
    
    public static LocalDate convertJodaLocalDateToJava(org.joda.time.LocalDate date) {
        return LocalDate.of(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth());
    }
}