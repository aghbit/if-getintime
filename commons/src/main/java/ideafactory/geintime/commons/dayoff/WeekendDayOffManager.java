package ideafactory.geintime.commons.dayoff;

import de.jollyday.Holiday;
import de.jollyday.HolidayManager;
import ideafactory.geintime.commons.preconditions.GetintimePreconditions;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Piotr Góralczyk
 */
@Component
public class WeekendDayOffManager implements DayOffManager {
    private HolidayManager holidayManager;
    
    @Autowired
    public WeekendDayOffManager(HolidayManager holidayManager) {
        this.holidayManager = holidayManager;
    }
    
    @Override
    public boolean isDayOff(LocalDate date) {
        return isWeekend(date)
                || holidayManager.isHoliday(JodaTimeConverter.convertJavaLocalDateToJoda(date));
    }
    
    @Override
    public Set<LocalDate> getDaysOff(LocalDate from, LocalDate to) {
        GetintimePreconditions.checkChronologicalDateOrder(from, to,
                "Interval begin date cannot be after interval end date",
                "Exception.DayOffManager.DaysOffStartDateAfterEndDate");
        final Set<LocalDate> daysOff = getHolidays(from, to);
        daysOff.addAll(getWeekendDays(from, to));
        return daysOff;
    }
    
    private Set<LocalDate> getHolidays(LocalDate from, LocalDate to) {
        final Set<Holiday> holidays = holidayManager.getHolidays(createInterval(from, to));
        return holidays.stream()
                .map(Holiday::getDate)
                .map(JodaTimeConverter::convertJodaLocalDateToJava)
                .collect(Collectors.toSet());
    }
    
    private static boolean isWeekend(LocalDate date) {
        final DayOfWeek dayOfWeek = date.getDayOfWeek();
        return dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY;
    }
    
    private static Interval createInterval(LocalDate from, LocalDate to) {
        final org.joda.time.LocalDate jodaFrom = JodaTimeConverter.convertJavaLocalDateToJoda(from);
        final org.joda.time.LocalDate jodaDayAfterTo = JodaTimeConverter
                .convertJavaLocalDateToJoda(to).plusDays(1);
        return new Interval(jodaFrom.toDateTimeAtStartOfDay(),
                jodaDayAfterTo.toDateTimeAtStartOfDay());
    }
    
    private static Set<LocalDate> getWeekendDays(LocalDate from, LocalDate to) {
        final Set<LocalDate> weekendDays = new HashSet<>();
        for (LocalDate date = from; !date.isAfter(to); date = date.plusDays(1)) {
            if (isWeekend(date)) {
                weekendDays.add(date);
            }
        }
        return weekendDays;
    }
}
