package ideafactory.geintime.commons.dayoff;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Piotr Góralczyk
 */
public interface DayOffManager {
    boolean isDayOff(LocalDate date);
    
    Set<LocalDate> getDaysOff(LocalDate from, LocalDate to);
}
