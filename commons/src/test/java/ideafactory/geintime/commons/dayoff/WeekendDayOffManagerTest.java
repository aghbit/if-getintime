package ideafactory.geintime.commons.dayoff;

import de.jollyday.HolidayManager;
import ideafactory.geintime.commons.preconditions.IllegalStateStatusException;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.junit.matchers.JUnitMatchers.hasItems;

public class WeekendDayOffManagerTest {
    private static final String HOLIDAY_FILE_SUFFIX = "test";
    
    private static final LocalDate MONDAY_28TH_APRIL = LocalDate.of(2014, 4, 28);
    private static final LocalDate THURSDAY_1ST_MAY_LABOUR_DAY = LocalDate.of(2014, 5, 1);
    private static final LocalDate FRIDAY_2ND_MAY = LocalDate.of(2014, 5, 2);
    private static final LocalDate SATURDAY_3RD_MAY_CONSTITUTION_DAY = LocalDate.of(2014, 5, 3);
    private static final LocalDate SUNDAY_4TH_MAY = LocalDate.of(2014, 5, 4);
    private static final LocalDate MONDAY_5TH_MAY = LocalDate.of(2014, 5, 5);
    
    private static final HolidayManager HOLIDAY_MANAGER = HolidayManager
            .getInstance(HOLIDAY_FILE_SUFFIX);
    
    private DayOffManager instance = new WeekendDayOffManager(HOLIDAY_MANAGER);
    
    @Test
    public void shouldReturnFalseForNonWeekendNonHoliday() throws Exception {
        final LocalDate date = FRIDAY_2ND_MAY;
        
        boolean isDayOff = instance.isDayOff(date);
        
        assertFalse(isDayOff);
    }
    
    @Test
    public void shouldReturnTrueForWeekendNonHoliday() throws Exception {
        final LocalDate date = SUNDAY_4TH_MAY;
        
        boolean isDayOff = instance.isDayOff(date);
        
        assertTrue(isDayOff);
    }
    
    @Test
    public void shouldReturnTrueForNonWeekendHoliday() throws Exception {
        final LocalDate date = THURSDAY_1ST_MAY_LABOUR_DAY;
        
        boolean isDayOff = instance.isDayOff(date);
        
        assertTrue(isDayOff);
    }
    
    @Test
    public void shouldReturnTrueForWeekendHoliday() throws Exception {
        final LocalDate date = SATURDAY_3RD_MAY_CONSTITUTION_DAY;
        
        boolean isDayOff = instance.isDayOff(date);
        
        assertTrue(isDayOff);
    }
    
    @Test(expected = IllegalStateStatusException.class)
    public void shouldThrowAnExceptionGivenStartDateAfterEndDate() throws Exception {
        LocalDate from = FRIDAY_2ND_MAY;
        LocalDate to = THURSDAY_1ST_MAY_LABOUR_DAY;
        
        instance.getDaysOff(from, to);
    }
    
    @Test
    public void shouldReturnDayOffGivenStartDateEqualEndDateForDayOff() throws Exception {
        LocalDate date = SATURDAY_3RD_MAY_CONSTITUTION_DAY;
        
        Set<LocalDate> daysOff = instance.getDaysOff(date, date);
        
        assertEquals(1, daysOff.size());
        assertThat(daysOff, hasItem(date));
    }
    
    @Test
    public void shouldReturnEmptySetGivenStartDateEqualEndDateForWorkingDay() throws Exception {
        LocalDate date = FRIDAY_2ND_MAY;
        
        Set<LocalDate> daysOff = instance.getDaysOff(date, date);
        
        assertEquals(0, daysOff.size());
    }
    
    @Test
    public void shouldReturnDaysOffGivenIntervalEndingWithNonWeekendNonHoliday() throws Exception {
        LocalDate from = MONDAY_28TH_APRIL;
        LocalDate to = MONDAY_5TH_MAY;
        
        Set<LocalDate> daysOff = instance.getDaysOff(from, to);
        
        assertEquals(3, daysOff.size());
        assertThat(
                daysOff,
                hasItems(THURSDAY_1ST_MAY_LABOUR_DAY, SATURDAY_3RD_MAY_CONSTITUTION_DAY,
                        SUNDAY_4TH_MAY));
    }
    
    @Test
    public void shouldReturnDaysOffGivenIntervalEndingWithWeekendNonHoliday() throws Exception {
        final LocalDate from = MONDAY_28TH_APRIL;
        final LocalDate to = SUNDAY_4TH_MAY;
        
        Set<LocalDate> daysOff = instance.getDaysOff(from, to);
        
        assertEquals(3, daysOff.size());
        assertThat(
                daysOff,
                hasItems(THURSDAY_1ST_MAY_LABOUR_DAY, SATURDAY_3RD_MAY_CONSTITUTION_DAY,
                        SUNDAY_4TH_MAY));
    }
    
    @Test
    public void shouldReturnDaysOffGivenIntervalEndingWithNonWeekendHoliday() throws Exception {
        final LocalDate from = MONDAY_28TH_APRIL;
        final LocalDate to = THURSDAY_1ST_MAY_LABOUR_DAY;
        
        Set<LocalDate> daysOff = instance.getDaysOff(from, to);
        
        assertEquals(1, daysOff.size());
        assertThat(daysOff, hasItems(THURSDAY_1ST_MAY_LABOUR_DAY));
    }
    
    @Test
    public void shouldReturnDaysOffGivenIntervalEndingWithWeekendHoliday() throws Exception {
        final LocalDate from = MONDAY_28TH_APRIL;
        final LocalDate to = SATURDAY_3RD_MAY_CONSTITUTION_DAY;
        
        Set<LocalDate> daysOff = instance.getDaysOff(from, to);
        
        assertEquals(2, daysOff.size());
        assertThat(daysOff,
                hasItems(THURSDAY_1ST_MAY_LABOUR_DAY, SATURDAY_3RD_MAY_CONSTITUTION_DAY));
    }
}